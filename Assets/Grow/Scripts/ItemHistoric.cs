﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Grow
{

    public class ItemHistoric : MonoBehaviour
    {
        public Image imageIconStatus;
        public Text textStatus;
        public Text textDate;
        public Text textTime;

        public GameObject prefabHistoricPlayer;

        public Transform content;

        public Text textRoomType;



        /// <summary>
        /// Função para configurar o item
        /// </summary>
        public void Configure(Partida match)
        {
            if (match.Vencedor)
            {
                imageIconStatus.color = Color.green;
                textStatus.text = "GANHOU";
            }
            else
            {
                imageIconStatus.color = Color.red;
                textStatus.text = "PERDEU";
            }

            if (match.IdRoom.StartsWith("R_"))
            {
                textRoomType.text = "PARTIDA RANQUEADA";
            }
            else
            {
                textRoomType.text = "ENTRE AMIGOS";
            }

            if (match.Data.DayOfYear == DateTime.Today.DayOfYear && match.Data.Year == DateTime.Today.Year)
            {
                textDate.text = "HOJE";
            }
            else if (match.Data.AddDays(1).DayOfYear == DateTime.Today.DayOfYear && match.Data.AddDays(1).Year == DateTime.Today.Year)
            {
                textDate.text = "ONTEM";
            }
            else
            {
                textDate.text = match.Data.ToString("dd/MM/yy");
            }

            textTime.text = match.Data.ToString("hh:mm");

            for (int i = 0; i < match.Jogadores.Count; i++)
            {
                GameObject obj = Instantiate(prefabHistoricPlayer, content);
                obj.GetComponent<ItemHistoricPlayer>().Configure(match.Jogadores[i], match.Jogadores[i].Posicao);
            }
        }

    }
}