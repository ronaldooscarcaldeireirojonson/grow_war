﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Grow
{

    /// <summary>
    /// Classe que representa os avatares na tela de seleção de avatar
    /// </summary>
    public class AvatarLoader : MonoBehaviour
    {

        public Image imageAvatar;
        public GameObject check;
        public GameObject lockpick;

        [HideInInspector]
        // O Id desse avatar
        public int avatarId;

        // O index no array
        private int avatarIndex;

        // Flag que indica se é somente para vips
        private bool onlyVip = false;

        // Callback chamado quando o jogador aperta no botão
        private Action<int, int, bool, Sprite> callback;

        // A url da imagem
        [HideInInspector]
        public string avatarUrl;


        /// <summary>
        /// Função para configurar a imagem do jogador
        /// </summary>
        public void Configure(int index, int id, string url, Action<int, int, bool, Sprite> call)
        {
            avatarIndex = index;
            avatarId = id;
            callback = call;
            avatarUrl = url;

            ServerManager.instance.GetAvatarSprite(url, (pic) =>
            {
                imageAvatar.sprite = pic;
            });
        }



        /// <summary>
        /// Função para configurar a imagem do jogador
        /// </summary>
        public void Configure(int index, int id, string url, bool vip, Action<int, int, bool, Sprite> call)
        {
            avatarIndex = index;
            avatarId = id;
            onlyVip = vip;
            callback = call;
            avatarUrl = url;

            ServerManager.instance.GetAvatarSprite(url, (pic) =>
            {
                imageAvatar.sprite = pic;
                imageAvatar.color = Color.white;

                if (vip && !Globals.playerIsVIP)
                {
                    imageAvatar.color = new Color(0.3f, 0.3f, 0.3f, 1);
                }
                else
                {
                    lockpick.SetActive(false);
                }
            });
        }


        /// <summary>
        /// Função para checar o avatar
        /// </summary>
        public void SelectAvatar()
        {
            check.SetActive(true);
        }


        /// <summary>
        /// Função para dechecar o avatar
        /// </summary>
        public void DeselectAvatar()
        {
            check.SetActive(false);
        }


        /// <summary>
        /// Função para atualizar o avatar
        /// </summary>
        public void UpdateAvatar(string url)
        {
            avatarUrl = url;

            ServerManager.instance.GetAvatarSprite(url, (pic) =>
            {
                imageAvatar.sprite = pic;
            });
        }



        public void OnClickAvatar()
        {
            callback(avatarIndex, avatarId, onlyVip, imageAvatar.sprite);
        }
    }

}