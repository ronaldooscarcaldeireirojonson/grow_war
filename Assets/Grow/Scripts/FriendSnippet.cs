﻿using Sfs2X.Entities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Grow
{

    public class FriendSnippet : MonoBehaviour
    {
        public Image background;
        public Image imageAvatar;
        public Text textName;
        public Text textStatus;
        public Image imageGame;

        public Button buttonRemove;

        private int buddyIdGrow = 0;
        private string buddyName = "";
        private string buddyAvatar = "";


        /// <summary>
        /// Função para configurar esse snippet
        /// </summary>
        public void Configure(Buddy friend, bool isOnline)
        {
            this.buddyName = friend.Name;

            textName.text = this.buddyName;

            List<Sfs2X.Entities.Variables.BuddyVariable> offlineVars = friend.GetOfflineVariables();

            for (int i = 0; i < offlineVars.Count; i++)
            {
                if (offlineVars[i].Name == "$avatar")
                {
                    buddyAvatar = offlineVars[i].GetStringValue();
                }
                else if (offlineVars[i].Name == "$idGrow")
                {
                    buddyIdGrow = offlineVars[i].GetIntValue();
                }
            }

            ServerManager.instance.GetAvatarSprite(buddyAvatar, (pic) =>
            {
                imageAvatar.sprite = pic;
            });

            buttonRemove.gameObject.SetActive(false);

            if (isOnline)
            {
                textName.color = new Color32(242, 122, 22, 255);

                object inGame = FriendsSFS.GetVar(friend, "inGame");
                object game = FriendsSFS.GetVar(friend, "game");

                // Verifica se o jogador está jogando alguma coisa
                if (inGame != null && game != null)
                {
                    if ((bool)inGame)
                    {
                        textStatus.text = "Jogando";
                    }
                    else
                    {
                        textStatus.text = "Online";
                    }

                    // NOTE: Se deixar comentado isso, um dos blobs não estoura. Que é quando um amigo do jogador está online e ele abre a lista de amigos normal
                    ServerManager.instance.GetPictureFromUrl(Globals.gameIconsURL + game.ToString() + ".png", (imageWWW) =>
                    {
                        Sprite sprite = Sprite.Create(imageWWW.texture, new Rect(0, 0, imageWWW.texture.width, imageWWW.texture.height), new Vector2(0, 0));

                        imageGame.sprite = sprite;
                        imageGame.color = new Color(1, 1, 1, 1);
                    });
                }
                else
                {
                    textStatus.text = "Online";
                }
                textStatus.color = new Color32(242, 122, 22, 255);

                background.color = Color.white;
            }
            else
            {
                textName.color = Color.white;
                textStatus.text = "Offline";
                textStatus.color = Color.white;
                imageGame.gameObject.SetActive(false);
                background.color = new Color32(73, 73, 73, 255);
            }

        }


        /// <summary>
        /// Função para configurar esse snippet
        /// </summary>
        public void Configure(int buddyIdGrow, string buddyName, string buddyAvatar)
        {
            this.buddyIdGrow = buddyIdGrow;
            this.buddyName = buddyName;
            this.buddyAvatar = buddyAvatar;

            textName.text = buddyName;
            textName.color = Color.white;

            ServerManager.instance.GetAvatarSprite(buddyAvatar, (pic) =>
            {
                //Debug.Log("---------|||||||||||||||||||||||||||||||||||||--------------------------------------pic " + pic);
                if (pic != null)
                    imageAvatar.sprite = pic;
            });

            // Convidou a pessoa mas ela não aceitou
            buttonRemove.gameObject.SetActive(true);
            textStatus.text = "Pendente";
            textStatus.color = Color.white;
            imageGame.gameObject.SetActive(false);
            background.color = new Color32(92, 92, 92, 255);

        }


        public void OnClickProfile()
        {
            ServerManager.instance.OpenNewProfilePopup(buddyIdGrow);
        }


        public void OnClickRemove()
        {
            SFS.Friends.RemoveRequest(this.buddyName);
        }
    }
}