﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.SceneManagement;


namespace Grow
{

    /// <summary>
    /// Classe para controlar as cenas do jogo
    /// </summary>
    public class SceneManager : MonoBehaviour
    {
        #region SINGLETON
        private static SceneManager _instance;
        public static SceneManager instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new GameObject("ManagerScene").AddComponent<SceneManager>();
                }
                return _instance;
            }
        }
        #endregion



        #region CONSTANTS E STATICS
        #endregion



        #region VARIABLES
        // Histórico de cenas
        public List<string> scenesHistoric = new List<string>();
        #endregion



        #region UNITY FUNCTIONS
        void Awake()
        {
            DontDestroyOnLoad(this);
        }


        void Start()
        {
            // Não deixa a tela se apagar
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
        }
        #endregion



        #region OTHER FUNCTIONS
        /// <summary>
        /// Função para trocar de cena
        /// </summary>
        public virtual void ChangeScene(string nameSceneToGo, bool addOnHistoric = true)
        {
            // Sempre deixar o timeScale = 1
            // NOTE: Isso serve para corrigir o problema de sair do jogo pausado e ir para o menu por exemplo
            Time.timeScale = 1;

            // Adiciona no histórico
            if (addOnHistoric)
            {
                scenesHistoric.Add(nameSceneToGo);
            }

            UnityEngine.SceneManagement.SceneManager.LoadScene(nameSceneToGo);
        }
        #endregion

    }

}
