﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Facebook.Unity;
using Newtonsoft.Json;

namespace Grow
{

    /// <summary>
    /// Classe para gerenciara comunicação com o Facebook
    /// </summary>
    public class FacebookManager : MonoBehaviour
    {
        #region CLASSES
        public class FbLoginResult
        {
            public string access_token { get; set; }
            public long user_id { get; set; }
        }
        #endregion



        #region SINGLETON
        private static FacebookManager _instance;
        public static FacebookManager instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new GameObject("FacebookManager").AddComponent<FacebookManager>();
                }
                return _instance;
            }
        }
        #endregion



        #region CONSTANTS E STATICS
        #endregion



        #region VARIABLES
        private Action<Facebook.Unity.AccessToken> OnSuccessCallBack = null;
        private Action<string> OnErrorCallBack = null;
        #endregion



        #region UNITY FUNCTIONS
        void Awake()
        {
            _instance = this;
            DontDestroyOnLoad(this);
        }


        // Use this for initialization
        void Start()
        {
            //FB.Init();
        }
        #endregion



        #region OTHER FUNCTIONS

        /// <summary>
        /// Função para fazer login com o Facebook
        /// </summary>
        public void DoFBLogin(Action<Facebook.Unity.AccessToken> successCallBack, Action<string> errorCallBack)
        {
            OnErrorCallBack = errorCallBack;
            OnSuccessCallBack = successCallBack;

            FB.LogInWithReadPermissions(new List<string>() { "public_profile", "email", "user_friends" }, this.HandleLoginResult);
        }



        /// <summary>
        /// Callback do login
        /// </summary>
        protected void HandleLoginResult(ILoginResult result)
        {
            if (result == null)
            {
                this.OnErrorCallBack(null);
                return;
            }

            // Some platforms return the empty string instead of null.
            if (!string.IsNullOrEmpty(result.Error))
            {
                this.OnErrorCallBack(result.Error);
                return;
            }
            else if (result.Cancelled)
            {
                this.OnErrorCallBack(result.RawResult);
                return;
            }
            else if (FB.IsLoggedIn)
            {
                this.OnSuccessCallBack(Facebook.Unity.AccessToken.CurrentAccessToken);
                return;
            }
            else
            {
                this.OnErrorCallBack(null);
                return;
            }
        }
        #endregion

    }
}