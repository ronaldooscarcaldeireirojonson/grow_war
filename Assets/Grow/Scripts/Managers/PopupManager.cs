﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Grow
{

    /// <summary>
    /// Classe para controlar as popups do jogo
    /// </summary>
    public class PopupManager : MonoBehaviour
    {

        #region SINGLETON
        private static PopupManager _instance;
        public static PopupManager instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new GameObject("ManagerPopup").AddComponent<PopupManager>();
                }
                return _instance;
            }
        }
        #endregion



        #region CONSTANTS E STATICS
        public const float TIME_POPUP_IN = 0.5f;
        public const float TIME_POPUP_OUT = 0.25f;

        protected const string NAME_CANVAS_POPUP = "CanvasPopup";
        protected const string NAME_PREFAB_POPUP_SETTINGS = "PopupSettings";
        protected const string NAME_PREFAB_POPUP_CREDITS = "PopupCredits";
        #endregion



        #region VARIABLES
        private GameObject canvasPopup = null;

        // Uma pilha de popups
        private Stack<GameObject> popupStack = new Stack<GameObject>();
        #endregion



        #region UNITY FUNCTIONS
        void Awake()
        {
            DontDestroyOnLoad(this);
        }


        // Use this for initialization
        void Start()
        {

        }
        #endregion



        #region OTHER FUNCTIONS
        /// <summary>
        /// Função para mostrar uma popup
        /// </summary>
        public virtual GameObject OpenPopup(string popupName)
        {
            // Se não tiver o canvasPopup, instancia
            if (canvasPopup == null)
            {
                popupStack = new Stack<GameObject>();
                canvasPopup = Instantiate(Resources.Load(NAME_CANVAS_POPUP)) as GameObject;
                canvasPopup.GetComponentInChildren<Image>().DOFade(0.7059f, PopupManager.TIME_POPUP_IN);
            }

            // Desabilita a popup atual
            if (popupStack.Count > 0)
            {
                popupStack.Peek().gameObject.SetActive(false);
            }

            GameObject newPopup = Instantiate(Resources.Load(popupName, typeof(GameObject)), canvasPopup.transform) as GameObject;
            newPopup.name = popupName;
            newPopup.transform.localScale = Vector3.zero;
            newPopup.transform.DOScale(1, PopupManager.TIME_POPUP_IN).SetEase(Ease.OutElastic, 0.1f, 0.25f);

            // Coloca na pilha
            popupStack.Push(newPopup);

            // Retorna a popup
            return newPopup;
        }


        /// <summary>
        /// Função para esconder/destruir a popup atual
        /// </summary>
        public virtual void ClosePopup(bool destroyImmediately = false)
        {
            if (popupStack.Count > 0)
            {
                // Remove da pilha
                GameObject popup = popupStack.Pop();
                if (destroyImmediately)
                {
                    // Destroy a popup
                    Destroy(popup);

                    if (popupStack.Count > 0)
                    {
                        popupStack.Peek().gameObject.SetActive(true);
                    }
                    else
                    {
                        Destroy(canvasPopup);
                    }
                }
                else
                {
                    popup.transform.DOScale(0, PopupManager.TIME_POPUP_OUT).OnComplete(
                        () =>
                        {
                            // Destroy a popup
                            Destroy(popup);
                        });

                    // Verifica se temos outra popup na pilha
                    if (popupStack.Count > 0)
                    {
                        popupStack.Peek().gameObject.SetActive(true);
                    }
                    else
                    {
                        // Não temos nenhum popup na pilha
                        canvasPopup.GetComponentInChildren<Image>().DOFade(0f, PopupManager.TIME_POPUP_OUT).OnComplete(() =>
                        {
                            if (popupStack.Count == 0)
                                Destroy(canvasPopup);
                        });

                    }
                }
            }
        }
        #endregion
    }

}