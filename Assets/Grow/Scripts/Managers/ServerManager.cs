﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace Grow
{

    /// <summary>
    /// Classe para gerenciar a comunicação com o nosso server
    /// </summary>
    public class ServerManager : MonoBehaviour
    {

        public static Sprite spriteTest;

        #region SINGLETON
        private static ServerManager _instance;
        public static ServerManager instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new GameObject("ManagerServer").AddComponent<ServerManager>();
                }
                return _instance;
            }
        }
        #endregion



        #region CONSTANTS E STATICS
        private const string NAME_CANVAS_WAITING = "CanvasWaiting";
        #endregion



        #region VARIABLES
        // O gameObject do animação de esperar
        private GameObject waitingAnimation = null;

        // Dicionário dos avatares com a <id do avatar, url no resources/facebook>
        // Essa lista é populada no início do Start do ServerManager
        public static Dictionary<string, string> avatarsDictionary = new Dictionary<string, string>();
        #endregion



        #region UNITY FUNCTIONS
        void Awake()
        {
            DontDestroyOnLoad(this);
        }


        void Start()
        {
            // Pega o dicionário dos avatares, se existir
            avatarsDictionary = Sioux.Storage.Load<Dictionary<string, string>>("Avatars");

            // Se a lista está vazia, popula a lista de avatares do jogador "na mão", pelo menos os iniciais
            if (avatarsDictionary == null)
            {
                avatarsDictionary = new Dictionary<string, string>();
            }

            //for (int i = 0; i < 30; i++)
            //{
            //    string newKey = "http://grow.blob.core.windows.net/avatar/Photo" + i.ToString("00") + ".jpg";
            //    if (avatarsDictionary.ContainsKey(newKey))
            //    {
            //        avatarsDictionary[newKey] = "Avatars/Photo" + i.ToString("00");
            //    }
            //    else
            //    {
            //        avatarsDictionary.Add(newKey, "Avatars/Photo" + i.ToString("00"));
            //    }
            //}

            //for (int i = 0; i < 30; i++)
            //{
            //    string newKey = "http://grow.blob.core.windows.net/avatar/Clan" + i.ToString("00") + ".png";
            //    if (avatarsDictionary.ContainsKey(newKey))
            //    {
            //        avatarsDictionary[newKey] = "Avatars/Clan" + i.ToString("00");
            //    }
            //    else
            //    {
            //        avatarsDictionary.Add(newKey, "Avatars/Clan" + i.ToString("00"));
            //    }
            //}

            Sioux.Storage.Save("Avatars", avatarsDictionary);
        }

        
        #endregion



        #region OTHER FUNCTIONS
        /// <summary>
        /// Função para chamar a nossa WebAPI e retorna a resposta em string para cada função trate de forma diferente
        /// </summary>
        private IEnumerator CallWebAPI(string controller, string dataToSend, Action<string> callback)
        {
            // Mostra a animação de espera
            ShowWaitingAnim();

            bool hasLoad = VersionController.versionConfig != null;
            if (!hasLoad)
            {
                yield return StartCoroutine(VersionController.DownloadConnectionStrings(
                    (isSuccess) =>
                    {
                        hasLoad = isSuccess;
                    }));
            }

            if (hasLoad)
            {
                byte[] bodyRaw = Encoding.UTF8.GetBytes(dataToSend);

                //Debug.Log("VersionController.versionConfig.webapiUrl: " + VersionController.versionConfig.webapiUrl);

                // Cria o WebRequest
                UnityWebRequest request = new UnityWebRequest(VersionController.versionConfig.webapiUrl + controller + "/", "POST");
                request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
                request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
                request.SetRequestHeader("Content-Type", "application/json");
                request.SetRequestHeader("wrtoken", Globals.playerToken);

                // Envia a requisição
                yield return request.Send();

                // Se não deu erro
                if (string.IsNullOrEmpty(request.error))
                {
                    // Se estiver voltando uma página html, deu algum erro no server
                    if (request.downloadHandler.text.IndexOf("<!DOCTYPE html>") == -1)
                    {
                        // Formata a resposta (vêm com barra+aspas [\"] ao invés de aspas ["] e com aspas no começo e no fim)
                        string responseFormatted = request.downloadHandler.text;

                        if (responseFormatted.IndexOf(@"\") > -1)
                            responseFormatted = responseFormatted.Replace(@"\", string.Empty);

                        if (responseFormatted.IndexOf("\"") > -1)
                            responseFormatted = responseFormatted.Substring(1, responseFormatted.Length - 1);

                        if (responseFormatted.LastIndexOf("\"") > -1)
                            responseFormatted = responseFormatted.Substring(0, responseFormatted.Length - 1);

                        // Chama a função que irá tratá-la
                        HideWaitingAnim(); // Esconde a animação de espera
                        callback(responseFormatted);
                    }
                    else
                    {
                        HideWaitingAnim(); // Esconde a animação de espera
                        callback(null); // Chama a função que irá tratá-la
                    }
                }
                else
                {
                    HideWaitingAnim(); // Esconde a animação de espera
                    callback(null); // Chama a função que irá tratá-la
                }
            }
            else
            {
                HideWaitingAnim(); // Esconde a animação de espera
                callback(null); // Chama a função que irá tratá-la
            }
        }



        // ------------------------------------------------------------------------------------------
        // ------ XXXXXXXXXXXXXXXXXXX ---------------------------------------------------------------
        // ------------------------------------------------------------------------------------------


        public enum ImageOrigin
        {
            Facebook,
            Avatar,
            Clan,
            Others
        }


        /// <summary>
        /// Função para pegar o avatar do jogador ao passarmos o nome da imagem
        /// Atualmente a imagem pode ter uma estrutura nesses dois estilos:
        /// - http://graph.facebook.com/1575217602749679/picture?width=128&height=128
        /// - http://desenv.sioux.com.br/GROW/GrowGames/Files/Avatares/Photo03.jpg
        /// </summary>
        public void GetAvatarSprite(string spriteName, Action<Sprite> callback)
        {
            // O tipo do avatar, pra carregarmos do jeito certo
            ImageOrigin avatarType = ImageOrigin.Avatar;

            try
            {
                // Prepara o nome do avatar
                // - Primeiro verifico se é parecido com a imagem do Facebook
                if (spriteName.IndexOf("graph.facebook.com") != -1)
                {   // É do Facebook

                    avatarType = ImageOrigin.Facebook;
                    // NOTE: Não preciso modificar o nome
                }
                else if (spriteName.IndexOf(Globals.gameAvatarURL) != -1)
                {   // É imagem padrão da Grow

                    avatarType = ImageOrigin.Avatar;
                    // NOTE: Não preciso modificar o nome
                }
                else
                {
                    // Verifico se tem algo com Photo ou Clan (são as duas imagens possíveis que usamos)
                    if (spriteName.IndexOf("Photo") != -1)
                    {   // O nome é PhotoXX

                        avatarType = ImageOrigin.Avatar;
                        spriteName = Globals.gameAvatarURL + spriteName;
                    }
                    if (spriteName.IndexOf("Clan") != -1)
                    {   // O nome é ClanXX

                        avatarType = ImageOrigin.Clan;
                        spriteName = Globals.gameClanURL + spriteName;
                    }
                    else
                    {   // Não sei como é a imagem, pode ser um link qualquer

                        avatarType = ImageOrigin.Others;
                    }
                }

                // Tenta pegar o avatar
                GetPicture(avatarType, spriteName,
                    (sprite) =>
                    {
                        if (sprite != null)
                        {
                            callback(sprite);
                        }
                        else
                        {
                            GetPicture(ImageOrigin.Avatar, Globals.gameAvatarURL + "Photo00.jpg",
                                (sprite2) =>
                                {
                                    if (sprite2 != null)
                                    {
                                        callback(sprite2);
                                    }
                                    else
                                    {
                                        // NOTE: Não deveria cair aqui!
                                        callback(spriteTest);
                                    }
                                });
                        }
                    });
            }
            catch (Exception e)
            {
                Debug.Log("!!! ERRO !!! ServerManager - GetAvatarSprite: " + e);
                Debug.LogError("!!! ERRO !!! ServerManager - GetAvatarSprite: " + e);

                callback(spriteTest);
            }
        }



        /// <summary>
        /// Função para pegar uma imagem local
        /// </summary>
        public void GetPicture(ImageOrigin avatarType, string avatarName, Action<Sprite> callback)
        {
            // Verifica na lista de avatares se temos o avatar na memória (resources ou persistentDataPath)
            if (avatarsDictionary.ContainsKey(avatarName))
            {   // Temos o avatar na memória

                Sprite sprite = null;

                try
                {
                    sprite = Resources.Load<Sprite>(avatarsDictionary[avatarName]);
                }
                catch (Exception e)
                {
                    sprite = null;
                }

                // Se encontrou o asset no Resources
                if (sprite != null)
                {
                    callback(sprite);
                }
                else
                {   // Não encontrou no Resources

                    // Cria o nome certo
                    string spriteName = string.Empty;
                    string[] nameSplit = null;

                    switch (avatarType)
                    {
                        case ImageOrigin.Facebook:
                            nameSplit = avatarName.Split('/');
                            spriteName = nameSplit[3];
                            break;

                        case ImageOrigin.Avatar:
                            spriteName = AdjustPictureName(avatarName);
                            break;

                        case ImageOrigin.Others:
                            spriteName = AdjustPictureName(avatarName);
                            break;

                        case ImageOrigin.Clan:
                            spriteName = AdjustPictureName(avatarName);
                            break;

                        default:
                            break;
                    }

                    byte[] bytes = null;
                    try
                    {
                        // Procura no persistentDataPath
                        bytes = Sioux.Storage.ReadAllBytes(spriteName);
                    }
                    catch (Exception e)
                    {
                        avatarsDictionary.Remove(avatarName);
                        bytes = null;
                    }

                    // Se tiver alguma coisa carregada
                    if (bytes != null)
                    {
                        // NOTE: A largura e a altura da textura serão sobreescritas no LoadImage
                        Texture2D texture2D = new Texture2D(0, 0);
                        texture2D.LoadImage(bytes);

                        sprite = Sprite.Create(texture2D, new Rect(0, 0, texture2D.width, texture2D.height), new Vector2(0, 0));

                        callback(sprite);
                    }
                    else
                    {
                        callback(null);
                    }
                }
            }
            else
            {   // Não existe o avatar na memória

                // Baixa a imagem
                GetPictureFromUrl(avatarName,
                    (imageWWW) =>
                    {
                        // Cria o nome certo
                        string spriteName = string.Empty;
                        string[] nameSplit = null;

                        switch (avatarType)
                        {
                            case ImageOrigin.Facebook:
                                nameSplit = avatarName.Split('/');
                                spriteName = nameSplit[3];
                                break;

                            case ImageOrigin.Avatar:
                                spriteName = AdjustPictureName(avatarName);
                                break;

                            case ImageOrigin.Others:
                                spriteName = AdjustPictureName(avatarName);
                                break;

                            case ImageOrigin.Clan:
                                spriteName = AdjustPictureName(avatarName);
                                break;

                            default:
                                break;
                        }

                        // Salva o sprite (em array de bytes) no persistentDataPath
                        Sioux.Storage.Save(spriteName, null, imageWWW.bytes);

                        // Cria o sprite
                        Sprite sprite = Sprite.Create(imageWWW.texture, new Rect(0, 0, imageWWW.texture.width, imageWWW.texture.height), new Vector2(0, 0));

                        // Adiciona no dicionário de avatares
                        avatarsDictionary.Add(avatarName, spriteName);

                        // Salva o dicionário
                        Sioux.Storage.Save("Avatars", avatarsDictionary);

                        // Retorna o sprite
                        callback(sprite);
                    });
            }
        }



        /// <summary>
        /// Função para pegar uma imagem de uma URL
        /// </summary>
        public void GetPictureFromUrl(string url, Action<WWW> callback)
        {
            StartCoroutine(_GetPictureFromUrl(url, callback));
        }
        private IEnumerator _GetPictureFromUrl(string url, Action<WWW> callback)
        {
            // Se não tiver http: eu coloco manualmente
            if (url.IndexOf("http") == -1)
            {
                url = Globals.gameAvatarURL + url;
            }

            WWW www = new WWW(url);
            yield return www;

            callback(www);
        }


        /// <summary>
        /// Função que arruma o nome do arquivo que será salvo a partir de uma URL
        /// Exemplo: https://steemitimages.com/DQmZtSNYi1v1uZwkkMtxHytw9X59wt6EnwHhha9puhEdPm5/perritos-schnauzer-malos-12.jpg -> perritos-schnauzer-malos-12.jpg
        /// </summary>
        public string AdjustPictureName(string url)
        {
            string newName = url;

            // Verifica se existe essa barra
            if (url.IndexOf("/") != -1)
            {
                newName = url.Substring(url.LastIndexOf("/") + 1);
            }
            else if (url.IndexOf("\\") != -1)
            {
                newName = url.Substring(url.LastIndexOf("\\") + 1);
            }
            else
            {
                // NOTE: Acho que nunca vai cair aqui, mas por via das dúvidas
                newName = newName.Replace("/", "");
                newName = newName.Replace(":", "");
                newName = newName.Substring(Mathf.RoundToInt((float)newName.Length / (float)3));
            }

            return newName;
        }


        /// <summary>
        /// Função para criar e mostrar a animação de esperar resposta do server
        /// </summary>
        public void ShowWaitingAnim(string msg = "")
        {
            if (waitingAnimation == null)
            {
                waitingAnimation = Instantiate(Resources.Load(NAME_CANVAS_WAITING)) as GameObject;
                if (msg != "")
                    waitingAnimation.GetComponentInChildren<UnityEngine.UI.Text>().text = msg;
            }
        }



        /// <summary>
        /// Função para remover a animação de esperar
        /// </summary>
        public void HideWaitingAnim()
        {
            if (waitingAnimation != null)
            {
                Destroy(waitingAnimation);
            }
        }



        /// <summary>
        /// Função para fazer o login no jogo
        /// </summary>
        public void DoLogin(string nickname, string password, Action<string> callback)
        {
            Usuario user = new Usuario();
            user.Apelido = nickname;
            user.Senha = password;
            user.plataforma = Plataforma.WEB;
            user.versao = VersionController.versionConfig.versionNumber;

            // Serializa
            string dataToSend = JsonConvert.SerializeObject(user);

            // Chama a API
            StartCoroutine(CallWebAPI("login", dataToSend, callback));

        }



        /// <summary>
        /// Função para mudar o avatar do jogador
        /// </summary>
        public void ChangeAvatar(int playerId, int avatarId, string avatarUrl, Action<string> callback)
        {
            Usuario user = new Usuario();
            user.IdUsuario = playerId;
            user.IdAvatar = avatarId;
            user.Avatar = avatarUrl;

            // Serializa
            string dataToSend = JsonConvert.SerializeObject(user);

            // Chama a API
            StartCoroutine(CallWebAPI("userchangeavatar", dataToSend, callback));
        }



        /**
         * Função para pegar o ranking
         * @param idUsuario O id do jogador
         * @param amount A quantidade de pessoas que tem do ranking
         * @param nPag A quantidade de páginas
         * @param callback Função chamada quando vamos receber a resposta do server. Recebe como parâmetro 4 strings
         */
        public void GetRanking(int playerId, Action<string> callback, string action = "ranking")
        {
            Ranking.RankingNovo ranking = new Ranking.RankingNovo();
            ranking.IdUsuario = playerId;
            ranking.Versao = VersionController.VERSION;

            // Serializa
            string dataToSend = JsonConvert.SerializeObject(ranking);

            // Chama a API
            StartCoroutine(CallWebAPI(action, dataToSend, callback));
        }


        /// <summary>
        /// Função para fazer o login no jogo via Facebbok
        /// </summary>
        public void DoLoginFB(string fbToken, Action<string> callback)
        {
            Usuario user = new Usuario();
            user.FBAccessToken = fbToken;
            user.plataforma = Plataforma.WEB;
            user.versao = VersionController.VERSION;

            // Serializa
            string dataToSend = JsonConvert.SerializeObject(user);

            // Chama a API
            StartCoroutine(CallWebAPI("loginfb", dataToSend, callback));

        }

        /**
        * Função que faz o login do usuário
        * @param apelido Apelido do jogador
        * @param senha Senha do jogador
        * @param callback Função chamada quando o login receber os dados do jogador. Recebe como parâmetro 4 strings
*/
        public void DoLoginToken(string sToken, Action<string> callback)
        {
            //Usuario user = new Usuario();
            //user.Token = sToken;
            //user.plataforma = Plataforma.WEB;
            //user.versao = VersionController.VERSION;

            //// Serializa
            //string dataToSend = JsonConvert.SerializeObject(user);

            //// Transforma em bytes
            //byte[] bodyRaw = Encoding.UTF8.GetBytes(dataToSend);

            //StartCoroutine(CallWebAPI("logintoken", dataToSend, callback));

            // --------------

            Usuario.UsuarioToken user = new Usuario.UsuarioToken();
            user.AccessToken = sToken;
            user.Plataforma = Plataforma.WEB;
            user.Versao = VersionController.VERSION;

            // Serializa
            string dataToSend = JsonConvert.SerializeObject(user);

            StartCoroutine(CallWebAPI("token", dataToSend, callback));
        }

        /**
         * Função que faz o cadastro do usuário
         * @param apelido Apelido do jogador
         * @param email Email do jogador
         * @param senha Senha do jogador - creio que a confirmação de senha é melhor fazer no client
         * @param callback Função chamada quando o login receber os dados do jogador. Recebe como parâmetro 4 strings
         */
        public void DoCadastroFB(string apelido, string fbToken, Action<string> callback)
        {

            Usuario dataToSend = new Usuario();
            dataToSend.Apelido = apelido;
            dataToSend.FBAccessToken = fbToken;

            // Serializa
            string sDataToSend = JsonConvert.SerializeObject(dataToSend);

            // Chama a API
            StartCoroutine(CallWebAPI("cadastrofb", sDataToSend, callback));
        }

        /**
         * Função que faz o cadastro do usuário
         * @param apelido Apelido do jogador
         * @param email Email do jogador
         * @param senha Senha do jogador - creio que a confirmação de senha é melhor fazer no client
         * @param callback Função chamada quando o login receber os dados do jogador. Recebe como parâmetro 4 strings
         */
        public void DoCadastro(string apelido, string email, string senha, Action<string> callback)
        {
            Usuario dataToSend = new Usuario();
            dataToSend.Apelido = apelido;
            dataToSend.Email = email;
            dataToSend.Senha = senha;

            // Serializa
            string sDataToSend = JsonConvert.SerializeObject(dataToSend);

            // Chama a API
            StartCoroutine(CallWebAPI("cadastro", sDataToSend, callback));
        }


        public void ChangeUserAvatar(int idUsuario, int idAvatar, Action<string> callback)
        {
            Usuario.UserData dataToSend = new Usuario.UserData();

            dataToSend.Id = idUsuario;
            dataToSend.IdAvatar = idAvatar;

            // Serializa
            string sDataToSend = JsonConvert.SerializeObject(dataToSend);

            // Chama a API
            StartCoroutine(CallWebAPI("changeuseravatar", sDataToSend, callback));
        }

        public void GetPostGameData(Action<string> callback)
        {
            Usuario.UserData dataToSend = new Usuario.UserData();

            // Serializa
            string sDataToSend = JsonConvert.SerializeObject(dataToSend);

            // Chama a API
            StartCoroutine(CallWebAPI("postdata", sDataToSend, callback));
        }

        /**
         * Função que pega os dados do jogador, como os pontos, o nível, etc
         * @param idUsuario ID do jogador
         * @param callback Função chamada quando o login receber os dados do jogador. Recebe como parâmetro 4 strings
         */
        public void GetUserData(int idUsuario, Action<string> callback)
        {
            Usuario.UserData dataToSend = new Usuario.UserData();
            dataToSend.Id = idUsuario;

            // Serializa
            string sDataToSend = JsonConvert.SerializeObject(dataToSend);

            // Chama a API
            StartCoroutine(CallWebAPI("userdata", sDataToSend, callback));
        }





        /**
         * Função que reseta a senha do jogador
         * @param email Email do jogador
         * @param callback Função chamada quando vamos receber a resposta do server. Recebe como parâmetro 4 strings
         */
        public void DoResetPassword(string email, Action<string> callback)
        {

            Usuario dataToSend = new Usuario();
            dataToSend.Email = email;

            // Serializa
            string sDataToSend = JsonConvert.SerializeObject(dataToSend);

            // Chama a API
            StartCoroutine(CallWebAPI("alterarsenha", sDataToSend, callback));
        }





        /**
         * Função para pegar o ranking
         * @param idUsuario O id do jogador
         * @param amount A quantidade de pessoas que tem do ranking
         * @param nPag A quantidade de páginas
         * @param callback Função chamada quando vamos receber a resposta do server. Recebe como parâmetro 4 strings
         */
        public void GetRankingTorneio(int idUsuario, Action<string> callback)
        {
            Ranking.RankingNovo dataToSend = new Ranking.RankingNovo();
            dataToSend.IdUsuario = idUsuario;

            // Serializa
            string sDataToSend = JsonConvert.SerializeObject(dataToSend);

            // Chama a API
            StartCoroutine(CallWebAPI("rankingTorneio", sDataToSend, callback));
        }

        /**
         * Função que retorna os itens cadastrados na loja
         * @param idCategoria Id da categoria de item que deve ser listada
         * @param callback Função chamada com a lista de itens
         */
        public void GetStoreItems(int idCategoria, Plataforma platform, Action<String> callback)
        {

            StoreItemsParams dataToSend = new StoreItemsParams()
            {
                plataforma = (int)platform,
                categoria = idCategoria
            };

            // Serializa
            string sDataToSend = JsonConvert.SerializeObject(dataToSend);

            // Chama a API
            StartCoroutine(CallWebAPI("StoreItems", sDataToSend, callback));
        }

        /**
         * Processa compra feita pela apple store
         * @param idCategoria Id da categoria de item que deve ser listada
         * @param callback Função chamada com a lista de itens
         */
        public void VerifyPurchaseAppleStore(string TransactionID, string Receipt, string ProductId, int IdUsuario, Action<string> callback)
        {

            VerifyPurchaseApple dataToSend = new VerifyPurchaseApple()
            {
                TransactionID = TransactionID,
                Receipt = Receipt,
                ProductId = ProductId,
                IdUsuario = IdUsuario
            };

            // Serializa
            string sDataToSend = JsonConvert.SerializeObject(dataToSend);

            // Chama a API
            StartCoroutine(CallWebAPI("StoreApple", sDataToSend, callback));
        }

        /**
         * Processa compra feita pela google play
         * @param idCategoria Id da categoria de item que deve ser listada
         * @param callback Função chamada com a lista de itens
         */
        public void VerifyPurchaseGooglePlay(string JsonData, Action<String> callback)
        {

            VerifyPurchaseGoogle dataToSend = new VerifyPurchaseGoogle()
            {
                JsonData = JsonData
            };

            // Serializa
            string sDataToSend = JsonConvert.SerializeObject(dataToSend);

            // Chama a API
            StartCoroutine(CallWebAPI("StoreGooglePlay", sDataToSend, callback));
        }



        /// <summary>
        /// Função para pegar os dados do Perfil de um jogador
        /// </summary>
        public void GetFriendDetails(int idGrowGames, Action<string> callback)
        {
            GetFriendDetailsParams dataToSend = new GetFriendDetailsParams()
            {
                id = idGrowGames
            };

            // Serializa
            string sDataToSend = JsonConvert.SerializeObject(dataToSend);

            // Chama a API
            StartCoroutine(CallWebAPI("UserDetails", sDataToSend, callback));
        }



        public void OpenNewProfilePopup(int idGrow)
        {
            GetFriendDetails(idGrow,
                (response) =>
                {
                    //Debug.Log("OpenNewProfilePopup: " + response);

                    if (!string.IsNullOrEmpty(response))
                    {
                        Usuario.UserDetails userData = JsonConvert.DeserializeObject<Usuario.UserDetails>(response);

                        GameObject popup = PopupManager.instance.OpenPopup("PopupNewProfile");
                        popup.GetComponent<PopupNewProfile>().Configure(userData);
                    }
                });
        }


        /// <summary>
        /// Função para pegar os dados da aba Info do perfil do jogador
        /// </summary>
        public void GetUserProfileInfo(int idGrowGames, Action<string> callback)
        {
            GetFriendDetailsParams dataToSend = new GetFriendDetailsParams()
            {
                id = idGrowGames
            };

            // Serializa
            string sDataToSend = JsonConvert.SerializeObject(dataToSend);

            // Chama a API
            StartCoroutine(CallWebAPI("UserDetailsInfo", sDataToSend, callback));
        }


        /// <summary>
        /// Função para pegar os dados da aba Partidas do perfil do jogador
        /// </summary>
        public void GetUserProfileMatches(int idGrowGames, Action<string> callback)
        {
            GetFriendDetailsParams dataToSend = new GetFriendDetailsParams()
            {
                id = idGrowGames
            };

            // Serializa
            string sDataToSend = JsonConvert.SerializeObject(dataToSend);

            // Chama a API
            StartCoroutine(CallWebAPI("UserDetailsPartidas", sDataToSend, callback));
        }



        /// <summary>
        /// Função para pegar os dados da aba Conquistas do perfil do jogador
        /// </summary>
        public void GetUserProfileAchievements(int idUsuario, Action<string> callback)
        {
            GetFriendDetailsParams dataToSend = new GetFriendDetailsParams()
            {
                id = idUsuario
            };

            // Serializa
            string sDataToSend = JsonConvert.SerializeObject(dataToSend);

            // Chama a API
            StartCoroutine(CallWebAPI("UserDetailsConquistas", sDataToSend, callback));
        }


        /// <summary>
        /// Função para salvar os dados no server
        /// </summary>
        public void SetUserProfileInfo(Usuario.UserChangePerfilData data, Action<string> callback)
        {
            // Chama a API
            StartCoroutine(CallWebAPI("PerfilUsuario", JsonConvert.SerializeObject(data), callback));
        }




        /// <summary>
        /// Função para enviar um report para o server
        /// </summary>
        public void SendReportPlayer(int senderId, int targetId, int reportType, string message, Action<string> callback)
        {
            //GetFriendDetailsParams dataToSend = new GetFriendDetailsParams()
            //{
            //    id = idGrowGames
            //};

            //// Serializa
            //string sDataToSend = JsonConvert.SerializeObject(dataToSend);

            //// Chama a API
            //StartCoroutine(CallWebAPI("NewProfileData", sDataToSend, callback));

            //Debug.LogWarning("<color=yellow><b>!!! TODO !!!</b></color> <b>ServerManager</b> - SendReportPlayer");

            callback("");
        }


        /// <summary>
        /// Função para enviar os ratings para o server
        /// </summary>
        public void SendRating(List<RatingModel> ratings, Action<string> callback)
        {
            // Serializa
            string sDataToSend = JsonConvert.SerializeObject(ratings);

            // Chama a API
            StartCoroutine(CallWebAPI("AddRating", sDataToSend, callback));
        }


        /// <summary>
        /// Função para criar um clan
        /// </summary>
        public void CreateClan(Cla clan, Action<string> callback)
        {
            // Serializa
            string sDataToSend = JsonConvert.SerializeObject(clan);

            // Chama a API
            StartCoroutine(CallWebAPI("Cla/CreateClan", sDataToSend, callback));
        }


        /// <summary>
        /// Função para criar um clan
        /// </summary>
        public void SearchClan(string clanName, Action<string> callback)
        {
            // Serializa
            string sDataToSend = JsonConvert.SerializeObject(clanName);

            // Chama a API
            StartCoroutine(CallWebAPI("Cla/SearchClan", sDataToSend, callback));
        }


        /// <summary>
        /// Função para enviar uma requisição para participar do clã
        /// </summary>
        public void EnterClan(ClaMemberRequest request, Action<string> callback)
        {
            // Serializa
            string sDataToSend = JsonConvert.SerializeObject(request);

            // Chama a API
            StartCoroutine(CallWebAPI("ClaMember/EnterClan", sDataToSend, callback));
        }



        /// <summary>
        /// Função para criar um clan
        /// </summary>
        public void AnswerClanMember(ClaMemberApprovalRequest request, Action<string> callback)
        {
            // Serializa
            string sDataToSend = JsonConvert.SerializeObject(request);

            // Chama a API
            StartCoroutine(CallWebAPI("ClaMember/AnswerClanMember", sDataToSend, callback));
        }



        /// <summary>
        /// Função para enviar uma requisição de expulsar o jogador de um clã
        /// </summary>
        public void KickClanMember(ClanRequest request, Action<string> callback)
        {
            // Serializa
            string sDataToSend = JsonConvert.SerializeObject(request);

            // Chama a API
            StartCoroutine(CallWebAPI("Cla/KickClanMember", sDataToSend, callback));
        }


        /// <summary>
        /// Função para pegar os membros do clã
        /// </summary>
        public void GetClanMembers(long idClan, Action<string> callback)
        {
            // Serializa
            string sDataToSend = JsonConvert.SerializeObject(idClan);

            // Chama a API
            StartCoroutine(CallWebAPI("Cla/GetClanMembers", sDataToSend, callback));
        }


        /// <summary>
        /// Função para pegar os membros do clã
        /// </summary>
        public void GetClanMembersPoints(long idClan, Action<string> callback)
        {
            // Serializa
            string sDataToSend = JsonConvert.SerializeObject(idClan);

            // Chama a API
            StartCoroutine(CallWebAPI("RankingCla", sDataToSend, callback));
        }


        /// <summary>
        /// Função para atribuir um membro para ser moderador
        /// </summary>
        public void SetClanModerator(ClanRequest request, Action<string> callback)
        {
            // Serializa
            string sDataToSend = JsonConvert.SerializeObject(request);

            // Chama a API
            StartCoroutine(CallWebAPI("Cla/SetClanModerator", sDataToSend, callback));
        }



        /// <summary>
        /// Função para sair do clã
        /// </summary>
        public void LeaveClan(ClanRequest request, Action<string> callback)
        {
            // Serializa
            string sDataToSend = JsonConvert.SerializeObject(request);

            // Chama a API
            StartCoroutine(CallWebAPI("ClaMember/LeaveClan", sDataToSend, callback));
        }


        /// <summary>
        /// Função para editar o escudo do clã
        /// </summary>
        public void EditClanAvatar(ClanRequest request, Action<string> callback)
        {
            // Serializa
            string sDataToSend = JsonConvert.SerializeObject(request);

            // Chama a API
            StartCoroutine(CallWebAPI("Cla/EditClanAvatar", sDataToSend, callback));
        }



        /// <summary>
        /// Função para pegar o ranking de clãs
        /// </summary>
        public void GetClanRanking(long idClan, Action<string> callback)
        {
            // Serializa
            string sDataToSend = JsonConvert.SerializeObject(idClan);

            // Chama a API
            StartCoroutine(CallWebAPI("RankingCla", sDataToSend, callback));
        }

        #endregion




        /// <summary>
        /// Função para um moderador adicionar um membro
        /// </summary>
        public void AddMemberToClan(ClanRequest request, Action<string> callback)
        {
            // Serializa
            string sDataToSend = JsonConvert.SerializeObject(request);

            // Chama a API
            StartCoroutine(CallWebAPI("Cla/AddMemberToClan", sDataToSend, callback));
        }



        /// <summary>
        /// Função para enviar uma foto na base64 para o server
        /// </summary>
        public void SendImageToServer(byte[] image, Action<string> callback)
        {
            // Serializa
            string sDataToSend = JsonConvert.SerializeObject(image);

            // Chama a API
            StartCoroutine(CallWebAPI("Picture", sDataToSend, callback));
        }



        /// <summary>
        /// Função para checar o status do jogador se ele é membro de um clã
        /// </summary>
        public void CheckMemberStatus(ClanRequest request, Action<string> callback)
        //public void CheckMemberStatus(Action<string> callback)
        {
            // Serializa
            string sDataToSend = JsonConvert.SerializeObject(request);

            // Chama a API
            StartCoroutine(CallWebAPI("Cla/CheckMemberStatus", sDataToSend, callback));
        }


        /// <summary>
        /// Função para pegar os achievements que ainda não foram vistos pelo jogador
        /// </summary>
        public void GetUnseenAchievements(Action<string> callback)
        {
            // Serializa
            string sDataToSend = JsonConvert.SerializeObject(Globals.playerId);

            // Chama a API
            StartCoroutine(CallWebAPI("Achievement", sDataToSend, callback));
        }
    }

}