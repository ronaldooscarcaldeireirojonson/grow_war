﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Grow;
using System.Runtime.InteropServices;
using System;
using System.Linq;
using Newtonsoft.Json;
using UnityEngine.Purchasing;
using TMPro;

namespace Grow
{

    /// <summary>
    /// Classe que representa o Header no menu principal
    /// </summary>
    public class Header : MonoBehaviour
    {

        public Text textName;
        public Text textLevel;
        public Text textEnergy;
        public Image progressBarLevel;
        public Image progressBarEnergy;
        public Image imageAvatar;
        public GameObject buttonFullscreen;
        public GameObject prefabFriends;

        public Image patentImage;
        public List<Sprite> patents;
        public Image alertFriends;
        public static int friendsInviteCount;

        public GameObject buttonSeeClan;
        public GameObject buttonMyClan;
        public Image clanPicture;
        public TextMeshProUGUI textClanTag;
        public GameObject alertClan;
        public static bool HAS_CLAN_ALERT = false;

        public GameObject buttonVIP;
        public GameObject buttonStore;


#if UNITY_WEBGL
        // Função chamada na hora de abrir uma Url, o corpo dela está dentro do plugin na pasta Plugins
        [DllImport("__Internal")]
        private static extern void PopupOpenerCaptureClick(string url);
#endif

        // Use this for initialization
        IEnumerator Start()
        {
            // Atualiza a barra de Level
            progressBarLevel.fillAmount = Globals.GetNextLevelPercentage(Globals.playerPoints);

            // Atualiza o nome do jogador
            textName.text = Globals.playerNickname;

            // Atualiza a foto do jogador
            //imageAvatar.sprite = Player.GlobalAvatar;
            if (Globals.playerAvatarSprite == null)
            {
                ServerManager.instance.GetAvatarSprite(Globals.playerAvatarUrl,
                    (sprite) =>
                    {
                        imageAvatar.sprite = sprite;

                        Globals.playerAvatarSprite = sprite;
                    });
            }
            else
            {
                imageAvatar.sprite = Globals.playerAvatarSprite;
            }

            // Atualiza o nível do jogador
            textLevel.text = Globals.playerLevel.IdNivel.ToString();

            // Se inscreve no evento de update das moedas e de Vip
            Globals.onCoinsUpdate += OnUpdateCoins;
            Globals.onIsVipUpdate += OnUpdateIsVip;
            Globals.onPlayerAvatarUpdate += OnUpdatePlayerAvatar;

            // Atualiza o botão de fullscreen
            if (Globals.GetBuildPlatform() != Plataforma.WEB)
            {
                buttonFullscreen.SetActive(false);
            }

            // Verifica se tem bonus diário
            if (Globals.gameDailyBonus)
            {
                PopupManager.instance.OpenPopup("PopupDailyBonus");

                // Impede que abre o pop up mais de uma vez (quando volta do fim de jogo pro menu)
                Globals.gameDailyBonus = false;
            }

            // Verifica se tem novidades
            if (Globals.gameNews != null)
            {
                PopupManager.instance.OpenPopup("PopupNews");
            }

            // Patente
            patentImage.sprite = patents[Globals.playerPatent - 1];

            // Clã
            UpdateClanButton();

            // Colocar/retirar os botões de loja e VIP
            buttonStore.SetActive(!Globals.playerIsVIP);
            buttonVIP.SetActive(!Globals.playerIsVIP);

            OnUpdateCoins();
            yield return new WaitUntil(() => SFS.Friends != null);
            friendsInviteCount = SFS.Friends.invitesReceived.Count;
            SFS.Friends.OnBuddyListUpdate += UpdateFriendList;

        }

        void OnDestroy()
        {
            Globals.onCoinsUpdate -= OnUpdateCoins;
            Globals.onIsVipUpdate -= OnUpdateIsVip;
            Globals.onPlayerAvatarUpdate -= OnUpdatePlayerAvatar;
            if (SFS.Friends)
                SFS.Friends.OnBuddyListUpdate -= UpdateFriendList;
        }

        public void UpdateFriendList()
        {
            alertFriends.gameObject.SetActive(SFS.Friends.invitesReceived.Count > friendsInviteCount);
        }

        ///// <summary>
        ///// Função para deduzir as moedas do jogador, mas somente o visual! Só para o jogador sentir que suas moedas estão sendo gastas
        ///// </summary>
        //public void UpdateFakeCoins()
        //{
        //    Debug.Log("1");
        //    if (!Globals.playerIsVIP)
        //    {
        //        Debug.Log("XXXXXXX: " + ((int)Globals.playerCoins).ToString());
        //        // Atualiza a barra de energia
        //        progressBarEnergy.fillAmount = Mathf.Min(1, (float)(Globals.playerCoins - Globals.gameQuickplayCost) / (float)Globals.gameMaxCoins);

        //        // Atualiza a quantidade de energias
        //        textEnergy.text = ((int)(Globals.playerCoins - Globals.gameQuickplayCost)).ToString();
        //    }
        //}



        /// <summary>
        /// Função para atualizar o botão de clã
        /// </summary>
        public void UpdateClanButton()
        {
            if (Globals.playerClan.Id != -1)
            {
                ServerManager.instance.GetAvatarSprite("Clan" + Globals.playerClan.IdEscudo.ToString("00") + ".png",
                    (pic) =>
                    {
                        clanPicture.sprite = pic;
                    });
                buttonMyClan.SetActive(true);
                buttonSeeClan.SetActive(false);

                textClanTag.text = Globals.playerClan.Tag;
            }
            else
            {
                buttonMyClan.SetActive(false);
                buttonSeeClan.SetActive(true);
            }

            alertClan.SetActive(HAS_CLAN_ALERT);
        }


        /// <summary>
        /// Evento para atualizar as energias
        /// </summary>
        public void OnUpdateCoins()
        {
            if (Globals.playerIsVIP)
            {
                // Atualiza a barra de energia
                progressBarEnergy.fillAmount = 1;

                // Atualiza a quantidade de energias
                textEnergy.text = "∞";
                textEnergy.fontSize = 75;
            }
            else
            {
                // Atualiza a barra de energia
                progressBarEnergy.fillAmount = Mathf.Min(1, (float)Globals.playerCoins / (float)Globals.gameMaxCoins);

                // Atualiza a quantidade de energias
                textEnergy.text = ((int)Globals.playerCoins).ToString();
                if ((int)Globals.playerCoins < 0)
                    textEnergy.text = "0";
            }
        }


        /// <summary>
        /// Evento para atualizar se o jogador é vip ou não
        /// </summary>
        public void OnUpdateIsVip()
        {
            if (Globals.playerIsVIP)
            {
                // Atualiza a barra de energia
                progressBarEnergy.fillAmount = 1;

                // Atualiza a quantidade de energias
                textEnergy.text = "∞";
                textEnergy.fontSize = 75;
            }
            else
            {
                // Atualiza a barra de energia
                progressBarEnergy.fillAmount = Mathf.Min(1, (float)Globals.playerCoins / (float)Globals.gameMaxCoins);

                // Atualiza a quantidade de energias
                textEnergy.text = ((int)Globals.playerCoins).ToString();
                if ((int)Globals.playerCoins < 0)
                    textEnergy.text = "0";
            }
        }


        /// <summary>
        /// Evento para atualizar o avatar do jogador
        /// </summary>
        public void OnUpdatePlayerAvatar(Sprite pic)
        {
            imageAvatar.sprite = pic;
        }

        /// <summary>
        /// Clique no botão do perfil do jogador
        /// </summary>
        public void OnClickProfile()
        {
            Sioux.Audio.Play("Click");
            ServerManager.instance.OpenNewProfilePopup(Globals.playerIdGrowGames);
        }



        /// <summary>
        /// Clique no botão de comprar mais energia
        /// </summary>
        public void OnClickStore()
        {
            Sioux.Audio.Play("Click");

            AnalyticsManager.Instance.SendDesignEvent("Click:Loja");

#if UNITY_WEBGL && !UNITY_EDITOR
            //Application.ExternalEval("window.open(\"" + Globals.MORE_GAMES_URL + "\", \"_blank\")");
            PopupOpenerCaptureClick("https://assinatura.growgames.com.br/Energia/War");
#else
            // pega os produtos da API
            ServerManager.instance.GetStoreItems(1, Globals.GetBuildPlatform(), result =>
            {
                if (string.IsNullOrEmpty(result))
                {
                    Debug.Log("TODO: mostrar popup falando que não deu certo o 'ServerManager.instance.GetStoreItems'");
                    return;
                }

                Debug.Log(result);
                var itemList = JsonConvert.DeserializeObject<List<StoreItem>>(result);

                if (itemList.Count <= 0)
                {
                    Debug.Log("TODO: mostrar popup falando que não tem produtos para serem comprados");
                    return;
                }

                ServerManager.instance.ShowWaitingAnim();

                // inicializa a loja
                Sioux.IAPManager.Init(itemList.ToDictionary(item => item.codigo, item => ProductType.Consumable), null, success =>
                {
                    ServerManager.instance.HideWaitingAnim();
                    if (!success)
                        Debug.Log("TODO: popup de erro de inicialização da loja");
                    else
                    {
                        List<LojaItemInfo> infoList = new List<LojaItemInfo>();
                        itemList.ForEach(item =>
                        {
                            var p = Sioux.IAPManager.GetProduct(item.codigo);
                            if (p != null)
                                infoList.Add(new LojaItemInfo() { desc = item.nome, price = p.metadata.localizedPriceString, productId = item.codigo, isEnergy = item.idCategoria == 1 });
                        });
                        PopupManager.instance.OpenPopup("PopupStore").GetComponent<PopupStore>().Configure(infoList);
                    }
                });
            });
#endif
        }



        /// <summary>
        /// Clique no botão de amigos
        /// </summary>
        public void OnClickFriends()
        {
            Sioux.Audio.Play("Click");
            GameObject canvasMain = GameObject.FindGameObjectWithTag("CanvasMain");
            if (canvasMain != null)
            {
                GameObject panel = Instantiate(prefabFriends, canvasMain.transform);
                panel.transform.SetAsLastSibling();
            }
            alertFriends.gameObject.SetActive(false);
        }



        /// <summary>
        /// Clique no botão fullscreen
        /// </summary>
        public void OnClickFullscreen()
        {
            Sioux.Audio.Play("Click");
            Screen.fullScreen = !Screen.fullScreen;
        }



        /// <summary>
        /// Clique no botão de configurações
        /// </summary>
        public void OnClickSettings()
        {
            Sioux.Audio.Play("Click");
            PopupManager.instance.OpenPopup("PopupSettings");
        }



        /// <summary>
        /// Clique no logo da Grow
        /// </summary>
        public void OnClickLogoGrow()
        {
            Sioux.Audio.Play("Click");
            AnalyticsManager.Instance.ClickMaisJogos(1);
#if UNITY_WEBGL && !UNITY_EDITOR
            //Application.ExternalEval("window.open(\"" + Globals.MORE_GAMES_URL + "\", \"_blank\")");
            PopupOpenerCaptureClick("http://www.growgames.com.br");
#else
            Application.OpenURL(Globals.MORE_GAMES_URL);
#endif
        }



        public void OnClickMyClan()
        {
            alertClan.SetActive(HAS_CLAN_ALERT = false);
            if (GameObject.FindObjectOfType<ScreenClan>() == null)
            {
                GameObject clanScreen = Instantiate(Resources.Load("ScreenClan"), this.transform.parent) as GameObject;

                // Coloca o Header depois da tela
                transform.SetAsLastSibling();
            }
        }




        public void OnClickSeeClan()
        {
            alertClan.SetActive(HAS_CLAN_ALERT = false);
            if (GameObject.FindObjectOfType<ScreenClan>() == null)
            {
                GameObject clanScreen = Instantiate(Resources.Load("ScreenClan"), this.transform.parent) as GameObject;

                // Coloca o Header depois da tela
                transform.SetAsLastSibling();
            }
        }



        public void OnClickVIP()
        {
            Sioux.Audio.Play("Click");
#if UNITY_WEBGL && !UNITY_EDITOR
            //Application.ExternalEval("window.open(\"" + Globals.MORE_GAMES_URL + "\", \"_blank\")");
            PopupOpenerCaptureClick("https://assinatura.growgames.com.br/Assinatura");
#else
            Application.OpenURL("https://assinatura.growgames.com.br/Assinatura");
#endif
        }


    }
}