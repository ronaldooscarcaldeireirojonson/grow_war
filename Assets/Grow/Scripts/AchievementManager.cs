﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;
using Grow;

public class Achievement
{
    public int Id { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }

    public bool Conquistou { get; set; }
    public bool Mostrou { get; set; }
    public DateTime? Data { get; set; }
    public int IdConquistaTipo { get; set; }
}


public class AchievementManager : MonoBehaviour
{

    // Canvas gameObj
    public static GameObject canvasGameObj = null;

    // Lista com os Ids dos achievements que precisam ser mostrados
    public static Queue<int> achievementsToShow = new Queue<int>();

    // Coroutine que mostra o
    public static Coroutine showingCoroutine = null;

    public RectTransform rectTrans;
    public CanvasGroup cg;
    public Text textTitle;
    //public Text textDescription;
    public Image image;

    // Sprites dos achievements
    public Sprite achievementIconElimination;
    public Sprite achievementIconGeneral;
    public Sprite achievementIconPatent;
    public Sprite achievementIconTerritory;
    public Sprite achievementIconRanking;


    public void Start()
    {
        DontDestroyOnLoad(gameObject);
    }


    /// <summary>
    /// Função para mostrar o achievement
    /// </summary>
    public static void Show(int id)
    {
        if (canvasGameObj == null)
        {
            canvasGameObj = (GameObject)Instantiate(Resources.Load("CanvasAchievement"));
        }

        achievementsToShow.Enqueue(id);

        if (showingCoroutine == null)
        {
            var script = canvasGameObj.GetComponent<AchievementManager>();
            showingCoroutine = script.StartCoroutine(script.ShowingAchievements());
        }
    }


    /// <summary>
    /// Coroutine para mostrar os achievements
    /// </summary>
    public IEnumerator ShowingAchievements()
    {
        do
        {
            // Retira o id do achievement da lista dos achievements para serem mostrados
            int achieveId = achievementsToShow.Dequeue();

            // Pega o achievement
            Achievement achievement = GetAchievement(achieveId);

            // Atualiza os dados do gameObj do achievement
            textTitle.text = achievement.Title.ToUpper();
            //textDescription.text = achievement.description;

            cg.alpha = 0;
            rectTrans.localPosition = new Vector3(0, 365, 0);
            cg.DOFade(1, 1);
            rectTrans.DOLocalMoveY(304, 0.7f);
            cg.DOFade(0, 1).SetDelay(5);

            switch (achievement.IdConquistaTipo)
            {
                case 1:
                    image.sprite = achievementIconGeneral;
                    break;

                case 2:
                    image.sprite = achievementIconElimination;
                    break;

                case 3:
                    image.sprite = achievementIconPatent;
                    break;

                case 4:
                    image.sprite = achievementIconTerritory;
                    break;

                case 5:
                    image.sprite = achievementIconRanking;
                    break;

                default:
                    image.sprite = achievementIconGeneral;
                    break;
            }

            yield return new WaitForSeconds(6);

        } while (achievementsToShow.Count > 0);

        showingCoroutine = null;
        Destroy(canvasGameObj);
    }



    /// <summary>
    /// Função para pegar o achievement pelo Id
    /// </summary>
    public Achievement GetAchievement(int id)
    {
        for (int i = 0; i < Globals.playerAchievements.Count; i++)
        {
            if (Globals.playerAchievements[i].Id == id)
            {
                return Globals.playerAchievements[i];
            }
        }

        return null;
    }
}
