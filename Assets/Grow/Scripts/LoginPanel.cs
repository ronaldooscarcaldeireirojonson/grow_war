﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Grow;
using System.Runtime.InteropServices;
using UnityEngine.Analytics;
using System;

namespace Grow
{
    /// <summary>
    /// Classe que gerencia a cena de Login
    /// </summary>
    public class LoginPanel : MonoBehaviour
    {
        public const string MARKER_TEXT = "XXXxxxXXXxxxXXX";
        public const string LOGIN_ERROR_MESSAGE = "ESTAMOS COM DIFICULDADE EM CONECTAR SEU JOGO COM O SERVER.\n\nPOR FAVOR, TENTE NOVAMENTE MAIS TARDE.";
        public const string FACEBOOK_INSTRUCTION_MESSAGE = "IDENTIFICAMOS QUE VOCÊ JÁ JOGOU COM UMA CONTA FACEBOOK. VOCÊ RECEBERÁ NO E-MAIL \nXXXxxxXXXxxxXXX\nAS INSTRUÇÕES PARA CRIAR SEU CADASTRO GROWGAMES E VINCULAR AS CONTAS.\n\nSE VOCÊ NÃO RECEBÊ-LO APÓS ALGUNS MINUTOS, VERIFIQUE SUA CAIXA DE SPAM OU FAÇA UMA BUSCA PELO REMETENTE\ncontato@growgames.com.br";
        public const string SIGNUP_NOT_ACTIVE_MESSAGE = "VOCÊ AINDA NÃO ATIVOU SEU CADASTRO. PARA ATIVÁ-LO, SIGA AS INSTRUÇÕES QUE ENVIAMOS PARA\nXXXxxxXXXxxxXXX";
        public const string SIGNUP_NOT_FINISHED_MESSAGE = "PARA CONCLUIR O SEU CADASTRO, VOCÊ PRECISA ATIVÁ-LO ATRAVÉS DO E-MAIL QUE ENVIAMOS PARA\nXXXxxxXXXxxxXXX\n\nSE VOCÊ NÃO RECEBÊ-LO APÓS ALGUNS MINUTOS, VERIFIQUE SUA CAIXA DE SPAM OU FAÇA UMA BUSCA PELO REMETENTE\ncontato@growgames.com.br";
        public const string CHANGE_PASSWORD_MESSAGE = "UM E-MAIL COM O PROCEDIMENTO PARA A ALTERAÇÃO DE SENHA FOI ENVIADO PARA\nXXXxxxXXXxxxXXX\n\nSE VOCÊ NÃO RECEBÊ-LO APÓS ALGUNS MINUTOS, VERIFIQUE SUA CAIXA DE SPAM OU FAÇA UMA BUSCA PELO REMETENTE\ncontato@growgames.com.br\n\n";

        // O tipo de painel (conteúdo dentro do painel roxo)
        public enum PanelType
        {
            Login,
            Signup,
            ForgetPassword,
            InvalidUser,
            InvalidEmail,
            UsedNickname,
            Message,
            InsertNickname
        }



        [Header("Panels")]
        public GameObject panelLogin;
        public GameObject panelSignup;
        public GameObject panelForgetPassword;
        public GameObject panelInvalidUser;
        public GameObject panelInvalidEmail;
        public GameObject panelUsedNickname;
        public GameObject panelMessage;
        public GameObject panelInsertNickname;
        public GameObject errorToast;


        [Header("InputFields")]
        public InputField inputLoginNickname;
        public InputField inputLoginPassword;
        public InputField inputSignupNickname;
        public InputField inputSignupEmail;
        public InputField inputSignupPassword;
        public InputField inputSignupPassword2;
        public InputField inputForgetPasswordEmail;
        public InputField inputInsertNickname;

        [Header("ErrorBoxes")]
        public GameObject errorBoxLoginNickname;
        public GameObject errorBoxLoginPassword;
        public GameObject errorBoxSignupNickname;
        public GameObject errorBoxSignupEmail;
        public GameObject errorBoxSignupPassword;
        public GameObject errorBoxSignupPassword2;
        public GameObject errorBoxForgetPasswordEmail;
        public GameObject errorBoxInsertNickname;


        [Header("Others")]
        public Button buttonTabLogin;
        public Text textTabLogin;
        public Button buttonTabSignup;
        public Text textTabSignup;

        public Text textMessage;
        public Toggle keepConnectedToggle;

#if UNITY_WEBGL
        // Função chamada na hora de abrir uma Url, o corpo dela está dentro do plugin na pasta Plugins
        [DllImport("__Internal")]
        private static extern void FacebookOpenerCaptureClick();
#endif



        // O painel atual
        private PanelType _panelType = PanelType.Login;
        public PanelType panelType
        {
            get
            {
                return _panelType;
            }
            set
            {
                _panelType = value;

                // Desabilito todas os paineis
                panelForgetPassword.SetActive(false);
                panelInvalidEmail.SetActive(false);
                panelInvalidUser.SetActive(false);
                panelLogin.SetActive(false);
                panelSignup.SetActive(false);
                panelUsedNickname.SetActive(false);
                panelMessage.SetActive(false);
                panelInsertNickname.SetActive(false);

                // Habilito apenas o painel correto
                switch (_panelType)
                {
                    case PanelType.Login:
                        panelLogin.SetActive(true);
                        break;

                    case PanelType.Signup:
                        panelSignup.SetActive(true);
                        break;

                    case PanelType.ForgetPassword:
                        panelForgetPassword.SetActive(true);
                        break;

                    case PanelType.InvalidUser:
                        panelInvalidUser.SetActive(true);
                        break;

                    case PanelType.InvalidEmail:
                        panelInvalidEmail.SetActive(true);
                        break;

                    case PanelType.UsedNickname:
                        panelUsedNickname.SetActive(true);
                        break;

                    case PanelType.Message:
                        panelMessage.SetActive(true);
                        break;

                    case PanelType.InsertNickname:
                        panelInsertNickname.SetActive(true);
                        break;

                    default:
                        panelLogin.SetActive(true);
                        break;
                }
            }
        }


        public Action LoginFinishedCallback = () =>
        {
            SceneManager.instance.ChangeScene("MainMenu");
        };

        // Use this for initialization
        IEnumerator Start()
        {
            panelType = PanelType.Login;
            Grow.Init(this);

            VersionController.URL_START_WEB_API = Grow.Properties.VERSION_CONTROLLER_URL;

            ServerManager.instance.ShowWaitingAnim();
            bool hasLoad = VersionController.versionConfig != null;
            if (hasLoad)
            {
                // Troca o release notes
                UpdateReleaseNotes();

                // Verifica se a versão está bloqueada e precisa pegar uma nova versão
                if (VersionController.versionConfig.isBlocked)
                {
                    GameObject popup = PopupManager.instance.OpenPopup("PopupMessage");
                    popup.GetComponent<PopupMessage>().Configure("ATENÇÃO", VersionController.versionConfig.updateMessage,
                        () =>
                        {
                            if (!string.IsNullOrEmpty(VersionController.versionConfig.storeUrl))
                            {
                                Application.OpenURL(VersionController.versionConfig.storeUrl);
                            }

                            SceneManager.instance.ChangeScene("ChooseMode");

                            PopupManager.instance.ClosePopup();

                        });

                    ServerManager.instance.HideWaitingAnim();
                }
                else
                {
                    CheckToken();
                }
            }
            else
            {
                while (!hasLoad)
                {
                    yield return StartCoroutine(VersionController.DownloadConnectionStrings(
                        (version) =>
                        {
                            hasLoad = version;

                            if (!version)
                            {
                                ServerManager.instance.HideWaitingAnim();

                                Globals.triedToGetVersionController = true;
                                SceneManager.instance.ChangeScene("ChooseMode");
                            }
                            else
                            {
                                // Troca o release notes
                                UpdateReleaseNotes();

                                // Verifica se a versão está bloqueada e precisa pegar uma nova versão
                                if (VersionController.versionConfig.isBlocked)
                                {
                                    GameObject popup = PopupManager.instance.OpenPopup("PopupMessage");
                                    popup.GetComponent<PopupMessage>().Configure("ATENÇÃO", VersionController.versionConfig.updateMessage,
                                        () =>
                                        {
                                            if (!string.IsNullOrEmpty(VersionController.versionConfig.storeUrl))
                                            {
                                                Application.OpenURL(VersionController.versionConfig.storeUrl);
                                            }

                                            SceneManager.instance.ChangeScene("ChooseMode");

                                            PopupManager.instance.ClosePopup();

                                        });

                                    ServerManager.instance.HideWaitingAnim();
                                }
                                else
                                {
                                    CheckToken();
                                }
                            }
                        }));
                }
            }

            if (PlayerPrefs.GetInt("keepConnected") == 1)
            {
                inputLoginNickname.text = PlayerPrefs.GetString("username");
                inputLoginPassword.text = PlayerPrefs.GetString("password");
                keepConnectedToggle.isOn = true;
            }

            // Verifica se estamos voltando do Login e deu algum erro na hora de logar com o jogador
            if (Globals.errorOnLoginUserAlreadyLoggedIn)
            {
                Globals.errorOnLoginUserAlreadyLoggedIn = false;

                // Mostra a mensagem de erro
                GameObject popup = PopupManager.instance.OpenPopup("PopupMessage");
                popup.GetComponent<PopupMessage>().Configure("ERRO", "Esse usuário já está logado. Se não for você, recomendamos que troque sua senha imediatamente clicando em \"Esqueci a senha\".",
                    () =>
                    {
                        PopupManager.instance.ClosePopup();
                    });
            }
        }



        void Update()
        {
            // Se o jogador apertar TAB
            if (Input.GetKeyDown(KeyCode.Tab))
            {
                // Se o jogador estiver segurando o SHIFT
                if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
                {
                    if (panelType == PanelType.Login)
                    {
                        if (inputLoginNickname.isFocused)
                        {
                            inputLoginPassword.ActivateInputField();
                        }
                        else if (inputLoginPassword.isFocused)
                        {
                            inputLoginNickname.ActivateInputField();
                        }
                    }
                    else if (panelType == PanelType.Signup)
                    {
                        if (inputSignupNickname.isFocused)
                        {
                            inputSignupPassword2.ActivateInputField();
                        }
                        else if (inputSignupEmail.isFocused)
                        {
                            inputSignupNickname.ActivateInputField();
                        }
                        else if (inputSignupPassword.isFocused)
                        {
                            inputSignupEmail.ActivateInputField();
                        }
                        else if (inputSignupPassword2.isFocused)
                        {
                            inputSignupPassword.ActivateInputField();
                        }
                    }
                }
                else
                {
                    if (panelType == PanelType.Login)
                    {
                        if (inputLoginNickname.isFocused)
                        {
                            inputLoginPassword.ActivateInputField();
                        }
                        else if (inputLoginPassword.isFocused)
                        {
                            inputLoginNickname.ActivateInputField();
                        }
                    }
                    else if (panelType == PanelType.Signup)
                    {
                        if (inputSignupNickname.isFocused)
                        {
                            inputSignupEmail.ActivateInputField();
                        }
                        else if (inputSignupEmail.isFocused)
                        {
                            inputSignupPassword.ActivateInputField();
                        }
                        else if (inputSignupPassword.isFocused)
                        {
                            inputSignupPassword2.ActivateInputField();
                        }
                        else if (inputSignupPassword2.isFocused)
                        {
                            inputSignupNickname.ActivateInputField();
                        }
                    }
                }
            }

            // Se o jogador apertar o Enter
            if (Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Return))
            {
                switch (panelType)
                {
                    case PanelType.Login:
                        OnClickEnter();
                        break;

                    case PanelType.Signup:
                        OnClickSign();
                        break;

                    case PanelType.ForgetPassword:
                        OnClickConfirm();
                        break;

                    case PanelType.InsertNickname:
                        OnClickConfirmNickname();
                        break;

                    case PanelType.InvalidUser:
                    case PanelType.InvalidEmail:
                    case PanelType.UsedNickname:
                    case PanelType.Message:
                    default:
                        // Troca o tipo de painel
                        panelType = PanelType.Login;
                        break;
                }
            }
        }



        public void CheckToken()
        {
            // Verifica se a URL está vindo com o access token, ou seja, se o jogador já está conectado no site
            int interrogationIndex = Application.absoluteURL.IndexOf("?");
            if (interrogationIndex != -1)
            {
                string urlVariables = Application.absoluteURL.Split('?')[1];

                string[] variables = urlVariables.Split('&');

                bool hasAccessToken = false;

                for (int i = 0; i < variables.Length; i++)
                {
                    if (variables[i].IndexOf("access_token=") != -1)
                    {
                        string accessToken = variables[i].Substring("access_token=".Length);
                        if (accessToken != string.Empty)
                        {
                            hasAccessToken = true;
                            ServerManager.instance.DoLoginToken(accessToken,
                                (response) =>
                                {
                                    ServerManager.instance.HideWaitingAnim();

                                    if (!string.IsNullOrEmpty(response))
                                    {
                                        Usuario.UsuarioGrowLogin responseLogin = JsonConvert.DeserializeObject<Usuario.UsuarioGrowLogin>(response);

                                        if (responseLogin.Sucesso)
                                        {
                                            if (responseLogin.ContaFacebook)
                                            {
                                                textMessage.text = FACEBOOK_INSTRUCTION_MESSAGE.Replace(MARKER_TEXT, responseLogin.Jogador.Email);

                                                panelType = PanelType.Message;
                                            }
                                            else if (responseLogin.AtivacaoPendente)
                                            {
                                                textMessage.text = SIGNUP_NOT_ACTIVE_MESSAGE.Replace(MARKER_TEXT, responseLogin.Jogador.Email);

                                                panelType = PanelType.Message;
                                            }
                                            else
                                            {   // Deu tudo certo

                                                Globals.SetConfigData(responseLogin);
                                                AnalyticsManager.Instance.CheckUserDev(responseLogin.UserData.Apelido);
                                                AnalyticsManager.Instance.Login(responseLogin.UserData.IdGrowGames);
                                                LoginFinishedCallback();
                                            }
                                        }
                                        else
                                        {   // Sucesso é false

                                            if (string.IsNullOrEmpty(responseLogin.Mensagem))
                                            {
                                                panelType = PanelType.InvalidUser;
                                            }
                                            else
                                            {
                                                textMessage.text = responseLogin.Mensagem;

                                                panelType = PanelType.Message;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        textMessage.text = "ERRO NO LOGIN AUTOMÁTICO DO SEU USUÁRIO NO SERVIDOR.\n\nPOR FAVOR, TENTE NOVAMENTE MAIS TARDE.\n[Código de erro 100]";

                                        panelType = PanelType.Message;
                                    }
                                });
                        }
                    }
                }

                if (!hasAccessToken)
                {
                    ServerManager.instance.HideWaitingAnim();
                }
            }
            else
            {
                ServerManager.instance.HideWaitingAnim();
            }
        }



        /// <summary>
        /// Função que atualiza os releaseNotes
        /// </summary>
        private void UpdateReleaseNotes()
        {
            GameObject releaseNotesObj = GameObject.Find("TextReleaseNotes");
            if (releaseNotesObj != null)
            {
                Text textReleaseNotes = releaseNotesObj.GetComponent<Text>();
                if (textReleaseNotes != null)
                {
                    textReleaseNotes.text = VersionController.versionConfig.releaseNotes;
                }
            }
        }



        /// <summary>
        /// Função para checar a senha do jogador
        /// </summary>
        private bool IsPasswordValid(string password)
        {
            if (password.Length < 6)
            {
                return false;
            }

            return true;
        }



        /// <summary>
        /// Função para checar se o e-mail é válido
        /// </summary>
        private bool IsEmailValid(string email)
        {
            // http://stackoverflow.com/questions/5342375/regex-email-validation
            return Regex.IsMatch(email, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
        }




        /// <summary>
        /// Clique na aba de login
        /// </summary>
        public void OnClickTabLogin()
        {
            // Deixa os botões não interativos
            buttonTabLogin.interactable = false;
            buttonTabSignup.interactable = true;

            // Troca a cor dos textos dentro dos botões
            textTabLogin.color = Color.white;
            textTabSignup.color = Globals.COLOR_ORANGE;

            // Troca o tipo de painel
            panelType = PanelType.Login;
        }


        /// <summary>
        /// Clique na aba de cadastrar
        /// </summary>
        public void OnClickTabSignup()
        {
            // Deixa os botões não interativos
            buttonTabLogin.interactable = true;
            buttonTabSignup.interactable = false;

            // Troca a cor dos textos dentro dos botões
            textTabLogin.color = Globals.COLOR_ORANGE;
            textTabSignup.color = Color.white;

            // Troca o tipo de painel
            panelType = PanelType.Signup;
        }


        /// <summary>
        /// Clique no botão de esquecer senha
        /// </summary>
        public void OnClickForgetPassword()
        {
            panelType = PanelType.ForgetPassword;
        }


        /// <summary>
        /// Clique no botão entrar
        /// </summary>
        public void OnClickEnter()
        {
            bool hasError = false;

            if (string.IsNullOrEmpty(inputLoginNickname.text))
            {
                hasError = true;
                inputLoginNickname.gameObject.GetComponent<Animation>().Play("InputFieldError");
                errorBoxLoginNickname.gameObject.SetActive(true);
            }

            if (string.IsNullOrEmpty(inputLoginPassword.text) || !IsPasswordValid(inputLoginPassword.text))
            {
                hasError = true;
                inputLoginPassword.gameObject.GetComponent<Animation>().Play("InputFieldError");
                errorBoxLoginPassword.gameObject.SetActive(true);
            }

            // Se não tiver nenhum erro
            if (!hasError)
            {
                errorBoxLoginNickname.gameObject.SetActive(false);
                errorBoxLoginPassword.gameObject.SetActive(false);

                // Chama a API para fazer o Login do jogador
                ServerManager.instance.DoLogin(inputLoginNickname.text, inputLoginPassword.text, (response) =>
                {
                    //Debug.Log("login response: " + response);

                    if (!string.IsNullOrEmpty(response))
                    {
                        Usuario.UsuarioGrowLogin responseLogin = JsonConvert.DeserializeObject<Usuario.UsuarioGrowLogin>(response);

                        if (responseLogin.Sucesso)
                        {
                            if (responseLogin.ContaFacebook)
                            {
                                textMessage.text = FACEBOOK_INSTRUCTION_MESSAGE.Replace(MARKER_TEXT, responseLogin.Jogador.Email);

                                panelType = PanelType.Message;
                            }
                            else if (responseLogin.AtivacaoPendente)
                            {
                                textMessage.text = SIGNUP_NOT_ACTIVE_MESSAGE.Replace(MARKER_TEXT, responseLogin.Jogador.Email);

                                panelType = PanelType.Message;
                            }
                            else
                            {   // Deu tudo certo
                                if (keepConnectedToggle.isOn)
                                {
                                    PlayerPrefs.SetInt("keepConnected", 1);
                                    PlayerPrefs.SetString("username", inputLoginNickname.text);
                                    PlayerPrefs.SetString("password", inputLoginPassword.text);
                                }
                                else
                                {
                                    PlayerPrefs.SetInt("keepConnected", 0);
                                    PlayerPrefs.DeleteKey("username");
                                    PlayerPrefs.DeleteKey("password");
                                }
                                Globals.SetConfigData(responseLogin);
                                AnalyticsManager.Instance.CheckUserDev(responseLogin.UserData.Apelido);
                                AnalyticsManager.Instance.Login(responseLogin.UserData.IdGrowGames);
                                LoginFinishedCallback();

                            }
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(responseLogin.Mensagem))
                            {
                                panelType = PanelType.InvalidUser;
                            }
                            else
                            {
                                textMessage.text = responseLogin.Mensagem;

                                panelType = PanelType.Message;
                            }
                        }
                    }
                    else
                    {
                        textMessage.text = "ERRO NO LOGIN DO SEU USUÁRIO NO SERVIDOR.\n\nPOR FAVOR, TENTE NOVAMENTE MAIS TARDE.\n[Código de erro 101]";

                        panelType = PanelType.Message;
                    }
                });
            }
        }


        /// <summary>
        /// Clique no botão Cadastrar da aba Cadastrar
        /// </summary>
        public void OnClickSign()
        {
            bool hasError = false;

            if (string.IsNullOrEmpty(inputSignupNickname.text))
            {
                hasError = true;
                inputSignupNickname.gameObject.GetComponent<Animation>().Play("InputFieldError");
                errorBoxSignupNickname.gameObject.SetActive(true);
            }

            if (Sioux.Profanity.Check(inputSignupNickname.text))
            {
                hasError = true;
                inputSignupNickname.gameObject.GetComponent<Animation>().Play("InputFieldError");
                errorBoxSignupNickname.gameObject.SetActive(true);
                PopupManager.instance.OpenPopup("PopupMessage").GetComponent<PopupMessage>().Configure("ATENÇÃO", "Não é permitido utilizar linguagem ofensiva no apelido.", () => PopupManager.instance.ClosePopup());
            }

            if (string.IsNullOrEmpty(inputSignupEmail.text) || !IsEmailValid(inputSignupEmail.text))
            {
                hasError = true;
                inputSignupEmail.gameObject.GetComponent<Animation>().Play("InputFieldError");
                errorBoxSignupEmail.gameObject.SetActive(true);
            }

            if (string.IsNullOrEmpty(inputSignupPassword.text) || !IsPasswordValid(inputSignupPassword.text))
            {
                hasError = true;
                inputSignupPassword.gameObject.GetComponent<Animation>().Play("InputFieldError");
                errorBoxSignupPassword.gameObject.SetActive(true);
            }

            if (string.IsNullOrEmpty(inputSignupPassword2.text) || !IsPasswordValid(inputSignupPassword2.text))
            {
                hasError = true;
                inputSignupPassword2.gameObject.GetComponent<Animation>().Play("InputFieldError");
                errorBoxSignupPassword2.gameObject.SetActive(true);
            }

            if (inputSignupPassword.text != inputSignupPassword2.text)
            {
                hasError = true;
                inputSignupPassword.gameObject.GetComponent<Animation>().Play("InputFieldError");
                inputSignupPassword2.gameObject.GetComponent<Animation>().Play("InputFieldError");
                errorBoxSignupPassword2.gameObject.SetActive(true);
            }

            if (!hasError)
            {
                errorBoxSignupNickname.gameObject.SetActive(false);
                errorBoxSignupEmail.gameObject.SetActive(false);
                errorBoxSignupPassword.gameObject.SetActive(false);
                errorBoxSignupPassword2.gameObject.SetActive(false);

                ServerManager.instance.DoCadastro(inputSignupNickname.text, inputSignupEmail.text, inputSignupPassword.text, (response) =>
                {
                    //Debug.Log("ResponseCadastro: " + response);

                    if (!string.IsNullOrEmpty(response))
                    {
                        Usuario.UsuarioGrowLogin responseSign = JsonConvert.DeserializeObject<Usuario.UsuarioGrowLogin>(response);

                        if (!responseSign.Sucesso)
                        {
                            textMessage.text = responseSign.Mensagem.ToUpper();

                            // TODO: O ideal seria melhorar o tipo de mensagem, mas por enquanto vou usar um if
                            if (responseSign.Mensagem.IndexOf("apelido", System.StringComparison.OrdinalIgnoreCase) != -1)
                            {
                                panelType = PanelType.UsedNickname;
                            }
                            else
                            {
                                panelType = PanelType.Message;
                            }
                        }
                        else if (responseSign.Sucesso && responseSign.ContaFacebook)
                        {
                            textMessage.text = FACEBOOK_INSTRUCTION_MESSAGE.Replace(MARKER_TEXT, inputSignupEmail.text);

                            panelType = PanelType.Message;
                        }
                        else if (responseSign.Sucesso && responseSign.AtivacaoPendente)
                        {
                            textMessage.text = SIGNUP_NOT_FINISHED_MESSAGE.Replace(MARKER_TEXT, inputSignupEmail.text);

                            panelType = PanelType.Message;
                        }
                        else if (responseSign.Sucesso && !responseSign.ContaFacebook && !responseSign.AtivacaoPendente)
                        {
                            textMessage.text = SIGNUP_NOT_FINISHED_MESSAGE.Replace(MARKER_TEXT, inputSignupEmail.text);

                            panelType = PanelType.Message;
                        }
                        else
                        {
                            textMessage.text = "ERRO NO CADASTRO DO SEU USUÁRIO NO SERVIDOR.\n\nPOR FAVOR, TENTE NOVAMENTE MAIS TARDE\n[Código de erro 102]";

                            panelType = PanelType.Message;
                        }
                    }
                    else
                    {
                        textMessage.text = "ERRO NO CADASTRO DO SEU USUÁRIO NO SERVIDOR.\n\nPOR FAVOR, TENTE NOVAMENTE MAIS TARDE\n[Código de erro 103]";

                        panelType = PanelType.Message;
                    }
                });
            }
        }


        /// <summary>
        /// Clique no botão Voltar do painel de Esqueci a senha
        /// </summary>
        public void OnClickBackForgetPassword()
        {
            panelType = PanelType.Login;
        }



        /// <summary>
        /// Clique no botão Confirmar do painel de Esqueci a senha
        /// </summary>
        public void OnClickConfirm()
        {
            bool hasError = false;

            if (string.IsNullOrEmpty(inputForgetPasswordEmail.text) || !IsEmailValid(inputForgetPasswordEmail.text))
            {
                hasError = true;
                inputForgetPasswordEmail.gameObject.GetComponent<Animation>().Play("InputFieldError");
                errorBoxForgetPasswordEmail.gameObject.SetActive(true);
            }

            if (!hasError)
            {
                errorBoxForgetPasswordEmail.gameObject.SetActive(false);

                ServerManager.instance.DoResetPassword(inputForgetPasswordEmail.text, (response) =>
                {
                    if (!string.IsNullOrEmpty(response))
                    {
                        Usuario.UsuarioResetPassword responseForgetPass = JsonConvert.DeserializeObject<Usuario.UsuarioResetPassword>(response);

                        if (responseForgetPass.Sucesso)
                        {
                            textMessage.text = CHANGE_PASSWORD_MESSAGE.Replace(MARKER_TEXT, inputForgetPasswordEmail.text);

                            panelType = PanelType.Message;
                        }
                        else
                        {
                            panelType = PanelType.InvalidEmail;
                        }
                    }
                    else
                    {
                        textMessage.text = "ERRO NA SOLICITAÇÃO DA TROCA DE SENHA.\n\nPOR FAVOR, TENTE NOVAMENTE MAIS TARDE.\n[Código de erro 104]";

                        panelType = PanelType.Message;
                    }
                });
            }
        }


        /// <summary>
        /// Clique no botão Voltar do painel de Usuário ou senha inválido
        /// </summary>
        public void OnClickBackInvalidUser()
        {
            panelType = PanelType.Login;
        }


        /// <summary>
        /// Clique no botão Voltar do painel de E-mail inválido (e-mail não cadastrado)
        /// </summary>
        public void OnClickBackInvalidEmail()
        {
            panelType = PanelType.ForgetPassword;
        }


        /// <summary>
        /// Clique no botão Voltar do painel de Troca de senha
        /// </summary>
        public void OnClickBackChangedPassword()
        {
            panelType = PanelType.Login;
        }


        /// <summary>
        /// Clique no botão Voltar do painel de Apelido já cadastrado
        /// </summary>
        public void OnClickBackUsedNickname()
        {
            panelType = PanelType.Signup;
        }

        private void OnFacebookLoginSuccess(Facebook.Unity.AccessToken result)
        {
            if (result != null)
            {
                // FacebookManager.FbLoginResult oRetVal = JsonConvert.DeserializeObject<FacebookManager.FbLoginResult>(result);
                // Globals.playerTokenFacebook = oRetVal.access_token;
                Globals.playerTokenFacebook = result.TokenString;

                // Chama a API para fazer o Login do jogador
                ServerManager.instance.DoLoginFB(result.TokenString, (response) =>
                {
                    if (!string.IsNullOrEmpty(response))
                    {
                        Usuario.UsuarioGrowLogin responseSignup = JsonConvert.DeserializeObject<Usuario.UsuarioGrowLogin>(response);

                        if (responseSignup.Sucesso)
                        {
                            Globals.SetConfigData(responseSignup);
                            AnalyticsManager.Instance.CheckUserDev(responseSignup.UserData.Apelido);
                            AnalyticsManager.Instance.Login(responseSignup.UserData.IdGrowGames);
                            LoginFinishedCallback();
                        }
                        else
                        {
                            // Acho que o ideal seria colocar um erro mesmo e criar um outro campo para o apelido
                            panelType = PanelType.InsertNickname;
                        }
                    }
                    else
                    {
                        textMessage.text = "ERRO NO LOGIN COM O FACEBOOK NO SERVIDOR\n\nPOR FAVOR, TENTE NOVAMENTE MAIS TARDE\n[Código de erro 105]";

                        panelType = PanelType.Message;
                    }
                });
            }
            else
            {
                panelType = PanelType.InvalidUser;
            }
        }



        private void OnFacebookLoginError(string result)
        {
            if (string.IsNullOrEmpty(result))
            {
                textMessage.text = "ERRO NO LOGIN COM O FACEBOOK NO SERVIDOR\n\nPOR FAVOR, TENTE NOVAMENTE MAIS TARDE\n[Código de erro 106]";
            }
            else
            {
                textMessage.text = result;
            }

            // Mostrar Erro
            panelType = PanelType.Message;
        }



        /// <summary>
        /// Clique no botão de conectar com o Facebook
        /// </summary>
        public void OnClickFacebook()
        {
#if UNITY_WEBGL && !UNITY_EDITOR
            FacebookOpenerCaptureClick();
#else
            FacebookManager.instance.DoFBLogin(this.OnFacebookLoginSuccess, this.OnFacebookLoginError);
#endif

        }
        public void OnClickFacebook2()
        {
            FacebookManager.instance.DoFBLogin(this.OnFacebookLoginSuccess, this.OnFacebookLoginError);
        }




        /// <summary>
        /// Clique no botão de voltar da tela de escolher apelido no cadastro do Facebook
        /// </summary>
        public void OnClickBackFacebook()
        {
            panelType = PanelType.Login;
        }



        /// <summary>
        /// Clique no botão de Confirmar da tela de escolher apelido no cadastro do Facebook
        /// </summary>
        public void OnClickConfirmNickname()
        {
            bool hasError = false;

            if (string.IsNullOrEmpty(inputInsertNickname.text))
            {
                hasError = true;
                inputInsertNickname.gameObject.GetComponent<Animation>().Play("InputFieldError");
                errorBoxInsertNickname.gameObject.SetActive(true);
            }

            if (!hasError)
            {
                ServerManager.instance.DoCadastroFB(inputInsertNickname.text, Globals.playerTokenFacebook, (response) =>
                {
                    if (!string.IsNullOrEmpty(response))
                    {
                        Usuario.UsuarioGrowLogin responseSignup = JsonConvert.DeserializeObject<Usuario.UsuarioGrowLogin>(response);

                        if (responseSignup.Sucesso)
                        {
                            Globals.SetConfigData(responseSignup);
                            AnalyticsManager.Instance.CheckUserDev(responseSignup.UserData.Apelido);
                            AnalyticsManager.Instance.Login(responseSignup.UserData.IdGrowGames);
                            LoginFinishedCallback();
                        }
                        else
                        {
                            textMessage.text = responseSignup.Mensagem.ToUpper();

                            // TODO: O ideal seria melhorar o tipo de mensagem, mas por enquanto vou usar um if
                            if (responseSignup.Mensagem.IndexOf("apelido") != -1)
                            {
                                panelType = PanelType.UsedNickname;
                            }
                            else
                            {
                                panelType = PanelType.Message;
                            }
                        }
                    }
                    else
                    {
                        textMessage.text = "ERRO NO CADASTRO DO FACEBOOK NO SERVIDOR.\n\nPOR FAVOR, TENTE NOVAMENTE MAIS TARDE\n[Código de erro 107]";

                        panelType = PanelType.Message;
                    }
                });
            }
        }

        public void OnClickBack()
        {
            Sioux.Audio.Play("Click");
            SceneManager.instance.ChangeScene("ChooseMode");
        }
    }
}