﻿using DG.Tweening;
using Sfs2X.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Grow
{
    public class PanelFriendsPlay : MonoBehaviour
    {
        public static PanelFriendsPlay instance;


        // O prefab de convite do jogador
        public GameObject prefabFriendSnippetPlay;

        // O rectTransform do painel todo
        public RectTransform panelRect;

        // O rectTransform do botão de seta
        public RectTransform arrowRect;

        // Mensagem de nenhum amigo online
        public GameObject message;

        // Scroll view que precisamos ativar quando tem alguém online
        public GameObject scrollView;

        // O conteúdo da scroll view
        public Transform content;

        // Flag que indica se o painel está ligado/desligado
        public bool isOpen = false;

        // Lista de amigos que estão disponíveis para jogar
        private List<FriendSnippetPlay> friends = new List<FriendSnippetPlay>();

        // Flag que indica que está passando pela função Configure
        private bool isConfiguring = false;

        void Awake()
        {
            instance = this;
        }

        // Use this for initialization
        void Start()
        {
            SFS.Friends.OnBuddyListUpdate += UpdateFriendList;

            UpdateFriendList();
        }



        void OnDestroy()
        {
            SFS.Friends.OnBuddyListUpdate -= UpdateFriendList;
        }


        /// <summary>
        /// Função para configurar o painel
        /// </summary>
        public IEnumerator Configure()
        {
            // NOTE: Parece que essa função está sendo executada muitas vezes ao mesmo tempo! Esse if é um workaround
            if (!isConfiguring)
            {
                isConfiguring = true;

                //Debug.Log("PanelFriendsPlay - Configure ANTES");

                // Espera 1 frame
                //yield return null;
                yield return new WaitForSeconds(1f);

                //Debug.Log("PanelFriendsPlay - Configure DEPOIS");

                List<Buddy> onlineBuddies = new List<Buddy>();

                // Verifica se a sala é nula, pois pode ser que o jogador esteja saindo da sala no momento que alterou a lista de amigos
                if (SFS.Grow.sfs.LastJoinedRoom != null)
                {
                    try
                    {
                        //Debug.Log("Update Players on list");

                        // Remove os objetos da content
                        foreach (Transform t in content)
                        {
                            GameObject obj = t.gameObject;
                            if (!obj.name.Contains("Separator"))
                            {
                                Destroy(obj);
                            }
                        }
                        friends = new List<FriendSnippetPlay>();

                        // Percorre por todos os amigos filtrando somente os amigos online
                        for (int i = 0; i < SFS.Friends.BuddyList.Count; i++)
                        {
                            // Percorre as pessoas na sala
                            // TODO: Tem um jeito melhor pra fazer isso, que é usando as variáveis inGame e Game
                            bool foundedOnRoom = false;

                            //Debug.Log("SFS.Grow.sfs.LastJoinedRoom.UserCount: " + SFS.Grow.sfs.LastJoinedRoom.UserCount);
                            for (int j = 0; j < SFS.Grow.sfs.LastJoinedRoom.UserList.Count; j++)
                            {
                                //Debug.Log("SFS.Friends.BuddyList[i].Name: " + SFS.Friends.BuddyList[i].Name);
                                //Debug.Log("SFS.Friends.BuddyList[i].Id: " + SFS.Friends.BuddyList[i].Id);

                                //Debug.Log("SFS.Grow.sfs.LastJoinedRoom.PlayerList[j].Name: " + SFS.Grow.sfs.LastJoinedRoom.UserList[j].Name);
                                //Debug.Log("SFS.Grow.sfs.LastJoinedRoom.PlayerList[j].Id: " + SFS.Grow.sfs.LastJoinedRoom.UserList[j].Id);

                                if (SFS.Friends.BuddyList[i].Name == SFS.Grow.sfs.LastJoinedRoom.UserList[j].Name)
                                {
                                    foundedOnRoom = true;
                                }
                            }

                            // Verifica se o jogador está jogando
                            object inGame = FriendsSFS.GetVar(SFS.Friends.BuddyList[i], "inGame");

                            // Se o amigo estiver online, não foi encontrado na sala e se não está em jogo
                            if (SFS.Friends.BuddyList[i].IsOnline && !foundedOnRoom && inGame != null && !((bool)inGame))
                            {
                                onlineBuddies.Add(SFS.Friends.BuddyList[i]);
                            }
                        }

                        // Se tiver algum amigo online
                        if (onlineBuddies.Count > 0)
                        {
                            message.SetActive(false);
                            scrollView.SetActive(true);
                            panelRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 766);

                            for (int i = 0; i < onlineBuddies.Count; i++)
                            {
                                //Debug.Log("PanelFriendsPlay: " + i);
                                GameObject obj = Instantiate(prefabFriendSnippetPlay, content);
                                FriendSnippetPlay script = obj.GetComponent<FriendSnippetPlay>();
                                script.Configure(onlineBuddies[i]);

                                friends.Add(script);
                            }
                        }
                        else
                        {   // Não tem nenhum amigo online

                            message.SetActive(true);
                            scrollView.SetActive(false);
                            panelRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 185);
                        }
                    }
                    catch (Exception e)
                    {
                        // ERRO DO BLOB!
                        // System.NullReferenceException: Object reference not set to an instance of an object at Grow.PanelFriendsPlay +< Configure > c__Iterator0.MoveNext()[0x00141]
                        Debug.LogWarning("<color=red><b>!!! ERRO !!!</b></color> <b>PanelFriendsPlay</b> - BLOB no 'for (int j = 0; j < SFS.Grow.sfs.LastJoinedRoom.UserList.Count; j++)': " + e);
                    }

                    // Espera 1 frame
                    yield return null;

                    // Pra atualizar o alinhamento
                    content.GetComponent<VerticalLayoutGroup>().spacing = 2.1f;

                    yield return null;

                    content.GetComponent<VerticalLayoutGroup>().spacing = 2;
                }
                else
                {
                    Debug.Log("SFS.Grow.sfs.LastJoinedRoom == NULL");
                }

                //Debug.Log("CHEGOU NO FINAL DO CONFIGURE PANELFRIENDSPLAY");
                isConfiguring = false;
            }
        }


        /// <summary>
        /// Função para atualizar a lista de amigos
        /// </summary>
        public void UpdateFriendList()
        {
            if (gameObject.activeInHierarchy)
            {
                //Debug.Log("UpdateFriendList: PanelFriendsPlay!!");
                StartCoroutine(Configure());
            }
        }



        /// <summary>
        /// Função para atualizar o pedido do jogador
        /// </summary>
        public void UpdatePlayerRequest(int id)
        {
            //Debug.Log("UpdatePlayerRequest: " + friends.Count);
            for (int i = 0; i < friends.Count; i++)
            {
                //Debug.Log("Friend: " + friends[i].buddyIdGrow);
                if (friends[i].buddyIdGrow == id)
                {
                    friends[i].UpdatePlayerRequest();
                }
            }
        }



        public void OnClickArrow()
        {
            Sioux.Audio.Play("Click");
            if (isOpen)
            {
                arrowRect.localScale = new Vector3(-1, 1, 1);
                panelRect.DOAnchorPosX(318, 0.4f);
                isOpen = false;
            }
            else
            {
                arrowRect.localScale = new Vector3(1, 1, 1);
                panelRect.DOAnchorPosX(-218, 0.4f);
                isOpen = true;
            }
        }

    }
}