﻿using Grow;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemClan : MonoBehaviour
{
    private Cla clan;

    public Image clanPicture;

    public Text textClanName;

    public Text textMembersAmount;

    public Text textPoints;



    public void Configure(Cla cla)
    {
        clan = cla;

        ServerManager.instance.GetAvatarSprite("Clan" + cla.IdEscudo.ToString("00") + ".png",
            (pic) =>
            {
                clanPicture.sprite = pic;
            });

        textClanName.text = cla.Nome;
        textMembersAmount.text = cla.MembersAmount.ToString("00") + "/50";
        textPoints.text = cla.Pontos + " PTS";
    }




    public void OnClickClan()
    {
        GameObject popup = PopupManager.instance.OpenPopup("PopupEnterClan");
        popup.GetComponent<PopupEnterClan>().Configure(clan);
    }
    
}
