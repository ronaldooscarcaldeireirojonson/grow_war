﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

#if UNITY_EDITOR
    using UnityEditor;
#endif


[CreateAssetMenu]
public class Cities : ScriptableObject
{
    [SerializeField]
    public Grow.StateCityData data;


#if UNITY_EDITOR
    [ContextMenu("Load")]
    public void Load()
    {
        EditorUtility.SetDirty(this);

        data = JsonConvert.DeserializeObject<Grow.StateCityData>(File.ReadAllText(Sioux.Storage.GetPersistentPath("cidades.txt")));

        for (int i = 0; i < data.estados.Count; i++)
        {
            data.estados[i].nome = CorrectName(data.estados[i].nome);

            for (int j = 0; j < data.estados[i].cidades.Count; j++)
            {
                data.estados[i].cidades[j].nome = CorrectName(data.estados[i].cidades[j].nome);
            }
        }
    }



    private string CorrectName(string wrongName)
    {
        wrongName = wrongName.Replace("Sao", "São");
        wrongName = wrongName.Replace("Joao", "João");
        wrongName = wrongName.Replace("Agua", "Água");
        wrongName = wrongName.Replace("Bonifacio", "Bonifácio");
        wrongName = wrongName.Replace("Belem", "Belém");
        wrongName = wrongName.Replace("Sebastiao", "Sebastião");

        return wrongName;
    }
#endif



}
