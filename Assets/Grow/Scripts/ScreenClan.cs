﻿using Grow;
using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

public class ScreenClan : MonoBehaviour
{

    public GameObject buttonCreateClan;
    public GameObject buttonMyClan;

    public InputField inputSearch;

    public Transform contentTransform;


#if UNITY_WEBGL
    // Função chamada na hora de abrir uma Url, o corpo dela está dentro do plugin na pasta Plugins
    [DllImport("__Internal")]
    private static extern void PopupOpenerCaptureClick(string url);
#endif


    private void Start()
    {
        ClanRequest clanRequest = new ClanRequest();
        clanRequest.IdMember = Globals.playerId;

        ServerManager.instance.CheckMemberStatus(clanRequest,
            (response) =>
            {
                //Debug.Log("response: " + response);

                ClanResponse clanResponse = JsonConvert.DeserializeObject<ClanResponse>(response);

                switch (clanResponse.Code)
                {
                    case 1:

                        Globals.playerClanPendent = new UsuarioCla()
                        {
                            Id = clanResponse.Clans[0].Id,
                            IdEscudo = clanResponse.Clans[0].IdEscudo,
                            Nome = clanResponse.Clans[0].Nome,
                            Tag = clanResponse.Clans[0].Tag,
                            MembersAmount = clanResponse.Clans[0].MembersAmount
                        };

                        GameObject popup = PopupManager.instance.OpenPopup("PopupMessage");
                        popup.GetComponent<PopupMessage>().Configure("CONVITE", "Você foi convidado para participar do clã " + Globals.playerClanPendent.Nome + ". Aceitar convite?",
                            () =>
                            {
                                // Responde para o server
                                ClaMemberApprovalRequest request = new ClaMemberApprovalRequest();
                                request.Id = Globals.playerClanPendent.Id;
                                request.IdParticipante = Globals.playerId;
                                request.IsApproved = true;

                                ServerManager.instance.AnswerClanMember(request,
                                    (response2) =>
                                    {
                                        //Debug.Log("AnswerClanMember: " + response2);

                                        ClanResponse clanResponse2 = JsonConvert.DeserializeObject<ClanResponse>(response2);

                                        if (clanResponse2.Code == 0)
                                        {
                                            Cla clan = new Cla();
                                            clan.Id = Globals.playerClanPendent.Id;
                                            clan.IdEscudo = Globals.playerClanPendent.IdEscudo;
                                            clan.Nome = Globals.playerClanPendent.Nome;
                                            clan.Tag = Globals.playerClanPendent.Tag;
                                            clan.MembersAmount = Globals.playerClanPendent.MembersAmount;

                                            Globals.playerClan = clan;

                                            // Atualiza o botão no Header
                                            Header script = GameObject.FindObjectOfType<Header>();
                                            if (script != null)
                                            {
                                                script.UpdateClanButton();
                                            }

                                            // Verifica se o jogador merece ganhar o achievement
                                            ServerManager.instance.GetUnseenAchievements(
                                                (response3) =>
                                                {
                                                    if (!string.IsNullOrEmpty(response3))
                                                    {
                                                        List<Achievement> achieves = JsonConvert.DeserializeObject<List<Achievement>>(response3);

                                                        for (int i = 0; i < achieves.Count; i++)
                                                        {
                                                            AchievementManager.Show(achieves[i].Id);
                                                        }
                                                    }
                                                });
                                        }
                                        else
                                        {
                                            ScreenClan.ShowErrorCode(clanResponse2.Code);
                                        }

                                        Globals.playerClanPendent = null;
                                        PopupManager.instance.ClosePopup();

                                        // Nesse caso precisamos destruir a tela de gerenciamento de clãs
                                        Destroy(gameObject);
                                    });
                            },
                            () =>
                            {
                                ClanRequest request = new ClanRequest();
                                request.IdClan = Globals.playerClanPendent.Id;
                                //request.IdModerator = Globals.playerId;
                                request.IdMember = Globals.playerId;

                                ServerManager.instance.KickClanMember(request,
                                    (response2) =>
                                    {
                                        //Debug.Log("KickClanMember: " + response2);

                                        ClanResponse clanResponse2 = JsonConvert.DeserializeObject<ClanResponse>(response2);

                                        if (clanResponse2.Code != 0)
                                        {
                                            ScreenClan.ShowErrorCode(clanResponse2.Code);
                                        }

                                        Globals.playerClanPendent = null;
                                        PopupManager.instance.ClosePopup();
                                    });
                            });
                        break;

                    default:
                        break;
                }

                UpdateButton();
            });
    }



    /// <summary>
    /// Função para atualizar o botão de Criar Clã ou Meu Clã
    /// </summary>
    public void UpdateButton()
    {
        if (Globals.playerClan.Id != -1)
        {
            buttonMyClan.SetActive(true);
            buttonCreateClan.SetActive(false);
        }
        else
        {
            buttonMyClan.SetActive(false);
            buttonCreateClan.SetActive(true);
        }
    }



    public static void ShowErrorCode(int code)
    {
        if (code != 0)
        {
            string message;

            switch (code)
            {
                case 1:
                    message = "Erro interno no servidor. Por favor, tente novamente mais tarde";
                    break;

                case 2:
                    message = "Para criar um Clã você precisa ser assinante";
                    GameObject popupErroVip = PopupManager.instance.OpenPopup("PopupMessage");
                    popupErroVip.GetComponent<PopupMessage>().Configure("ERRO", message,
                        "OK",
                        () =>
                        {
                            PopupManager.instance.ClosePopup();
                        },
                        "ASSINE JÁ",
                        () =>
                        {
#if UNITY_WEBGL && !UNITY_EDITOR
                            //Application.ExternalEval("window.open(\"" + Globals.MORE_GAMES_URL + "\", \"_blank\")");
                            PopupOpenerCaptureClick("https://assinatura.growgames.com.br/Assinatura");
#else
                            Application.OpenURL("https://assinatura.growgames.com.br/Assinatura");
#endif
                        }
                        );
                    return;
                case 3:
                    message = "O nome do clã já está sendo usado";
                    break;

                case 4:
                    message = "A tag do clã já está sendo usada";
                    break;

                case 5:
                    message = "O jogador precisa estar acima do nível 10 pra participar do clã";
                    break;

                case 6:
                    message = "O jogador não é moderador e não tem permissão para alterar o clã";
                    break;

                case 7:
                    message = "O moderador não pertence ao clã";
                    break;

                case 8:
                    message = "O usuário não pertence ao clã";
                    break;

                case 9:
                    message = "Não foi possível encontrar o clã";
                    break;

                case 10:
                    message = "Não foi possível encontrar o id do jogador";
                    break;

                case 11:
                    message = "Não foi possível encontrar o id do jogador";
                    break;

                case 12:
                    message = "Não foi possível adicionar o usuário pois ele já faz parte de outro clã";
                    break;

                case 14:
                    message = "O clã já está com o número máximo de membros";
                    break;

                case 15:
                    message = "O clã já possui o número máximo de moderadores";
                    break;

                case 16:
                    message = "O usuário não faz parte do clã";
                    break;

                default:
                    message = "Erro interno no servidor. Por favor, tente novamente mais tarde";
                    break;
            }

            GameObject popup = PopupManager.instance.OpenPopup("PopupMessage");
            popup.GetComponent<PopupMessage>().Configure("ERRO", message,
                () =>
                {
                    PopupManager.instance.ClosePopup();
                });
        }
    }




    public void OnClickCreateClan()
    {
        if (Globals.playerClanPendent == null)
        {
            if (!Globals.playerIsVIP)
            {
                ShowErrorCode(2);
                return;
            }
            PopupManager.instance.OpenPopup("PopupCreateClan");
        }
        else
        {
            if (Globals.playerClanPendent.IdStatus == 4)
            {
                GameObject popup = PopupManager.instance.OpenPopup("PopupMessage");
                popup.GetComponent<PopupMessage>().Configure("ERRO", "É necessário esperar uma resposta do moderador do clã " + Globals.playerClanPendent.Nome + ". Deseja cancelar o pedido para entrar no clã?",
                    () =>
                    {
                        ClanRequest request = new ClanRequest();
                        request.IdClan = Globals.playerClanPendent.Id;
                        //request.IdModerator = Globals.playerId;
                        request.IdMember = Globals.playerId;

                        ServerManager.instance.KickClanMember(request,
                            (response) =>
                            {
                                //Debug.Log("KickClanMember: " + response);

                                ClanResponse clanResponse = JsonConvert.DeserializeObject<ClanResponse>(response);

                                if (clanResponse.Code == 0)
                                {
                                }
                                else
                                {
                                    ScreenClan.ShowErrorCode(clanResponse.Code);
                                }
                            });

                        PopupManager.instance.ClosePopup();
                    },
                    () =>
                    {
                        PopupManager.instance.ClosePopup();
                    });
            }
        }
    }




    public void OnClickMyClan()
    {
        GameObject popup = PopupManager.instance.OpenPopup("PopupEditClan");
        popup.GetComponent<PopupEditClan>().Configure();
    }



    public void OnClickSearch()
    {
        if (inputSearch.text.Length < 3)
        {
            GameObject popup = PopupManager.instance.OpenPopup("PopupMessage");
            popup.GetComponent<PopupMessage>().Configure("ERRO", "Digite pelo menos 3 caracteres",
                () =>
                {
                    PopupManager.instance.ClosePopup();
                });
            return;
        }

        // Destroi os filhos anteriores
        foreach (Transform child in contentTransform)
        {
            Destroy(child.gameObject);
        }

        ServerManager.instance.SearchClan(inputSearch.text,
            (response) =>
            {
                //Debug.Log("OnClickSearch: " + response);

                // Deserializa a resposta do servidor
                ClanResponse request = JsonConvert.DeserializeObject<ClanResponse>(response);

                if (request != null)
                {
                    for (int i = 0; i < request.Clans.Count; i++)
                    {
                        GameObject itemClan = Instantiate(Resources.Load("ItemClan"), contentTransform) as GameObject;
                        itemClan.GetComponent<ItemClan>().Configure(request.Clans[i]);
                    }
                }
                else
                {
                    ShowErrorCode(request.Code);
                }
            });
    }



    public void OnClickBack()
    {
        Destroy(gameObject);
    }

}
