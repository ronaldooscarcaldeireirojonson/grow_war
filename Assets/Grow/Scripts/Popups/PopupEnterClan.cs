﻿using Grow;
using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupEnterClan : MonoBehaviour
{
    public Cla clan;

    public Image clanPicture;

    public Text textClanName;

    public Text textClanTag;

    public Text textMembersAmount;





    /// <summary>
    /// Função para configurar os dados do clã
    /// </summary>
    public void Configure(Cla cla)
    {
        clan = cla;

        // Troca a imagem do clã
        ServerManager.instance.GetAvatarSprite("Clan" + clan.IdEscudo.ToString("00") + ".png",
            (photo) =>
            {
                clanPicture.sprite = photo;
            });

        // Troca o nome do clã
        textClanName.text = clan.Nome;

        // Troca a tag do clã
        textClanTag.text = clan.Tag;

        // Troca a quantidade de membros
        textMembersAmount.text = clan.MembersAmount.ToString("00") + "/50";
    }



    public void OnClickClose()
    {
        PopupManager.instance.ClosePopup();
    }



    public void OnClickSendRequest()
    {
        if (Globals.playerClanPendent == null)
        {
            ClaMemberRequest request = new ClaMemberRequest();
            request.Id = clan.Id;

            // Pede informações para o server
            ServerManager.instance.EnterClan(request,
                (response) =>
                {
                    //Debug.Log("SendParticipateClan: " + response);

                    if (!string.IsNullOrEmpty(response))
                    {
                        ClanResponse clanResponse = JsonConvert.DeserializeObject<ClanResponse>(response);

                        if (clanResponse.Code == 0)
                        {
                            PopupManager.instance.ClosePopup();

                            // Avisa o jogador que deu certo
                            GameObject popup = PopupManager.instance.OpenPopup("PopupMessage");
                            popup.GetComponent<PopupMessage>().Configure("CLA", "O convite foi enviado. Aguarde a resposta do administrador do clã",
                                () =>
                                {
                                    PopupManager.instance.ClosePopup();
                                });
                        }
                        else
                        {
                            ScreenClan.ShowErrorCode(clanResponse.Code);
                        }
                    }
                    else
                    {
                        ScreenClan.ShowErrorCode(1);
                    }
                });
        }
        else
        {
            if (Globals.playerClanPendent.IdStatus == 4)
            {
                GameObject popup = PopupManager.instance.OpenPopup("PopupMessage");
                popup.GetComponent<PopupMessage>().Configure("ERRO", "É necessário esperar uma resposta do moderador do clã " + Globals.playerClanPendent.Nome + ". Deseja cancelar o pedido para entrar no clã?",
                    () =>
                    {
                        ClanRequest request = new ClanRequest();
                        request.IdClan = Globals.playerClanPendent.Id;
                        //request.IdModerator = Globals.playerId;
                        request.IdMember = Globals.playerId;

                        ServerManager.instance.KickClanMember(request,
                            (response) =>
                            {
                                //Debug.Log("KickClanMember: " + response);

                                ClanResponse clanResponse = JsonConvert.DeserializeObject<ClanResponse>(response);

                                if (clanResponse.Code == 0)
                                {
                                }
                                else
                                {
                                    ScreenClan.ShowErrorCode(clanResponse.Code);
                                }
                            });

                        PopupManager.instance.ClosePopup();
                    },
                    () =>
                    {
                        PopupManager.instance.ClosePopup();
                    });
            }
        }
    }
}
