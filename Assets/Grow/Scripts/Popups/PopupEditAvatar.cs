﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Grow;

namespace Grow
{
    /// <summary>
    /// Classe que representa a popup de editar avatar dentro do perfil do jogador
    /// </summary>
    public class PopupEditAvatar : MonoBehaviour
    {
        // Os avatares
        public List<AvatarLoader> avatarLoaders;

        // O index do avatar inicial do jogador
        private int indexAvatarInitial = 0;

        // O index do avatar que está escolhido
        private int indexAvatarSelected = 0;



        // Use this for initialization
        void Start()
        {
            // Se o jogador ainda não escolheu o avatar (não tenho certeza se o idAvatar vem 0 se o jogador ainda não escolheu)
            if (Globals.playerAvatarId == 0)
            {
                Globals.playerAvatarId = 101;
            }

            // Atualiza os avatares
            for (int i = 0; i < avatarLoaders.Count; i++)
            {
                avatarLoaders[i].Configure(i, Globals.gameAvatars[i].IdAvatar, Globals.gameAvatars[i].TxAvatar, SelectAvatar);

                if (Globals.playerAvatarId == Globals.gameAvatars[i].IdAvatar)
                {
                    avatarLoaders[i].SelectAvatar();

                    indexAvatarInitial = i;
                    indexAvatarSelected = i;
                }
            }
        }



        /// <summary>
        /// Função chamada quando o jogador aperta em um avatar
        /// Chamada da classe AvatarLoader
        /// </summary>
        public void SelectAvatar(int index, int id, bool vip, Sprite sprite)
        {
            for (int i = 0; i < avatarLoaders.Count; i++)
            {
                avatarLoaders[i].DeselectAvatar();
            }

            avatarLoaders[index].SelectAvatar();

            indexAvatarSelected = index;
        }



        public void OnClickClose()
        {
            Sioux.Audio.Play("Click");
            PopupManager.instance.ClosePopup();
        }



        public void OnClickConfirm()
        {
            Sioux.Audio.Play("Click");
            if (indexAvatarInitial != indexAvatarSelected)
            {
                ServerManager.instance.ChangeAvatar(Globals.playerId, avatarLoaders[indexAvatarSelected].avatarId, Globals.gameAvatars[indexAvatarSelected].TxAvatar, (response) =>
                {
                    if (response != null)
                    {
                        Usuario.UsuarioChangeAvatar responseUser = JsonConvert.DeserializeObject<Usuario.UsuarioChangeAvatar>(response);

                        if (responseUser.Sucesso)
                        {
                            Globals.playerAvatarId = avatarLoaders[indexAvatarSelected].avatarId;
                            Globals.playerAvatarSprite = avatarLoaders[indexAvatarSelected].imageAvatar.sprite;
                            Globals.playerAvatarUrl = Globals.gameAvatars[indexAvatarSelected].TxAvatar;

                            // Atualiza o avatar no Smartfox
                            SFS.Grow.SetAvatar(Globals.playerAvatarUrl);

                            PopupManager.instance.ClosePopup();
                        }
                        else
                        {
                            // TODO: Mostra uma mensagem de erro
                        }
                    }
                    else
                    {
                        // TODO: Mostra uma mensagem de erro
                    }
                });
            }
            else
            {
                PopupManager.instance.ClosePopup();
            }

        }




    }

}