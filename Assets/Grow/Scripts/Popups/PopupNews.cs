﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Grow;
using System.Runtime.InteropServices;


namespace Grow
{

    /// <summary>
    /// Classe que representa a popup de novidades
    /// </summary>
    public class PopupNews : MonoBehaviour
    {
        public Text textTitle;
        public Image imageNews;
        public Text textButton;

        private string urlToGo = string.Empty;


#if UNITY_WEBGL
        // Função chamada na hora de abrir uma Url, o corpo dela está dentro do plugin na pasta Plugins
        [DllImport("__Internal")]
        private static extern void PopupOpenerCaptureClick(string url);
#endif

        void Start()
        {
            if (!string.IsNullOrEmpty(Globals.gameNews.Link))
            {
                urlToGo = Globals.gameNews.Link;
            }
            textButton.text = Globals.gameNews.TextoBotao.ToUpper();
            textTitle.text = Globals.gameNews.Titulo;

            ServerManager.instance.GetPictureFromUrl(Globals.gameNews.Imagem, (imageWWW) =>
            {
                Sprite sprite = Sprite.Create(imageWWW.texture, new Rect(0, 0, imageWWW.texture.width, imageWWW.texture.height), new Vector2(0, 0));

                imageNews.sprite = sprite;
                imageNews.color = new Color(1, 1, 1, 1);
            });
        }



        public void OnClickClose()
        {
            Sioux.Audio.Play("Click");

            AnalyticsManager.Instance.SendDesignEvent("Novidades:Fechar");

            PopupManager.instance.ClosePopup();
        }



        public void OnClickOk()
        {
            Sioux.Audio.Play("Click");

            AnalyticsManager.Instance.SendDesignEvent("Novidades:Clique");

            if (urlToGo != string.Empty)
            {
#if UNITY_WEBGL
                PopupOpenerCaptureClick(urlToGo);
#else
                Application.OpenURL(urlToGo);
#endif
            }

            PopupManager.instance.ClosePopup();
        }
    }

}