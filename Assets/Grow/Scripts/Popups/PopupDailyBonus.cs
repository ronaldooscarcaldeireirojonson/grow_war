﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Grow;


namespace Grow
{
    /// <summary>
    /// Classe que representa a popup de bonus diário
    /// </summary>
    public class PopupDailyBonus : MonoBehaviour
    {

        public Text textTitle;
        public List<Image> imageDayBackground;
        public List<Text> textDayEnergy;
        public Text textNext;



        // Use this for initialization
        void Start()
        {
            // Atualiza as energias diárias
            for (int i = 0; i < Globals.gameDailyBonusEnergies.Count; i++)
            {
                textDayEnergy[i].text = ((int)Globals.gameDailyBonusEnergies[i]).ToString();
            }

            // Atualiza o tempo para o próximo bônus diário
            //textNext.text = "VOLTE EM " + Globals.gameDailyBonusDate.Value.Day + "h" + Globals.gameDailyBonusDate.Value.Hour + " PARA MAIS BÔNUS!";   // Tá errado!
            textNext.text = "VOLTE AMANHÃ PARA MAIS BÔNUS!";

            // Atualiza os backgrounds
            for (int i = 0; i < imageDayBackground.Count; i++)
            {
                // Verifica se o dia é igual ao bônus diário
                if (i < (Globals.gameDailyBonusDays - 1))
                {
                    imageDayBackground[i].color = Grow.Properties.DAILY_BONUS_BACK_DAY_COLOR;
                }
                else if (i == (Globals.gameDailyBonusDays - 1))
                {
                    imageDayBackground[i].color = Grow.Properties.DAILY_BONUS_CURRENT_DAY_COLOR;

                    textTitle.text = "PARABÉNS!\nHOJE VOCÊ GANHOU " + (int)Globals.gameDailyBonusEnergies[i] + " ENERGIAS!";
                }
                else
                    imageDayBackground[i].color = Grow.Properties.DAILY_BONUS_FRONT_DAY_COLOR;
            }
        }




        public void OnClickOk()
        {
            Sioux.Audio.Play("Click");

            AnalyticsManager.Instance.SendDesignEvent("Bonus", Globals.gameDailyBonusDays);

            Globals.playerCoins += (int)Globals.gameDailyBonusEnergies[Globals.gameDailyBonusDays - 1];

            PopupManager.instance.ClosePopup();
        }

    }

}