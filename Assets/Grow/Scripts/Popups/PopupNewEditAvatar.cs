﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Grow;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using UnityEngine.Networking;


#if !UNITY_EDITOR && (UNITY_ANDROID || UNITY_IOS)
using NativeGalleryNamespace;
#endif

namespace Grow
{

    /// <summary>
    /// Classe que representa a popup de editar avatar dentro do perfil do jogador
    /// </summary>
    public class PopupNewEditAvatar : MonoBehaviour
    {
#if UNITY_WEBGL
        [DllImport("__Internal")]
        private static extern void ImageUploaderCaptureClick();
#endif



        public GameObject prefabAvatarEdit;

        public Transform contentAvatars;

        public Image imageCurrentAvatar;

        private List<AvatarLoader> avatarLoaders = new List<AvatarLoader>();

        // O index do avatar inicial do jogador
        private int indexAvatarInitial = -1;

        // O index do avatar que está escolhido
        private int indexAvatarSelected = -1;

        private string avatarBase64;

        //textura quando carrega uma imagem do celular
        private Texture2D texture;

        // Flag que indica que já criamos um avatar personalizado
        private int customAvatarIndex = -1;

        // Flag que indica que o jogador já estava com a imagem customizada selecionada mas subiu outra imagem
        private bool hasChangedCustomImage = false;


#if UNITY_WEBGL
        // Função chamada na hora de abrir uma Url, o corpo dela está dentro do plugin na pasta Plugins
        [DllImport("__Internal")]
        private static extern void PopupOpenerCaptureClick(string url);
#endif



        // Use this for initialization
        IEnumerator Start()
        {
            // Se o jogador ainda não escolheu o avatar (não tenho certeza se o idAvatar vem 0 se o jogador ainda não escolheu)
            if (Globals.playerAvatarId == 0)
            {
                Globals.playerAvatarId = 100;
            }

            // Instancia os avatares
            for (int i = 0; i < Globals.gameAvatars.Count; i++)
            {
                GameObject obj = Instantiate(prefabAvatarEdit, contentAvatars);
                AvatarLoader script = obj.GetComponent<AvatarLoader>();

                yield return new WaitForEndOfFrame();

                //Debug.Log("i: " + i);
                //Debug.Log("IdAvatar: " + Globals.gameAvatars[i].IdAvatar);
                //Debug.Log("TxAvatar: " + Globals.gameAvatars[i].TxAvatar);

                script.Configure(i, Globals.gameAvatars[i].IdAvatar, Globals.gameAvatars[i].TxAvatar, Globals.gameAvatars[i].onlyVip, SelectAvatar);

                if (Globals.playerAvatarId == Globals.gameAvatars[i].IdAvatar)
                {
                    script.SelectAvatar();

                    indexAvatarInitial = i;
                    indexAvatarSelected = i;

                    // Muda o avatar atual do jogador
                    ServerManager.instance.GetAvatarSprite(Globals.gameAvatars[i].TxAvatar, (pic) =>
                    {
                        imageCurrentAvatar.sprite = pic;
                    });
                }

                // Se esse avatar for um custom avatar
                if (Globals.gameAvatars[i].IdAvatar == 3)
                {
                    customAvatarIndex = i;

                    // Coloca esse avatar em segundo
                    obj.transform.SetSiblingIndex(1);
                }

                avatarLoaders.Add(script);
            }

            //// Já percorreu a lista inteira e não encontrou o avatar selecionado
            //if (indexAvatarInitial == -1 && indexAvatarSelected == -1)
            //{
            //    // Cria um objeto do tipo da imagem
            //    GameObject obj = Instantiate(prefabAvatarEdit, contentAvatars);
            //    AvatarLoader script = obj.GetComponent<AvatarLoader>();
            //    script.Configure(avatarLoaders.Count, 3, Globals.playerAvatarUrl, true, SelectAvatar);

            //    // Coloca a imagem na segunda posição
            //    obj.transform.SetSiblingIndex(1);

            //    // Adiciona o avatar na lista dos avatares
            //    avatarLoaders.Add(script);

            //    script.SelectAvatar();

            //    indexAvatarSelected = avatarLoaders.Count - 1;
            //    indexAvatarInitial = indexAvatarSelected;
            //    customAvatarIndex = indexAvatarSelected;

            //    // Muda o avatar atual do jogador
            //    ServerManager.instance.GetAvatarSprite(Globals.playerAvatarUrl, (pic) =>
            //    {
            //        imageCurrentAvatar.sprite = pic;
            //    });
            //}

            //Debug.Log("Globals.playerAvatarId: " + Globals.playerAvatarId);

            //// Verifica se o jogador está com uma imagem personalizada
            //if (Globals.playerAvatarId == 3)
            //{
            //    // Cria um objeto do tipo da imagem
            //    GameObject obj = Instantiate(prefabAvatarEdit, contentAvatars);
            //    AvatarLoader script = obj.GetComponent<AvatarLoader>();
            //    script.Configure(avatarLoaders.Count, 3, Globals.playerAvatarUrl, true, SelectAvatar);

            //    // Coloca a imagem na segunda posição
            //    obj.transform.SetSiblingIndex(1);

            //    // Adiciona o avatar na lista dos avatares
            //    avatarLoaders.Add(script);

            //    script.SelectAvatar();

            //    indexAvatarSelected = avatarLoaders.Count - 1;
            //    indexAvatarInitial = indexAvatarSelected;
            //    customAvatarIndex = indexAvatarSelected;

            //    // Muda o avatar atual do jogador
            //    ServerManager.instance.GetAvatarSprite(Globals.playerAvatarUrl, (pic) =>
            //    {
            //        imageCurrentAvatar.sprite = pic;
            //    });
            //}
        }



        /// <summary>
        /// Função chamada quando o jogador aperta em um avatar
        /// Chamada da classe AvatarLoader
        /// </summary>
        public void SelectAvatar(int index, int id, bool onlyVip, Sprite sprite)
        {
            // Verifica se o avatar é somente para VIPs
            if (onlyVip && !Globals.playerIsVIP)
            {
                GameObject popup = PopupManager.instance.OpenPopup("PopupMessage");
                popup.GetComponent<PopupMessage>().ConfigureWithSpecialButton("ASSINANTES", "Avatar exlusivo para assinantes",
                    "Assine já",
                    () =>
                    {
#if UNITY_WEBGL && !UNITY_EDITOR
                        PopupOpenerCaptureClick("https://assinatura.growgames.com.br/Assinatura");
#else
                        Application.OpenURL("https://assinatura.growgames.com.br/Assinatura");
#endif
                        PopupManager.instance.ClosePopup();
                    },
                    "OK",
                    () =>
                    {
                        PopupManager.instance.ClosePopup();
                    });
            }
            else
            {   // O avatar não é exclusivo de VIPs

                ServerManager.instance.ChangeAvatar(Globals.playerId, avatarLoaders[index].avatarId, avatarLoaders[index].avatarUrl,
                    (response) =>
                    {
                        if (response != null)
                        {
                            Usuario.UsuarioChangeAvatar responseUser = JsonConvert.DeserializeObject<Usuario.UsuarioChangeAvatar>(response);

                            if (responseUser.Sucesso)
                            {
                                // Deseleciona todos os avatares
                                for (int i = 0; i < avatarLoaders.Count; i++)
                                {
                                    avatarLoaders[i].DeselectAvatar();
                                }

                                // Seleciona o avatar correto
                                avatarLoaders[index].SelectAvatar();

                                // Atualiza o avatar selecionado
                                indexAvatarSelected = index;

                                // Atualiza a imagem selecionada
                                imageCurrentAvatar.sprite = sprite;

                                // Atualiza as variáveis do jogador
                                Globals.playerAvatarId = avatarLoaders[index].avatarId;
                                Globals.playerAvatarSprite = avatarLoaders[index].imageAvatar.sprite;
                                Globals.playerAvatarUrl = avatarLoaders[index].avatarUrl;

                                // Atualiza o avatar no Smartfox
                                SFS.Grow.SetAvatar(Globals.playerAvatarUrl);
                            }
                            else
                            {
                                // Mostra uma mensagem de erro
                                GameObject popup = PopupManager.instance.OpenPopup("PopupMessage");
                                popup.GetComponent<PopupMessage>().Configure("ERRO", "Houve um erro na hora de atualizar o seu avatar no servidor. Por favor, verifique sua conexão com a internet e tente novamente mais tarde",
                                    () =>
                                    {
                                        PopupManager.instance.ClosePopup();
                                    });
                            }
                        }
                        else
                        {
                            // Mostra uma mensagem de erro
                            GameObject popup = PopupManager.instance.OpenPopup("PopupMessage");
                            popup.GetComponent<PopupMessage>().Configure("ERRO", "Houve um erro na hora de atualizar o seu avatar no servidor. Por favor, verifique sua conexão com a internet e tente novamente mais tarde",
                                () =>
                                {
                                    PopupManager.instance.ClosePopup();
                                });
                        }
                    });
            }
        }



        public void OnClickClose()
        {
            Sioux.Audio.Play("Click");
            PopupManager.instance.ClosePopup();
        }



        public void OnClickChoosePicture()
        {
            Sioux.Audio.Play("Click");

            // Se o jogador for VIP
            if (Globals.playerIsVIP)
            {
#if UNITY_EDITOR 
                PickImage(512);
#elif UNITY_ANDROID || UNITY_IOS
                PickImage(512);
#elif UNITY_WEBGL
                ImageUploaderCaptureClick();
#else
                Debug.LogWarning("<color=red><b>!!! ERRO !!!</b></color> <b>TODO:</b> - Abrir a url de editar foto do portal");
#endif
            }
            else
            {
                // Mostra a popup de exclusivo para assinantes
                GameObject popup = PopupManager.instance.OpenPopup("PopupMessage");
                popup.GetComponent<PopupMessage>().ConfigureWithSpecialButton("ASSINANTES", "Carregar uma imagem personalizada é exlusivo para assinantes",
                    "Assine já",
                    () =>
                    {
#if UNITY_WEBGL && !UNITY_EDITOR
                        PopupOpenerCaptureClick("https://assinatura.growgames.com.br/Assinatura");
#else
                        Application.OpenURL("https://assinatura.growgames.com.br/Assinatura");
#endif
                        PopupManager.instance.ClosePopup();
                    },
                    "OK",
                    () =>
                    {
                        PopupManager.instance.ClosePopup();
                    });
            }
        }

        private void PickImage(int maxSize)
        {
            hasChangedCustomImage = true;

#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
            NativeGallery.Permission permission = NativeGallery.GetImageFromGallery(
                (path) =>
                {
                    //Debug.Log("Image path: " + path);

                    if (path != null)
                    {
                        // Create Texture from selected image
                        texture = NativeGallery.LoadImageAtPath(path, maxSize);
                        if (texture == null)
                        {
                            Debug.Log("Couldn't load texture from " + path);
                            return;
                        }

                        // Envia a imagem para o servidor
                        ServerManager.instance.SendImageToServer(File.ReadAllBytes(path),
                            (serverUrl) =>
                            {
                                // NOTE: A resposta do server é a url da imagem no server
                                //Debug.Log("serverUrl: " + serverUrl);

                                // Pega a imagem do avatar e já aproveita pra salvar localmente
                                ServerManager.instance.GetAvatarSprite(serverUrl,
                                    (avatarSprite) =>
                                    {
                                        // Se o sprite não for nulo
                                        if (avatarSprite != null)
                                        {
                                            // Verifica se ainda não foi criado um avatarLoader para a imagem customizada
                                            if (customAvatarIndex == -1)
                                            {
                                                // Cria um objeto do tipo da imagem
                                                GameObject obj = Instantiate(prefabAvatarEdit, contentAvatars);
                                                AvatarLoader script = obj.GetComponent<AvatarLoader>();
                                                script.Configure(avatarLoaders.Count, 3, serverUrl, true, SelectAvatar);

                                                // Coloca a imagem na segunda posição
                                                obj.transform.SetSiblingIndex(1);

                                                // Adiciona o avatar na lista dos avatares
                                                avatarLoaders.Add(script);

                                                // Atualiza o avatar selecionado
                                                customAvatarIndex = avatarLoaders.Count - 1;
                                            }
                                            else
                                            {   // Se já foi criado um avatarLoader, não precisamos criar novamente, só atualiza ele

                                                //Debug.Log("customAvatarIndex: " + customAvatarIndex);

                                                // Atualiza o avatar no avatarLoader
                                                avatarLoaders[customAvatarIndex].UpdateAvatar(serverUrl);
                                            }

                                            // Deseleciona todos os avatares
                                            for (int i = 0; i < avatarLoaders.Count; i++)
                                            {
                                                avatarLoaders[i].DeselectAvatar();
                                            }

                                            // Seleciona o avatar correto
                                            avatarLoaders[customAvatarIndex].SelectAvatar();

                                            // Atualiza o avatar selecionado
                                            indexAvatarSelected = customAvatarIndex;

                                            // Atualiza a imagem selecionada
                                            imageCurrentAvatar.sprite = avatarSprite;

                                            // Atualiza as variáveis do jogador
                                            Globals.playerAvatarSprite = avatarSprite;
                                            Globals.playerAvatarId = 3;
                                            Globals.playerAvatarUrl = serverUrl;

                                            // Atualiza o avatar no Smartfox
                                            SFS.Grow.SetAvatar(Globals.playerAvatarUrl);

                                            // Atualiza o Globals.gameAvatars
                                            CreateOrUpdateCustomAvatarOnGlobals(serverUrl);
                                        }
                                        else
                                        {
                                            Debug.LogWarning("<color=red><b>!!! TODO !!!</b></color> <b>PopupNewEditAvatar</b> - PickImage - Erro ao voltar imagem do server");
                                        }
                                    });
                            });
                    }
                }, "Selecione uma imagem", "image/png", maxSize);

            Debug.Log("Permission result: " + permission);

#else

            string path = string.Empty;

            path = Application.dataPath + "/Resources/Kimi.jpg";
            //path = Application.dataPath + "/Resources/Kimi2.png";
            //path = Application.dataPath + "/Resources/Kimi3.bmp";

            Debug.Log("path: " + path);

            // Envia a imagem para o servidor
            ServerManager.instance.SendImageToServer(File.ReadAllBytes(path),
                (serverUrl) =>
                {
                    // NOTE: A resposta do server é a url da imagem no server
                    //Debug.Log("serverUrl: " + serverUrl);

                    // Pega a imagem do avatar e já aproveita pra salvar localmente
                    ServerManager.instance.GetAvatarSprite(serverUrl,
                        (avatarSprite) =>
                        {
                            // Se o sprite não for nulo
                            if (avatarSprite != null)
                            {
                                // Verifica se ainda não foi criado um avatarLoader para a imagem customizada
                                if (customAvatarIndex == -1)
                                {
                                    // Cria um objeto do tipo da imagem
                                    GameObject obj = Instantiate(prefabAvatarEdit, contentAvatars);
                                    AvatarLoader script = obj.GetComponent<AvatarLoader>();
                                    script.Configure(avatarLoaders.Count, 3, serverUrl, true, SelectAvatar);

                                    // Coloca a imagem na segunda posição
                                    obj.transform.SetSiblingIndex(1);

                                    // Adiciona o avatar na lista dos avatares
                                    avatarLoaders.Add(script);

                                    // Atualiza o avatar selecionado
                                    customAvatarIndex = avatarLoaders.Count - 1;
                                }
                                else
                                {   // Se já foi criado um avatarLoader, não precisamos criar novamente, só atualiza ele

                                    // Atualiza o avatar no avatarLoader
                                    avatarLoaders[customAvatarIndex].UpdateAvatar(serverUrl);
                                }

                                // Deseleciona todos os avatares
                                for (int i = 0; i < avatarLoaders.Count; i++)
                                {
                                    avatarLoaders[i].DeselectAvatar();
                                }

                                // Seleciona o avatar correto
                                avatarLoaders[customAvatarIndex].SelectAvatar();

                                // Atualiza o avatar selecionado
                                indexAvatarSelected = customAvatarIndex;

                                // Atualiza a imagem selecionada
                                imageCurrentAvatar.sprite = avatarSprite;

                                // Atualiza as variáveis do jogador
                                Globals.playerAvatarSprite = avatarSprite;
                                Globals.playerAvatarId = 3;
                                Globals.playerAvatarUrl = serverUrl;

                                // Atualiza o avatar no Smartfox
                                SFS.Grow.SetAvatar(Globals.playerAvatarUrl);

                                // Atualiza o Globals.gameAvatars
                                CreateOrUpdateCustomAvatarOnGlobals(serverUrl);
                            }
                            else
                            {
                                Debug.LogWarning("<color=red><b>!!! TODO !!!</b></color> <b>PopupNewEditAvatar</b> - PickImage - Erro ao voltar imagem do server");
                            }
                        });
                });
#endif
        }



        private void PickImageWebGL(string url)
        {
            hasChangedCustomImage = true;

#if UNITY_WEBGL

            //// NOTE: na versão da WebGL, precisamos usar o streamingAssets ao invés do Resources
            //string path = string.Empty;

            ////path = Path.Combine(Application.streamingAssetsPath, "Kimi.jpg");
            //path = Path.Combine(Application.streamingAssetsPath, "Kimi2.png");
            ////path = Path.Combine(Application.streamingAssetsPath, "Kimi3.bmp");

            //Debug.Log("path: " + path);

            //StartCoroutine(GetBytesFromStreamingAssets(path,
            //    (bytearray) =>
            //    {
            //        ServerManager.instance.SendImageToServer(bytearray,
            //            (serverUrl) =>
            //            {
            //                // NOTE: A resposta do server é a url da imagem no server
            //                Debug.Log("serverUrl: " + serverUrl);

            //                // Pega a imagem do avatar e já aproveita pra salvar localmente
            //                ServerManager.instance.GetAvatarSprite(serverUrl,
            //                    (avatarSprite) =>
            //                    {
            //                // Se o sprite não for nulo
            //                if (avatarSprite != null)
            //                        {
            //                    // Verifica se ainda não foi criado um avatarLoader para a imagem customizada
            //                    if (customAvatarIndex == -1)
            //                            {
            //                        // Cria um objeto do tipo da imagem
            //                        GameObject obj = Instantiate(prefabAvatarEdit, contentAvatars);
            //                                AvatarLoader script = obj.GetComponent<AvatarLoader>();
            //                                script.Configure(avatarLoaders.Count, 3, serverUrl, true, SelectAvatar);

            //                        // Coloca a imagem na segunda posição
            //                        obj.transform.SetSiblingIndex(1);

            //                        // Adiciona o avatar na lista dos avatares
            //                        avatarLoaders.Add(script);

            //                        // Atualiza o avatar selecionado
            //                        customAvatarIndex = avatarLoaders.Count - 1;
            //                            }
            //                            else
            //                            {   // Se já foi criado um avatarLoader, não precisamos criar novamente, só atualiza ele

            //                        Debug.Log("customAvatarIndex: " + customAvatarIndex);

            //                        // Atualiza o avatar no avatarLoader
            //                        avatarLoaders[customAvatarIndex].UpdateAvatar(serverUrl);
            //                            }

            //                    // Deseleciona todos os avatares
            //                    for (int i = 0; i < avatarLoaders.Count; i++)
            //                            {
            //                                avatarLoaders[i].DeselectAvatar();
            //                            }

            //                    // Seleciona o avatar correto
            //                    avatarLoaders[customAvatarIndex].SelectAvatar();

            //                    // Atualiza o avatar selecionado
            //                    indexAvatarSelected = customAvatarIndex;

            //                    // Atualiza a imagem selecionada
            //                    imageCurrentAvatar.sprite = avatarSprite;

            //                    // Atualiza as variáveis do jogador
            //                    Globals.playerAvatarSprite = avatarSprite;
            //                            Globals.playerAvatarId = 3;
            //                            Globals.playerAvatarUrl = serverUrl;

            //                    // Atualiza o avatar no Smartfox
            //                    SFS.Grow.SetAvatar(Globals.playerAvatarUrl);

            //                    // Atualiza o Globals.gameAvatars
            //                    CreateOrUpdateCustomAvatarOnGlobals(serverUrl);
            //                        }
            //                        else
            //                        {
            //                            Debug.LogWarning("<color=red><b>!!! TODO !!!</b></color> <b>PopupNewEditAvatar</b> - PickImage - Erro ao voltar imagem do server");
            //                        }
            //                    });
            //            });
            //    }));




            // NOTE: na versão da WebGL, precisamos usar o streamingAssets ao invés do Resources
            string path = string.Empty;

            //path = Path.Combine(Application.streamingAssetsPath, "Kimi.jpg");
            //path = Path.Combine(Application.streamingAssetsPath, "Kimi2.png");
            //path = Path.Combine(Application.streamingAssetsPath, "Kimi3.bmp");
            path = url;

            //Debug.Log("path: " + path);

            StartCoroutine(GetBytesFromStreamingAssets(path,
                (bytearray) =>
                {

                    ServerManager.instance.SendImageToServer(bytearray,
                        (serverUrl) =>
                        {
                            // NOTE: A resposta do server é a url da imagem no server
                            //Debug.Log("serverUrl: " + serverUrl);

                            // Pega a imagem do avatar e já aproveita pra salvar localmente
                            ServerManager.instance.GetAvatarSprite(serverUrl,
                                (avatarSprite) =>
                                {
                                    // Se o sprite não for nulo
                                    if (avatarSprite != null)
                                    {
                                        // Verifica se ainda não foi criado um avatarLoader para a imagem customizada
                                        if (customAvatarIndex == -1)
                                        {
                                            // Cria um objeto do tipo da imagem
                                            GameObject obj = Instantiate(prefabAvatarEdit, contentAvatars);
                                            AvatarLoader script = obj.GetComponent<AvatarLoader>();
                                            script.Configure(avatarLoaders.Count, 3, serverUrl, true, SelectAvatar);

                                            // Coloca a imagem na segunda posição
                                            obj.transform.SetSiblingIndex(1);

                                            // Adiciona o avatar na lista dos avatares
                                            avatarLoaders.Add(script);

                                            // Atualiza o avatar selecionado
                                            customAvatarIndex = avatarLoaders.Count - 1;
                                        }
                                        else
                                        {   // Se já foi criado um avatarLoader, não precisamos criar novamente, só atualiza ele

                                            // Atualiza o avatar no avatarLoader
                                            avatarLoaders[customAvatarIndex].UpdateAvatar(serverUrl);
                                        }

                                        // Deseleciona todos os avatares
                                        for (int i = 0; i < avatarLoaders.Count; i++)
                                        {
                                            avatarLoaders[i].DeselectAvatar();
                                        }

                                        // Seleciona o avatar correto
                                        avatarLoaders[customAvatarIndex].SelectAvatar();

                                        // Atualiza o avatar selecionado
                                        indexAvatarSelected = customAvatarIndex;

                                        // Atualiza a imagem selecionada
                                        imageCurrentAvatar.sprite = avatarSprite;

                                        // Atualiza as variáveis do jogador
                                        Globals.playerAvatarSprite = avatarSprite;
                                        Globals.playerAvatarId = 3;
                                        Globals.playerAvatarUrl = serverUrl;

                                        // Atualiza o avatar no Smartfox
                                        SFS.Grow.SetAvatar(Globals.playerAvatarUrl);

                                        // Atualiza o Globals.gameAvatars
                                        CreateOrUpdateCustomAvatarOnGlobals(serverUrl);
                                    }
                                    else
                                    {
                                        Debug.LogWarning("<color=red><b>!!! TODO !!!</b></color> <b>PopupNewEditAvatar</b> - PickImage - Erro ao voltar imagem do server");
                                    }
                                });
                        });
                }));

#endif
        }


        /// <summary>
        /// Função para criar ou atualizar o url do customAvatar
        /// NOTE: Estamos sempre adicionando o avatar por último, portanto atenção na hora de mostrar o avatar em segundo
        /// </summary>
        private void CreateOrUpdateCustomAvatarOnGlobals(string customAvatarUrl)
        {
            // Se não tiver o avatar na lista de avatares global, adiciona
            bool hasFound = false;

            for (int i = 0; i < Globals.gameAvatars.Count; i++)
            {
                if (Globals.gameAvatars[i].IdAvatar == 3)
                {
                    hasFound = true;

                    Globals.gameAvatars[i].TxAvatar = customAvatarUrl;
                }
            }

            if (!hasFound)
            {
                // Atualiza a variável global de avatares
                GrowAvatar newAvatar = new GrowAvatar();
                newAvatar.IdAvatar = 3;
                newAvatar.TxAvatar = customAvatarUrl;
                newAvatar.onlyVip = false;

                Globals.gameAvatars.Add(newAvatar);
            }
        }


        public IEnumerator GetBytesFromStreamingAssets(string path, Action<byte[]> callback)
        {
            WWW www = new WWW(path);

            yield return www;

            callback(www.bytes);
        }


        public string ConvertToBase64(string path)
        {
            return Convert.ToBase64String(File.ReadAllBytes(path));
        }

        //public string ConvertToBase64(Texture2D tex)
        //{
        //    var newTex = new Texture2D(tex.width, tex.height, tex.format, tex.mipmapCount > 1);
        //    newTex.LoadRawTextureData(tex.GetRawTextureData());
        //    newTex.Apply();
        //    byte[] imageData = newTex.GetRawTextureData();
        //    return Convert.ToBase64String(imageData);


        //    //byte[] bytes;
        //    //using (MemoryStream ms = new MemoryStream())
        //    //{
        //    //    BinaryFormatter bf = new BinaryFormatter();
        //    //    bf.Serialize(ms, tex);
        //    //    bytes = ms.ToArray();
        //    //}

        //    //return Convert.ToBase64String(bytes);
        //}

    }

}