﻿using Grow;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupClanAvatar : MonoBehaviour
{

    public Transform content;

    public GameObject prefabClanAvatar;


    // Os avatares
    private List<AvatarLoader> avatarLoaders = new List<AvatarLoader>();

    // O index do avatar inicial do jogador
    private int indexAvatarInitial = -1;

    // O index do avatar que está escolhido
    private int indexAvatarSelected = 0;

    // Callback chamado quando o jogador apertar em confirmar
    private Action<AvatarLoader> callback;



    // Use this for initialization
    void Start()
    {
        // NOTE: Esses nomes precisam vir do server igual o avatar
        // Atualmente estamos deixando aqui pois pra carregar as imagens da erro de CORS
        Globals.gameClanAvatars = new List<GrowAvatar>()
        {
            new GrowAvatar() { IdAvatar = 0, TxAvatar = "Clan00.png"},
            new GrowAvatar() { IdAvatar = 1, TxAvatar = "Clan01.png"},
            new GrowAvatar() { IdAvatar = 2, TxAvatar = "Clan02.png"},
            new GrowAvatar() { IdAvatar = 3, TxAvatar = "Clan03.png"},
            new GrowAvatar() { IdAvatar = 4, TxAvatar = "Clan04.png"},
            new GrowAvatar() { IdAvatar = 5, TxAvatar = "Clan05.png"},
            new GrowAvatar() { IdAvatar = 6, TxAvatar = "Clan06.png"},
            new GrowAvatar() { IdAvatar = 7, TxAvatar = "Clan07.png"},
            new GrowAvatar() { IdAvatar = 8, TxAvatar = "Clan08.png"},
            new GrowAvatar() { IdAvatar = 9, TxAvatar = "Clan09.png"},

            new GrowAvatar() { IdAvatar = 10, TxAvatar = "Clan10.png"},
            new GrowAvatar() { IdAvatar = 11, TxAvatar = "Clan11.png"},
            new GrowAvatar() { IdAvatar = 12, TxAvatar = "Clan12.png"},
            new GrowAvatar() { IdAvatar = 13, TxAvatar = "Clan13.png"},
            new GrowAvatar() { IdAvatar = 14, TxAvatar = "Clan14.png"},
            new GrowAvatar() { IdAvatar = 15, TxAvatar = "Clan15.png"},
            new GrowAvatar() { IdAvatar = 16, TxAvatar = "Clan16.png"},
            new GrowAvatar() { IdAvatar = 17, TxAvatar = "Clan17.png"},
            new GrowAvatar() { IdAvatar = 18, TxAvatar = "Clan18.png"},
            new GrowAvatar() { IdAvatar = 19, TxAvatar = "Clan19.png"},

            new GrowAvatar() { IdAvatar = 20, TxAvatar = "Clan20.png"},
            new GrowAvatar() { IdAvatar = 21, TxAvatar = "Clan21.png"},
            new GrowAvatar() { IdAvatar = 22, TxAvatar = "Clan22.png"},
            new GrowAvatar() { IdAvatar = 23, TxAvatar = "Clan23.png"},
            new GrowAvatar() { IdAvatar = 24, TxAvatar = "Clan24.png"},
            new GrowAvatar() { IdAvatar = 25, TxAvatar = "Clan25.png"},
            new GrowAvatar() { IdAvatar = 26, TxAvatar = "Clan26.png"},
            new GrowAvatar() { IdAvatar = 27, TxAvatar = "Clan27.png"},
            new GrowAvatar() { IdAvatar = 28, TxAvatar = "Clan28.png"},
            new GrowAvatar() { IdAvatar = 29, TxAvatar = "Clan29.png"},
        };


        // Carrega todos os avatares
        for (int i = 0; i < Globals.gameClanAvatars.Count; i++)
        {
            GameObject avatar = (GameObject)Instantiate(prefabClanAvatar, content);

            AvatarLoader script = avatar.GetComponent<AvatarLoader>();

            script.Configure(i, Globals.gameClanAvatars[i].IdAvatar, Globals.gameClanAvatars[i].TxAvatar, SelectAvatar);

            avatarLoaders.Add(script);

            // Marca o escudo que o jogador já tem escolhido se ele está no Clã
            if (Globals.playerClan.IdEscudo == Globals.gameClanAvatars[i].IdAvatar)
            {
                script.SelectAvatar();

                indexAvatarInitial = i;
                indexAvatarSelected = i;
            }
        }
    }


    /// <summary>
    /// Função para configurar a popup
    /// </summary>
    public void Configure(Action<AvatarLoader> callback)
    {
        this.callback = callback;
    }



    /// <summary>
    /// Função chamada quando o jogador aperta em um avatar
    /// Chamada da classe AvatarLoader
    /// </summary>
    public void SelectAvatar(int index, int id, bool vip, Sprite sprite)
    {
        avatarLoaders[indexAvatarSelected].DeselectAvatar();

        avatarLoaders[index].SelectAvatar();

        indexAvatarSelected = index;
    }


    public void OnClickClose()
    {
        Sioux.Audio.Play("Click");
        PopupManager.instance.ClosePopup();
    }



    public void OnClickConfirm()
    {
        Sioux.Audio.Play("Click");
        if (indexAvatarInitial != indexAvatarSelected)
        {
            callback(avatarLoaders[indexAvatarSelected]);
        }
        else
        {
            PopupManager.instance.ClosePopup();
        }

    }



}
