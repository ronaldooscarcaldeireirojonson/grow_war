﻿using Grow;
using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupCreateClan : MonoBehaviour
{


    public InputField inputClanName;

    public InputField inputClanTag;

    public Image clanPicture;



    private void Start()
    {
        if (Globals.playerClan != null && Globals.playerClan.IdEscudo != -1)
        {
            ServerManager.instance.GetAvatarSprite("Clan" + Globals.playerClan.IdEscudo.ToString("00") + ".png",
            (pic) =>
            {
                clanPicture.sprite = pic;
            });
        }
        else
        {
            ServerManager.instance.GetAvatarSprite("Clan00.png",
            (pic) =>
            {
                clanPicture.sprite = pic;
            });
        }
    }



    public void OnClickClose()
    {
        PopupManager.instance.ClosePopup();
    }



    public void OnClickEditPicture()
    {
        GameObject popup = PopupManager.instance.OpenPopup("PopupClanAvatar");
        popup.GetComponent<PopupClanAvatar>().Configure(
            (avatarLoader) =>
            {
                Globals.playerClan.IdEscudo = avatarLoader.avatarId;
                clanPicture.sprite = avatarLoader.imageAvatar.sprite;
                PopupManager.instance.ClosePopup();
            });
    }


    public void OnClickCreate()
    {
        // NOTE: Eu já limitei os caracteres no inspector

        if (string.IsNullOrEmpty(inputClanName.text))
        {
            // Avisa o jogador que falta dados
            GameObject popup = PopupManager.instance.OpenPopup("PopupMessage");
            popup.GetComponent<PopupMessage>().Configure("ERRO", "O nome do clã não pode ser vazio",
                () =>
                {
                    PopupManager.instance.ClosePopup();
                });
            return;
        }

        if (Sioux.Profanity.Check(inputClanName.text))
        {
            GameObject popup = PopupManager.instance.OpenPopup("PopupMessage");
            popup.GetComponent<PopupMessage>().Configure("ATENÇÃO", "Não é permitido utilizar linguagem ofensiva no nome do clã",
                () =>
                {
                    PopupManager.instance.ClosePopup();
                });
            return;
        }

        if (string.IsNullOrEmpty(inputClanTag.text))
        {
            // Avisa o jogador que falta dados
            GameObject popup = PopupManager.instance.OpenPopup("PopupMessage");
            popup.GetComponent<PopupMessage>().Configure("ERRO", "A tag do clã não pode ser vazio",
                () =>
                {
                    PopupManager.instance.ClosePopup();
                });
            return;
        }

        if (Sioux.Profanity.Check(inputClanTag.text))
        {
            GameObject popup = PopupManager.instance.OpenPopup("PopupMessage");
            popup.GetComponent<PopupMessage>().Configure("ATENÇÃO", "Não é permitido utilizar linguagem ofensiva na tag do clã",
                () =>
                {
                    PopupManager.instance.ClosePopup();
                });
            return;
        }

        Cla clan = new Cla();
        clan.Nome = inputClanName.text;
        clan.Tag = inputClanTag.text;
        clan.IdEscudo = Globals.playerClan.IdEscudo;

        // Faz a requisição do server para criar um clã
        ServerManager.instance.CreateClan(clan,
            (response) =>
            {
                //Debug.Log("SendCreateClan: " + response);

                ClanResponse request = JsonConvert.DeserializeObject<ClanResponse>(response);

                if (request.Code == 0)
                {
                    // Atualiza o clã do jogador
                    Globals.playerClan = request.Clans[0];

                    // Troca o botão para o jogador ver seu clã
                    ScreenClan script = GameObject.FindObjectOfType<ScreenClan>();
                    script.UpdateButton();

                    // Atualiza o botão do Header
                    Header header = GameObject.FindObjectOfType<Header>();
                    if (header != null)
                    {
                        header.UpdateClanButton();
                    }

                    // Verifica se o jogador merece ganhar o achievement
                    ServerManager.instance.GetUnseenAchievements(
                        (response2) =>
                        {
                            if (!string.IsNullOrEmpty(response2))
                            {
                                List<Achievement> achieves = JsonConvert.DeserializeObject<List<Achievement>>(response2);

                                for (int i = 0; i < achieves.Count; i++)
                                {
                                    AchievementManager.Show(achieves[i].Id);
                                }
                            }
                        });

                    PopupManager.instance.ClosePopup();
                }
                else
                {
                    ScreenClan.ShowErrorCode(request.Code);
                }
            });
    }


}
