﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Grow.War;

namespace Grow
{


    public class CanvasPopup : MonoBehaviour
    {

        public void OnClickAlpha()
        {
            if(GameController.Instance != null)
            {
                GameController.Instance.canvasUnderPopUp.SetActive(true);
            }

            PopupManager.instance.ClosePopup();


        }
    }
}