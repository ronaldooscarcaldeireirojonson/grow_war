﻿using System.Collections.Generic;
using UnityEngine;

namespace Grow
{
    /// <summary>
    /// Função que representa a popup da loja
    /// </summary>
    public class PopupStore : MonoBehaviour
    {
        public LojaItem storeItemPrefab;
        public Transform container;

        public void Configure(List<LojaItemInfo> list)
        {
            list.ForEach(item => Instantiate(storeItemPrefab, container).Configure(item));
        }

        public void OnClickClose()
        {
            Sioux.Audio.Play("Click");

            AnalyticsManager.Instance.SendDesignEvent("Desistir:Loja");

            PopupManager.instance.ClosePopup();
        }
    }
}