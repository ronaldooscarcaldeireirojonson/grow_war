﻿using Grow;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Grow
{

    public class PopupReport : MonoBehaviour
    {
        private int targetId;
        public Text textName;
        public Image imageAvatar;

        public Toggle toggleAvatar;
        public Toggle toggleNickname;
        public Toggle toggleOther;

        public InputField inputFieldDescription;




        /// <summary>
        /// Função para configurar o item
        /// </summary>
        public void Configure(int id, string name, string avatarUrl)
        {
            targetId = id;
            textName.text = name;
            ServerManager.instance.GetAvatarSprite(name, (pic) =>
            {
                imageAvatar.sprite = pic;
            });
        }




        public void OnClickClose()
        {
            PopupManager.instance.ClosePopup();
        }


        public void OnClickConfirm()
        {
            int reportType = 0;
            if (toggleAvatar.isOn)
            {
                reportType = 0;
            }
            else if (toggleNickname.isOn)
            {
                reportType = 1;
            }
            else
            {
                reportType = 2;
            }

            ServerManager.instance.SendReportPlayer(Globals.playerId, targetId, reportType, inputFieldDescription.text, (result) =>
            {
                PopupManager.instance.ClosePopup();

                GameObject popup = PopupManager.instance.OpenPopup("PopupMessage");

                if (result == "")
                {
                    popup.GetComponent<PopupMessage>().Configure("REPORTAR", "Mensagem enviada com sucesso. Obrigado por colaborar.", () =>
                    {
                        PopupManager.instance.ClosePopup();
                    });
                }
                else
                {
                    popup.GetComponent<PopupMessage>().Configure("REPORTAR", "Falha ao enviar a mensagem. Por favor, tente novamente mais tarde.", () =>
                    {
                        PopupManager.instance.ClosePopup();
                    });
                }
            });
        }
    }
}