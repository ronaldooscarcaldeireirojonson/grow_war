﻿using Grow;
using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupEditClan : MonoBehaviour
{

    public Image clanPicture;

    public Text textClanName;
    public Text textClanTag;

    public Text textMembersAmount;

    public Transform contentTransform;

    private bool isOwner;
    private bool isModerator;

    private List<ClanMember> members;

    public GameObject buttonAddMember;



    public void Configure()
    {
        // Troca a imagem do clã
        ServerManager.instance.GetAvatarSprite("Clan" + Globals.playerClan.IdEscudo.ToString("00") + ".png",
            (photo) =>
            {
                clanPicture.sprite = photo;
            });

        // Troca o nome do clã
        textClanName.text = Globals.playerClan.Nome;

        // Troca a tag do clã
        textClanTag.text = Globals.playerClan.Tag;

        // Desabilita o botão
        buttonAddMember.SetActive(false);

        ServerManager.instance.GetClanMembers(Globals.playerClan.Id,
            (response) =>
            {
                //Debug.Log("GetClanMembers: " + response);

                ClanResponse request = JsonConvert.DeserializeObject<ClanResponse>(response);

                members = request.Members;

                if (request.Code == 0)
                {
                    // TODO: Melhorar depois chamando apenas 1 função
                    ServerManager.instance.GetClanMembersPoints(Globals.playerClan.Id,
                        (response2) =>
                        {
                            //Debug.Log("GetClanMembersPoints: " + response2);

                            if (!string.IsNullOrEmpty(response2))
                            {
                                ClanResponse clanResponse2 = JsonConvert.DeserializeObject<ClanResponse>(response2);

                                if (clanResponse2.Code == 0)
                                {
                                    for (int i = 0; i < clanResponse2.Members.Count; i++)
                                    {
                                        for (int j = 0; j < members.Count; j++)
                                        {
                                            if (clanResponse2.Members[i].IdUsuario == members[j].IdUsuario)
                                            {
                                                members[j].Points = clanResponse2.Members[i].Points;
                                                members[j].Pos = clanResponse2.Members[i].Pos;
                                            }
                                        }
                                    }

                                    // Ordena de acordo com os pontos
                                    members.Sort((a, b) => b.Points.CompareTo(a.Points));

                                    // Verifica se o próprio jogador é moderador do clã
                                    for (int i = 0; i < members.Count; i++)
                                    {
                                        if (members[i].IdUsuario == Globals.playerId)
                                        {
                                            Globals.playerClanModerator = members[i].isModerator;
                                            break;
                                        }
                                    }

                                    // Instancia os membros
                                    for (int i = 0; i < members.Count; i++)
                                    {
                                        GameObject itemClanMember = Instantiate(Resources.Load("ItemClanMember"), contentTransform) as GameObject;
                                        itemClanMember.GetComponent<ItemClanMember>().Configure(members[i]);
                                    }

                                    // Troca a quantidade de pessoas do clã
                                    textMembersAmount.text = members.Count.ToString("00") + "/50";

                                    if (Globals.playerClanModerator)
                                    {
                                        // Habilita o botão
                                        buttonAddMember.SetActive(true);
                                    }
                                }
                                else
                                {
                                    ScreenClan.ShowErrorCode(clanResponse2.Code);
                                }
                            }
                        });
                }
                else
                {
                    ScreenClan.ShowErrorCode(request.Code);
                }
            });
    }


    /// <summary>
    /// Função para atualizar os membros
    /// </summary>
    public void UpdateMembers(int idUserRemoved)
    {
        // Remove todos os filhos
        foreach (Transform child in contentTransform)
        {
            Destroy(child.gameObject);
        }

        //// Instancia os membros
        //for (int i = 0; i < members.Count; i++)
        //{
        //    // Se não for o usuário que foi removido
        //    if (members[i].IdUsuario != idUserRemoved)
        //    {
        //        GameObject itemClanMember = Instantiate(Resources.Load("ItemClanMember"), contentTransform) as GameObject;
        //        itemClanMember.GetComponent<ItemClanMember>().Configure(members[i], i);
        //    }
        //}

        Configure();
    }



    public void OnClickClose()
    {
        PopupManager.instance.ClosePopup();
    }



    public void OnClickLeaveClan()
    {
        ClanRequest request = new ClanRequest();
        request.IdClan = Globals.playerClan.Id;
        request.IdMember = Globals.playerId;

        ServerManager.instance.LeaveClan(request,
            (response) =>
            {
                //Debug.Log("LeaveClan: " + response);

                ClanResponse clanResponse = JsonConvert.DeserializeObject<ClanResponse>(response);

                if (clanResponse.Code == 0)
                {
                    Globals.playerClan = new Cla() { Id = -1, IdEscudo = 0, Nome = "", Tag = "" };

                    // Fecha a popup de editar o clã
                    PopupManager.instance.ClosePopup();

                    ScreenClan script = GameObject.FindObjectOfType<ScreenClan>();
                    if (script != null)
                    {
                        script.OnClickBack();
                    }

                    // Troca o ícone do clã do Header
                    Header header = GameObject.FindObjectOfType<Header>();
                    header.UpdateClanButton();
                }
                else
                {
                    ScreenClan.ShowErrorCode(clanResponse.Code);
                }
            });
    }




    public void OnClickChangeAvatar()
    {
        GameObject popup = PopupManager.instance.OpenPopup("PopupClanAvatar");
        popup.GetComponent<PopupClanAvatar>().Configure(
            (avatarLoader) =>
            {
                ClanRequest request = new ClanRequest();
                request.IdClan = Globals.playerClan.Id;
                request.IdModerator = Globals.playerId;

                ServerManager.instance.EditClanAvatar(request,
                    (response) =>
                    {
                        //Debug.Log("EditClanAvatar: " + response);

                        ClanResponse clanResponse = JsonConvert.DeserializeObject<ClanResponse>(response);

                        if (clanResponse.Code == 0)
                        {
                            Globals.playerClan.IdEscudo = avatarLoader.avatarId;
                            clanPicture.sprite = avatarLoader.imageAvatar.sprite;
                            PopupManager.instance.ClosePopup();
                        }
                        else
                        {
                            ScreenClan.ShowErrorCode(clanResponse.Code);
                        }
                    });
            });
    }



    public void OnClickAddMember()
    {
        GameObject popup = PopupManager.instance.OpenPopup("PopupAddClanMember");
    }
}
