﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Grow;
using System.Runtime.InteropServices;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Newtonsoft.Json;

namespace Grow
{
    /// <summary>
    /// Classe que representa a popup de configurações do jogo
    /// </summary>
    public class PopupTournament : MonoBehaviour
    {

        public GameObject screenPrize;
        public GameObject screenPodium;
        public GameObject screenCalendar;
        public GameObject screenInterrogation;

        public Image imagePrize;
        public Transform contentPodium;
        public GameObject scrollView;
        public GameObject miniWaiting;
        public GameObject textNoneScored;
        public Text textStart;
        public Text textEnd;
        public Image imageTerms;

        public Image tab;

        public GameObject prefabRankingPos;


#if UNITY_WEBGL
        // Função chamada na hora de abrir uma Url, o corpo dela está dentro do plugin
        [DllImport("__Internal")]
        private static extern void PopupOpenerCaptureClick(string url);
#endif



        // Use this for initialization
        void Start()
        {
            // Coloca a imagem do prêmio
            ServerManager.instance.GetPictureFromUrl(Globals.gameTournament.ImagePopup1,
                (imageWWW) =>
                {
                    Sprite sprite = Sprite.Create(imageWWW.texture, new Rect(0, 0, imageWWW.texture.width, imageWWW.texture.height), new Vector2(0, 0));

                    imagePrize.sprite = sprite;
                    imagePrize.color = new Color(1, 1, 1, 1);
                });

            // Coloca a imagem dos termos de uso
            ServerManager.instance.GetPictureFromUrl(Globals.gameTournament.ImagePopup2,
                (imageWWW) =>
                {
                    Sprite sprite = Sprite.Create(imageWWW.texture, new Rect(0, 0, imageWWW.texture.width, imageWWW.texture.height), new Vector2(0, 0));

                    imageTerms.sprite = sprite;
                    imageTerms.color = new Color(1, 1, 1, 1);
                });

            // Mudar os textos de início e fim
            textStart.text = Globals.gameTournament.DataInicio.ToString("dd/MM/yyyy HH:mm");
            textEnd.text = Globals.gameTournament.DataFim.ToString("dd/MM/yyyy HH:mm");

            // Colocar o ranking
            miniWaiting.SetActive(true);
            scrollView.SetActive(false);
            textNoneScored.SetActive(false);
            ServerManager.instance.GetRankingTorneio(Globals.playerId,
                (response) =>
                {
                    miniWaiting.SetActive(false);
                    //Debug.Log("response: " + response);

                    if (string.IsNullOrEmpty(response))
                    {
                        textNoneScored.SetActive(true);
                    }
                    else
                    {
                        Ranking.RankingSemanal ranking = JsonConvert.DeserializeObject<Ranking.RankingSemanal>(response);

                        if (ranking.ListaRanking.Count == 0)
                        {
                            textNoneScored.SetActive(true);
                        }
                        else
                        {
                            List<Ranking> rank = new List<Ranking>();
                            rank = ranking.ListaRanking;

                            for (int i = 0; i < ranking.ListaRanking.Count; i++)
                            {
                                GameObject gameObj = Instantiate(prefabRankingPos, contentPodium);
                                gameObj.GetComponent<RankingPosition>().Configure(ranking.ListaRanking[i]);
                            }

                            scrollView.SetActive(true);
                        }
                    }
                });

            // Muda a posição da imagem da aba
            tab.rectTransform.localPosition = new Vector3(-486.5f, 168f, 0f);

            // Desabilita as screens
            DisableAllScreens();

            // Habilita a screen correta
            screenPrize.SetActive(true);
        }




        /// <summary>
        /// Função para desabilitar todas as telas
        /// </summary>
        public void DisableAllScreens()
        {
            screenPrize.SetActive(false);
            screenPodium.SetActive(false);
            screenCalendar.SetActive(false);
            screenInterrogation.SetActive(false);
        }



        public void OnClickClose()
        {
            Sioux.Audio.Play("Click");

            PopupManager.instance.ClosePopup();
        }


        public void OnClickPrize()
        {
            Sioux.Audio.Play("Click");

            // Muda a posição da imagem da aba
            tab.rectTransform.localPosition = new Vector3(-486.5f, 168f, 0f);

            // Desabilita as screens
            DisableAllScreens();

            // Habilita a screen correta
            screenPrize.SetActive(true);
        }



        public void OnClickPodium()
        {
            Sioux.Audio.Play("Click");

            // Muda a posição da imagem da aba
            tab.rectTransform.localPosition = new Vector3(-486.5f, 34f, 0f);

            // Desabilita as screens
            DisableAllScreens();

            // Habilita a screen correta
            screenPodium.SetActive(true);
        }



        public void OnClickCalendar()
        {
            Sioux.Audio.Play("Click");

            // Muda a posição da imagem da aba
            tab.rectTransform.localPosition = new Vector3(-486.5f, -100f, 0f);

            // Desabilita as screens
            DisableAllScreens();

            // Habilita a screen correta
            screenCalendar.SetActive(true);
        }



        public void OnClickInterrogation()
        {
            Sioux.Audio.Play("Click");

            // Muda a posição da imagem da aba
            tab.rectTransform.localPosition = new Vector3(-486.5f, -234f, 0f);

            // Desabilita as screens
            DisableAllScreens();

            // Habilita a screen correta
            screenInterrogation.SetActive(true);
        }




        public void OnClickTermsOfUse()
        {
            Sioux.Audio.Play("Click");
#if UNITY_WEBGL && !UNITY_EDITOR
            //Application.ExternalEval("window.open(\"" + Globals.MORE_GAMES_URL + "\", \"_blank\")");
            PopupOpenerCaptureClick(Globals.gameTournament.UrlRegulamento);
#else
            Application.OpenURL(Globals.gameTournament.UrlRegulamento);
#endif
        }
    }

}