﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupMessage : MonoBehaviour
{

    public Text textTitle;
    public Text textMessage;

    public Button buttonYes;
    public Button buttonSpecialYes;
    public Button buttonNo;
    public Button buttonOk;

    private Action callbackOk;
    private Action callbackYes;
    private Action callbackSpecialYes;
    private Action callbackNo;





    /// <summary>
    /// Função para configurar a popup
    /// </summary>
    public void Configure(string title, string message, Action callbackYes, Action callbackNo)
    {
        textTitle.text = title;
        textMessage.text = message;

        this.callbackYes = callbackYes;
        this.callbackNo = callbackNo;

        buttonYes.gameObject.SetActive(true);
        buttonNo.gameObject.SetActive(true);
    }

    public void Configure(string title, string message, string yesText, Action callbackYes, string noText, Action callbackNo)
    {
        buttonYes.GetComponentInChildren<Text>().text = yesText;
        buttonNo.GetComponentInChildren<Text>().text = noText;
        Configure(title, message, callbackYes, callbackNo);
    }


    public void Configure(string title, string message, Action callbackOk)
    {
        textTitle.text = title;
        textMessage.text = message;

        this.callbackOk = callbackOk;

        buttonOk.gameObject.SetActive(true);
    }


    /// <summary>
    /// Essa função mostra um botão que tem EventTrigger ao invés do evento no botão
    /// </summary>
    public void ConfigureWithSpecialButton(string title, string message, string yesText, Action callbackYes, string noText, Action callbackNo)
    {
        buttonSpecialYes.GetComponentInChildren<Text>().text = yesText;
        buttonNo.GetComponentInChildren<Text>().text = noText;

        textTitle.text = title;
        textMessage.text = message;

        this.callbackSpecialYes = callbackYes;
        this.callbackNo = callbackNo;

        buttonNo.gameObject.SetActive(true);
        buttonSpecialYes.gameObject.SetActive(true);
    }



    /// <summary>
    /// Essa função mostra um botão que tem EventTrigger ao invés do evento no botão
    /// </summary>
    public void ConfigureWithSpecialButton(string title, string message, Action callbackYes)
    {
        textTitle.text = title;
        textMessage.text = message;

        this.callbackSpecialYes = callbackYes;

        buttonSpecialYes.gameObject.SetActive(true);
    }


    public void OnClickYes()
    {
        Sioux.Audio.Play("Click");
        callbackYes();
    }


    public void OnClickSpecialYes()
    {
        Sioux.Audio.Play("Click");
        callbackSpecialYes();
    }


    public void OnClickNo()
    {
        Sioux.Audio.Play("Click");
        callbackNo();
    }


    public void OnClickOk()
    {
        Sioux.Audio.Play("Click");
        callbackOk();
    }
}
