﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class PopupGrowMessage : MonoBehaviour
{

    public Text textTitle;
    public Text textMessage;

    public Button buttonYes;
    public Button buttonNo;
    public Button buttonOk;
    public Button buttonCancel;

    private Action callbackYes = null;
    private Action callbackNo = null;
    private Action callback = null;





    /// <summary>
    /// Função para configurar a popup com botão SIM e NÃO
    /// </summary>
    public void Configure(string title, string message, Action callbackYes, Action callbackNo)
    {
        this.callbackYes = callbackYes;
        this.callbackNo = callbackNo;

        buttonYes.gameObject.SetActive(true);
        buttonNo.gameObject.SetActive(true);
        buttonCancel.gameObject.SetActive(false);
        buttonOk.gameObject.SetActive(false);
    }



    /// <summary>
    /// Função para configurar a popup com botão OK ou CANCELAR
    /// </summary>
    public void Configure(string title, string message, Action callback, bool isOk)
    {
        this.callback = callback;

        buttonYes.gameObject.SetActive(false);
        buttonNo.gameObject.SetActive(false);
        if (isOk)
        {
            buttonOk.gameObject.SetActive(true);
            buttonCancel.gameObject.SetActive(false);
        }
        else
        {
            buttonOk.gameObject.SetActive(false);
            buttonCancel.gameObject.SetActive(true);
        }
    }



    public void OnClickYes()
    {
        callbackYes();
    }



    public void OnClickNo()
    {
        callbackNo();
    }



    public void OnClickOk()
    {
        callback();
    }



    public void OnClickCancel()
    {
        callback();
    }

}


