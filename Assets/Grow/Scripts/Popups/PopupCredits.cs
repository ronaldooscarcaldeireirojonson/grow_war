﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Grow;


namespace Grow
{

    /// <summary>
    /// Classe que representa a popup de créditos
    /// </summary>
    public class PopupCredits : MonoBehaviour
    {

        public Text textVersion;



        // Use this for initialization
        void Start()
        {
            // Atualiza a versão
            textVersion.text = "V " + VersionController.VERSION;
        }



        public void OnClickClose()
        {
            Sioux.Audio.Play("Click");
            PopupManager.instance.ClosePopup();
        }
    }

}