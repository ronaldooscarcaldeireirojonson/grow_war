﻿using Grow;
using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupAddClanMember : MonoBehaviour
{

    public InputField inputFieldNickname;



    public void OnClickClose()
    {
        PopupManager.instance.ClosePopup();
    }



    public void OnClickRequest()
    {
        // Se o input estiver vazio
        if (string.IsNullOrEmpty(inputFieldNickname.text))
        {
            // Avisa o jogador que falta dados
            GameObject popup = PopupManager.instance.OpenPopup("PopupMessage");
            popup.GetComponent<PopupMessage>().Configure("ERRO", "Preencha o apelido do jogador que você quer convidar para o clã",
                () =>
                {
                    PopupManager.instance.ClosePopup();
                });
            return;
        }


        ClanRequest request = new ClanRequest();
        request.IdClan = Globals.playerClan.Id;
        request.IdModerator = Globals.playerId;
        request.Nickname = inputFieldNickname.text;

        ServerManager.instance.AddMemberToClan(request,
            (response) =>
            {
                //Debug.Log("AddMemberToClan: " + response);

                if (!string.IsNullOrEmpty(response))
                {
                    ClanResponse clanResponse = JsonConvert.DeserializeObject<ClanResponse>(response);

                    if (clanResponse.Code == 0)
                    {
                        PopupManager.instance.ClosePopup();

                        GameObject popup = PopupManager.instance.OpenPopup("PopupMessage");
                        popup.GetComponent<PopupMessage>().Configure("SUCESSO", "Convite enviado! Aguarde a resposta do jogador",
                            () =>
                            {
                                PopupManager.instance.ClosePopup();
                            });

                        // envia pro smart fox
                        SFS.Grow.ClanInviteRequest(inputFieldNickname.text);
                    }
                    else
                    {
                        ScreenClan.ShowErrorCode(clanResponse.Code);
                    }
                }
                else
                {
                    ScreenClan.ShowErrorCode(1);
                }
            });
    }

}
