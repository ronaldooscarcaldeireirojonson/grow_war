﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Grow;
using System.Runtime.InteropServices;
using UnityEngine.SceneManagement;

namespace Grow
{
    /// <summary>
    /// Classe que representa a popup de configurações do jogo
    /// </summary>
    public class PopupSettings : MonoBehaviour
    {

        public GameObject buttonMusicOn;
        public GameObject buttonMusicOff;
        public GameObject buttonSoundEffectsOn;
        public GameObject buttonSoundEffectsOff;


#if UNITY_WEBGL
        // Função chamada na hora de abrir uma Url, o corpo dela está dentro do plugin
        [DllImport("__Internal")]
        protected static extern void PopupOpenerCaptureClick(string url);
#endif


        // Use this for initialization
        void Start()
        {
            //// Atualiza os botões mudo da música
            ////if (StorageManager.instance.Load<int>(FormatType.PlayerPrefs, "GetMuteMusic") == 0)
            //if (Sioux.LocalData.Get<int>("GetMuteMusic") == 0)
            //{
            //    buttonMusicOn.SetActive(true);
            //    buttonMusicOff.SetActive(false);
            //}
            //else
            //{
            //    buttonMusicOn.SetActive(false);
            //    buttonMusicOff.SetActive(true);
            //}

            //// Atualiza os botõtes mudo dos efeitos sonoros
            ////if (StorageManager.instance.Load<int>(FormatType.PlayerPrefs, "GetMuteSoundEffect") == 0)
            //if (Sioux.LocalData.Get<int>("GetMuteSoundEffect") == 0)
            //{
            //    buttonSoundEffectsOn.SetActive(true);
            //    buttonSoundEffectsOff.SetActive(false);
            //}
            //else
            //{
            //    buttonSoundEffectsOn.SetActive(false);
            //    buttonSoundEffectsOff.SetActive(true);
            //}
        }


        public virtual void OnClickClose()
        {
            Sioux.Audio.Play("Click");
            PopupManager.instance.ClosePopup();
            Scene currentScene = UnityEngine.SceneManagement.SceneManager.GetActiveScene();


        }



        public void OnClickMusicOn()
        {
            Sioux.Audio.SetMute(Sioux.Audio.Channel.BGM, true);
            Sioux.Audio.Play("Click");
            buttonMusicOn.SetActive(false);
            buttonMusicOff.SetActive(true);
        }


        public void OnClickMusicOff()
        {
            Sioux.Audio.SetMute(Sioux.Audio.Channel.BGM, false);
            Sioux.Audio.Play("Click");
            buttonMusicOn.SetActive(true);
            buttonMusicOff.SetActive(false);
        }



        public void OnClickSoundEffectsOn()
        {
            Sioux.Audio.SetMute(Sioux.Audio.Channel.SFX, true);
            Sioux.Audio.Play("Click");
            buttonSoundEffectsOn.SetActive(false);
            buttonSoundEffectsOff.SetActive(true);
        }



        public void OnClickSoundEffectsOff()
        {
            Sioux.Audio.SetMute(Sioux.Audio.Channel.SFX, false);
            Sioux.Audio.Play("Click");
            buttonSoundEffectsOn.SetActive(true);
            buttonSoundEffectsOff.SetActive(false);
        }







        public void OnClickCredits()
        {
            Sioux.Audio.Play("Click");

            PopupManager.instance.OpenPopup("PopupCredits");
        }


        public void OnClickMoreGames()
        {
            Sioux.Audio.Play("Click");
            AnalyticsManager.Instance.ClickMaisJogos(2);
#if UNITY_WEBGL && !UNITY_EDITOR
            //Application.ExternalEval("window.open(\"" + Globals.MORE_GAMES_URL + "\")");
            PopupOpenerCaptureClick("http://www.growgames.com.br");
#else
            Application.OpenURL(Globals.MORE_GAMES_URL);
#endif
        }



        public void OnClickDisconnect()
        {
            Sioux.Audio.Play("Click");
            // Limpa o token GrowGames do jogador
            Globals.playerTokenGrowGames = "";
            Sioux.LocalData.Set("SetGrowGameToken", "");

            // TODO: Remove todos os tokens ativos pelo usuário?

            // TODO: Chama o DoIssueToken do server?

            // Limpa as variáveis do jogador
            Globals.playerId = 0;
            Globals.playerIdGrowGames = 0;
            Globals.playerNickname = "";
            Globals.playerEmail = "";
            Globals.playerCompleteName = "";
            Globals.playerAvatarId = 0;
            Globals.playerAvatarUrl = "";
            Globals.playerAvatarSprite = null;
            Globals.playerAmountInvites = 0;
            Globals.playerIsVIP = false;
            Globals.playerLevel = new Nivel();
            Globals.playerCoins = 0;
            Globals.playerPoints = 0;
            Globals.playerToken = "";
            Globals.playerTokenGrowGames = "";
            Globals.playerTotalExperience = 0;
            Globals.playerTotalVictories = 0;
            Globals.playerTotalAbandonedGames = 0;
            Globals.playerPeriodExperience = 0;
            Globals.playerPeriodVictories = 0;
            Globals.playerPeriodAbandonedGames = 0;

            // Logout do smartfox e troca a cena
            SFS.DisconnectAndGoToLogin();
        }

    }

}