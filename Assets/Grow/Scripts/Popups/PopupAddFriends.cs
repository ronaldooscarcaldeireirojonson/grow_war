﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Grow
{
    public class PopupAddFriends : MonoBehaviour
    {

        public InputField inputField;


        // Use this for initialization
        void Start()
        {
            // Focus
            inputField.ActivateInputField();
        }


#if UNITY_WEBGL || UNITY_EDITOR
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Return))
            {
                OnClickAdd();
            }
        }
#endif


        public void OnClickCancel()
        {
            Sioux.Audio.Play("Click");
            PopupManager.instance.ClosePopup();
        }



        public void OnClickAdd()
        {
            Sioux.Audio.Play("Click");
            SFS.Friends.AddRequest(inputField.text.ToUpper());

            PopupManager.instance.ClosePopup(true);
        }

    }
}