﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Grow;


namespace Grow
{
    /// <summary>
    /// Classe que representa a popup de perfil do jogador
    /// </summary>
    public class PopupProfile : MonoBehaviour
    {

        public Text textNickname;
        public Text textLevel;
        public Text textExperience;
        public Text textVictories;
        public Text textAbandoned;

        public Image imageAvatar;

        public GameObject buttonEditAvatar;

        // Use this for initialization
        void Start()
        {
            //// Atualiza as informações
            //textNickname.text = Globals.playerNickname;
            //textLevel.text = Globals.playerLevel.IdNivel.ToString();
            //textExperience.text = Globals.playerPeriodExperience.ToString();
            //textVictories.text = Globals.playerPeriodVictories.ToString();
            //textAbandoned.text = Globals.playerPeriodAbandonedGames.ToString();

            //// Atualiza a foto do jogador
            //imageAvatar.sprite = Globals.playerAvatarSprite;

            //// Adiciona na lista de avatares para serem atualizados
            //MainMenuController.Instance.listImageAvatar.Add(imageAvatar);
        }



        /// <summary>
        /// Função para configurar as variáveis do jogador
        /// </summary>
        public void Configure(Usuario.UserDetails userData, bool fromHeader)
        {
            // Atualiza as informações
            textNickname.text = userData.User.Apelido;
            textLevel.text = userData.User.Nivel.IdNivel.ToString();
            textExperience.text = userData.UserGameData.ExperienciaTotal.ToString();
            textVictories.text = userData.UserGameData.VitoriasTotal.ToString();
            textAbandoned.text = userData.UserGameData.PartidasAbandonadasTotal.ToString();

            // Atualiza a foto do jogador
            ServerManager.instance.GetAvatarSprite(userData.User.Avatar,
                (sprite) =>
                {
                    imageAvatar.sprite = sprite;
                });

            // Se o usuário for o mesmo que o logado
            if (fromHeader && userData.User.IdGrowGames == Globals.playerIdGrowGames)
            {
                buttonEditAvatar.SetActive(true);
            }
            else
            {
                buttonEditAvatar.SetActive(false);
            }
        }



        public void OnClickClose()
        {
            PopupManager.instance.ClosePopup();
        }



        public void OnClickEditAvatar()
        {
            PopupManager.instance.OpenPopup("PopupEditAvatar");
        }
    }

}