﻿using Grow;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

namespace Grow
{
    public class PopupNewProfile : MonoBehaviour
    {

        public enum TabType
        {
            MyProfile,
            Info,
            Historic,
            Achievements,
            Friends
        }





        [Header("TagYourTurn")]
        public GameObject tagYourTurn;

        [Header("ProfileMain")]
        public GameObject panelProfileMain;
        public Text textName;
        public Image playerAvatar;
        public GameObject iconVip;
        public Button buttonEditAvatar;
        public Text textRank;
        public Button buttonAddFriend;
        public Button buttonRemoveFriend;
        public Button buttonLike;
        public Text textLike;
        public Button buttonReport;
        public Text textClan;
        public Text textRanking;


        [Header("MyProfile")]
        public Image tabMyProfile;
        public Text textTabMyProfile;
        public GameObject panelMyProfile;
        public Text textPlayerSince;
        public InputField inputFieldBirthday;
        public Dropdown dropdownMatiralStatus;
        public InputField inputFieldTeam;
        public Dropdown dropdownCountry;
        public Dropdown dropdownState;
        public InputField inputFieldCityOther;
        public Image imageCityOther;
        public Color colorCityOtherBrasil;
        public Color colorCityOtherOther;

        public Cities cityData;

        public GameObject iconEditBirthday;
        public GameObject iconEditTeam;
        public GameObject iconEditCityOther;

        [Header("Info")]
        public Image tabInfo;
        public Text textTabInfo;
        public GameObject panelInfo;
        public Text textVictories30;
        public Text textLosts30;
        public Text textAbandon30;
        public Text textPoints30;
        public Text textTimePlaying;
        public Text textVictories;
        public Text textLosts;
        public Text textAbandon;
        public Text textPoints;


        [Header("Historic")]
        public Image tabHistoric;
        public Text textTabHistoric;
        public GameObject panelHistoric;
        public GameObject prefabItemHistoric;
        public Text textStatusHistoric;
        public Transform contentHistoric;


        [Header("Achievements")]
        public Image tabAchievements;
        public Text textTabAchievements;
        public GameObject panelAchievements;
        public AchievementSnippet prefabItemAchievements;
        public Text textStatusAchievements;
        public Transform contentAchievements;

        [Header("LevelBar")]
        public Text textLevel;
        public Text textLevelProgress;
        public Image levelBar;


        [Header("Others")]
        public GameObject panelMyProfileAndInfo;

        protected NewProfileModel profileModel;

        protected Image currentTab;
        protected Text currentTextTab;

        protected Usuario.UserDetails userDetails;

        // String da data do último dia de nascimento
        private int lenghtDate = 0;

        // Respostas das chamadas da aba
        protected UsuarioEstatisticas responseInfo = null;
        protected List<Partida> responseMatches = null;
        protected Usuario.UserData responseAchieves = null;


        void Awake()
        {
            tagYourTurn.SetActive(false);
        }


        // Use this for initialization
        public virtual void Start()
        {
            currentTab = tabMyProfile;
            currentTextTab = textTabMyProfile;

            panelMyProfileAndInfo.SetActive(true);
            panelProfileMain.SetActive(true);
            panelMyProfile.SetActive(true);

            Globals.onPlayerAvatarUpdate += OnUpdatePlayerAvatar;
        }


        public virtual void OnDestroy()
        {
            Globals.onPlayerAvatarUpdate -= OnUpdatePlayerAvatar;
        }



        public void Configure(Usuario.UserDetails details, bool isYourTurn = false)
        {
            userDetails = details;

            //// TESTE
            //userDetails.Perfil = new UsuarioGrowPerfil();
            //userDetails.Perfil.DataNascimento = new DateTime(1987, 02, 04, 08, 08, 08);
            //userDetails.Perfil.EstadoCivil = UsuarioGrowPerfil.EnEstadoCivil.Solteiro;
            //userDetails.Perfil.Time = "Tricolor";
            //userDetails.Perfil.Pais = new UsuarioGrowPerfil.UsuarioGrowPais(1, "Brasil");
            //userDetails.Perfil.Estado = new UsuarioGrowPerfil.UsuarioGrowEstado(26, "SÂO Paulo");
            //userDetails.Perfil.Cidade = new UsuarioGrowPerfil.UsuarioGrowCidade(3, "asdadsaads");
            //// FIM de TESTE

            // Popula o dropdown de estados
            List<Dropdown.OptionData> options = new List<Dropdown.OptionData>();
            for (int i = 0; i < cityData.data.estados.Count; i++)
            {
                options.Add(new Dropdown.OptionData(cityData.data.estados[i].nome));
            }
            dropdownState.AddOptions(options);

            // Atualiza os dados da popup
            UpdateMyProfileLeftPart();
            UpdateMyProfileRightPart();

            // Habilita/desabilita a tag do turno do jogador
            SetTagYourTurn(isYourTurn);
        }



        /// <summary>
        /// Função para atualizar a porção esquerda da aba Meu Perfil (avatar, nome, clã, rank...)
        /// </summary>
        public void UpdateMyProfileLeftPart()
        {
            // - Popula os dados da aba MyProfile - Main
            textName.text = userDetails.User.Apelido;
            ServerManager.instance.GetAvatarSprite(userDetails.User.Avatar, (image) =>
            {
                playerAvatar.sprite = image;
            });

            textLevel.text = userDetails.User.Nivel.IdNivel.ToString();
            textRank.text = "Categoria: " + userDetails.User.Nivel.label;
            //textLike.text = userDetails.Perfil. profile.likeAmount.ToString();
            iconVip.SetActive(userDetails.User.isVIP);
            buttonEditAvatar.gameObject.SetActive(userDetails.User.Id == Globals.playerId);

            if (userDetails.Cla != null && userDetails.Cla.IdStatus == 2)
            {
                textClan.text = "CLÃ: " + userDetails.Cla.Nome;
            }

            if (userDetails.RankingPosition > 0)
            {
                textRanking.text = "RANKING: " + userDetails.RankingPosition + "º";
            }

            // Verifica se é o próprio jogador
            if (userDetails.User.IdGrowGames == Globals.playerIdGrowGames)
            {
                buttonAddFriend.gameObject.SetActive(false);
                buttonRemoveFriend.gameObject.SetActive(false);

                // Desabilita o botão report
                buttonReport.gameObject.SetActive(false);
            }
            else
            {
                // Se já for um amigo
                if (SFS.Friends.IsFriend(userDetails.User.IdGrowGames))
                {
                    buttonAddFriend.gameObject.SetActive(false);
                    buttonRemoveFriend.gameObject.SetActive(true);
                }
                else
                {   // Não é um amigo
                    buttonAddFriend.gameObject.SetActive(true);
                    buttonRemoveFriend.gameObject.SetActive(false);
                }

                // Habilita / desabilita o botão se o jogador for vip
                buttonReport.gameObject.SetActive(false);
                //buttonReport.gameObject.SetActive(Globals.playerIsVIP);
            }

            levelBar.fillAmount = Globals.GetNextLevelPercentage(userDetails.User.Pontos);

            Nivel currentLevel;
            Nivel nextLevel;
            for (int i = Globals.gameLevels.Count - 1; i >= 0; i--)
            {
                if (Globals.gameLevels[i].NuXP <= userDetails.User.Pontos)
                {
                    currentLevel = Globals.gameLevels[i];
                    nextLevel = Globals.gameLevels[i + 1];
                    textLevelProgress.text = userDetails.User.Pontos + "/" + nextLevel.NuXP;
                    break;
                    //return (float)(playerPoints - currentLevel.NuXP) / (float)(nextLevel.NuXP - currentLevel.NuXP);
                }
            }

        }



        /// <summary>
        /// Função para atualizar a porção direita da aba Meu Perfil (joga desde quando, idade, estado civil, time, ...)
        /// </summary>
        public virtual void UpdateMyProfileRightPart()
        {
            // Se não for a popup do próprio jogador
            if (userDetails.User.IdGrowGames != Globals.playerIdGrowGames)
            {
                iconEditBirthday.SetActive(false);
                iconEditTeam.SetActive(false);
                iconEditCityOther.SetActive(false);
                inputFieldBirthday.interactable = false;
                inputFieldTeam.interactable = false;
                inputFieldCityOther.interactable = false;
                dropdownMatiralStatus.interactable = false;
                dropdownCountry.interactable = false;
                dropdownState.interactable = false;
                //dropdownCity.interactable = false;
            }


            // - Popula a aba MyProfile - MyProfile
            // Joga desde...
            if (userDetails.Perfil.DataCadastro != null)
            {
                textPlayerSince.text = userDetails.Perfil.DataCadastro.ToString("MMMM yy", CultureInfo.CreateSpecificCulture("pt-BR"));
            }
            else
            {
                textPlayerSince.text = "";
            }

            // Data de nascimento
            if (userDetails.Perfil.DataNascimento.HasValue)
            {
                //int age = DateTime.Today.Year - userDetails.Perfil.DataNascimento.Value.Year;
                //if (userDetails.Perfil.DataNascimento.Value > DateTime.Today.AddYears(-age))
                //{
                //    age--;
                //}
                //inputFieldAge.text = age.ToString();
                inputFieldBirthday.text = userDetails.Perfil.DataNascimento.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            else
            {
                inputFieldBirthday.text = "";
            }

            // Estado civil
            if (userDetails.Perfil.EstadoCivil.HasValue && userDetails.Perfil.EstadoCivil.Value != UsuarioGrowPerfil.EnEstadoCivil.NaoDefinido)
            {
                dropdownMatiralStatus.value = (int)userDetails.Perfil.EstadoCivil.Value;
            }
            else
            {
                dropdownMatiralStatus.value = 0;
            }

            // Time
            if (userDetails.Perfil.Time != null)
            {
                inputFieldTeam.text = userDetails.Perfil.Time;
            }
            else
            {
                inputFieldTeam.text = "";
            }

            // Pais
            if (userDetails.Perfil.Pais != null)
            {
                //Debug.Log("userDetails.Perfil.Pais.IdPais: " + userDetails.Perfil.Pais.IdPais);

                // Verifica se é o Brasil ou outros
                if (userDetails.Perfil.Pais.IdPais == 1)
                {   // É o país vazio (-)
                    dropdownCountry.value = 0;
                }
                else if (userDetails.Perfil.Pais.IdPais == 32)
                {   // É o Brasil

                    dropdownCountry.value = 1;

                    // Estado
                    dropdownState.transform.parent.parent.gameObject.SetActive(true);
                    if (userDetails.Perfil.Estado != null)
                    {
                        dropdownState.value = userDetails.Perfil.Estado.IdEstado;

                        inputFieldCityOther.transform.parent.gameObject.SetActive(true);

                        if (userDetails.Perfil.CidadeOutra != null)
                        {
                            inputFieldCityOther.text = userDetails.Perfil.CidadeOutra;
                        }
                        else
                        {
                            inputFieldCityOther.text = "";
                        }
                    }
                    else
                    {
                        dropdownState.value = 0;
                    }
                }
                else
                {   // É outro país

                    // NOTE: Acho que nunca vai cair aqui! O server não está salvando se o país é outro
                    dropdownCountry.value = 2;

                    // Cidade
                    inputFieldCityOther.transform.parent.parent.gameObject.SetActive(true);
                    if (userDetails.Perfil.CidadeOutra != null)
                    {
                        inputFieldCityOther.text = userDetails.Perfil.CidadeOutra;
                    }
                    else
                    {
                        inputFieldCityOther.text = "";
                    }
                }
            }
            else
            {
                dropdownCountry.value = 0;

                //if (userDetails.Perfil.Cidade != null)
                //{
                //    Debug.Log("userDetails.Perfil.Cidade: " + userDetails.Perfil.Cidade);

                //    for (int i = 0; i < cityData.data.estados.Count; i++)
                //    {
                //        for (int j = 0; j < cityData.data.estados[i].cidades.Count; j++)
                //        {
                //            if (cityData.data.estados[i].cidades[j].id == userDetails.Perfil.Cidade.IdCidade.ToString())
                //            {
                //                Debug.Log("Achou!");
                //                Debug.Log("Estado: " + cityData.data.estados[i].nome);
                //                Debug.Log("Cidade: " + cityData.data.estados[i].cidades[j].nome);
                //            }
                //        }
                //    }
                //}

                if (!string.IsNullOrEmpty(userDetails.Perfil.CidadeOutra))
                {
                    dropdownCountry.value = 2;

                    // Cidade
                    inputFieldCityOther.transform.parent.parent.gameObject.SetActive(true);
                    if (userDetails.Perfil.CidadeOutra != null)
                    {
                        inputFieldCityOther.text = userDetails.Perfil.CidadeOutra;
                    }
                    else
                    {
                        inputFieldCityOther.text = "";
                    }
                }
            }
        }





        ///// <summary>
        ///// Função para configurar a popup
        ///// </summary>
        //public void Configure(NewProfileModel profile, bool isYourTurn = false)
        //{
        //    profileModel = profile;

        //    // Popula os dados da aba MyProfile - Main
        //    textName.text = profile.name;
        //    ServerManager.instance.GetAvatarSprite(profile.avatar, (image) =>
        //    {
        //        playerAvatar.sprite = image;
        //    });

        //    //textLevel.text = profile.level.ToString();
        //    textRank.text = profile.rank;
        //    textLike.text = profile.likeAmount.ToString();
        //    iconVip.SetActive(profile.isVip);
        //    buttonEditAvatar.gameObject.SetActive(profile.id == Globals.playerId);
        //    textClan.text = "CLÃ: " + profile.clan;

        //    // Verifica se é o próprio jogador
        //    if (profile.idGrowGames == Globals.playerIdGrowGames)
        //    {
        //        buttonAddFriend.gameObject.SetActive(false);
        //        buttonRemoveFriend.gameObject.SetActive(false);

        //        // Desabilita o botão report
        //        buttonReport.gameObject.SetActive(false);
        //    }
        //    else
        //    {
        //        // Se já for um amigo
        //        if (SFS.Friends.IsFriend(profile.idGrowGames))
        //        {
        //            buttonAddFriend.gameObject.SetActive(false);
        //            buttonRemoveFriend.gameObject.SetActive(true);
        //        }
        //        else
        //        {   // Não é um amigo
        //            buttonAddFriend.gameObject.SetActive(true);
        //            buttonRemoveFriend.gameObject.SetActive(false);
        //        }

        //        // Habilita / desabilita o botão se o jogador for vip
        //        buttonReport.gameObject.SetActive(Globals.playerIsVIP);
        //    }



        //    // Popula a aba MyProfile - MyProfile
        //    textPlayerSince.text = profile.playerSince.ToString("MMMM yy", CultureInfo.CreateSpecificCulture("pt-BR"));
        //    inputFieldAge.text = profile.age.ToString();
        //    inputFieldCity.text = profile.city;
        //    inputFieldMatiralStatus.text = profile.maritalStatus;
        //    inputFieldTeam.text = profile.team;

        //    // Popula a aba MyProfile - Info
        //    textVictories30.text = profile.victories30.ToString();
        //    textLosts30.text = profile.losts30.ToString();
        //    textAbandon30.text = profile.abandons30.ToString();
        //    textPoints30.text = profile.points30.ToString();
        //    textTimePlaying.text = (profile.timePlaying.Days * 24 + profile.timePlaying.Hours) + "h " + profile.timePlaying.Minutes + "min";
        //    textVictories.text = profile.victories.ToString();
        //    textLosts.text = profile.losts.ToString();
        //    textAbandon.text = profile.abandons.ToString();
        //    textPoints.text = profile.points.ToString();
        //    textCopperAchievements.text = profile.copperAchieve.ToString();
        //    textSilverAchievements.text = profile.silverAchieve.ToString();
        //    textGoldAchievements.text = profile.goldAchieve.ToString();

        //    // Específicos do Perfil
        //    textPersonHits.text = profile.personHits.ToString();
        //    textYearHits.text = profile.yearHits.ToString();
        //    textPlaceHits.text = profile.placeHits.ToString();
        //    textThingHits.text = profile.thingHits.ToString();

        //    // Popula a aba Historic
        //    for (int i = 0; i < profile.matches.Count; i++)
        //    {
        //        GameObject obj = Instantiate(prefabItemHistoric, contentHistoric);
        //        obj.GetComponent<ItemHistoric>().Configure(profile.matches[i], isYourTurn);
        //    }

        //    // Popula a aba Achievements
        //    for (int i = 0; i < profile.achieves.Count; i++)
        //    {
        //        GameObject obj = Instantiate(prefabItemAchievements, contentAchievements);
        //        obj.GetComponent<ItemAchievement>().Configure(profile.achieves[i]);
        //    }

        //    // Habilita/desabilita a tag do turno do jogador
        //    SetTagYourTurn(isYourTurn);
        //}


        /// <summary>
        /// Evento chamado quando a imagem do avatar do jogador é atualizado
        /// </summary>
        public void OnUpdatePlayerAvatar(Sprite pic)
        {
            playerAvatar.sprite = pic;
        }


        /// <summary>
        /// Função para desabilitar todos os painéis
        /// </summary>
        public void DisableAllPanels()
        {
            panelAchievements.SetActive(false);
            panelHistoric.SetActive(false);
            panelInfo.SetActive(false);
            panelMyProfile.SetActive(false);
            panelMyProfileAndInfo.SetActive(false);
            panelProfileMain.SetActive(false);
        }


        /// <summary>
        /// Função para habilitar/desabilitar a tag de "Seu turno"
        /// </summary>
        public void SetTagYourTurn(bool enabled)
        {
            tagYourTurn.SetActive(enabled);
        }


        /// <summary>
        /// Função para salvar o perfil do jogador
        /// </summary>
        public void SaveProfile()
        {
            // Verifica se é o próprio jogador
            if (userDetails.User.IdGrowGames == Globals.playerIdGrowGames)
            {
                Usuario.UserChangePerfilData data = new Usuario.UserChangePerfilData();

                // Data de nascimento
                DateTime date;
                string format = "dd/MM/yyyy";
                if (DateTime.TryParseExact(inputFieldBirthday.text, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    data.DtNascimento = date;
                }

                // Estado civil
                data.EstadoCivil = (UsuarioGrowPerfil.EnEstadoCivil)dropdownMatiralStatus.value;

                // País
                switch (dropdownCountry.value)
                {
                    case 0:
                        data.IdPais = 1;
                        break;

                    case 1:
                        data.IdPais = 32;
                        break;

                    case 2:
                        data.IdPais = 2;
                        break;

                    default:
                        data.IdPais = 1;
                        break;
                }

                // Estado
                if (dropdownState.value == 0)
                {
                    data.IdEstado = 1;
                }
                else
                {
                    data.IdEstado = dropdownState.value;
                }

                // Cidade
                if (!string.IsNullOrEmpty(inputFieldCityOther.text))
                {
                    data.CidadeOutra = inputFieldCityOther.text;
                }
                else
                {
                    data.CidadeOutra = "";
                }

                // Time de futebol
                data.Time = inputFieldTeam.text;

                ServerManager.instance.SetUserProfileInfo(data,
                    (response) =>
                    {
                        if (response != null)
                        {
                            //Usuario.UsuarioGrowLogin login = JsonConvert.DeserializeObject<Usuario.UsuarioGrowLogin>(response);
                            PopupManager.instance.ClosePopup();
                        }
                        else
                        {
                            GameObject popup = PopupManager.instance.OpenPopup("PopupMessage");
                            popup.GetComponent<PopupMessage>().Configure("ERRO", "Erro ao tentar salvar os dados do perfil no server. Deseja tentar novamente?",
                                () =>
                                {
                                    SaveProfile();
                                    PopupManager.instance.ClosePopup();
                                },
                                () =>
                                {
                                    PopupManager.instance.ClosePopup();
                                    PopupManager.instance.ClosePopup();
                                });
                        }
                    });
            }
            else
            {
                PopupManager.instance.ClosePopup();
            }
        }



        public void OnChangeDropdownCountry(int value)
        {
            // Verifica se é o Brasil ou outros
            if (value == 0)
            {   // É o default

                // Estado
                dropdownState.value = 0;
                dropdownState.transform.parent.gameObject.SetActive(false);
                inputFieldCityOther.text = "";
                inputFieldCityOther.transform.parent.gameObject.SetActive(false);
            }
            else if (value == 1)
            {   // É o Brasil

                // Estado
                dropdownState.value = 0;
                dropdownState.transform.parent.gameObject.SetActive(true);
                inputFieldCityOther.text = "";
                inputFieldCityOther.transform.parent.gameObject.SetActive(false);
                imageCityOther.color = colorCityOtherBrasil;
            }
            else
            {   // É outro país

                // Estado
                dropdownState.value = 0;
                dropdownState.transform.parent.gameObject.SetActive(false);
                inputFieldCityOther.text = "";
                inputFieldCityOther.transform.parent.gameObject.SetActive(true);
                imageCityOther.color = colorCityOtherOther;
            }
        }



        public void OnChangeDropdownState(int value)
        {
            // Verifica se é o default ou outros
            if (value == 0)
            {   // É o default

                inputFieldCityOther.transform.parent.gameObject.SetActive(false);
            }
            else
            {   // É um estado

                inputFieldCityOther.text = "";
                inputFieldCityOther.transform.parent.gameObject.SetActive(true);
            }
        }



        public void OnClickTagYourTurn()
        {
            Sioux.Audio.Play("Click");
            PopupManager.instance.ClosePopup();
        }


        public void OnClickTabMyProfile()
        {
            if (currentTab != tabMyProfile)
            {
                Sioux.Audio.Play("Click");
                currentTab.color = Grow.Properties.COLOR_TAB_OFF;
                currentTextTab.color = Grow.Properties.COLOR_TEXT_OFF;

                DisableAllPanels();

                tabMyProfile.color = Grow.Properties.COLOR_TAB_ON;
                textTabMyProfile.color = Grow.Properties.COLOR_TEXT_ON;
                panelMyProfileAndInfo.SetActive(true);
                panelProfileMain.SetActive(true);
                panelMyProfile.SetActive(true);

                currentTab = tabMyProfile;
                currentTextTab = textTabMyProfile;
            }
        }


        public void OnClickTabInfo()
        {
            Debug.LogWarning("<color=yellow><b>!!! NOTE !!!</b></color> <b>PopupNewProfile</b> - OnClickTabInfo: os jogos da Grow herdam essa classe e implementam essa função, portanto não usar mais!");
        }


        public void OnClickTabHistoric()
        {
            Debug.LogWarning("<color=yellow><b>!!! NOTE !!!</b></color> <b>PopupNewProfile</b> - OnClickTabHistoric: os jogos da Grow herdam essa classe e implementam essa função, portanto não usar mais!");
        }


        public void OnClickTabAchievements()
        {
            if (currentTab != tabAchievements)
            {
                Sioux.Audio.Play("Click");
                currentTab.color = Grow.Properties.COLOR_TAB_OFF;
                currentTextTab.color = Grow.Properties.COLOR_TEXT_OFF;

                DisableAllPanels();

                tabAchievements.color = Grow.Properties.COLOR_TAB_ON;
                textTabAchievements.color = Grow.Properties.COLOR_TEXT_ON;
                panelAchievements.SetActive(true);

                currentTab = tabAchievements;
                currentTextTab = textTabAchievements;
            }
        }


        public void OnClickEditAvatar()
        {
            Sioux.Audio.Play("Click");
            GameObject popup = PopupManager.instance.OpenPopup("PopupNewEditAvatar");
        }



        public void OnClickAddFriend()
        {
            Sioux.Audio.Play("Click");
            buttonAddFriend.interactable = false;

            SFS.Friends.AddRequest(textName.text.ToUpper());
        }



        public void OnClickRemoveFriend()
        {
            Sioux.Audio.Play("Click");
            buttonRemoveFriend.interactable = false;

            SFS.Friends.RemoveRequest(textName.text.ToUpper());
        }



        public void OnClickReport()
        {
            Sioux.Audio.Play("Click");
            GameObject popup = PopupManager.instance.OpenPopup("PopupReport");
            popup.GetComponent<PopupReport>().Configure(profileModel.id, profileModel.name, profileModel.avatar);
        }



        public void OnClickClose()
        {
            Sioux.Audio.Play("Click");

            SaveProfile();
        }



        public void OnClickEditClan()
        {
            Sioux.Audio.Play("Click");
        }



        public void OnChangeInputFieldBirthday(string newValue)
        {
            if ((Input.GetKey(KeyCode.Backspace) || inputFieldBirthday.text.Length < lenghtDate) && (inputFieldBirthday.text.Length == 2 || inputFieldBirthday.text.Length == 5))
            {
                inputFieldBirthday.text = inputFieldBirthday.text.Substring(0, inputFieldBirthday.text.Length - 1);
                lenghtDate = inputFieldBirthday.text.Length;
                return;
            }

            string final = "";
            List<char> accept = new List<char>() { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' };
            string text = inputFieldBirthday.text.Replace("/", "");

            for (int x = 0; x < text.Length; x++)
            {
                if (accept.Contains(text[x]))
                {
                    final += text[x];

                    if ((x + 1) % 2 == 0 && (x + 1) < 5)
                    {
                        final += "/";
                        inputFieldBirthday.caretPosition++;
                    }
                }
                else
                {
                    inputFieldBirthday.caretPosition--;
                }
            }
            lenghtDate = final.Length;
            inputFieldBirthday.text = final;
        }
    }
}
