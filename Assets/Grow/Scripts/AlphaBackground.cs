﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Grow
{
    public class AlphaBackground : MonoBehaviour
    {
        // Flag que indica se estamos animando as popups saindo ou não
        private bool isAnimating = false;



        public void OnClickBackground()
        {
            if (!isAnimating)
            {
                isAnimating = true;

                PopupManager.instance.ClosePopup();

                StartCoroutine(WaitToEnable());
            }
        }


        /// <summary>
        /// Função para esperar um tempo para reabilitar o clique
        /// </summary>
        private IEnumerator WaitToEnable()
        {
            // NOTE: Esperamos o tempo da animação da popup de fechar
            yield return new WaitForSeconds(PopupManager.TIME_POPUP_OUT);

            isAnimating = false;
        }
    }
}