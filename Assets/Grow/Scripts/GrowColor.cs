﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


namespace Grow
{
    public class GrowColor
    {
        public static int idCount = 0;

        public int id;
        public string name;
        public Color color;

        public GrowColor(){}
        public GrowColor(string name, Color color)
        {
            this.name = name;
            id = idCount++;
            this.color = color;
            LIST.Add(this);
        }

        // All Colors
        public static List<GrowColor> LIST = new List<GrowColor>();
    }
}
