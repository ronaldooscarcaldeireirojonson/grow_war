﻿using Newtonsoft.Json;
using Sfs2X.Entities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Grow
{

    public class PanelFriends : MonoBehaviour
    {

        public Transform contentPendants;
        public Transform contentFriends;

        public GameObject prefabFriendSnippetPendent;
        public GameObject prefabFriendSnippet;

        public Text textFriendsOnline;

        public Transform separatorPendingTransform;
        public Transform separatorFriendsTransform;




        // Use this for initialization
        void Start()
        {
            SFS.Friends.OnBuddyListUpdate += UpdateFriendList;

            UpdateFriendList();
        }



        void OnDestroy()
        {
            SFS.Friends.OnBuddyListUpdate -= UpdateFriendList;
        }



        /// <summary>
        /// Função para configurar/popular o painel dos amigos
        /// </summary>
        public IEnumerator Configure()
        {
            // Remove os objetos da content
            foreach (Transform t in contentFriends)
            {
                GameObject obj = t.gameObject;
                if (!obj.name.Contains("Separator"))
                {
                    Destroy(obj);
                }
            }
            foreach (Transform t in contentPendants)
            {
                GameObject obj = t.gameObject;
                if (!obj.name.Contains("Separator"))
                {
                    Destroy(obj);
                }
            }

            // Percorre a lista de jogadores e separa em 3 listas (pendente, online e offline)
            List<Buddy> onlineFriends = new List<Buddy>();
            List<Buddy> offlineFriends = new List<Buddy>();

            for (int i = 0; i < SFS.Friends.BuddyList.Count; i++)
            {
                if (SFS.Friends.BuddyList[i].IsOnline)
                {
                    onlineFriends.Add(SFS.Friends.BuddyList[i]);
                }
                else
                {
                    offlineFriends.Add(SFS.Friends.BuddyList[i]);
                }
            }

            // Popula os amigos que enviaram convite
            foreach (var buddyIR in SFS.Friends.invitesReceived.Values)
            {
                GameObject obj = Instantiate(prefabFriendSnippetPendent, contentPendants);
                obj.GetComponent<FriendSnippetPending>().Configure(buddyIR.GetInt("i"), buddyIR.GetUtfString("n"), buddyIR.GetUtfString("a"));
            }

            // Popula os amigos online
            for (int i = 0; i < onlineFriends.Count; i++)
            {
                GameObject obj = Instantiate(prefabFriendSnippet, contentFriends);
                obj.GetComponent<FriendSnippet>().Configure(onlineFriends[i], true);
            }

            // Popula os amigos offline
            for (int i = 0; i < offlineFriends.Count; i++)
            {
                GameObject obj = Instantiate(prefabFriendSnippet, contentFriends);
                obj.GetComponent<FriendSnippet>().Configure(offlineFriends[i], false);
            }

            // Popula os amigos que o jogador enviou os convites
            int counterInvitesSent = 0;
            foreach (var buddyIS in SFS.Friends.invitesSent.Values)
            {
                counterInvitesSent++;
                GameObject obj = Instantiate(prefabFriendSnippet, contentFriends);
                obj.GetComponent<FriendSnippet>().Configure(buddyIS.GetInt("i"), buddyIS.GetUtfString("n"), buddyIS.GetUtfString("a"));
            }

            // Altera o texto de amigos online
            textFriendsOnline.text = "AMIGOS: " + (onlineFriends.Count) + "/" + (onlineFriends.Count + counterInvitesSent + offlineFriends.Count);

            // Espera 1 frame
            yield return null;

            // Ativa ou desativa o objeto se tiver apenas 1 filho (é o separator)
            if (contentFriends.childCount == 1)
            {
                contentFriends.gameObject.SetActive(false);
            }
            else
            {
                contentFriends.gameObject.SetActive(true);
                separatorFriendsTransform.SetAsLastSibling();
            }

            if (contentPendants.childCount == 1)
            {
                contentPendants.gameObject.SetActive(false);
            }
            else
            {
                contentPendants.gameObject.SetActive(true);
                separatorPendingTransform.SetAsLastSibling();
            }

            //// Pra atualizar o alinhamento
            //contentFriends.GetComponent<VerticalLayoutGroup>().spacing = 2.1f;
            //contentPendants.GetComponent<VerticalLayoutGroup>().spacing = 2.1f;

            //yield return null;

            //contentFriends.GetComponent<VerticalLayoutGroup>().spacing = 2;
            //contentPendants.GetComponent<VerticalLayoutGroup>().spacing = 2;
        }




        public void UpdateFriendList()
        {
            //Debug.Log("UpdateFriendList: PanelFriends!!");
            StartCoroutine(Configure());
        }



        public void OnClickAddFriend()
        {
            Sioux.Audio.Play("Click");
            PopupManager.instance.OpenPopup("PopupAddFriend");
        }



        public void OnClickBackground()
        {
            Sioux.Audio.Play("Click");
            Destroy(gameObject);
        }
    }
}