﻿using Sfs2X.Entities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Grow
{

    public class FriendSnippetPlay : MonoBehaviour
    {
        public Image background;
        public Image imageAvatar;
        public Text textName;
        public Text textStatus;

        public Button buttonAdd;
        public GameObject miniWaiting;

        public int buddyIdGrow = 0;
        private string buddyName = "";
        private string buddyAvatar = "";


        void Start()
        {
            miniWaiting.SetActive(false);
            buttonAdd.gameObject.SetActive(true);
        }


        /// <summary>
        /// Função para configurar esse snippet
        /// </summary>
        public void Configure(Buddy friend)
        {
            this.buddyName = friend.Name;

            textName.text = this.buddyName;

            List<Sfs2X.Entities.Variables.BuddyVariable> offlineVars = friend.GetOfflineVariables();

            for (int i = 0; i < offlineVars.Count; i++)
            {
                if (offlineVars[i].Name == "$avatar")
                {
                    buddyAvatar = offlineVars[i].GetStringValue();
                }
                else if (offlineVars[i].Name == "$idGrow")
                {
                    buddyIdGrow = offlineVars[i].GetIntValue();
                }
            }

            ServerManager.instance.GetAvatarSprite(buddyAvatar, (pic) =>
            {
                imageAvatar.sprite = pic;
            });

            buttonAdd.gameObject.SetActive(true);

            textName.color = new Color32(242, 122, 22, 255);
            textStatus.text = "Online";
            textStatus.color = new Color32(242, 122, 22, 255);
            background.color = Color.white;
        }


        public void UpdatePlayerRequest()
        {
            miniWaiting.SetActive(false);
            buttonAdd.gameObject.SetActive(true);
        }


        public void OnClickProfile()
        {
            ServerManager.instance.OpenNewProfilePopup(buddyIdGrow);
        }


        public void OnClickAdd()
        {
            miniWaiting.SetActive(true);
            buttonAdd.gameObject.SetActive(false);

            SFS.Friends.InviteRequest(this.buddyName);
        }
    }
}