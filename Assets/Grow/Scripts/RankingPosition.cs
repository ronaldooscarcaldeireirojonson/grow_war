﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Grow;
using Newtonsoft.Json;

namespace Grow
{

    /// <summary>
    /// Classe que representa cada jogador no ranking
    /// </summary>
    public class RankingPosition : MonoBehaviour
    {
        public Image background;
        public Text textPosition;
        public Text textName;
        public Text textPoints;
        public Text textLevel;
        public Image imageAvatar;
        public Image currentPatent;
        public List<Sprite> patentSprites;

        private int idGrow;



        /// <summary>
        /// Função para configurar esse item
        /// </summary>
        public void Configure(Ranking rank)
        {
            textPosition.text = rank.Posicao.ToString() + "º";
            textName.text = rank.Apelido;
            textPoints.text = rank.Pontos.ToString() + " PONTOS";
            textLevel.text = rank.Nivel.ToString();
            currentPatent.sprite = patentSprites[rank.IdPatente - 1];
            currentPatent.gameObject.SetActive(true);
            idGrow = rank.IdGrowGames;

            ServerManager.instance.GetAvatarSprite(rank.Foto, (pic) =>
            {
                if (imageAvatar)
                    imageAvatar.sprite = pic;
            });
        }



        /// <summary>
        /// Função para configurar esse item
        /// </summary>
        public void Configure(string name, int level, string avatarUrl, int idGrow)
        {
            textName.text = name;
            textLevel.text = level.ToString();

            gameObject.SetActive(true);

            this.idGrow = idGrow;

            ServerManager.instance.GetAvatarSprite(avatarUrl, (pic) =>
            {
                imageAvatar.sprite = pic;
            });
        }




        /// <summary>
        /// Função para trocar a cor do background
        /// </summary>
        public void ChangeBackgroundColor(Color newColor)
        {
            background.color = newColor;
        }



        public void OnClickProfile()
        {
            Sioux.Audio.Play("Click");
            //Debug.Log("IdGrow: " + idGrow);
            ServerManager.instance.OpenNewProfilePopup(idGrow);
        }
    }

}