﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Sfs2X.Entities;

namespace Grow
{
    public class ChatPanel : MonoBehaviour
    {
        public int maxChatRollSize = 2000;

        [SerializeField]
        protected InputField inputField;
        [SerializeField]
        protected TextMeshProUGUI chatText;
        [SerializeField]
        protected Text logText;
        [SerializeField]
        protected RectTransform chatPanel;
        [SerializeField]
        protected Button backdropButton;
        [SerializeField]
        public RectTransform isYourTurnAlertPanel;
        [SerializeField]
        protected RectTransform notificationAlertRect;

        public Button btChat;
        public Image bgBtChat;
        public Text chatBtText;

        public bool isChatOpen;

        private Color disabledColor = new Color32(163, 157, 157, 255);
        private Color enabledColor = Color.white;
        private bool isChatActive = true;

        public GameObject chatRoll;
        public GameObject logRoll;

        public Button btLog;
        public Image bgBtLog;
        public Text logBtText;
        private string lastName;

        private TouchScreenKeyboard keyboard;
        public float scaleFactor { get { return GetComponentInParent<Canvas>().scaleFactor; } }


        public virtual void Start()
        {
            if (SFS.Grow)
                SFS.Grow.OnPublicMessage += UpdateChatRoll;
            notificationAlertRect.localScale = Vector3.zero;
            isYourTurnAlertPanel.localScale = Vector3.zero;
            backdropButton.gameObject.SetActive(false);
        }

        private void OnDestroy()
        {
            if (SFS.Grow)
                SFS.Grow.OnPublicMessage -= UpdateChatRoll;
        }

        public void ReferenceKeyboard()
        {
#if !UNITY_WEBGL && !UNITY_EDITOR
            if (keyboard == null)
                keyboard = TouchScreenKeyboard.Open(inputField.text, TouchScreenKeyboardType.Default);
#endif
        }

        public virtual void Update()
        {

            if ((Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Return)) && inputField.text.Length > 0)
            {
                OnClickSend();
            }

#if !UNITY_WEBGL && !UNITY_EDITOR

            if (keyboard != null && keyboard.done && inputField.text.Length > 0 && !keyboard.wasCanceled)
            {
                OnClickSend();
            }
#endif

        }

        public virtual void UpdateIsYourTurnAlert(bool isYourTurn)
        {
            if (isYourTurn)
            {
                isYourTurnAlertPanel.DOScale(Vector3.one, 0.3f);
            }
            else
            {
                isYourTurnAlertPanel.DOScale(Vector3.zero, 0.3f);
            }
        }

        public virtual void UpdateNotificationAlert(bool isUpToDate)
        {
            if (isChatOpen)
                return;

            if (isUpToDate)
            {
                notificationAlertRect.DOScale(Vector3.zero, 0.3f);
            }
            else
            {
                notificationAlertRect.DOScale(Vector3.one, 0.3f);
            }
        }

        public virtual void UpdateChatRoll(User user, Color color, string message)
        {
            if (user != null)
            {
                if (user.Name != null)
                {
                    if (user.Name != lastName)
                        chatText.text += "\n <color=" + ColorTypeConverter.ToRGBHex(color) + ">" + "<b>" + "> " + "</b>" + "</color>" + "<b>" + user.Name + "</b>" + ": " + message + "\n";
                    else
                        chatText.text += message + "\n";
                    UpdateNotificationAlert(false);
                }
                else
                {
                    chatText.text += "[ " + message + " ]\n";
                }

                while (chatText.text.Length > maxChatRollSize)
                {
                    int i = chatText.text.IndexOf('\n');
                    if (i >= 0) chatText.text = chatText.text.Substring(i + 1);
                }
                lastName = user.Name;
            }
        }

        public virtual void UpdateLogRoll(string name, Color color, string message)
        {
            if (name != null)
            {
                logText.text += name + " " + message + "\n";
                //UpdateNotificationAlert(false);
            }
            else
            {
                logText.text += "[ " + message + " ]\n";
            }

        }

        public void OnClickSend()
        {
            if (inputField.text == "")
                return;
            SFS.Grow.PublicMessageRequest(Sioux.Profanity.Replace(inputField.text));
            OnClickBtChat();
            inputField.text = "";
#if UNITY_WEBGL || UNITY_EDITOR
            inputField.ActivateInputField();
#endif
            AnalyticsManager.Instance.SendChatMessage();
        }

        public virtual void OnClickOpenPanel()
        {
            if (DOTween.IsTweening(chatPanel))
                return;
            isChatOpen = true;
            AnalyticsManager.Instance.ClickAbrirChat();
            backdropButton.gameObject.SetActive(true);
            UpdateNotificationAlert(true);
            chatPanel.DOLocalMoveX(chatPanel.sizeDelta.x, 0.4f).SetRelative();
            inputField.ActivateInputField();
        }

        public void OnClickClosePanel()
        {
            if (DOTween.IsTweening(chatPanel))
                return;
            isChatOpen = false;
            UpdateNotificationAlert(true);
            chatPanel.DOLocalMoveX(-chatPanel.sizeDelta.x, 0.4f).SetRelative().OnComplete(() => backdropButton.gameObject.SetActive(false));
        }

        public virtual void OnClickBackdrop()
        {
            OnClickClosePanel();
        }

        public void OnClickBtLog()
        {
            if (isChatActive)
            {
                isChatActive = false;

                btLog.GetComponent<Image>().color = enabledColor;
                bgBtLog.color = enabledColor;
                logBtText.color = enabledColor;

                btChat.GetComponent<Image>().color = disabledColor;
                bgBtChat.color = disabledColor;
                chatBtText.color = disabledColor;

                chatRoll.SetActive(false);
                logRoll.SetActive(true);
            }
        }

        public void OnClickBtChat()
        {
            if (!isChatActive)
            {
                isChatActive = true;

                btLog.GetComponent<Image>().color = disabledColor;
                bgBtLog.color = disabledColor;
                logBtText.color = disabledColor;

                btChat.GetComponent<Image>().color = enabledColor;
                bgBtChat.color = enabledColor;
                chatBtText.color = enabledColor;

                chatRoll.SetActive(true);
                logRoll.SetActive(false);
            }
        }
    }
}