﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Grow;
using System;
using System.Runtime.InteropServices;

namespace Grow
{

    public class MainMenuController : MonoBehaviour
    {

        #region SINGLETON
        public static MainMenuController Instance
        {
            get;
            private set;
        }
        #endregion



        #region CONSTANTS E STATICS
        public static float banTimer = 0;
        #endregion



        #region VARIABLES
        [HideInInspector]
        // A tela atual
        public GameObject currentScreen = null;

        #endregion

#if UNITY_WEBGL
        // Função chamada na hora de abrir uma Url, o corpo dela está dentro do plugin na pasta Plugins
        [DllImport("__Internal")]
        private static extern void PopupOpenerCaptureClick(string url);
#endif



        #region UNITY FUNCTIONS
        void Awake()
        {
            Instance = this;
        }


        void Start()
        {
            if (SFS.Grow == null)
            {
                SFS.CreateAndConnect();
            }
            else
            {
                // Coloca a tela MainMenu
                ChangeScreen("ScreenMainMenu");
            }
        }

        void OnDestroy()
        {
            Instance = null;
        }
        #endregion



        #region OTHER FUNCTIONS
        /// <summary>
        /// Função para mudar de tela
        /// </summary>
        public void ChangeScreen(string screenName)
        {
            // Destrói a tela anterior
            if (currentScreen != null)
            {
                Destroy(currentScreen);
            }

            // Busca o canvas principal
            GameObject screensContent = GameObject.FindGameObjectWithTag("Screens");
            if (screensContent != null)
            {
                // Cria a nova tela e já o coloca como filho do canvas principal
                currentScreen = Instantiate(Resources.Load(screenName, typeof(GameObject)), screensContent.transform) as GameObject;
            }
            else
            {
                throw new Exception("!!! ERRO !!! ManagerScreen - Objeto com a tag Screens não existe na cena!");
            }
        }


        /// <summary>
        /// Função para mostrar uma mensagem
        /// </summary>
        public void ShowPopupMessageRoomEnterError()
        {
            GameObject popup = PopupManager.instance.OpenPopup("PopupMessage");
            popup.GetComponent<PopupMessage>().Configure("SALDO INSUFICIENTE", "Você não possui energias suficientes para jogar. Obtenha através dos bônus diários ou da loja",
                () =>
                {
                    PopupManager.instance.ClosePopup();
                });
        }
        #endregion

        #region BETA (popup para assinar)
        public void OnClickVoltar()
        {
            Sioux.Audio.Play("Click");
            // Limpa o token GrowGames do jogador
            Globals.playerTokenGrowGames = "";
            Sioux.LocalData.Set("SetGrowGameToken", "");

            // TODO: Remove todos os tokens ativos pelo usuário?

            // TODO: Chama o DoIssueToken do server?

            // Limpa as variáveis do jogador
            Globals.playerId = 0;
            Globals.playerIdGrowGames = 0;
            Globals.playerNickname = "";
            Globals.playerEmail = "";
            Globals.playerCompleteName = "";
            Globals.playerAvatarId = 0;
            Globals.playerAvatarUrl = "";
            Globals.playerAvatarSprite = null;
            Globals.playerAmountInvites = 0;
            Globals.playerIsVIP = false;
            Globals.playerLevel = new Nivel();
            Globals.playerCoins = 0;
            Globals.playerPoints = 0;
            Globals.playerToken = "";
            Globals.playerTokenGrowGames = "";
            Globals.playerTotalExperience = 0;
            Globals.playerTotalVictories = 0;
            Globals.playerTotalAbandonedGames = 0;
            Globals.playerPeriodExperience = 0;
            Globals.playerPeriodVictories = 0;
            Globals.playerPeriodAbandonedGames = 0;

            // Logout do smartfox e troca a cena
            SFS.DisconnectAndGoToLogin();
            SceneManager.instance.ChangeScene("ChooseMode");
        }

        public void OnClickAssinar()
        {
            Sioux.Audio.Play("Click");
            AnalyticsManager.Instance.ClickMaisJogos(1);
#if UNITY_WEBGL && !UNITY_EDITOR
            //Application.ExternalEval("window.open(\"" + Globals.MORE_GAMES_URL + "\", \"_blank\")");
            PopupOpenerCaptureClick("https://assinatura.growgames.com.br/Assinatura");
#else
            Application.OpenURL("https://assinatura.growgames.com.br/Assinatura");
#endif
        }
        #endregion
    }

}