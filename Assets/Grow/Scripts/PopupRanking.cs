﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Grow;
using System;

namespace Grow
{

    /// <summary>
    /// Classe que representa a tela do Ranking
    /// </summary>
    public class PopupRanking : MonoBehaviour
    {
        public Text textEmpty;
        public GameObject textEmptyPlayer;

        public GameObject prefabRankingPosition;
        public GameObject prefabRankingPositionFooter;
        public Transform scrollviewContent;
        public Transform contentFooter;


        private Ranking.RankingSemanal ranking = null;

        public GameObject prefabClanRanking;

        public RectTransform scrollView;

        private List<Cla> clanRanking = null;

        public Text textSubtitle;



        // Use this for initialization
        void Start()
        {
            OnChangeGeneral(true);
        }



        public void OnClickBack()
        {
            Sioux.Audio.Play("Click");
            //MainMenuController.Instance.ChangeScreen("ScreenMainMenu");
            PopupManager.instance.ClosePopup();
        }



        public void OnChangeGeneral(bool isOn)
        {
            if (isOn)
            {
                foreach (Transform child in scrollviewContent)
                {
                    Destroy(child.gameObject);
                }

                // Muda o tamanho do scrollview
                scrollView.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 544);

                // Habilita o footer
                contentFooter.gameObject.SetActive(true);

                // Se o ranking está nulo, ou seja, o jogo ainda não pegou o ranking do server
                if (ranking == null)
                {
                    ServerManager.instance.GetRanking(Globals.playerId, (response) =>
                    {
                        if (!string.IsNullOrEmpty(response))
                        {
                            // O próprio jogador
                            Ranking playerData = null;

                            ranking = JsonConvert.DeserializeObject<Ranking.RankingSemanal>(response);

                            // Atualiza o título
                            if (ranking.LastUpdate.HasValue)
                            {
                                textSubtitle.text = "Atualizado " + ranking.LastUpdate.Value.ToString("dd/MM/yyyy") + " às " + ranking.LastUpdate.Value.ToString("hh:mm tt");
                            }
                            else
                            {
                                textSubtitle.text = "";
                            }

                            // Verifica se algum jogador pontuou
                            if (ranking.ListaRanking.Count == 0)
                            {   // Mostra um text indicando que nenhum jogador pontuou

                                textEmpty.gameObject.SetActive(true);
                                textEmpty.text = "NENHUM JOGADOR PONTUOU ATÉ O MOMENTO";
                            }
                            else
                            {
                                textEmpty.gameObject.SetActive(false);

                                // Cria a lista
                                for (int i = 0; i < ranking.ListaRanking.Count; i++)
                                {
                                    // Verifico se é o próprio jogador
                                    if (ranking.ListaRanking[i].IdUsuario == Globals.playerId)
                                    {
                                        // Armazena o jogador para verificar mais tarde
                                        playerData = ranking.ListaRanking[i];

                                        // Verifico se o jogado está na última posição
                                        if (i != 50)
                                        {
                                            // Cria o próprio jogador
                                            GameObject player = (GameObject)Instantiate(prefabRankingPosition, scrollviewContent);
                                            RankingPosition pos = player.GetComponent<RankingPosition>();
                                            pos.Configure(ranking.ListaRanking[i]);
                                            player.transform.localScale = Vector3.one;
                                            pos.ChangeBackgroundColor(Grow.Properties.RANKING_SELF_COLOR);
                                        }
                                    }
                                    else
                                    {
                                        // Cria o jogador
                                        GameObject player = (GameObject)Instantiate(prefabRankingPosition, scrollviewContent);
                                        player.GetComponent<RankingPosition>().Configure(ranking.ListaRanking[i]);
                                        player.transform.localScale = Vector3.one;
                                    }
                                }

                                // Verifica se o jogador já jogou uma partida esse mês
                                if (playerData != null)
                                {
                                    // Cria o jogador
                                    GameObject player = (GameObject)Instantiate(prefabRankingPositionFooter, contentFooter);
                                    RankingPosition script = player.GetComponent<RankingPosition>();
                                    script.Configure(playerData);
                                    textEmptyPlayer.SetActive(false);
                                }
                                else
                                {
                                    textEmptyPlayer.SetActive(true);
                                }
                            }
                        }
                    });
                }
                else
                {
                    // O próprio jogador
                    Ranking playerData = null;

                    // Verifica se algum jogador pontuou
                    if (ranking.ListaRanking.Count == 0)
                    {   // Mostra um text indicando que nenhum jogador pontuou

                        textEmpty.gameObject.SetActive(true);
                        textEmpty.text = "NENHUM JOGADOR PONTUOU ATÉ O MOMENTO";
                    }
                    else
                    {
                        textEmpty.gameObject.SetActive(false);
                        textEmpty.text = "";

                        // Cria a lista
                        for (int i = 0; i < ranking.ListaRanking.Count; i++)
                        {
                            // Verifico se é o próprio jogador
                            if (ranking.ListaRanking[i].IdUsuario == Globals.playerId)
                            {
                                // Armazena o jogador para verificar mais tarde
                                playerData = ranking.ListaRanking[i];

                                // Verifico se o jogado está na última posição
                                if (i != 50)
                                {
                                    // Cria o próprio jogador
                                    GameObject player = (GameObject)Instantiate(prefabRankingPosition, scrollviewContent);
                                    RankingPosition pos = player.GetComponent<RankingPosition>();
                                    pos.Configure(ranking.ListaRanking[i]);
                                    player.transform.localScale = Vector3.one;
                                    pos.ChangeBackgroundColor(Grow.Properties.RANKING_SELF_COLOR);
                                }
                            }
                            else
                            {
                                // Cria o jogador
                                GameObject player = (GameObject)Instantiate(prefabRankingPosition, scrollviewContent);
                                player.GetComponent<RankingPosition>().Configure(ranking.ListaRanking[i]);
                                player.transform.localScale = Vector3.one;
                            }
                        }

                        // Verifica se o jogador já jogou uma partida esse mês
                        if (playerData != null)
                        {
                            // Cria o jogador
                            GameObject player = (GameObject)Instantiate(prefabRankingPositionFooter, contentFooter);
                            RankingPosition script = player.GetComponent<RankingPosition>();
                            script.Configure(playerData);
                            textEmptyPlayer.SetActive(false);
                        }
                        else
                        {
                            textEmptyPlayer.SetActive(true);
                        }
                    }
                }
            }
        }



        public void OnChangeClan(bool isOn)
        {
            if (isOn)
            {
                foreach (Transform child in scrollviewContent)
                {
                    Destroy(child.gameObject);
                }

                // Muda o tamanho do scrollview
                scrollView.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 700);

                // Desabilita o footer
                contentFooter.gameObject.SetActive(false);

                // Desabilita o texto do jogador
                textEmptyPlayer.SetActive(false);

                // Se ainda não pegou os dados no server
                if (clanRanking == null)
                {
                    ServerManager.instance.GetClanRanking(-1,
                        (response) =>
                        {
                            if (!string.IsNullOrEmpty(response))
                            {
                                ClanResponse request = JsonConvert.DeserializeObject<ClanResponse>(response);

                                if (request.Code == 0)
                                {
                                    clanRanking = request.Clans;

                                    if (request.Clans.Count == 0)
                                    {
                                        textEmpty.gameObject.SetActive(true);
                                        textEmpty.text = "NENHUM CLÃ PONTUOU ATÉ O MOMENTO";
                                    }
                                    else
                                    {
                                        textEmpty.gameObject.SetActive(false);

                                        // Cria a lista
                                        for (int i = 0; i < request.Clans.Count; i++)
                                        {
                                            // Cria o clã
                                            GameObject clan = (GameObject)Instantiate(prefabClanRanking, scrollviewContent);
                                            clan.GetComponent<ItemClan>().Configure(request.Clans[i]);
                                            clan.transform.localScale = Vector3.one;
                                        }
                                    }
                                }
                                else
                                {
                                    ScreenClan.ShowErrorCode(request.Code);
                                }
                            }
                            else
                            {
                                ScreenClan.ShowErrorCode(1);
                            }
                        });
                }
                else
                {
                    if (clanRanking.Count == 0)
                    {
                        textEmpty.gameObject.SetActive(true);
                        textEmpty.text = "NENHUM CLÃ PONTUOU ATÉ O MOMENTO";
                    }
                    else
                    {
                        textEmpty.gameObject.SetActive(false);

                        // Cria a lista
                        for (int i = 0; i < clanRanking.Count; i++)
                        {
                            // Cria o clã
                            GameObject clan = (GameObject)Instantiate(prefabClanRanking, scrollviewContent);
                            clan.GetComponent<ItemClan>().Configure(clanRanking[i]);
                            clan.transform.localScale = Vector3.one;
                        }
                    }
                }
            }
        }
    }
}