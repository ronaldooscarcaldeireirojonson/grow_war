﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Grow
{
    public class ItemAchievement : MonoBehaviour
    {
        public const float WIDTH_PROGRESS_BAR_FULL = 620;


        public Image imageIcon;

        public Text textName;
        public Text textProgress;
        public Image imageProgressMask;
        public Image starCopper;
        public Image starSilver;
        public Image starGold;



        /// <summary>
        /// Função para configurar o item
        /// </summary>
        public void Configure(NewProfileModel.AchievementModel achieve)
        {
            //Debug.LogWarning("<color=yellow><b>!!! TODO !!!</b></color> <b>ItemAchievement</b> - Configure: colocar a imagem do achievement");

            textName.text = achieve.name;
            textProgress.text = achieve.progressCurrent + " / " + achieve.progressTotal + " " + achieve.progressText;
            imageProgressMask.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, ((float)achieve.progressCurrent / (float)achieve.progressTotal) * WIDTH_PROGRESS_BAR_FULL);

            if (achieve.hasCopperMedal)
            {
                starCopper.color = Color.white;
            }

            if (achieve.hasSilverMedal)
            {
                starSilver.color = Color.white;
            }

            if (achieve.hasGoldMedal)
            {
                starGold.color = Color.white;
            }
        }

    }
}
