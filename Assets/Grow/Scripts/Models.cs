﻿using System;
using System.Collections.Generic;


namespace Grow
{

    public enum Plataforma
    {
        Null = 0,
        WEB = 1,
        IOS,
        Android,
        WindowsPhone,
        Desconhecida
    }





    public class StoreItemRequest
    {
        public int platform { get; set; }
        public int idCategoria { get; set; }
    }

    public class StoreItem
    {
        public int id { get; set; }
        public int idCategoria { get; set; }
        public string nome { get; set; }
        public string codigo { get; set; }
        public string valorMoedaDe { get; set; }
        public string valorMoeda { get; set; }
        public int valorEnergia { get; set; }
        public bool isPromocao { get; set; }
        public bool isAtivo { get; set; }
        public bool isValido { get; set; }
        public string label { get; set; }

        public StoreItem()
        {
            this.isValido = false;
        }
    }

    public enum EnStoreResult
    {
        OK = 1,
        CompraInvalida = 2,
        Erro = 3,
        ProdutoNaoEncontrado = 4,
        CompraJaCreditada = 5,
        PlataformaInvalida = 6
    }

    public class VerifyPurchaseReturn
    {
        public EnStoreResult code { get; set; }
        public string message { get; set; }
    }

    public class StoreVerifyResult
    {
        public EnStoreResult resultCode { get; set; }
        public string data { get; set; }
    }


    public class VerifyPurchaseApple
    {
        public string TransactionID { get; set; }
        public string Receipt { get; set; }
        public string ProductId { get; set; }
        public int IdUsuario { get; set; }
    };


    public class VerifyPurchaseGoogle
    {
        public string JsonData { get; set; }
    };


    public class GetFriendDetailsParams
    {
        public int id { get; set; }
    }


    public class StoreItemsParams
    {
        public int plataforma { get; set; }
        public int categoria { get; set; }
    }


    public class Usuario
    {
        public int IdUsuario { get; set; }
        public string Email { get; set; }
        public string NomeCompleto { get; set; }
        public string Apelido { get; set; }
        public string Sexo { get; set; }
        public string Senha { get; set; }
        public DateTime? DataNascimento { get; set; }
        public string Token { get; set; }
        public int IdJogo { get; set; }

        // Id avatar
        public int IdAvatar { get; set; }

        // Url da foto avatar/facebook
        public string Avatar { get; set; }

        // Mapping the same user across multiple apps
        public string TokenForBusiness { get; set; }

        // Facebook
        public string TokenFB { get; set; }

        // Click Jogos
        public string TokenCJ { get; set; }

        // Token de acesso facebook
        public string FBAccessToken { get; set; }

        // Nivel do bonus atual
        public int NuBonusDiario { get; set; }

        // Data do ultimo resgate do bonus
        public DateTime? DtBonusDiario { get; set; }

        // Plataforma de request do login
        public Plataforma? plataforma { get; set; }

        // Versão da app que esta logando
        public string versao { get; set; }

        // Retorna true se jogador resgatou bonus diario
        public bool Bonus { get; set; }




        public class PostGameData
        {
            public Ranking Ranking { get; set; }
            public UserData UserData { get; set; }
        }



        public class UsuarioGrowLogin
        {
            public bool Sucesso { get; set; }
            public string Mensagem { get; set; }
            public Usuario Jogador { get; set; }
            public Config Config { get; set; }
            public bool AtivacaoPendente { get; set; }
            public NovidadeModel Novidade { get; set; }
            public bool ContaFacebook { get; set; }
            public UserData UserData { get; set; }
            public string TokenGrowGames { get; set; }
            public Torneio Torneio { get; set; }
        }



        public class UsuarioResetPassword
        {
            public bool Sucesso { get; set; }
            public string Mensagem { get; set; }
            public string Email { get; set; }
        }



        public class UsuarioChangeAvatar
        {
            public bool Sucesso { get; set; }
            public string Mensagem { get; set; }
        }



        public class UsuarioIssueTokenRequest
        {
            public string access_token { get; set; }
            public int plataforma { get; set; }
            public string versao { get; set; }
        }


        public class UsuarioToken
        {
            private string _versao = "";
            private Plataforma _plataforma = Plataforma.Null;

            public string AccessToken { get; set; }
            public string Versao
            {
                get
                {
                    if (String.IsNullOrEmpty(this._versao))
                        return "1.2.0";
                    else
                        return this._versao;
                }
                set
                {
                    this._versao = value;
                }
            }


            public Plataforma Plataforma
            {
                get
                {
                    if (_plataforma == Plataforma.Null)
                        return Plataforma.WEB;
                    else
                        return _plataforma;
                }

                set
                {
                    _plataforma = value;
                }
            }
        }


        public class UserData
        {
            public int Id { get; set; }
            public int IdGrowGames { get; set; }
            public string Apelido { get; set; }
            public string Email { get; set; }
            public string NomeCompleto { get; set; }
            public int IdAvatar { get; set; }
            public int IdPatente { get; set; }
            public string Avatar { get; set; }
            public float Pontos { get; set; }
            public int Vitorias { get; set; }
            public int Convites { get; set; }
            public int PartidasAbandonadas { get; set; }
            public bool isVIP { get; set; }
            public Nivel Nivel { get; set; }
            public Extrato Extrato { get; set; }
            public Plataforma Plataforma { get; set; }
            public string Versao { get; set; }
            public UserGameData UserGameData { get; set; }
            public UsuarioCla Cla { get; set; }
            public List<Achievement> Conquistas { get; set; }
        }


        public class UserPerfil
        {
            public int IdUsuario { get; set; }
            public string Apelido { get; set; }
            public int IdAvatar { get; set; }
            public string Avatar { get; set; }
            public Nivel Nivel { get; set; }
            public int NivelMax { get; set; }
            public int NuVitorias { get; set; }
            public int NuDerrotas { get; set; }
            public List<Jogo.JogoHistorico> Partidas { get; set; }
        }


        public class UserDetails
        {
            public UserData User;
            public UsuarioGrowPerfil Perfil;
            public UserGameData UserGameData;
            public UsuarioPatente Patent;
            public UsuarioCla Cla;
            public long RankingPosition;
        }


        public class UserChangePerfilData
        {
            public DateTime DtNascimento { get; set; }
            public UsuarioGrowPerfil.EnEstadoCivil EstadoCivil { get; set; }
            public string Time { get; set; }
            public int IdCidade { get; set; }
            public string CidadeOutra { get; set; }
            public int IdEstado { get; set; }
            public int IdPais { get; set; }
        }
    }


    public class UsuarioGrowPerfil
    {
        public class UsuarioGrowPais
        {
            public int IdPais { get; set; }
            public string Nome { get; set; }

            public UsuarioGrowPais()
            {

            }

            public UsuarioGrowPais(int id, string nome)
            {
                this.IdPais = id;
                this.Nome = nome;
            }
        }

        public class UsuarioGrowEstado
        {
            public int IdEstado { get; set; }
            public string Nome { get; set; }

            public UsuarioGrowEstado()
            {

            }

            public UsuarioGrowEstado(int id, string nome)
            {
                this.IdEstado = id;
                this.Nome = nome;
            }
        }

        public class UsuarioGrowCidade
        {
            public int IdCidade { get; set; }
            public string Nome { get; set; }

            public UsuarioGrowCidade()
            {
            }

            public UsuarioGrowCidade(int id, string nome)
            {
                this.IdCidade = id;
                this.Nome = nome;
            }
        }

        public class UsuarioGrowEstadoCivil
        {
            public int IdEstadoCivil { get; set; }
            public string Nome { get; set; }
        }

        public enum EnEstadoCivil
        {
            NaoDefinido = 0,
            Solteiro = 1,
            Casado = 2,
            Divorciado = 3,
            Viúvo = 4,
            Separado = 5
        }

        public EnEstadoCivil? EstadoCivil { get; set; }
        public string Time { get; set; }
        public DateTime? DataNascimento { get; set; }
        public UsuarioGrowPais Pais { get; set; }
        public UsuarioGrowEstado Estado { get; set; }
        public UsuarioGrowCidade Cidade { get; set; }
        public string CidadeOutra { get; set; }
        public DateTime DataCadastro { get; set; }
    }




    public class Ranking
    {
        public int IdUsuario { get; set; }
        public long Posicao { get; set; }
        public int NuIndex { get; set; }
        public string Apelido { get; set; }
        public string Foto { get; set; }
        public decimal Vitorias { get; set; }
        public decimal Jogos { get; set; }
        public decimal Ratio { get; set; }
        public int Nivel { get; set; }
        public int Pontos { get; set; }
        public int PontosGeral { get; set; }
        public int IdGrowGames { get; set; }
        public int IdPatente { get; set; }


        public enum EnTipoRanking
        {
            MENSAL = 1,
            SEMANAL = 2,
            DIARIO = 3,
            TORNEIO = 4
        }


        /// <summary>
        /// Ranking geral
        /// </summary>
        public class RankingGeral
        {
            public Ranking MeuRanking { get; set; }
            public List<Ranking> ListaRanking { get; set; }
        }



        /// <summary>
        /// Ranking Semanal
        /// </summary>
        public class RankingSemanal
        {
            public List<Ranking> ListaRanking { get; set; }
            public List<Ranking> ListaVencedores { get; set; }
            public EnTipoRanking Tipo { get; set; }
            public long TempoFim { get; set; }
            public DateTime? LastUpdate { get; set; }
        }



        /// <summary>
        /// Objeto usado para comunicacao entre server - client
        /// </summary>
        public class RankingNovo
        {
            // Id do usuario
            public int IdUsuario { get; set; }

            // Quantidade de itens por pagina
            public int Quantidade { get; set; }

            // Numero da pagina
            public int Pagina { get; set; }

            // Versao do jogo
            public string Versao { get; set; }
        }

    }


    public class Config
    {
        // Bonus diário
        public decimal NuBonus1 { get; set; }
        public decimal NuBonus2 { get; set; }
        public decimal NuBonus3 { get; set; }
        public decimal NuBonus4 { get; set; }
        public decimal NuBonus5 { get; set; }

        // Preço da partida
        public int valorJogoRapido { get; set; }

        // Preço da partida com promoção
        public bool promocaoJogoRapido { get; set; }

        // Pontos ganhos por vitória
        public decimal NuPontosVitoria { get; set; }

        // Lista de avatares
        public List<GrowAvatar> ListaAvatar { get; set; }

        // A quantidade de experiência para cada nível
        public List<Nivel> Niveis { get; set; }

        // A quantidade de moedas para encher a barra
        public int limiteMoedas { get; set; }

        // URLs
        public string perfilURL { get; set; }
        public string photonURL { get; set; }
        public string photonFriendsURL { get; set; }
        public string avatarURL { get; set; }
        public string apiBaseURL { get; set; }
        public string gameIconsURL { get; set; }
        public string avatarCustomURL { get; set; }
        public string clanURL { get; set; }
        public string avatarCustomPath { get; set; }

        // ?
        public decimal NuMoedaJogo { get; set; }
        public bool betaModeOn { get; set; }

        public List<PatenteItem> Patentes { get; set; }

        public class PatenteItem
        {
            public int Id { get; set; }
            public string Label { get; set; }

        }
    }


    public class NovidadeModel
    {
        public int IdNovidade { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public string Link { get; set; }
        public string Imagem { get; set; }
        public bool Ativo { get; set; }
        public bool MostrarSempre { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime Fim { get; set; }
        public string TextoBotao { get; set; }
        public bool Ranking { get; set; }
        public bool Todos { get; set; }
    }


    public class Torneio
    {
        public int IdTorneio { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }
        public DateTime DivulgacaoInicio { get; set; }
        public DateTime DivulgacaoFim { get; set; }
        public DateTime HoraLogin { get; set; }

        public long DataInicioSeconds
        {
            get
            {
                var seconds = (DataInicio - DateTime.UtcNow).TotalSeconds;

                if (seconds < 0)
                    return 0;

                return (long)seconds;
            }
        }
        public long DataFimSeconds
        {
            get
            {
                var seconds = (DataFim - DateTime.UtcNow).TotalSeconds;

                if (seconds < 0)
                    return 0;

                return (long)seconds;
            }
        }
        public long DivulgacaoInicioSeconds
        {
            get
            {
                var seconds = (DivulgacaoInicio - DateTime.UtcNow).TotalSeconds;

                if (seconds < 0)
                    return 0;

                return (long)seconds;
            }
        }
        public long DivulgacaoFimSeconds
        {
            get
            {
                var seconds = (DivulgacaoFim - DateTime.UtcNow).TotalSeconds;

                if (seconds < 0)
                    return 0;

                return (long)seconds;
            }
        }

        public string ImagePopup1 { get; set; }
        public string ImagePopup2 { get; set; }
        public int QuantidadePremiados { get; set; }
        public decimal? Price { get; set; }
        public TorneioFase Fase { get; set; }
        public string UrlRegulamento { get; set; }
        public bool IsVipOnly { get; set; }

        public enum TorneioFase
        {
            SemTorneioEmAndamento = 0,
            Pre = 1,
            EmAndamento = 2,
            Encerrado = 3
        }

        public override string ToString()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }

        public static Torneio GetActive(int jogo)
        {
            return GetActive(DateTime.UtcNow, jogo);
        }

        public static Torneio GetActive(DateTime now, int jogoId)
        {
            return null;
        }

        public static Torneio GetNotExpiredForRanking(int jogoId)
        {
            return null;
        }
    }


    public class Nivel
    {
        // Id nivel
        public int IdNivel { get; set; }

        // Nivel atual
        public int NuXP { get; set; }

        // O label que o jogador irá receber
        public string label { get; set; }
    }


    public class Extrato
    {
        public enum EnOrigemValor
        {
            BonusDiario = 1,
            Presente = 2,
            Compra = 3,
            JogoRapido = 4
        }

        // Id usuario
        public int IdUsuario { get; set; }

        // Saldo de moedas do usuario
        public decimal NuSaldoMoeda { get; set; }

        // Saldo de dinhieros do usuario
        //public decimal NuSaldoDinheiro { get; set; }

    }


    public class UserGameData
    {
        public int ExperienciaTotal { get; set; }
        public int VitoriasTotal { get; set; }
        public int PartidasAbandonadasTotal { get; set; }
        public int ExperienciaPeriodo { get; set; }
        public int VitoriasPeriodo { get; set; }
        public int PartidasAbandonadasPeriodo { get; set; }
        public List<UserRatingItem> Rating { get; set; }
    }


    public class UserRatingItem
    {
        public string Rating { get; set; }
        public int Total { get; set; }
        public decimal Percentual { get; set; }
    }


    public class Jogo
    {
        public long IdJogo { get; set; }
        public string IdRoom { get; set; }
        public int IdJogador1 { get; set; }
        public int IdJogador2 { get; set; }
        public decimal NuPontos1 { get; set; }
        public decimal NuPontos2 { get; set; }
        public int IdJogadorVencedor { get; set; }
        public int IdJogadorAbandono { get; set; }
        public string TxGameState { get; set; }
        public DateTime DtCriacao { get; set; }
        public DateTime DAlteracao { get; set; }


        public class JogoHistorico
        {
            public long IdJogo { get; set; }
            public int IdUsuarioOponente { get; set; }
            public string ApelidoOponente { get; set; }
            public string FotoOponente { get; set; }
            public int NivelOponente { get; set; }
            public DateTime DataJogo { get; set; }
        }
    }


    public class GrowAvatar
    {
        // Id avatar
        public int IdAvatar { get; set; }

        // AVATAR/FOTO
        public string TxAvatar { get; set; }

        // Somente para vip
        public bool onlyVip { get; set; }
    }


    public class NewProfileModel
    {
        public class HistoricPlayerModel
        {
            public int id { get; set; }
            public int idGrow { get; set; }
            public string name { get; set; }
            public string avatar { get; set; }
            public bool isWinner { get; set; }
            public int colorR { get; set; }
            public int colorG { get; set; }
            public int colorB { get; set; }
        }

        public class HistoricMatchModel
        {
            public int id { get; set; }
            public bool isWinner { get; set; }
            public DateTime date { get; set; }
            public List<HistoricPlayerModel> players { get; set; }
        }


        public class AchievementModel
        {
            public int id { get; set; }
            public string name { get; set; }
            public int progressCurrent { get; set; }
            public int progressTotal { get; set; }
            public string progressText { get; set; }
            public bool hasCopperMedal { get; set; }
            public bool hasSilverMedal { get; set; }
            public bool hasGoldMedal { get; set; }
        }


        // Específicos do GrowPerfil (não sei se é melhor deixarmos aqui)
        public int personHits { get; set; }
        public int yearHits { get; set; }
        public int placeHits { get; set; }
        public int thingHits { get; set; }

        // Main
        public int id { get; set; }
        public int idGrowGames { get; set; }
        public string name { get; set; }
        public bool isVip { get; set; }
        public string avatar { get; set; }
        public int idAvatar { get; set; }
        public int level { get; set; }
        public string rank { get; set; }
        public int likeAmount { get; set; }
        public string clan { get; set; }


        // My Profile
        public DateTime playerSince { get; set; }
        public int age { get; set; }
        public string city { get; set; }
        public string maritalStatus { get; set; }
        public string team { get; set; }

        // Info
        public int victories30 { get; set; }
        public int losts30 { get; set; }
        public int abandons30 { get; set; }
        public int points30 { get; set; }
        public TimeSpan timePlaying { get; set; }
        public int victories { get; set; }
        public int losts { get; set; }
        public int abandons { get; set; }
        public int points { get; set; }
        public int goldAchieve { get; set; }
        public int silverAchieve { get; set; }
        public int copperAchieve { get; set; }

        // Historic
        public List<HistoricMatchModel> matches { get; set; }

        // Achievements
        public List<AchievementModel> achieves { get; set; }


    }


    [Serializable]
    public class StateCityData
    {
        public List<State> estados;
    }

    [Serializable]
    public class State
    {
        public string uf;
        public string nome;
        public List<City> cidades;
    }

    [Serializable]
    public class City
    {
        public string id;
        public string nome;
    }



    public class UsuarioEstatisticas
    {
        public enum EnTipoEstatisticaItem
        {
            TempoDeJogo = 1,
            NumeroDePartidas = 2,
            QuantidadeAcertosCategoria = 3,
            MediaDicasAcerto = 4,
            MediaRatingOutrosJogadores = 5,
            VitoriasConsecutivas = 6,
            TotalVitorias = 7,
            TotalDerrotas = 8,
            TropasGanhas = 9,
            TropasPerdidas = 10,
            ObjetivosMaisAtingidos = 11,
            ContinentesMaisConquistados = 12,
            TotalAtributosOutrosJogadores = 13
        }

        public class Totalizadores
        {
            public int Vitorias { get; set; }
            public int Derrotas { get; set; }
            public int Abandonos { get; set; }
            public int Pontos { get; set; }
        }

        public class EstatisticaItem
        {
            public EnTipoEstatisticaItem Tipo { get; set; }
            public string Titulo { get; set; }
            public string Dados { get; set; }
            public string Valor { get; set; }

            //public EstatisticaItem(EstatisticasResult data)
            //{
            //    this.Tipo = (EnTipoEstatisticaItem)data.id_estatistica;
            //    this.Valor = (data.nu_valor.HasValue ? data.nu_valor.Value : 0).ToString();
            //}
        }

        public long TempoJogo { get; set; }
        public Totalizadores Totalizadores30Dias { get; set; }
        public Totalizadores TotalizadoresGeral { get; set; }
        public List<EstatisticaItem> Itens { get; set; }


        public class EstatisticasResult
        {
            public int id_estatistica { get; set; }
            public string tx_titulo { get; set; }
            public Nullable<long> nu_valor { get; set; }
        }
    }



    public class Partida
    {
        public long IdJogo { get; set; }
        public string IdRoom { get; set; }
        public bool Vencedor { get; set; }
        public int Posicao { get; set; }
        public DateTime Data { get; set; }
        public List<Jogador> Jogadores { get; set; }

        public class Jogador
        {
            private int _idAvatar;
            private string _avatar;

            public int IdAvatar
            {
                get
                {
                    return this._idAvatar;
                }
            }

            public string Avatar
            {
                get
                {
                    return _avatar;
                }
            }

            public int Id { get; set; }
            public int IdGrow { get; set; }
            public string Apelido { get; set; }
            public bool Vencedor { get; set; }
            public int Posicao { get; set; }
            public int Cor { get; set; }
            public bool hasAbandoned { get; set; }
            public bool hasEliminated { get; set; }
            public bool hasDeservedPoints { get; set; }

            public Jogador(int idAvatar, string avatar, string sFacebook)
            {
                this._idAvatar = idAvatar;

                if (idAvatar == 1)
                {
                    if (avatar.IndexOf("http") > -1)
                    {
                        this._avatar = avatar;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(sFacebook))
                        {
                            this._avatar = String.Format("http://graph.facebook.com/{0}/picture?width=128&height=128", sFacebook);
                        }
                        else
                        {
                            this._idAvatar = 100;
                            this._avatar = "Photo00.jpg";
                        }
                    }
                }
                else if (idAvatar == 2)
                {
                    if (avatar.IndexOf("http") > -1)
                    {
                        this._avatar = avatar;
                    }
                    else
                    {
                        this._avatar = "http://img2.clickjogos.com.br/avatars/default/alienigena_big.png";
                    }
                }
                else if (idAvatar == 3)
                {
                    this._avatar = avatar;
                }
                else if (idAvatar >= 100)
                {
                    this._avatar = avatar;
                }
                else
                {
                    this._avatar = avatar;
                }

                //if (idAvatar == 1 && avatar.IndexOf("http") > -1)
                //    this._avatar = avatar;
                //else if (idAvatar == 1 && avatar.IndexOf("http") == -1)
                //{
                //    if (!string.IsNullOrEmpty(sFacebook))
                //    {
                //        this._avatar = String.Format("http://graph.facebook.com/{0}/picture?width=128&height=128", sFacebook);
                //    }
                //    else
                //    {
                //        this._avatar = "http://grow.blob.core.windows.net/avatar/Photo00.jpg";
                //    }
                //}
                //else if (idAvatar == 2 && avatar.IndexOf("http") > -1)
                //    this._avatar = avatar;
                //else if (idAvatar == 2 && avatar.IndexOf("http") == -1)
                //    this._avatar = "http://img2.clickjogos.com.br/avatars/default/alienigena_big.png";
                //else if (idAvatar >= 100)
                //{
                //    if (avatar.IndexOf("grow.blob.core.windows.net") == -1)
                //    {
                //        this._avatar = String.Format("http://grow.blob.core.windows.net/avatar/{0}", avatar);
                //    }
                //    else
                //    {
                //        this._avatar = avatar;
                //    }
                //}
                //else
                //    this._avatar = "http://grow.blob.core.windows.net/avatar/Photo00.jpg";
            }
        }
    }


    public class RatingModel
    {
        public string idRoom { get; set; }
        public int idUsuarioAutor { get; set; }
        public int idUsuario { get; set; }
        public int idRating { get; set; }
    }



    public class UsuarioPatente
    {
        public int Id { get; set; }
        public int IdProxima { get; set; }
        public int Vitorias { get; set; }
        public int Derrotas { get; set; }
        public int VitoriasSobe { get; set; }
        public int DerrotasDesce { get; set; }
    }


    public class Cla
    {
        public long Id { get; set; }
        public string Nome { get; set; }
        public string Tag { get; set; }
        public int IdEscudo { get; set; }
        public int MembersAmount { get; set; }
        public long Pontos { get; set; }
    }


    public class ClaMemberRequest
    {
        public long Id { get; set; }
    }


    public class ClaMemberApprovalRequest
    {
        public long Id { get; set; }
        public int IdParticipante { get; set; }
        public bool IsApproved { get; set; }
    }

    public class ClanRequest
    {
        public long IdClan { get; set; }
        public int IdModerator { get; set; }
        public int IdMember { get; set; }
        public bool IsModerator { get; set; }
        public int IdEscudo { get; set; }
        public string Nickname { get; set; }
    }


    public class ClanResponse
    {
        public int Code { get; set; }
        public List<Cla> Clans { get; set; }
        public List<ClanMember> Members { get; set; }
    }



    public class ClanMember
    {
        public long? Pos { get; set; }
        public int IdUsuario { get; set; }
        public long IdClan { get; set; }
        public int IdStatus { get; set; }
        public string Avatar { get; set; }
        public string Nickname { get; set; }
        public bool isModerator { get; set; }
        public bool isVip { get; set; }
        public int Points { get; set; }
    }



    public class UsuarioCla
    {
        public long Id { get; set; }
        public string Nome { get; set; }
        public string Tag { get; set; }
        public int IdEscudo { get; set; }
        public int IdStatus { get; set; }
        public int MembersAmount { get; set; }
        public string NomeMembroPendente { get; set; }
        public int IdParticipante { get; set; }
    }
}