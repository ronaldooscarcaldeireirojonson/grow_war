﻿using Grow;
using UnityEngine;
using UnityEngine.UI;

public class LojaItemInfo
{
    public string desc;
    public string price;
    public string productId;
    public bool isEnergy;
}

public class LojaItem : MonoBehaviour
{
    public Text itemDesc;
    public Text itemPrice;
    //public Image energyIcon;
    public string productId;

    public void Configure(LojaItemInfo info)
    {
        itemDesc.text = info.desc;
        itemPrice.text = info.price;
        productId = info.productId;
        //energyIcon.gameObject.SetActive(info.isEnergy);
    }

    public void OnClickBuy()
    {
        Sioux.IAPManager.BuyProductID(productId, product =>
        {
            if (product == null)
            {
                Debug.Log("[LojaItem.OnClickBuy] TODO: popup dizendo que a compra deu erro!");
            }
            else
            {
                /*
                #if !UNITY_ANDROID
                                // product.transactionID, product.receipt, product.definition.id, Globals.playerIdGrowGames
                                ServerManager.instance.VerifyPurchaseGooglePlay("", result =>
                                {
                                    PopupManager.instance.ClosePopup();
                                });
                #else
                #endif
                */
                ServerManager.instance.VerifyPurchaseAppleStore(product.transactionID, product.receipt, product.definition.id, Globals.playerId, result =>
                {
                    Debug.Log(result);
                    var purchaseReturn = Newtonsoft.Json.JsonConvert.DeserializeObject<VerifyPurchaseReturn>(result);
                    if (purchaseReturn.code == EnStoreResult.OK)
                    {
                        Debug.Log("[LojaItem.OnClickBuy] deu certo");
                        Globals.playerCoins = Mathf.Max((int)Globals.playerCoins, Globals.gameMaxCoins);
                        PopupManager.instance.ClosePopup();
                    }
                    else
                    {
                        Debug.Log("[LojaItem.OnClickBuy] não deu certo");
                        Debug.Log(purchaseReturn.code);
                        Debug.Log(purchaseReturn.message);
                    }
                });
            }
        });
    }
}
