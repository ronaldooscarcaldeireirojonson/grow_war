﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;


public enum Platform
{
    Web = 1,
    IOS,
    Android,
    WindowsPhone,
    Unknown,
}


public class Version
{
    public bool isDev { get; set; }

    public int platform { get; set; }
    public string versionNumber { get; set; }
    public string storeUrl { get; set; }

    public bool isBlocked { get; set; }
    public string updateMessage { get; set; }

    // WebApi
    public string webapiUrl { get; set; }

    // Smartfox
    public string serverIp { get; set; }
    public int serverPort { get; set; }
    public int serverPortWebGl { get; set; }
    public string zoneName { get; set; }
    public string zoneNameFriends { get; set; }

    public string releaseNotes { get; set; }
}


/// <summary>
/// Classe para gerenciar a versão do projeto e assim pegar as URL corretas
/// </summary>
public static class VersionController
{
    // As configurações da versão
    public static Version versionConfig = null;

    // Flag que indica se o jogo é DEV ou PROD
    public static bool IS_DEV
    {
        get { return PlayerPrefs.HasKey("IS_DEV") ? PlayerPrefs.GetInt("IS_DEV") == 1 : IS_DEV = false; }
        set { PlayerPrefs.SetInt("IS_DEV", value ? 1 : -1); }
    }
    //public static bool IS_DEV = false;

    // TODO: Pegar o número da versão do Player Settings (precisa de código nativo de cada plataforma)
    public const string VERSION = "4.1.0";

    // A url da webapi de PROD para pegar as URLs corretas da WebAPI e do Smartfox
    public static string URL_START_WEB_API = "";

    // Timeout para a resposta
    private const int TIMEOUT_SECONDS = 60;



    /// <summary>
    /// Função para fazer download das configurações da WebAPI e do Smartfox
    /// </summary>
    public static void DownloadConnectionStrings(MonoBehaviour monoBehaviour, Action<bool> callback)
    {
        monoBehaviour.StartCoroutine(DownloadConnectionStrings(callback));
    }


    /// <summary>
    /// Função para fazer download das configurações da WebAPI e do Smartfox.
    /// Para ser usado com yield return StartCoroutine... e parar a execução
    /// </summary>
    public static IEnumerator DownloadConnectionStrings(Action<bool> callback)
    {
        Version version = new Version()
        {
            isDev = IS_DEV,
#if UNITY_IOS
            platform = (int)Platform.IOS,
#elif UNITY_ANDROID
            platform = (int)Platform.Android,
#else 
            platform = (int)Platform.Web,
#endif
            versionNumber = VERSION
        };

        // Converte o objeto para um byte array
        byte[] bodyRaw = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(version));

        // Cria o WebRequest
        //http://warv2.growgames.com.br/api/
        UnityWebRequest request = new UnityWebRequest(URL_START_WEB_API + "version/", "POST");
        //request = new UnityWebRequest("http://localhost:8197/api/version/", "POST");
        request.downloadHandler = new DownloadHandlerBuffer();
        request.uploadHandler = new UploadHandlerRaw(bodyRaw);
        request.SetRequestHeader("Content-Type", "application/json");
        request.timeout = TIMEOUT_SECONDS;

        // Envia a requisição
        yield return request.SendWebRequest();

        //Debug.Log("request.downloadHandler.text: " + request.downloadHandler.text);
        if (string.IsNullOrEmpty(request.error))
        {
            // Se estiver voltando uma página html, deu algum erro no server
            if (request.downloadHandler.text.IndexOf("<!DOCTYPE html>") == -1)
            {
                //Debug.Log("DownloadConnectionStrings: " + request.downloadHandler.text);

                versionConfig = JsonConvert.DeserializeObject<Version>(request.downloadHandler.text);

                // TEST
                //versionConfig.webapiUrl = "http://localhost:8197/api/";

                callback(true);
            }
            else
            {
                Debug.LogWarning("<color=red><b>!!! ERRO !!!</b></color> <b>VersionController</b> - Não foi possível pegar as configurações do jogo");
                callback(false);
            }
        }
        else
        {
            Debug.LogWarning("<color=red><b>!!! ERRO !!!</b></color> <b>VersionController</b> - " + request.error);
            callback(false);
        }
    }


}
