﻿using Newtonsoft.Json;
using Sfs2X.Entities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Grow
{

    public class FriendSnippetPending : MonoBehaviour
    {
        public Image imageAvatar;
        public Text textName;

        private int buddyIdGrow;
        private string buddyAvatar;
        private string buddyName;



        /// <summary>
        /// Função para configurar esse snippet
        /// </summary>
        public void Configure(Buddy friend)
        {
            List<Sfs2X.Entities.Variables.BuddyVariable> offlineVars = friend.GetOfflineVariables();

            for (int i = 0; i < offlineVars.Count; i++)
            {
                if (offlineVars[i].Name == "$avatar")
                {
                    buddyAvatar = offlineVars[i].GetStringValue();
                }
                else if (offlineVars[i].Name == "$idGrow")
                {
                    buddyIdGrow = offlineVars[i].GetIntValue();
                }
            }

            Configure(buddyIdGrow, friend.Name, buddyAvatar);
        }


        public void Configure(int buddyIdGrow, string buddyName, string buddyAvatar)
        {
            this.buddyIdGrow = buddyIdGrow;
            this.buddyName = buddyName;
            this.buddyAvatar = buddyAvatar;

            textName.text = buddyName;
            ServerManager.instance.GetAvatarSprite(buddyAvatar, (pic) =>
            {
                imageAvatar.sprite = pic;
            });
        }



        public void OnClickProfile()
        {
            ServerManager.instance.OpenNewProfilePopup(buddyIdGrow);
        }


        public void OnClickAccept()
        {
            SFS.Friends.AddRequest(textName.text);
        }



        public void OnClickReject()
        {
            SFS.Friends.RemoveRequest(textName.text);
        }

    }
}