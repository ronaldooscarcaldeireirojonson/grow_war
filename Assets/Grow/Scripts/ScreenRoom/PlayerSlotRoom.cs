﻿using Sfs2X.Entities;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace Grow
{
    public class PlayerSlotRoom : MonoBehaviour
    {
        [SerializeField]
        private Image tintImage;
        [SerializeField]
        private Image avatarImage;

        public Text playerNameText;


        [SerializeField]
        private GameObject miniLoading;

        [SerializeField]
        private Image borderImage;

        [SerializeField]
        private Image crownImage;

        [SerializeField]
        private Button btKick;

        public bool playWithFriends;
        public GameObject userIcon;
        public PanelFriendsPlay panelFriends;

        public Image patentImage;
        public List<Sprite> patents;

        private int playerGrowId = -1;
        private int playerPatent = 1;

        private void Start()
        {
            Assert.IsNotNull(tintImage);
            Assert.IsNotNull(avatarImage);
            Assert.IsNotNull(playerNameText);
        }

        public void SetPlayer(User user)
        {
            int colorId = 0;
            Color color = Color.black;

            try
            {
                try
                {
                    playerGrowId = GrowSFS.GetVar(user, "idGrow").GetIntValue();
                    colorId = GrowSFS.GetVar(user, "color").GetIntValue();
                    playerPatent = GrowSFS.GetVar(user, "patente").GetIntValue();
                }
                catch (Exception e)
                {
                    Debug.LogError("[PlayerSlotRoom.SetPlayer] blob -> " + e);
                    Debug.Log("[PlayerSlotRoom.SetPlayer] blob -> user=" + user);
                }
                color = SFS.Grow.GetColor(colorId);

                if (playerGrowId == Globals.playerIdGrowGames)
                {
                    // TODO: Melhorar workaround...
                    var obj = GameObject.Find("AvatarTopLayer");
                    if (obj)
                        obj.GetComponent<Image>().color = color;
                }

                // Patente
                patentImage.gameObject.SetActive(true);
                patentImage.sprite = patents[playerPatent - 1];
            }
            catch (Exception e)
            {
                Debug.LogError("!!! ERRO !!! PlayerSlotRoom - SetPlayer parte 1: " + e);
            }


            try
            {
                tintImage.color = color;
                borderImage.color = color;
                tintImage.gameObject.SetActive(true);
                playerNameText.text = user.Name;
            }
            catch (Exception e)
            {
                Debug.LogError("!!! ERRO !!! PlayerSlotRoom - SetPlayer parte 2: " + e);
            }

            try
            {
                if (playWithFriends)
                {
                    var rvOwner = SFSManager.GetVar(SFS.Grow.sfs.LastJoinedRoom, "owner");
                    var ownerId = rvOwner == null ? -1 : rvOwner.GetIntValue();
                    crownImage.gameObject.SetActive(playerGrowId == ownerId);
                    userIcon.SetActive(false);
                    btKick.gameObject.SetActive(!user.IsItMe && Globals.playerIdGrowGames == ownerId);
                }
                else
                    miniLoading.SetActive(false);

                ServerManager.instance.GetAvatarSprite(GrowSFS.GetVar(user, "avatar").GetStringValue(), sprite => avatarImage.sprite = sprite);
            }
            catch (Exception e)
            {
                Debug.LogError("!!! ERRO !!! PlayerSlotRoom - SetPlayer parte 3: " + e);
            }
        }



        /// <summary>
        /// Função para limpar o slot para o jogador
        /// </summary>
        public void ClearSlot()
        {
            tintImage.color = Color.white;
            borderImage.color = Color.white;
            tintImage.gameObject.SetActive(false);
            playerNameText.text = "";
            patentImage.gameObject.SetActive(false);

            avatarImage.sprite = null;

            playerGrowId = -1;

            if (playWithFriends)
            {
                userIcon.SetActive(true);
                btKick.gameObject.SetActive(false);
            }
            else
                miniLoading.SetActive(true);
        }



        public void OnClickSlot()
        {
            if (playerGrowId == -1) // não tem ngm
            {
                if (panelFriends && !panelFriends.isOpen)
                    panelFriends.OnClickArrow();
            }
            else // tem alguém no slot
            {
                Sioux.Audio.Play("Click");
                ServerManager.instance.OpenNewProfilePopup(playerGrowId);
            }
        }

        public void OnClickKick()
        {
            Sioux.Audio.Play("Click");
            SFS.Grow.KickPrivateRequest(playerGrowId);
        }
    }
}