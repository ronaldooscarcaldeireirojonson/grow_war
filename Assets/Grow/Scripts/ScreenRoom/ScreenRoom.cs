﻿using Newtonsoft.Json;
using Sfs2X.Entities;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace Grow
{

    /// <summary>
    /// Classe que representa a tela de Esperar os adversários
    /// </summary>
    public class ScreenRoom : MonoBehaviour
    {
        [SerializeField]
        private List<PlayerSlotRoom> playerSnippets;
        public Text textWaitingEnemies;
        public Button BtDesistir;
        public GameObject chatCanvasPrefab;

        private float timeWaiting;

        public virtual void Awake()
        {
            Assert.IsNotNull(playerSnippets);

            textWaitingEnemies.text = "AGUARDE OS ADVERSÁRIOS";

            SFS.Grow.OnCountdownUpdate += UpdateCountDown;
            SFS.Grow.OnRoomUserListUpdate += UpdatePlayerSnippets;

            GameObject chat = Instantiate(chatCanvasPrefab);

        }

        public virtual void Update()
        {
            timeWaiting += Time.deltaTime;
        }

        void OnDestroy()
        {
            SFS.Grow.OnCountdownUpdate -= UpdateCountDown;
            SFS.Grow.OnRoomUserListUpdate -= UpdatePlayerSnippets;
        }

        void UpdateCountDown(int countdown)
        {

            textWaitingEnemies.text = countdown == 0 ? "A PARTIDA ESTÁ COMEÇANDO..." : countdown < 0 ? "AGUARDE OS ADVERSÁRIOS" : ("A PARTIDA COMEÇARÁ EM " + countdown);
            BtDesistir.interactable = countdown > 3 || countdown < 0;

            if (countdown == 2)
                AnalyticsManager.Instance.MatchmakingTime((int)timeWaiting);

        }

        public void UpdatePlayerSnippets(List<User> users)
        {
            try
            {
                if (users == null)
                {
                    Debug.Log("ScreenRoom - UpdatePlayerSnippets é null");
                }
                else
                {
                    for (int j = 0; j < playerSnippets.Count; j++)
                    {
                        if (j < users.Count)
                        {
                            playerSnippets[j].SetPlayer(users[j]);
                        }
                        else
                        {
                            playerSnippets[j].ClearSlot();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.Log("!!! ERRO !!! ScreenRoom - UpdatePlayerSnippets - " + e);
            }
        }

        public void OnClickQuit()
        {
            Sioux.Audio.Play("Click");

            AnalyticsManager.Instance.SendDesignEvent("Desistir:Ranqueado");

            MainMenuController.banTimer = Time.time;
            SFS.Grow.ExitRoomRequest();
        }
    }
}