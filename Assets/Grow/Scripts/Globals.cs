﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



namespace Grow
{

    public class Globals
    {
        public const string MORE_GAMES_URL = "http://www.growgames.com.br";
        public const string BETA_SURVEY_URL = "https://goo.gl/forms/TG57UejCLYtnBBwp1";

        // Cor laranja padrão GrowGames
        public static readonly Color COLOR_ORANGE = new Color32(252, 127, 23, 255);

        // Variáveis do jogador (populado depois do login)
        public static int playerId = 0;
        public static int playerIdGrowGames = 0;
        public static string playerNickname = "";
        public static string playerEmail = "";
        public static string playerCompleteName = "";
        public static int playerAvatarId = 0;
        public static string playerAvatarUrl = "";
        public static int playerAmountInvites = 0;
        public static Nivel playerLevel = new Nivel();
        public static int playerPatent = 1;
        public static float playerPoints = 0;
        public static string playerToken = "";
        public static string playerTokenGrowGames = "";
        public static string playerTokenFacebook = "";
        public static int playerTotalExperience = 0;
        public static int playerTotalVictories = 0;
        public static int playerTotalAbandonedGames = 0;
        public static int playerPeriodExperience = 0;
        public static int playerPeriodVictories = 0;
        public static int playerPeriodAbandonedGames = 0;
        public static Cla playerClan = new Cla() { Id = -1, IdEscudo = 0, Nome = "", Tag = "" };
        public static UsuarioCla playerClanPendent = null;
        public static bool playerClanModerator = false;
        public static List<Achievement> playerAchievements = new List<Achievement>();

        private static bool _playerIsVIP = false;
        public static bool playerIsVIP
        {
            get
            {
                return _playerIsVIP;
            }
            set
            {
                _playerIsVIP = value;
                if (onIsVipUpdate != null)
                {
                    onIsVipUpdate();
                }
            }
        }
        private static decimal _playerCoins = 0;
        public static decimal playerCoins
        {
            get
            {
                return _playerCoins;
            }
            set
            {
                _playerCoins = value;
                if (onCoinsUpdate != null)
                {
                    onCoinsUpdate();
                }
            }
        }
        private static Sprite _playerAvatarSprite = null;
        public static Sprite playerAvatarSprite
        {
            get
            {
                return _playerAvatarSprite;
            }
            set
            {
                _playerAvatarSprite = value;
                if (onPlayerAvatarUpdate != null)
                {
                    onPlayerAvatarUpdate(value);
                }
            }
        }



        // Variáveis do jogo (populado depois do login)
        public static int gameMaxCoins = 0;
        public static NovidadeModel gameNews = null;
        public static bool gameDailyBonus = false;
        public static int gameDailyBonusDays = 0;
        public static DateTime? gameDailyBonusDate = null;
        public static List<decimal> gameDailyBonusEnergies = new List<decimal>();
        public static double gameLoginTime = 0;
        public static Torneio gameTournament = null;
        public static decimal gamePointsPerVictory = 0;
        public static string gameAvatarURL = "";
        public static string gamePhotonServerURL = "";
        public static string gamePhotonFriendsURL = "";
        public static string gameIconsURL = "";
        public static bool gameHasPromotion = false;
        public static int gameQuickplayCost = 0;
        public static int gameQuickplayTime = 0;
        public static List<Nivel> gameLevels = new List<Nivel>();
        public static List<GrowAvatar> gameAvatars = new List<GrowAvatar>();
        public static List<Config.PatenteItem> gamePatentLabels = new List<Config.PatenteItem>();
        public static int readyTimeout = 0;
        public static List<GrowAvatar> gameClanAvatars = new List<GrowAvatar>();
        public static string gameClanURL = "";

        // Flag que indica se o jogador apertou em criar uma sala privada
        public static bool hasClickedPrivateGame = false;

        // Flag que indica que o jogador tentou pegar os dados do Version Controller mas não conseguiu (na tela de login)
        public static bool triedToGetVersionController = false;

        // Flag que indica que o jogador tentou logar mas já tem um usuário logado
        public static bool errorOnLoginUserAlreadyLoggedIn = false;

        // Eventos
        public delegate void EventPlayerVarUpdate();
        public static event EventPlayerVarUpdate onCoinsUpdate;
        public static event EventPlayerVarUpdate onIsVipUpdate;

        public delegate void EventPlayerAvatarUpdate(Sprite pic);
        public static event EventPlayerAvatarUpdate onPlayerAvatarUpdate;

        // Flag que indica que removemos algumas moedas do jogador quando ele apertou no botão de jogar para atualizar o visual apenas
        public static bool hasTakenCoinsFalsely = false;


        /// <summary>
        /// Função para retornar a plataforma do jogador
        /// </summary>
        public static Plataforma GetBuildPlatform()
        {
#if UNITY_ANDROID
            return Plataforma.Android;
#elif UNITY_IOS
            return Plataforma.IOS;
#elif UNITY_WEBGL
            return Plataforma.WEB;
#else
            return Plataforma.Desconhecida;
#endif

        }


        /// <summary>
        /// Função para atribuir algumas variáveis vindas do login
        /// </summary>
        public static void SetConfigData(Usuario.UsuarioGrowLogin data)
        {
            // Popula o token do jogador
            playerToken = data.Jogador.Token;

            // Popula as novidades
            gameNews = data.Novidade;

            if (data.Jogador.Bonus)
            {
                // Popula o bônus diário
                gameDailyBonus = true;
                gameDailyBonusDays = data.Jogador.NuBonusDiario;
            }

            // Popula a data do bônus diário
            gameDailyBonusDate = data.Jogador.DtBonusDiario;

            // Popula o tokenGrowGames (acho que é para o login automático)
            if (!string.IsNullOrEmpty(data.TokenGrowGames))
            {
                playerTokenGrowGames = data.TokenGrowGames;

                // Se não for a plataforma WEB
                if (GetBuildPlatform() != Plataforma.WEB)
                {
                    // Salva os dados na memória
                    Sioux.LocalData.Set("GrowGameToken", data.TokenGrowGames);
                }
                else
                {   // É a plataforma WEB

                    // TODO: Login no portal? DoLoginGrowGamesByToken
                }
            }

            // Popula o UserData
            if (data.UserData != null)
            {
                SetPlayerData(data.UserData);
            }

            // Popula as configurações do jogo
            if (data.Config != null)
            {
                gameDailyBonusEnergies = new List<decimal>()
                {
                    data.Config.NuBonus1,
                    data.Config.NuBonus2,
                    data.Config.NuBonus3,
                    data.Config.NuBonus4,
                    data.Config.NuBonus5
                };

                gameMaxCoins = data.Config.limiteMoedas;

                if (data.Config.NuPontosVitoria > 0)
                {
                    gamePointsPerVictory = data.Config.NuPontosVitoria;
                }

                if (data.Config.avatarURL != null && data.Config.avatarURL.Length > 0)
                {
                    gameAvatarURL = data.Config.avatarURL;
                }

                if (data.Config.gameIconsURL != null && data.Config.gameIconsURL.Length > 0)
                {
                    gameIconsURL = data.Config.gameIconsURL;
                }

                if (data.Config.photonURL.Length > 0)
                {
                    gamePhotonServerURL = data.Config.photonURL;
                }

                if (data.Config.photonFriendsURL != null && data.Config.photonFriendsURL.Length > 0)
                {
                    gamePhotonFriendsURL = data.Config.photonFriendsURL;
                }

                gameHasPromotion = data.Config.promocaoJogoRapido;

                if (data.Config.valorJogoRapido >= 0)
                {
                    gameQuickplayCost = data.Config.valorJogoRapido;
                }

                if (data.Config.Niveis != null)
                {
                    gameLevels = data.Config.Niveis;
                }

                if (data.Config.ListaAvatar != null)
                {
                    for (int i = 0; i < data.Config.ListaAvatar.Count; i++)
                    {
                        GrowAvatar newAvatar = new GrowAvatar();
                        newAvatar.IdAvatar = data.Config.ListaAvatar[i].IdAvatar;
                        newAvatar.TxAvatar = data.Config.ListaAvatar[i].TxAvatar;
                        newAvatar.onlyVip = data.Config.ListaAvatar[i].onlyVip;

                        gameAvatars.Add(newAvatar);
                    }
                }

                if (data.Config.Patentes != null)
                {
                    gamePatentLabels = data.Config.Patentes;
                }

                if (!string.IsNullOrEmpty(data.Config.clanURL))
                {
                    gameClanURL = data.Config.clanURL;
                }
            }

            gameLoginTime = Time.time;

            if (data.Torneio != null)
            {
                gameTournament = data.Torneio;
            }

            if (data.UserData != null && data.UserData.Cla != null)
            {
                // Se usuário estiver aprovado
                switch (data.UserData.Cla.IdStatus)
                {
                    case 1: // Pendente membro
                        playerClanPendent = data.UserData.Cla;
                        break;

                    case 2: // Aprovado
                        Cla clan = new Cla();
                        clan.Id = data.UserData.Cla.Id;
                        clan.IdEscudo = data.UserData.Cla.IdEscudo;
                        clan.Nome = data.UserData.Cla.Nome;
                        clan.Tag = data.UserData.Cla.Tag;
                        clan.MembersAmount = data.UserData.Cla.MembersAmount;

                        playerClan = clan;
                        break;

                    case 3: // Reprovado
                        break;

                    case 4: // Pendente moderador
                        playerClanPendent = data.UserData.Cla;
                        break;

                    default:
                        break;
                }
            }
        }


        /// <summary>
        /// Função para setar os dados do jogador
        /// </summary>
        public static void SetPlayerData(Usuario.UserData data)
        {
            playerId = data.Id;
            playerIdGrowGames = data.IdGrowGames;
            playerNickname = data.Apelido;
            playerEmail = data.Email;
            playerCompleteName = data.NomeCompleto;
            playerAvatarId = data.IdAvatar;
            playerAvatarUrl = data.Avatar;
            playerAmountInvites = data.Convites;
            playerIsVIP = data.isVIP;

            AdMobManager.Instance.SetPremium(playerIsVIP);

            playerLevel = data.Nivel;
            playerPatent = data.IdPatente;
            playerCoins = data.Extrato.NuSaldoMoeda;
            playerPoints = data.Pontos;
            playerTotalExperience = data.UserGameData.ExperienciaTotal;
            playerTotalVictories = data.UserGameData.VitoriasTotal;
            playerTotalAbandonedGames = data.UserGameData.PartidasAbandonadasTotal;
            playerPeriodExperience = data.UserGameData.ExperienciaPeriodo;
            playerPeriodVictories = data.UserGameData.VitoriasPeriodo;
            playerPeriodAbandonedGames = data.UserGameData.PartidasAbandonadasPeriodo;
            playerAchievements = data.Conquistas;
        }



        /// <summary>
        /// Função para pegar a porcentagem que o jogador está para o próximo nível (entre 0 e 1)
        /// </summary>
        public static float GetNextLevelPercentage(float playerPoints)
        {
            Nivel currentLevel;
            Nivel nextLevel;

            for (int i = gameLevels.Count - 1; i >= 0; i--)
            {
                if (gameLevels[i].NuXP <= playerPoints)
                {
                    currentLevel = gameLevels[i];
                    nextLevel = gameLevels[i + 1];

                    return (float)(playerPoints - currentLevel.NuXP) / (float)(nextLevel.NuXP - currentLevel.NuXP);
                }
            }

            // O jogador está no nível 0
            return (float)playerPoints / (float)gameLevels[0].NuXP;
        }



        /// <summary>
        /// Função de TEST
        /// </summary>
        public static NewProfileModel GetNewProfileDataTEST()
        {
            NewProfileModel newProfile = new NewProfileModel();
            newProfile.id = Globals.playerId;
            newProfile.name = "-";
            newProfile.avatar = "-";
            newProfile.isVip = false;    // (UnityEngine.Random.Range(0, 2) == 0);
            newProfile.level = 8;
            newProfile.rank = "-";
            newProfile.likeAmount = 0;
            newProfile.clan = "-";

            newProfile.playerSince = DateTime.Now;
            newProfile.age = 0;
            newProfile.city = "-";
            newProfile.maritalStatus = "-";
            newProfile.team = "-";

            newProfile.victories30 = 0;
            newProfile.losts30 = 0;
            newProfile.abandons30 = 0;
            newProfile.points30 = 0;
            newProfile.timePlaying = new TimeSpan(0, 0, 0);
            newProfile.victories = 0;
            newProfile.losts = 0;
            newProfile.abandons = 0;
            newProfile.points = 0;
            newProfile.personHits = 0;
            newProfile.yearHits = 0;
            newProfile.placeHits = 0;
            newProfile.thingHits = 0;
            newProfile.goldAchieve = 0;
            newProfile.silverAchieve = 0;
            newProfile.copperAchieve = 0;

            NewProfileModel.HistoricPlayerModel player1 = new NewProfileModel.HistoricPlayerModel();
            player1.id = 9;
            player1.idGrow = 79422;
            player1.name = "FOKUBO01";
            player1.avatar = "Photo10.jpg";
            player1.isWinner = true;
            player1.colorR = 255;
            player1.colorG = 0;
            player1.colorB = 0;

            NewProfileModel.HistoricPlayerModel player2 = new NewProfileModel.HistoricPlayerModel();
            player2.id = 10;
            player2.idGrow = 79423;
            player2.name = "FOKUBO02";
            player2.avatar = "Photo11.jpg";
            player2.isWinner = false;
            player2.colorR = 0;
            player2.colorG = 255;
            player2.colorB = 0;

            NewProfileModel.HistoricPlayerModel player3 = new NewProfileModel.HistoricPlayerModel();
            player3.id = 20;
            player3.idGrow = 79424;
            player3.name = "FOKUBO03";
            player3.avatar = "Photo12.jpg";
            player3.isWinner = false;
            player3.colorR = 0;
            player3.colorG = 0;
            player3.colorB = 255;

            NewProfileModel.HistoricPlayerModel player4 = new NewProfileModel.HistoricPlayerModel();
            player4.id = 21;
            player4.idGrow = 79425;
            player4.name = "FOKUBO04";
            player4.avatar = "Photo13.jpg";
            player4.isWinner = false;
            player4.colorR = 0;
            player4.colorG = 255;
            player4.colorB = 255;

            NewProfileModel.HistoricMatchModel match1 = new NewProfileModel.HistoricMatchModel();
            match1.id = 1;
            match1.isWinner = true;
            match1.date = new DateTime(2017, 1, 1, 1, 1, 1);
            match1.players = new List<NewProfileModel.HistoricPlayerModel>() { player1, player2, player3, player4 };

            player1.isWinner = false;
            player3.isWinner = true;

            NewProfileModel.HistoricMatchModel match2 = new NewProfileModel.HistoricMatchModel();
            match2.id = 2;
            match2.isWinner = false;
            match2.date = new DateTime(2017, 2, 2, 2, 2, 2);
            match2.players = new List<NewProfileModel.HistoricPlayerModel>() { player1, player2, player3 };

            player3.isWinner = false;
            player4.isWinner = true;

            NewProfileModel.HistoricMatchModel match3 = new NewProfileModel.HistoricMatchModel();
            match3.id = 3;
            match3.isWinner = false;
            match3.date = new DateTime(2017, 3, 3, 3, 3, 3);
            match3.players = new List<NewProfileModel.HistoricPlayerModel>() { player1, player2, player3, player4 };

            player4.isWinner = false;
            player1.isWinner = true;

            NewProfileModel.HistoricMatchModel match4 = new NewProfileModel.HistoricMatchModel();
            match4.id = 4;
            match4.isWinner = true;
            match4.date = new DateTime(2017, 4, 4, 4, 4, 4);
            match4.players = new List<NewProfileModel.HistoricPlayerModel>() { player1, player2, player3, player4 };

            player1.isWinner = false;
            player2.isWinner = true;

            NewProfileModel.HistoricMatchModel match5 = new NewProfileModel.HistoricMatchModel();
            match5.id = 5;
            match5.isWinner = true;
            match5.date = new DateTime(2017, 5, 5, 5, 5, 5);
            match5.players = new List<NewProfileModel.HistoricPlayerModel>() { player1, player2, player3 };

            newProfile.matches = new List<NewProfileModel.HistoricMatchModel>();
            //newProfile.matches.Add(match1);
            //newProfile.matches.Add(match2);
            //newProfile.matches.Add(match3);
            //newProfile.matches.Add(match4);
            //newProfile.matches.Add(match5);

            NewProfileModel.AchievementModel achieve1 = new NewProfileModel.AchievementModel();
            achieve1.id = 1;
            achieve1.name = "Acertar a primeira pergunta";
            achieve1.progressCurrent = 5;
            achieve1.progressTotal = 30;
            achieve1.progressText = "acertos";
            achieve1.hasCopperMedal = true;
            achieve1.hasSilverMedal = false;
            achieve1.hasGoldMedal = false;

            NewProfileModel.AchievementModel achieve2 = new NewProfileModel.AchievementModel();
            achieve2.id = 2;
            achieve2.name = "Ganhar partidas";
            achieve2.progressCurrent = 2;
            achieve2.progressTotal = 100;
            achieve2.progressText = "partidas";
            achieve2.hasCopperMedal = false;
            achieve2.hasSilverMedal = false;
            achieve2.hasGoldMedal = false;

            NewProfileModel.AchievementModel achieve3 = new NewProfileModel.AchievementModel();
            achieve3.id = 3;
            achieve3.name = "Ficar em segundo lugar";
            achieve3.progressCurrent = 4;
            achieve3.progressTotal = 5;
            achieve3.progressText = "ganhos";
            achieve3.hasCopperMedal = true;
            achieve3.hasSilverMedal = true;
            achieve3.hasGoldMedal = true;

            NewProfileModel.AchievementModel achieve4 = new NewProfileModel.AchievementModel();
            achieve4.id = 4;
            achieve4.name = "Usar carta Palpite a Qualquer Hora";
            achieve4.progressCurrent = 26;
            achieve4.progressTotal = 100;
            achieve4.progressText = "usos";
            achieve4.hasCopperMedal = true;
            achieve4.hasSilverMedal = false;
            achieve4.hasGoldMedal = false;

            NewProfileModel.AchievementModel achieve5 = new NewProfileModel.AchievementModel();
            achieve5.id = 5;
            achieve5.name = "Usar carta Escolher Categoria";
            achieve5.progressCurrent = 8;
            achieve5.progressTotal = 100;
            achieve5.progressText = "usos";
            achieve5.hasCopperMedal = false;
            achieve5.hasSilverMedal = false;
            achieve5.hasGoldMedal = false;

            NewProfileModel.AchievementModel achieve6 = new NewProfileModel.AchievementModel();
            achieve6.id = 6;
            achieve6.name = "Usar carta Anula Questão";
            achieve6.progressCurrent = 87;
            achieve6.progressTotal = 100;
            achieve6.progressText = "usos";
            achieve6.hasCopperMedal = true;
            achieve6.hasSilverMedal = false;
            achieve6.hasGoldMedal = false;

            newProfile.achieves = new List<NewProfileModel.AchievementModel>();
            //newProfile.achieves.Add(achieve1);
            //newProfile.achieves.Add(achieve2);
            //newProfile.achieves.Add(achieve3);
            //newProfile.achieves.Add(achieve4);
            //newProfile.achieves.Add(achieve5);
            //newProfile.achieves.Add(achieve6);

            return newProfile;
        }
    }

}