﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class ScaleDragThreshold : MonoBehaviour
{
    
    public Canvas canvasMain;


    // Use this for initialization
    void Start()
    {
        // Calculo do drag threshold
        EventSystem.current.pixelDragThreshold = Mathf.RoundToInt(10 * canvasMain.scaleFactor);
    }
}
