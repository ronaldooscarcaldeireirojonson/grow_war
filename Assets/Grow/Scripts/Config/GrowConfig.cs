﻿using System;
using System.Collections;
using UnityEngine;

namespace Grow
{
    public static class Grow
    {
        public static GrowPropertiesData Properties;

        public static void Init(MonoBehaviour monoBehaviour)
        {
            Properties = Resources.Load<GrowPropertiesData>("Grow/GrowPropertiesData");
        }
    }

    [Serializable]
    public class GrowConfig
    {
        public string URL_WEB_API;
    }
}
