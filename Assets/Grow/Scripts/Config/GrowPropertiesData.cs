﻿using UnityEngine;

namespace Grow
{
    [CreateAssetMenu]
    public class GrowPropertiesData : ScriptableObject
    {
        // Versão do jogo (lembrar de mudar esse número junto com a versão no PlayerSettings)
        public string VERSION_CONTROLLER_URL;

        // Cor cinza escuro do bônus diário
        [Header("Cores Bônus Diário")]
        public Color32 DAILY_BONUS_FRONT_DAY_COLOR; // = new Color32(157, 72, 26, 255);
        public Color32 DAILY_BONUS_BACK_DAY_COLOR; // = new Color32(49, 49, 49, 255); // essa era o COLOR_DARK_GRAY_LIGHT 
        public Color32 DAILY_BONUS_CURRENT_DAY_COLOR; // = new Color32(49, 49, 49, 255);

        [Header("Cores Novo Perfil Usuário")]
        public Color32 COLOR_TAB_ON;
        public Color32 COLOR_TAB_OFF;
        public Color32 COLOR_TEXT_ON;
        public Color32 COLOR_TEXT_OFF;

        [Header("Ranking")]
        public Color32 RANKING_SELF_COLOR;
    }
}