﻿using Grow;
using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemClanMember : MonoBehaviour
{
    private ClanMember member;

    public Text textMemberPosition;

    public Image memberPicture;

    public Text textMemberName;

    public GameObject textModerator;
    public GameObject buttonMenu;
    public GameObject buttonPromote;
    public GameObject buttonDemote;
    public GameObject buttonKick;
    public GameObject vipIcon;

    public GameObject buttonAccept;
    public GameObject buttonDecline;

    public Text textPoints;

    public GameObject textMenuClose;
    public GameObject textMenuOpen;

    // Flag que indica que o menu de botões está aberto
    private bool isOpen = false;


    /// <summary>
    /// Função para atualizar os dados do membro do clã
    /// </summary>
    public void Configure(ClanMember member)
    {
        this.member = member;

        // Atualiza a posição do jogador
        if (member.Pos != null)
        {
            textMemberPosition.text = member.Pos + "º";
        }
        else
        {
            textMemberPosition.text = string.Empty;
        }

        // Atualiza a imagem do jogador
        ServerManager.instance.GetAvatarSprite(member.Avatar,
            (photo) =>
            {
                memberPicture.sprite = photo;
            });

        // Atualiza o nome do jogador
        textMemberName.text = member.Nickname;

        // Atualiza os pontos do jogador
        textPoints.text = member.Points.ToString() + " PONTOS";

        // Verifica qual botão pode aparecer
        // - Se o jogador for moderador do clã
        if (Globals.playerClanModerator)
        {
            buttonMenu.SetActive(true);
            buttonPromote.SetActive(false);
            buttonDemote.SetActive(false);
            buttonKick.SetActive(false);

            textMenuClose.SetActive(true);
            textMenuOpen.SetActive(false);

            // Verifica se o membro vai ser adicionado no clã
            if (member.IdStatus == 4)
            {
                textMemberPosition.gameObject.SetActive(false);
                textPoints.gameObject.SetActive(false);

                buttonAccept.SetActive(true);
                buttonDecline.SetActive(true);
            }
            else
            {
                buttonAccept.SetActive(false);
                buttonDecline.SetActive(false);
            }
        }
        else
        {
            buttonMenu.SetActive(false);
            buttonPromote.SetActive(false);
            buttonDemote.SetActive(false);
            buttonKick.SetActive(false);

            buttonAccept.SetActive(false);
            buttonDecline.SetActive(false);
        }


        // Verifica se o jogador é VIP / Moderador
        if (member.isVip)
        {
            if (member.isModerator)
            {
                textModerator.SetActive(true);
                vipIcon.SetActive(false);
            }
            else
            {
                textModerator.SetActive(false);
                vipIcon.SetActive(true);
            }
        }
        else
        {
            textModerator.SetActive(false);
            vipIcon.SetActive(false);
        }
    }



    public void RemoveButtons()
    {
        buttonMenu.SetActive(false);
        buttonPromote.SetActive(false);
        buttonDemote.SetActive(false);
        buttonKick.SetActive(false);

        // Verifica se o jogador é VIP / Moderador
        if (member.isVip)
        {
            if (member.isModerator)
            {
                textModerator.SetActive(true);
                vipIcon.SetActive(false);
            }
            else
            {
                textModerator.SetActive(false);
                vipIcon.SetActive(true);
            }
        }
        else
        {
            textModerator.SetActive(false);
            vipIcon.SetActive(false);
        }
    }



    public void OnClickMenu()
    {
        if (isOpen)
        {
            isOpen = false;

            textMenuClose.SetActive(true);
            textMenuOpen.SetActive(false);

            buttonPromote.SetActive(false);
            buttonDemote.SetActive(false);
            buttonKick.SetActive(false);

            textPoints.gameObject.SetActive(true);
        }
        else
        {
            isOpen = true;

            textMenuClose.SetActive(false);
            textMenuOpen.SetActive(true);

            if (member.isModerator)
            {
                buttonPromote.SetActive(false);
                buttonDemote.SetActive(true);
                buttonKick.SetActive(true);
            }
            else
            {
                buttonKick.SetActive(true);
                buttonDemote.SetActive(false);

                if (member.isVip)
                {
                    buttonPromote.SetActive(true);
                }
                else
                {
                    buttonPromote.SetActive(false);
                }
            }

            textPoints.gameObject.SetActive(false);
        }
    }




    public void OnClickKickMember()
    {
        ClanRequest request = new ClanRequest();
        request.IdClan = Globals.playerClan.Id;
        //request.IdModerator = Globals.playerId;
        request.IdMember = member.IdUsuario;

        // Faz a requisição do server para criar um clã
        ServerManager.instance.KickClanMember(request,
            (response) =>
            {
                Debug.Log("SendKickClanMember: " + response);

                ClanResponse clanResponse = JsonConvert.DeserializeObject<ClanResponse>(response);

                if (clanResponse.Code == 0)
                {
                    // Se o jogador está se removendo
                    if (member.IdUsuario == Globals.playerId)
                    {
                        Globals.playerClan = new Cla() { Id = -1, IdEscudo = 0, Nome = "", Tag = "" };

                        // Fecha a popup de editar o clã
                        PopupManager.instance.ClosePopup();

                        ScreenClan script = GameObject.FindObjectOfType<ScreenClan>();
                        if (script != null)
                        {
                            script.OnClickBack();
                        }

                        // Troca o ícone do clã do Header
                        Header header = GameObject.FindObjectOfType<Header>();
                        header.UpdateClanButton();
                    }
                    else
                    {   // O jogador está removendo outra pessoa do clã

                        // Atualiza a lista de jogadores
                        PopupEditClan script = GameObject.FindObjectOfType<PopupEditClan>();
                        if (script != null)
                        {
                            script.UpdateMembers(member.IdUsuario);
                        }
                    }
                }
                else
                {
                    ScreenClan.ShowErrorCode(clanResponse.Code);
                }
            });
    }



    public void OnClickModerator(bool isPromoting)
    {
        ClanRequest request = new ClanRequest();
        request.IdClan = Globals.playerClan.Id;
        request.IdModerator = Globals.playerId;
        request.IdMember = member.IdUsuario;
        request.IsModerator = isPromoting;

        // Fecha o menu
        //OnClickMenu();

        // Faz a requisição do server para criar um clã
        ServerManager.instance.SetClanModerator(request,
            (response) =>
            {
                Debug.Log("SetClanModerator: " + response);

                ClanResponse clanResponse = JsonConvert.DeserializeObject<ClanResponse>(response);

                if (clanResponse.Code == 0)
                {
                    if (isPromoting)
                    {
                        buttonPromote.SetActive(false);
                        buttonDemote.SetActive(true);
                        vipIcon.SetActive(false);
                        textModerator.SetActive(true);
                    }
                    else
                    {
                        // Se for o próprio jogador
                        if (member.IdUsuario == Globals.playerId)
                        {
                            ItemClanMember[] members = GameObject.FindObjectsOfType<ItemClanMember>();
                            for (int i = 0; i < members.Length; i++)
                            {
                                members[i].RemoveButtons();
                            }

                            PopupEditClan script = GameObject.FindObjectOfType<PopupEditClan>();
                            if (script != null)
                            {
                                script.buttonAddMember.SetActive(false);
                            }

                            vipIcon.SetActive(true);
                            textModerator.SetActive(false);
                        }
                        else
                        {
                            buttonPromote.SetActive(true);
                            buttonDemote.SetActive(false);
                            vipIcon.SetActive(true);
                            textModerator.SetActive(false);
                        }
                    }
                }
                else
                {
                    ScreenClan.ShowErrorCode(clanResponse.Code);
                }
            });
    }



    public void OnClickAccept()
    {
        // Responde para o server
        ClaMemberApprovalRequest request = new ClaMemberApprovalRequest();
        request.Id = Globals.playerClan.Id;
        request.IdParticipante = member.IdUsuario;
        request.IsApproved = true;

        ServerManager.instance.AnswerClanMember(request,
            (response) =>
            {
                Debug.Log("AnswerClanMember: " + response);

                ClanResponse clanResponse = JsonConvert.DeserializeObject<ClanResponse>(response);

                if (clanResponse.Code == 0)
                {
                    // Atualiza a lista de jogadores
                    PopupEditClan script = GameObject.FindObjectOfType<PopupEditClan>();
                    if (script != null)
                    {
                        script.UpdateMembers(member.IdUsuario);
                    }
                }
                else
                {
                    ScreenClan.ShowErrorCode(clanResponse.Code);
                }
            });
    }



    public void OnClickDecline()
    {
        ClanRequest request = new ClanRequest();
        request.IdClan = Globals.playerClan.Id;
        //request.IdModerator = Globals.playerId;
        request.IdMember = member.IdUsuario;

        // Faz a requisição do server para criar um clã
        ServerManager.instance.KickClanMember(request,
            (response) =>
            {
                Debug.Log("SendKickClanMember: " + response);

                ClanResponse clanResponse = JsonConvert.DeserializeObject<ClanResponse>(response);

                if (clanResponse.Code == 0)
                {
                    // Atualiza a lista de jogadores
                    PopupEditClan script = GameObject.FindObjectOfType<PopupEditClan>();
                    if (script != null)
                    {
                        script.UpdateMembers(member.IdUsuario);
                    }
                }
                else
                {
                    ScreenClan.ShowErrorCode(clanResponse.Code);
                }
            });
    }
}
