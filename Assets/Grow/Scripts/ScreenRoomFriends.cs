﻿using Sfs2X.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Grow
{
    /// <summary>
    /// Classe que representa a tela de Jogar com amigos
    /// </summary>
    public class ScreenRoomFriends : MonoBehaviour
    {

        [SerializeField]
        private List<PlayerSlotRoom> playerSnippets;
        public Text textWaitingEnemies;
        public Button BtDesistir;
        public Button buttonStartGame;
        public GameObject panelFriends;
        public GameObject chatCanvasPrefab;

        public virtual void Awake()
        {
            SFS.Grow.OnRoomUserListUpdate += UpdatePlayerSnippets;
            GameObject chat = Instantiate(chatCanvasPrefab);
        }

        void Start()
        {
            // Se foi o próprio jogador a clicar no jogo privado (se ele for o dono da sala)
            if (Globals.hasClickedPrivateGame)
            {
                Globals.hasClickedPrivateGame = false;

                buttonStartGame.gameObject.SetActive(true);
                panelFriends.SetActive(true);
                textWaitingEnemies.text = "Convide amigos e comece a partida";
            }
            else
            {
                buttonStartGame.gameObject.SetActive(false);
                panelFriends.SetActive(false);
                textWaitingEnemies.text = "Espere o dono da sala começar a partida";
            }
        }

        public virtual void Update()
        {

        }

        void OnDestroy()
        {
            SFS.Grow.OnRoomUserListUpdate -= UpdatePlayerSnippets;
        }

        public void UpdatePlayerSnippets(List<User> users)
        {
            try
            {
                if (users == null)
                {
                }
                else
                {
                    if (users.Count > 2)
                        buttonStartGame.interactable = true;
                    else
                        buttonStartGame.interactable = false;

                    for (int j = 0; j < playerSnippets.Count; j++)
                    {
                        if (j < users.Count)
                        {
                            playerSnippets[j].SetPlayer(users[j]);
                        }
                        else
                        {
                            playerSnippets[j].ClearSlot();
                        }
                    }
                }

                // Atualiza o painel de amigos
                // TODO: Tem um jeito melhor pra fazer isso, que é usando as variáveis inGame e Game
                PanelFriendsPlay.instance.UpdateFriendList();
            }
            catch (Exception e)
            {
                Debug.Log("!!! ERRO !!! ScreenRoomFriends - UpdatePlayerSnippets - " + e);
            }
        }

        public void OnClickQuit()
        {
            Sioux.Audio.Play("Click");

            AnalyticsManager.Instance.SendDesignEvent("Desistir:ComAmigos");

            SFS.Grow.ExitRoomRequest();
        }

        public void OnClickStartGame()
        {
            Sioux.Audio.Play("Click");
            SFS.Grow.PlayPrivateRequest();
        }
    }
}