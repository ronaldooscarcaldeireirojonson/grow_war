﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Analytics;
using GameAnalyticsSDK;

namespace Grow
{



    public class AnalyticsManager : MonoBehaviour
    {


        public static AnalyticsManager Instance;

        public bool abriuChat;
        public bool abriuPlayers;

        public bool isDeveloper;
        public List<string> devApelidos;

        public static int matchesPlayed = 0;



        public virtual void Awake()
        {
            if (AnalyticsManager.Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }


        public void OutOfEnergy(int idGrow)
        {
            GameAnalytics.NewDesignEvent("OutOfEnergy", idGrow);
        }
        public void OnApplicationFocus(bool hasFocus)
        {
            if (!hasFocus)
            {
                SendDesignEvent("Foco:" + UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
            }
        }


        public void OnApplicationPause(bool isPaused)
        {
            if (isPaused)
            {
                SendDesignEvent("Pause:" + UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
            }
        }


        public void OnApplicationQuit()
        {
            SendDesignEvent("Saiu:" + UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
        }




        public void Login(int idGrow)
        {
            GameAnalytics.NewDesignEvent("GrowLogin", idGrow);
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "version", VersionController.VERSION);
        }

        public void ClickAbandonarPartida()
        {
            GameAnalytics.NewDesignEvent("ClickAbandonarPartida", 1);
        }

        //i = 0 clicou no mais jogos na home, i = 1 clicou no mais jogos logo grow, i = 3 clicou no mais jogos menu config
        public void ClickMaisJogos(int i)
        {
            switch (i)
            {
                case 0:
                    GameAnalytics.NewDesignEvent("Clicou no mais jogos na home", 1);
                    break;

                case 1:
                    GameAnalytics.NewDesignEvent("Clicou no mais jogos no logo grow", 1);
                    break;

                case 2:
                    GameAnalytics.NewDesignEvent("Clicou no mais jogos no menu config", 1);
                    break;

                default:
                    break;
            }


        }

        public void ClickAbrirChat()
        {
            if (!abriuChat)
            {
                GameAnalytics.NewDesignEvent("ClickAbrirChat", 1);
                abriuChat = true;
            }

        }

        public void SendChatMessage()
        {
            GameAnalytics.NewDesignEvent("SendChatMessage", 1);
        }

        public void CheckUserDev(string apelido)
        {
            if (devApelidos.Contains(apelido.ToLower()))
            {
                isDeveloper = true;
            }
        }

        public void ClickPlayers()
        {
            if (!abriuPlayers)
            {
                GameAnalytics.NewDesignEvent("ClickPlayers", 1);
                abriuPlayers = true;
            }

        }

        public void MatchmakingTime(int time)
        {
            GameAnalytics.NewDesignEvent("MatchmakingTime", time);
        }

        public void ClickPerfilUsuario()
        {
            GameAnalytics.NewDesignEvent("ClickPerfilUsuario", 1);
        }

        public void ClickPerfilOutroUsuario()
        {
            GameAnalytics.NewDesignEvent("ClickPerfilOutroUsuario", 1);
        }

        /// <summary>
        /// Função para enviar um evento para o analytics
        /// </summary>
        public void SendDesignEvent(string eventName, float eventValue = float.NegativeInfinity)
        {
            if (eventValue == float.NegativeInfinity)
            {
                GameAnalytics.NewDesignEvent(eventName, 1);
            }
            else
            {
                GameAnalytics.NewDesignEvent(eventName, eventValue);
            }
        }




        /// <summary>
        /// Função para enviar um evento do tipo progressão para o analytics
        /// </summary>
        public void SendProgressionEvent(string progression = "", int score = int.MinValue)
        {
            if (score == int.MinValue)
            {
                GameAnalytics.NewProgressionEvent(GAProgressionStatus.Undefined, progression);
            }
            else
            {
                GameAnalytics.NewProgressionEvent(GAProgressionStatus.Undefined, progression, score);
            }
        }
        

    }
}
