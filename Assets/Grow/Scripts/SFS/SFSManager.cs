﻿using Sfs2X;
using Sfs2X.Core;
using Sfs2X.Entities;
using Sfs2X.Entities.Data;
using Sfs2X.Entities.Variables;
using Sfs2X.Logging;
using Sfs2X.Protocol.Serialization;
using Sfs2X.Requests;
using System;
using System.Collections;
using System.Reflection;
using UnityEngine;

public abstract class SFSManager : MonoBehaviour
{
    protected int pingInterval = 3;
    protected int reconnectInterval = 5;
    protected bool reconnectActivated = true;
    protected ServerSettings settings;
    public SmartFox sfs = null;

    //public static T CreateAndConnect<T>(string name, string fileName) where T : SFSManager
    public static T CreateAndConnect<T>(string name, string serverIP = "", int serverPort = 9933, int serverPortWebGL = 8085, string zoneName = "") where T : SFSManager
    {
        var manager = new GameObject("SmartFoxManager." + name).AddComponent<T>();

        DontDestroyOnLoad(manager.gameObject);
        manager.gameObject.hideFlags = HideFlags.HideInHierarchy;

        //manager.StartCoroutine(manager.ConnectRoutine(fileName));
        manager.StartCoroutine(manager.ConnectRoutine(serverIP, serverPort, serverPortWebGL, zoneName));

        return manager;
    }

    protected void Send(string cmd, Room room = null, SFSObject obj = null)
    {
        sfs.Send(new ExtensionRequest(cmd, obj ?? new SFSObject(), room));
    }

    //private IEnumerator ConnectRoutine(string configFileName = "")
    private IEnumerator ConnectRoutine(string serverIP = "", int serverPort = 9933, int serverPortWebGL = 8085, string zoneName = "")
    {
        if (sfs != null)
            yield break;

        if (settings == null)
        {
            //yield return StartCoroutine(LoadJsonFromStreamingAssets<ServerSettings>(configFileName, result => settings = result));
            settings = new ServerSettings();
            settings.serverIP = serverIP;
            settings.ServerPort = serverPort;
            settings.ServerPortWebGL = serverPortWebGL;
            settings.ZoneName = zoneName;
        }

#if UNITY_WEBGL && !UNITY_EDITOR
        sfs = new SmartFox(Sfs2X.Util.UseWebSocket.WS);
#else
        sfs = new SmartFox();
#endif
        sfs.Log.LoggingLevel = LogLevel.INFO;
        DefaultSFSDataSerializer.RunningAssembly = Assembly.GetExecutingAssembly();
        sfs.ThreadSafeMode = true;

        sfs.AddLogListener(LogLevel.DEBUG, OnEventResponse);
        sfs.AddLogListener(LogLevel.INFO, OnEventResponse);
        sfs.AddLogListener(LogLevel.WARN, OnEventResponse);
        sfs.AddLogListener(LogLevel.ERROR, OnEventResponse);

        // conection
        sfs.AddEventListener(SFSEvent.CONNECTION, OnEventResponse);
        sfs.AddEventListener(SFSEvent.CONNECTION_LOST, OnEventResponse);
        sfs.AddEventListener(SFSEvent.CONNECTION_RESUME, OnEventResponse);
        sfs.AddEventListener(SFSEvent.CONNECTION_RETRY, OnEventResponse);
        sfs.AddEventListener(SFSEvent.CONNECTION_ATTEMPT_HTTP, OnEventResponse);
        sfs.AddEventListener(SFSEvent.SOCKET_ERROR, OnEventResponse);

        // room
        sfs.AddEventListener(SFSEvent.ROOM_VARIABLES_UPDATE, OnEventResponse);
        sfs.AddEventListener(SFSEvent.ROOM_JOIN, OnEventResponse);
        sfs.AddEventListener(SFSEvent.USER_ENTER_ROOM, OnEventResponse);
        sfs.AddEventListener(SFSEvent.USER_EXIT_ROOM, OnEventResponse);

        // other
        sfs.AddEventListener(SFSEvent.LOGIN, OnEventResponse);
        sfs.AddEventListener(SFSEvent.LOGIN_ERROR, OnEventResponse);
        sfs.AddEventListener(SFSEvent.EXTENSION_RESPONSE, OnEventResponse);
        sfs.AddEventListener(SFSEvent.USER_COUNT_CHANGE, OnEventResponse);
        sfs.AddEventListener(SFSEvent.USER_VARIABLES_UPDATE, OnEventResponse);
        sfs.AddEventListener(SFSEvent.PUBLIC_MESSAGE, OnEventResponse);

        // buddy
        sfs.AddEventListener(SFSBuddyEvent.BUDDY_ADD, OnEventResponse);
        sfs.AddEventListener(SFSBuddyEvent.BUDDY_BLOCK, OnEventResponse);
        sfs.AddEventListener(SFSBuddyEvent.BUDDY_ERROR, OnEventResponse);
        sfs.AddEventListener(SFSBuddyEvent.BUDDY_LIST_INIT, OnEventResponse);
        sfs.AddEventListener(SFSBuddyEvent.BUDDY_MESSAGE, OnEventResponse);
        sfs.AddEventListener(SFSBuddyEvent.BUDDY_ONLINE_STATE_UPDATE, OnEventResponse);
        sfs.AddEventListener(SFSBuddyEvent.BUDDY_REMOVE, OnEventResponse);
        sfs.AddEventListener(SFSBuddyEvent.BUDDY_VARIABLES_UPDATE, OnEventResponse);

        try
        {
#if UNITY_WEBGL && !UNITY_EDITOR
        sfs.Connect(settings.serverIP, settings.ServerPortWebGL);
#else
            sfs.Connect(settings.serverIP, settings.ServerPort);
#endif
        }
        catch (Exception e)
        {
            Debug.Log("[SFSManager.ConnectRoutine] " + e);
        }
    }

    public void Disconnect()
    {
        StopPing();
        if (sfs != null && sfs.IsConnected)
            sfs.Disconnect();
        sfs = null;
    }

    // GENERIC EVENT RESPONSE
    void OnEventResponse(BaseEvent evt)
    {
        if (evt.Type == "login" && pingCoroutine == null)
            pingCoroutine = StartCoroutine(Ping());

        OnEventResponse(evt.Type, evt.Params);
    }

    protected virtual void OnEventResponse(string type, IDictionary data)
    {
        // log all params
        //LogDebugParams(type, data);

        switch (type)
        {
            case "LOG_DEBUG":
                Debug.Log("[SmartFox.Log.Debug] <color=grey>" + data["message"] + "</color>\n");
                break;
            case "LOG_INFO":
                Debug.Log("[SmartFox.Log.Info] <color=blue>" + data["message"] + "</color>\n");
                break;
            case "LOG_WARN":
                Debug.Log("[SmartFox.Log.Warn] <color=yellow>" + data["message"] + "</color>\n");
                break;
            case "LOG_ERROR":
                Debug.Log("[SmartFox.Log.Error] <color=red>" + data["message"] + "</color>\n");
                break;

            case "connectionAttemptHttp":
                break;

            case "connection":
                if (!(bool)data["success"])
                    Debug.Log("[SmartFoxManager.OnEvent] Connection Failed");
                else
                    Debug.Log("[SmartFoxManager.OnEvent] Successfully Connected");
                break;

            case "login":
                Debug.Log("user=" + ((User)data["user"]).Name);
                break;

            case "loginError":
                Debug.Log("Login Error (" + data["errorCode"] + "): " + data["errorMessage"]);
                break;

            case "roomJoin":
                string dt = "[SmartFoxManager.OnEvent] Room Joined -> RVs:\n";
                sfs.LastJoinedRoom.GetVariables().ForEach(rv => dt += rv.Name + " -> " + rv.Value + "\n");
                Debug.Log(dt + "\n");
                break;

            case "roomVariablesUpdate":
                // An array of variable names that were updated for the Room
                var changedVars = data["changedVars"] as ArrayList;

                // log all changed vars
                var debugText = "[SmartFoxManager.OnEvent] ChangedVars: " + changedVars.Count + "\n";
                foreach (string var in changedVars)
                {
                    if (sfs == null || sfs.LastJoinedRoom == null)
                        continue;
                    var rv = GetVar(sfs.LastJoinedRoom, var);
                    if (rv == null)
                        continue;
                    var value = rv.Value;
                    debugText += var + " -> " + (value is SFSObject ? ((SFSObject)value).GetDump() : value) + " as " + value.GetType() + "\n";
                }
                //Debug.Log("[SFSManager.OnEventResponse.roomVariablesUpdate] " + debugText + "\n\n");
                break;

            case "connectionLost":
                Debug.Log("[SmartFoxManager.OnEvent] CONNECTION LOST! reason=" + data["reason"]);
                StartReconnect();
                break;

            case "extensionResponse":
                OnExtensionResponse((string)data["cmd"], (SFSObject)data["params"]);
                break;

            default:
                Debug.Log("[SmartFoxManager.OnEvent] Unknown event type: " + type);
                break;
        }
    }

    protected void LogDebugParams(string type, IDictionary data)
    {
        var debugParams = "Type=" + type + "\n";
        foreach (var key in data.Keys)
            debugParams += key + " -> " + (data[key] is SFSObject ? ((SFSObject)data[key]).GetDump(false) : data[key]) + "\n";
        Debug.Log("[SmartFoxManager.PARAMS] " + debugParams + "\n\n");
    }

    protected virtual void OnExtensionResponse(string cmd, SFSObject obj)
    {
        switch (cmd)
        {
            case "ping":
                break;

            default:
                Debug.Log("[SmartFoxManager.OnEvent] Unknown extension command: " + cmd);
                break;
        }
    }

    // server variables
    public static UserVariable GetVar(User user, string varName)
    {
        var uv = user.GetVariable(varName);
        //if (uv == null)
        //    Debug.Log("[GrowSFS.GetVar] user var is null (blob?) - " + varName);
        return uv;
    }

    public static RoomVariable GetVar(Room room, string varName)
    {
        var uv = room.GetVariable(varName);
        //if (uv == null)
        //    Debug.Log("[GrowSFS.GetVar] room var is null (blob?) - " + varName);
        return uv;
    }

    // ping
    protected Coroutine pingCoroutine = null;

    IEnumerator Ping()
    {
        while (true)
        {
            yield return new WaitForSeconds(pingInterval);
            if (sfs != null && sfs.IsConnected)
                Send("ping");
            else
                StopPing();
        }
    }

    protected void StopPing()
    {
        if (pingCoroutine == null)
            return;
        StopCoroutine(pingCoroutine);
        pingCoroutine = null;
    }

    // reconnect
    Coroutine reconnectRoutine = null;

    private void StartReconnect()
    {
        if (!reconnectActivated)
        {
            Debug.Log("[SFSManager.StartReconnect] Reconnect is not activated.");
            return;
        }

        if (reconnectRoutine != null)
        {
            Debug.Log("[SFSManager.StartReconnect] Reconnect routine is already running!");
            return;
        }

        Debug.Log("[SFSManager.StartReconnect] Starting reconnect routine.");
        reconnectRoutine = StartCoroutine(ReconnectRoutine());
    }

    IEnumerator ReconnectRoutine()
    {
        while (sfs == null || !sfs.IsConnected)
        {
            //if (sfs != null)
            // sfs.Disconnect();
            sfs = null;

            Debug.Log("[SFSManager.ReconnectRoutine." + gameObject.name + "] Trying to reconnect...");
            StartCoroutine(ConnectRoutine());

            yield return new WaitForSeconds(reconnectInterval);
        }
        Debug.Log("[SFSManager.ReconnectRoutine." + gameObject.name + "] RECONNECT FINISHED SUCCESSFULLY");
        reconnectRoutine = null;
    }

    // MonoBehaviour
    protected virtual void Update()
    {
        if (sfs != null)
        {
            sfs.ProcessEvents();
            //if (Input.GetKeyDown(KeyCode.Q))
            //sfs.KillConnection();
        }
    }

    void OnApplicationQuit()
    {
        if (sfs != null && sfs.IsConnected)
        {
            //Debug.Log("[SmartFoxManager.OnApplicationQuit] Desconectando o SFS...");
            sfs.Disconnect();
        }
    }

    // SETTINGS
    [Serializable]
    public class ServerSettings
    {
        public string serverIP;
        public int ServerPort;
        public int ServerPortWebGL;
        public string ZoneName;
    }

    public static IEnumerator LoadJsonFromStreamingAssets<T>(string fileName, Action<T> callback)
    {
        string fullFilePath = System.IO.Path.Combine(Application.streamingAssetsPath, fileName);
        T result = default(T);

#if (UNITY_ANDROID || UNITY_WEBGL) && !UNITY_EDITOR
        WWW www = new WWW(fullFilePath);
        yield return www;
        try
        {
            result = JsonUtility.FromJson<T>(www.text);
        }
        catch (Exception e)
        {
            Debug.LogError("[SFSManager.LoadFromStreamingAssets] Não foi possível carregar do Streaming Assets o arquivo " + fullFilePath);
            Debug.LogError(e);
        }
#else
        if (System.IO.File.Exists(fullFilePath))
            try
            {
                result = JsonUtility.FromJson<T>(System.IO.File.ReadAllText(fullFilePath));
            }
            catch (Exception e)
            {
                Debug.LogError("[SFSManager.LoadFromStreamingAssets] Não foi possível carregar do Streaming Assets o arquivo " + fullFilePath);
                Debug.LogError(e);
            }
        else
            Debug.LogError("[SFSManager.LoadFromStreamingAssets] Não foi possível carregar do Streaming Assets o arquivo " + fullFilePath);
#endif

        callback(result);
        yield return null;
    }
}


