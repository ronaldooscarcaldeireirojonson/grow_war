﻿using Newtonsoft.Json;
using Sfs2X.Entities;
using Sfs2X.Entities.Data;
using Sfs2X.Entities.Variables;
using Sfs2X.Requests;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Grow
{
    public abstract class GrowSFS : SFSManager
    {
        // sfs
        protected List<User> UserList
        {
            get
            {
                try
                {
                    return sfs.LastJoinedRoom.UserList;
                }
                catch (Exception e)
                {
                    //UnityEngine.Debug.Log("[GrowSFS.UserList.Get] " + e);
                    return new List<User>();
                }
            }
        }

        // events
        public event Action<int> OnCountdownUpdate = (a) => { };
        public event Action<User, Color, string> OnPublicMessage = (u, c, m) => { };
        public event Action<List<User>> OnRoomUserListUpdate = (a) => { };

        // abstract
        public abstract Color GetColor(int id);

        // server responses
        protected override void OnEventResponse(string type, IDictionary data)
        {
            User user;

            //SFSManager.LogDebugParams(type, data);
            switch (type)
            {
                case "LOG_ERROR":
                    Debug.Log("[SmartFox.Log.Error] <color=red>" + data["message"] + "</color>\n");
                    //Debug.Log(JsonConvert.SerializeObject(data));
                    break;

                case "loginError":
                    base.OnEventResponse(type, data);

                    if (data["errorCode"].ToString() == "6")
                    {
                        Globals.errorOnLoginUserAlreadyLoggedIn = true;
                    }

                    SFS.DisconnectAndGoToLogin();
                    break;

                case "connectionLost":
                    ServerManager.instance.ShowWaitingAnim("Reconectando...");
                    base.OnEventResponse(type, data);
                    break;

                case "connection":
                    if (!(bool)data["success"])
                    {
                        Debug.Log("[GrowSFS.OnEventResponse] Connection Failed");
                        return;
                    }
                    else
                    {
                        Debug.Log("[GrowSFS.OnEventResponse] Successfully Connected");
                        SFSObject obj = new SFSObject();
                        obj.PutText("Token", Globals.playerToken);
                        sfs.Send(new LoginRequest(Globals.playerNickname, "", settings.ZoneName, obj));
                    }
                    break;

                case "login":
                    ServerManager.instance.HideWaitingAnim();
                    if (MainMenuController.Instance != null)
                        MainMenuController.Instance.ChangeScreen("ScreenMainMenu");
                    SFS.Friends.SetInGameRequest(false);
                    break;

                case "roomJoin":
                    ServerManager.instance.HideWaitingAnim();

                    CancelInvoke("JoinPrivateTimeout");

                    if (MainMenuController.Instance == null)
                        break;

                    var room = (Room)data["room"];
                    if (room.Name.StartsWith("R_"))
                        MainMenuController.Instance.ChangeScreen("ScreenRoom");
                    else if (room.Name.StartsWith("P_"))
                        MainMenuController.Instance.ChangeScreen("ScreenRoomFriends");
                    else
                        Debug.LogError("!!! ERRO !!! GrowSFS - OnEventResponse: nome da sala não começa com R_ ou P_");
                    OnRoomUserListUpdate(UserList);
                    SFS.Friends.SetInGameRequest(true);
                    break;

                case "userEnterRoom":
                    OnRoomUserListUpdate(UserList);
                    break;

                case "userExitRoom":
                    user = (User)data["user"];
                    if (user.IsItMe)
                    {
                        SFS.Friends.SetInGameRequest(false);
                        ServerManager.instance.HideWaitingAnim();
                        if (MainMenuController.Instance == null)
                            SceneManager.instance.ChangeScene("MainMenu");
                        else
                            MainMenuController.Instance.ChangeScreen("ScreenMainMenu");

                        var chat = GameObject.FindWithTag("ChatCanvas");
                        if (chat)
                            Destroy(chat.gameObject);

                        var obj = GameObject.Find("AvatarTopLayer");
                        if (obj)
                            obj.GetComponent<Image>().color = Color.white;
                    }
                    else
                        OnRoomUserListUpdate(UserList);
                    break;

                case "userVariablesUpdate":
                    user = (User)data["user"];
                    List<string> changedVars = (data["changedVars"] as ArrayList).Cast<string>().ToList();

                    if (user.IsItMe)
                    {
                        // vip
                        if (GetVar(user, "vip") != null)
                            Globals.playerIsVIP = GetVar(user, "vip").GetBoolValue();

                        // coins
                        if (GetVar(user, "coins") != null)
                        {
                            // Verifica se alteramos o valor "falsamente"
                            if (Globals.hasTakenCoinsFalsely)
                            {
                                // Se mudou o valor 
                                if (Globals.playerCoins + Globals.gameQuickplayCost != GetVar(user, "coins").GetIntValue())
                                {
                                    // Atualiza os valores porém com o desconto
                                    Globals.playerCoins = GetVar(user, "coins").GetIntValue() - Globals.gameQuickplayCost;
                                }
                            }
                            else
                            {
                                // Se tiver bonus diário
                                if (GameObject.FindObjectOfType<PopupDailyBonus>())
                                {
                                    Globals.playerCoins = GetVar(user, "coins").GetIntValue() - (int)Globals.gameDailyBonusEnergies[Globals.gameDailyBonusDays - 1];
                                }
                                else
                                {
                                    Globals.playerCoins = GetVar(user, "coins").GetIntValue();
                                }
                            }

                        }
                    }
                    break;

                case "publicMessage":
                    user = (User)data["sender"];
                    OnPublicMessage(user, GetColor(GetVar(user, "color").GetIntValue()), data["message"].ToString());
                    break;

                default:
                    base.OnEventResponse(type, data);
                    break;
            }
        }

        protected override void OnExtensionResponse(string cmd, SFSObject obj)
        {
            // debug log
            var logMessage = "[GrowSFS.OnExtensionResponse] cmd -> " + cmd + "\nPARAMS START";
            if (obj == null)
                obj = new SFSObject();
            else
                foreach (var key in obj.GetKeys())
                    logMessage += "\n" + key + " -> " + obj.GetValue<object>(key);
            logMessage += "\nPARAMS END\n";

            switch (cmd)
            {
                case "cdu":
                    var cd = obj.GetInt("cd");
                    OnCountdownUpdate(cd);
                    break;

                case "canReconnect":
                    var roomName = obj.GetUtfString("roomName");
                    Debug.Log("[GrowSFS.OnExtensionResponse.canReconnect] " + roomName);
                    var popup = PopupManager.instance.OpenPopup("PopupMessage");
                    popup.GetComponent<PopupMessage>().Configure("RECONECTAR", "Você tem uma partida em andamento.\nDeseja se reconectar?",
                        () => { AcceptReconnect(roomName); PopupManager.instance.ClosePopup(); },
                        () => { DeclineReconnect(roomName); PopupManager.instance.ClosePopup(); });
                    break;

                case "cantReconnect":
                    Debug.Log("[GrowSFS.OnExtensionResponse.cantReconnect] ");
                    var loading = GameObject.FindWithTag("CanvasWaiting");
                    Destroy(loading);
                    GameObject popupError = PopupManager.instance.OpenPopup("PopupMessage");
                    popupError.GetComponent<PopupMessage>().Configure("RECONECTAR", "Essa partida já acabou, não é possível se reconectar.",
                        () =>
                        {
                            PopupManager.instance.ClosePopup();
                        });
                    break;

                case "errorJoinRoom":
                    ServerManager.instance.HideWaitingAnim();
                    // NOTE: Provavelmente esse if não é necessário
                    if (MainMenuController.Instance)
                    {
                        MainMenuController.Instance.ShowPopupMessageRoomEnterError();
                    }
                    break;

                case "au": // achievement unlocked
                    AchievementManager.Show(obj.GetInt("id"));
                    break;

                case "clanInvite":
                    Header.HAS_CLAN_ALERT = true;
                    var header = FindObjectOfType<Header>();
                    if (header)
                        header.alertClan.SetActive(true);
                    break;

                default:
                    base.OnExtensionResponse(cmd, obj);
                    break;
            }
        }

        // client requests
        public void PlayRankedRequest()
        {
            ServerManager.instance.ShowWaitingAnim();
            Send("playRanked");
        }

        public void CreatePrivateRequest()
        {
            ServerManager.instance.ShowWaitingAnim();
            Send("createPrivate");
        }

        public void JoinPrivateRequest(string roomName)
        {
            ServerManager.instance.ShowWaitingAnim();
            SFSObject obj = new SFSObject();
            obj.PutUtfString("r", roomName);
            Send("joinPrivate", null, obj);
            Invoke("JoinPrivateTimeout", 5);
        }

        public void JoinPrivateTimeout()
        {
            ServerManager.instance.HideWaitingAnim();
            PopupManager.instance.OpenPopup("PopupMessage")
                .GetComponent<PopupMessage>()
                .Configure("ERRO", "Não é possível entrar na sala pois ela não existe mais, está cheia ou a partida já começou.", () => PopupManager.instance.ClosePopup());
        }

        public void ExitRoomRequest()
        {
            ServerManager.instance.ShowWaitingAnim();
            Send("exitRoom");
        }

        public void PlayPrivateRequest()
        {
            ServerManager.instance.ShowWaitingAnim();
            Send("start", sfs.LastJoinedRoom);
        }

        public void KickPrivateRequest(int growId)
        {
            SFSObject obj = new SFSObject();
            obj.PutInt("p", growId);
            Send("kick", sfs.LastJoinedRoom, obj);
        }

        public void AcceptReconnect(string roomName)
        {
            ServerManager.instance.ShowWaitingAnim();
            SFSObject obj = new SFSObject();
            obj.PutUtfString("r", roomName);
            Send("acceptReconnect", null, obj);
        }

        public void DeclineReconnect(string roomName)
        {
            SFSObject obj = new SFSObject();
            obj.PutUtfString("r", roomName);
            Send("declineReconnect", null, obj);
        }

        public void SetAvatar(string avatar)
        {
            //sfs.MySelf.SetVariable(new SFSUserVariable("avatar", avatar));
            sfs.Send(new SetUserVariablesRequest(new List<UserVariable>() { new SFSUserVariable("avatar", avatar) }));
        }

        public void PublicMessageRequest(string message)
        {
            sfs.Send(new PublicMessageRequest(message));
        }

        public void AbandonMatchRequest()
        {
            Send("abandon");
        }

        public void ClanInviteRequest(string invitedName)
        {
            SFSObject obj = new SFSObject();
            obj.PutUtfString("n", invitedName);
            Send("clanInvite", null, obj);
        }
    }
}