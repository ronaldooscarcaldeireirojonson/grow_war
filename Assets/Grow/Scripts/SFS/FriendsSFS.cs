﻿using Sfs2X.Entities;
using Sfs2X.Entities.Data;
using Sfs2X.Entities.Variables;
using Sfs2X.Requests.Buddylist;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Grow
{
    public class FriendsSFS : SFSManager
    {
        public void UpdateList()
        {
            OnBuddyListUpdate();
        }

        public event Action OnBuddyListUpdate = () =>
        {
            /*var m = "[FriendsSFS.OnBuddyListUpdate] BuddyList.Count=" + SFS.Friends.BuddyList.Count +
                " | invitesSent.Count=" + SFS.Friends.invitesSent.Count +
                " | invitesReceived.Count=" + SFS.Friends.invitesReceived.Count;
            SFS.Friends.BuddyList.ForEach(b => m += "\n" + b.Name
            + " | IsOnline=" + b.IsOnline
            + " | Id=" + b.Id
            + " | game=" + (!b.IsOnline ? "[offline]" : GetVar(b, "game") ?? "[null]")
            + " | inGame=" + (!b.IsOnline ? "[offline]" : GetVar(b, "inGame") ?? "[null]")
            + " | $idGrow=" + (GetVar(b, "$idGrow") ?? "[null]"));

            m += "\n\ninvitesSent: ";
            foreach (var key in SFS.Friends.invitesSent.Keys)
                m += key + ", ";
            m += "\ninvitesReceived: ";
            foreach (var key in SFS.Friends.invitesReceived.Keys)
                m += key + ", ";
            Debug.Log(m + "\n");*/
        };

        public List<Buddy> BuddyList { get { return sfs.BuddyManager.BuddyList; } }

        public Dictionary<int, ISFSObject> invitesSent = new Dictionary<int, ISFSObject>();
        public Dictionary<int, ISFSObject> invitesReceived = new Dictionary<int, ISFSObject>();

        bool isInGameTemp = false;

        // server responses
        protected override void OnEventResponse(string type, IDictionary data)
        {
            //SFSManager.LogDebugParams(type, data);
            switch (type)
            {
                case "connection":
                    if (!(bool)data["success"])
                    {
                        Debug.Log("[FriendsSFS.OnEventResponse] Connection Failed");
                        return;
                    }
                    else
                    {
                        //Debug.Log("[FriendsSFS.OnEventResponse] Successfully Connected");
                        //SFSObject obj = new SFSObject();
                        //obj.PutText("Token", Globals.playerToken);
                        sfs.Send(new Sfs2X.Requests.LoginRequest(Globals.playerNickname, "", settings.ZoneName, null));
                    }
                    break;

                case "login":
                    //Debug.Log("[FriendSFS.OnEventResponse] LOGIN");
                    sfs.Send(new InitBuddyListRequest());
                    break;

                // buddy
                case "buddyError":
                    Debug.Log("[FriendSFS.OnEventResponse(buddyError)] " + data["errorMessage"]);
                    break;

                case "buddyMessage":
                    bool isItMe = (bool)data["isItMe"];
                    Buddy b = (Buddy)data["buddy"];
                    //Debug.Log("[FriendSFS.OnEventResponse(buddyMessage)] " + (b == null ? "[null]" : b.Name) + "(isItMe=" + isItMe + "): " + data["message"]);
                    break;

                case "buddyListInit":
                    //Debug.Log("[FriendSFS.OnEventResponse] " + type + " received");
                    OnBuddyListUpdate();
                    sfs.Send(new SetBuddyVariablesRequest(new List<BuddyVariable>() { new SFSBuddyVariable("game", SFS.GameName), new SFSBuddyVariable("inGame", isInGameTemp) }));
                    break;

                case "buddyAdd":
                case "buddyRemove":
                    //Debug.Log("[FriendSFS.OnEventResponse] " + type + " received");
                    var varObj = GetVar(((Buddy)data["buddy"]), "$idGrow");
                    if (varObj == null)
                    {
                        Debug.Log("[FriendsSFS.buddyRemove] budddy var is null (blob?)");
                        break;
                    }
                    var id = (int)varObj;
                    invitesSent.Remove(id);
                    invitesReceived.Remove(id);
                    OnBuddyListUpdate();
                    break;

                case "buddyBlock":
                case "buddyOnlineStateUpdate":
                case "buddyOnlineStateChange":
                case "buddyVariablesUpdate":
                    //Debug.Log("[FriendSFS.OnEventResponse] " + type + " received");
                    OnBuddyListUpdate();
                    break;

                default:
                    base.OnEventResponse(type, data);
                    break;
            }
        }

        protected override void OnExtensionResponse(string cmd, SFSObject obj)
        {
            switch (cmd)
            {
                case "init":
                    //Debug.Log("[FriendsSFS.OnExtensionResponse.init]");
                    invitesSent = new Dictionary<int, ISFSObject>();
                    invitesReceived = new Dictionary<int, ISFSObject>();

                    var sentList = obj.GetSFSArray("s");
                    if (sentList == null)
                        Debug.Log("[FriendsSFS.OnExtensionResponse.init] A sentList que veio do server está nula");
                    else
                        for (int i = 0; i < sentList.Size(); ++i)
                            invitesSent.Add(sentList.GetSFSObject(i).GetInt("i"), sentList.GetSFSObject(i));

                    var receivedList = obj.GetSFSArray("r");
                    if (receivedList == null)
                        Debug.Log("[FriendsSFS.OnExtensionResponse.init] A receivedList que veio do server está nula");
                    else
                        for (int i = 0; i < receivedList.Size(); ++i)
                            invitesReceived.Add(receivedList.GetSFSObject(i).GetInt("i"), receivedList.GetSFSObject(i));

                    break;

                case "is": // invite sent
                    invitesSent[obj.GetInt("i")] = obj;
                    OnBuddyListUpdate();
                    break;

                case "ir": // invite received
                    invitesReceived[obj.GetInt("i")] = obj;
                    OnBuddyListUpdate();
                    break;

                case "r":
                    var id = obj.GetInt("i");
                    invitesSent.Remove(id);
                    invitesReceived.Remove(id);
                    OnBuddyListUpdate();
                    break;

                case "invite":
                    string buddyName = obj.GetUtfString("b");
                    string roomName = obj.GetUtfString("r");

                    // Faz aparecer popup de convite para jogar
                    GameObject popup = PopupManager.instance.OpenPopup("PopupMessage");
                    popup.GetComponent<PopupMessage>().Configure("QUER JOGAR?", buddyName + " desafiou você para uma partida. Você aceita?",
                        () =>
                        {
                            // Responde sim
                            SFS.Grow.JoinPrivateRequest(roomName);
                            AnswerRequest(buddyName);

                            PopupManager.instance.ClosePopup();
                        },
                        () =>
                        {
                            // Responde não
                            AnswerRequest(buddyName);

                            PopupManager.instance.ClosePopup();
                        });
                    break;

                case "answer":
                    int buddyIdGrow = obj.GetInt("i");
                    //Debug.Log("Answer: " + buddyIdGrow);
                    // TODO: tira o spinner do convite do amigo "i"
                    if (PanelFriendsPlay.instance != null)
                    {
                        PanelFriendsPlay.instance.UpdatePlayerRequest(buddyIdGrow);
                    }
                    break;

                case "e":
                    //Debug.Log("[FriendsSFS.OnExtensionResponse.ERROR] " + obj.GetUtfString("e"));

                    GameObject popupMessage = PopupManager.instance.OpenPopup("PopupMessage");
                    popupMessage.GetComponent<PopupMessage>().Configure("ERRO", obj.GetUtfString("e"),
                        () =>
                        {
                            PopupManager.instance.ClosePopup();
                        });
                    break;

                default:
                    base.OnExtensionResponse(cmd, obj);
                    break;
            }
        }

        // client requests
        public void SetInGameRequest(bool isInGame)
        {
            if (isInGameTemp == isInGame)
                return;
            isInGameTemp = isInGame;
            if (sfs != null && sfs.IsConnected)
                sfs.Send(new SetBuddyVariablesRequest(new List<BuddyVariable>() { new SFSBuddyVariable("inGame", isInGameTemp) }));
        }

        public void AddRequest(string buddyName)
        {
            var obj = SFSObject.NewInstance();
            obj.PutUtfString("n", buddyName);
            Send("add", null, obj);
        }

        public void RemoveRequest(string buddyName)
        {
            var obj = SFSObject.NewInstance();
            obj.PutUtfString("n", buddyName);
            Send("remove", null, obj);
        }

        public void InviteRequest(string buddyName)
        {
            var obj = SFSObject.NewInstance();
            obj.PutUtfString("n", buddyName);
            obj.PutUtfString("r", SFS.Grow.sfs.LastJoinedRoom.Name);
            Send("invite", null, obj);
        }

        public void AnswerRequest(string buddyName)
        {
            var obj = SFSObject.NewInstance();
            obj.PutUtfString("n", buddyName);
            obj.PutInt("i", Globals.playerIdGrowGames);
            Send("answer", null, obj);
        }

        // static helper
        public static object GetVar(Buddy b, string varName)
        {
            var v = b.GetVariable(varName);

            if (v == null || v.IsNull())
            {
                //Debug.LogError("[FriendsSFS.GetVar] GetVar is null (blob?) - " + varName);
                return null;
            }
            return v.Value;
        }


        /// <summary>
        /// Função que indica se o amigo com o tal id é amigo do usuário
        /// </summary>
        public bool IsFriend(int idGrow)
        {
            // Percorre toda a buddyList primeiro
            for (int i = 0; i < BuddyList.Count; i++)
            {
                object id = GetVar(BuddyList[i], "$idGrow");

                if (id != null)
                {
                    // Se o id for igual
                    if ((int)id == idGrow)
                    {
                        return true;
                    }
                }
            }

            // Percorre a lista de convites enviados
            foreach (int buddyId in invitesSent.Keys)
            {
                if (buddyId == idGrow)
                {
                    return true;
                }
            }

            // Percorre a lista de convites recebidos
            foreach (int buddyId in invitesReceived.Keys)
            {
                if (buddyId == idGrow)
                {
                    return true;
                }
            }

            return false;
        }
    }
}