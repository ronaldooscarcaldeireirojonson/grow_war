﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemProfileInfoStats : MonoBehaviour
{

    public Text textTitle;
    public Text textAmount;



    /// <summary>
    /// Função para configurar o item
    /// </summary>
    public void Configure(string title, string amount)
    {
        textTitle.text = title;
        textAmount.text = amount;
    }
}
