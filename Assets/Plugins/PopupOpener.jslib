﻿var PopupOpenerPlugin = {

    PopupOpenerCaptureClick: function (link) 
	{
		var url = Pointer_stringify(link);
		document.onmouseup = function ()
		{
			window.open(url, null);
			document.onmouseup = null;
		}
    }
};
mergeInto(LibraryManager.library, PopupOpenerPlugin);