function UnityProgress(gameInstance, progress) {
	
	if (!gameInstance.Module)
		return;
	
	// Coloca o logo
	if (!gameInstance.logo) {
		gameInstance.logo = document.createElement("div");
		gameInstance.logo.className = "logo " + gameInstance.Module.splashScreenStyle;
		gameInstance.container.appendChild(gameInstance.logo);
	}
	
	// Coloca a barra de progresso
	if (!gameInstance.progress) {    
		gameInstance.progress = document.createElement("div");
		gameInstance.progress.className = "progress " + gameInstance.Module.splashScreenStyle;
		gameInstance.progress.empty = document.createElement("div");
		gameInstance.progress.empty.className = "empty";
		gameInstance.progress.appendChild(gameInstance.progress.empty);
		gameInstance.progress.full = document.createElement("div");
		gameInstance.progress.full.className = "full";
		gameInstance.progress.appendChild(gameInstance.progress.full);
		gameInstance.container.appendChild(gameInstance.progress);
	}
	
	// Atualiza a barra de progresso
	gameInstance.progress.full.style.width = (100 * progress) + "%";
	gameInstance.progress.empty.style.width = (100 * (1 - progress)) + "%";
	
	// NOTE: Da Unity 2017.2 para 2017.4 houve uma alteração na ordem das chamadas desses progress abaixo, por isso resolvemos colocar o mesmo código para ambos
	// NOTE: Se quiser ver a alteração/diferença, ver no SVN esse arquivo antes da data 03/07/2018
	
	// Se o download acabar e se não tiver o spinner
	if (progress >= 0.7 && !gameInstance.spinner) {
					
		// Remove o logo e a barra de progresso
		gameInstance.logo.style.display = "none";
		gameInstance.progress.style.display = "none";
		
		// Coloca o spinner
		var spinnerContainer = document.createElement("div")
		spinnerContainer.className = "spinnerContainer";		
		var spinner = document.createElement("div");
		spinner.className = "spinner " + gameInstance.Module.splashScreenStyle;
		spinnerContainer.appendChild(spinner);
		gameInstance.spinner = spinnerContainer;			
		gameInstance.container.appendChild(gameInstance.spinner);
	}
	
	
	// Se o jogo estiver totalmente carregado e pronto para ser chamado
	if (progress == 1)
	{
		gameInstance.spinner.style.display = "none";
	}
}