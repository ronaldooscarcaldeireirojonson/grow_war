﻿#if SIOUX_ADB
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

namespace Sioux.ADB
{
    public class ADBTools
    {
        public string feedbackText = "";

        [MenuItem("Sioux Tools/ADB/List Devices")]
        public static void ListDevices()
        {
            Debug.Log(ExecuteCommand("adb devices"));
        }

        [MenuItem("Sioux Tools/ADB/Logcat/Logcat on Console")]
        public static void Logcat()
        {
            System.Diagnostics.Process.Start("CMD.exe", "/C adb logcat -s Unity ActivityManager PackageManager dalvikvm DEBUG");
        }
        [MenuItem("Sioux Tools/ADB/Logcat/Clear")]
        public static void LogcatClear()
        {
            Debug.Log(ExecuteCommand("adb logcat -c"));
        }
        [MenuItem("Sioux Tools/ADB/Logcat/Logcat to File")]
        public static void LogcatFile()
        {
            string filePath = EditorUtility.SaveFilePanel("Select log path ", Application.dataPath, "LOG_" + System.DateTime.Now.ToString().Replace('/', '_').Replace(':', '_').Replace(' ', '_'), "txt");
            File.WriteAllText(filePath, "");
            if (File.Exists(filePath))
                Debug.Log(ExecuteCommand("adb logcat -s Unity ActivityManager PackageManager dalvikvm DEBUG - d > " + filePath));
        }

        [MenuItem("Sioux Tools/ADB/Delete Package")]
        public static void DeletePackage()
        {
            Debug.Log(ExecuteCommand("adb uninstall " + PlayerSettings.applicationIdentifier));
        }


        [MenuItem("Sioux Tools/ADB/Install Package")]
        public static void InstallPackage()
        {
            var apk = EditorUtility.OpenFilePanel("Select an APK file", Application.dataPath, "apk");
            Debug.Log(ExecuteCommand("adb install " + apk));
        }
        //static void Init()
        //{
        //    EditorWindow.GetWindow(typeof(ADBTools)).Show();
        //}

        //private void OnGUI()
        //{

        //    if (GUILayout.Button("Device List"))
        //    {
        //        feedbackText = ExecuteCommand("adb devices");
        //    }

        //    EditorGUILayout.LabelField(feedbackText);

        //}


        /// <summary>
        /// Executes a shell command synchronously.
        /// </summary>
        /// <param name="command">string command</param>
        /// <returns>string, as output of the command.</returns>
        private static string ExecuteCommand(string command)
        {
            try
            {
                Debug.Log("Running command: " + command);
                // create the ProcessStartInfo using "cmd" as the program to be run,
                // and "/c " as the parameters.
                // Incidentally, /c tells cmd that we want it to execute the command that follows,
                // and then exit.
                System.Diagnostics.ProcessStartInfo procStartInfo =
                    new System.Diagnostics.ProcessStartInfo("cmd", "/c " + command);
                // The following commands are needed to redirect the standard output.
                // This means that it will be redirected to the Process.StandardOutput StreamReader.
                procStartInfo.RedirectStandardOutput = true;
                procStartInfo.RedirectStandardError = true;
                procStartInfo.UseShellExecute = false;
                // Do not create the black window.
                procStartInfo.CreateNoWindow = true;
                // Now we create a process, assign its ProcessStartInfo and start it
                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.StartInfo = procStartInfo;
                proc.Start();
                // Get the output into a string
                proc.WaitForExit();
                string result = proc.StandardOutput.ReadToEnd();
                string error = proc.StandardError.ReadToEnd();
                // Display the command output.
                if (!string.IsNullOrEmpty(error))
                    Debug.Log(result + "\n" + error);
                return result;
            }
            catch (System.Exception e)
            {
                // Log the exception
                Debug.Log("Got exception: " + e.Message);
            }
            return "";
        }
    }
}
#endif