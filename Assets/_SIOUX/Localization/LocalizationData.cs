﻿#if SIOUX_LOCALIZATION
using System.Collections.Generic;

namespace Sioux.Localization
{
    [System.Serializable]
    public class LocalizationData
    {
        public List<LocalizationItem> items;
    }

    [System.Serializable]
    public class LocalizationItem
    {
        public string key;
        public string value;
    }
}
#endif