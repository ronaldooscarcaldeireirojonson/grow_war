﻿#if SIOUX_LOCALIZATION
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Sioux.Localization
{
    public class LocalizedText : MonoBehaviour
    {

        public string key;
        Text targetText;

        void Start()
        {
            ApplyText();
        }

        public void ApplyText()
        {
            targetText = GetComponent<Text>();
            targetText.text = Localization.GetValue(key);
        }

    }
}
#endif