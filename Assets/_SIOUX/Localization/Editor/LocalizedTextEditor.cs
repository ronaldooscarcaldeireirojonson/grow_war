﻿#if SIOUX_LOCALIZATION
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

namespace Sioux.Localization
{
    [CustomEditor(typeof(LocalizedText))]
    public class LocalizedTextEditor : Editor
    {
        string fileName = "";
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            LocalizedText myTarget = (LocalizedText)target;

            if (GUILayout.Button("Load"))
            {
                string filePath = EditorUtility.OpenFilePanel("Select localization data file", Application.streamingAssetsPath + "/Localization", "json");
                
                if (!string.IsNullOrEmpty(filePath))
                {
                    fileName = new FileInfo(filePath).Name;
                    Localization.LoadLocalizedText(fileName);
                    Repaint();
                }
            }

            EditorGUILayout.LabelField("File: ", fileName==""?"No File Loaded": fileName);

            myTarget.key = EditorGUILayout.TextField("Key", myTarget.key);
            
            if (Localization.HasKey(myTarget.key))
            {
                EditorGUILayout.LabelField(Localization.GetValue(myTarget.key), EditorStyles.boldLabel);
                if (GUILayout.Button("Preview Text"))
                {
                    myTarget.ApplyText();
                }
                
            }
            else if(fileName!="")
            {
                GUIStyle errorStyle = new GUIStyle(EditorStyles.boldLabel);
                errorStyle.normal.textColor = Color.red;
                EditorGUILayout.LabelField("Invalid Key",  errorStyle);
            }
			
            serializedObject.ApplyModifiedProperties();
            EditorUtility.SetDirty(myTarget);
        }

    }
}
#endif