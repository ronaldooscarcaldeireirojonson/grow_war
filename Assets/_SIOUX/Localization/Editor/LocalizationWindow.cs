﻿#if SIOUX_LOCALIZATION
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Linq;

namespace Sioux.Localization
{
    public class LocalizationWindow : EditorWindow
    {
        public LocalizationData localizationData;
        public string feedbackText = "";

        [MenuItem("Window/Sioux/Localization Editor")]
        static void Init()
        {
            EditorWindow.GetWindow(typeof(LocalizationWindow)).Show();
        }

        private void OnGUI()
        {

            if (GUILayout.Button("Load file"))
            {
                LoadLocalizationFile();
            }

            if (GUILayout.Button("Create new file"))
            {
                CreateNewLocalizationFile();
            }
            if (localizationData != null)
            {

                if (GUILayout.Button("Import keys from other file"))
                {
                    ImportKeysFromOtherFile();
                }
                if (GUILayout.Button("Save file as..."))
                {
                    SaveLocalizationFile();
                }
                EditorGUILayout.LabelField(feedbackText);
                SerializedObject serializedObject = new SerializedObject(this);
                SerializedProperty serializedProperty = serializedObject.FindProperty("localizationData");
                EditorGUILayout.PropertyField(serializedProperty, true);
                serializedObject.ApplyModifiedProperties();


            }

        }

        private void LoadLocalizationFile()
        {
            string filePath = EditorUtility.OpenFilePanel("Select localization data file", Application.streamingAssetsPath + "/Localization", "json");

            if (!string.IsNullOrEmpty(filePath))
            {
                string dataAsJson = File.ReadAllText(filePath);

                localizationData = JsonUtility.FromJson<LocalizationData>(dataAsJson);
                feedbackText = "Loaded file from " + filePath;

                Debug.Log("Data loaded, dictionary contains: " + localizationData.items.Count + " entries");
            }
        }

        private void SaveLocalizationFile()
        {
            string filePath = EditorUtility.SaveFilePanel("Save localization data file", Application.streamingAssetsPath + "/Localization", "lang_PT", "json");

            if (!string.IsNullOrEmpty(filePath) && !filePath.Contains("_"))
            {
                feedbackText = "File not saved";
                EditorUtility.DisplayDialog("Invalid file name", "Please use _ to define the file language, eg: lang_PT.json", "Close");
            }
            else
            {
                string dataAsJson = JsonUtility.ToJson(localizationData);
                File.WriteAllText(filePath, dataAsJson);

                feedbackText = "Saved file in " + filePath;
            }
        }

        private void CreateNewLocalizationFile()
        {
            localizationData = new LocalizationData();
        }

        private void ImportKeysFromOtherFile()
        {

            Debug.Log("Não Implementado");

            //string filePath = EditorUtility.OpenFilePanel("Select localization data file", Application.streamingAssetsPath+"/Localization", "json");

            //if (!string.IsNullOrEmpty(filePath))
            //{
            //    string dataAsJson = File.ReadAllText(filePath);

            //    var tempLocalizationData = JsonUtility.FromJson<LocalizationData>(dataAsJson);
            //    var count = 0;
            //    foreach(LocalizationItem importedItem in tempLocalizationData.items)
            //    {
            //        Debug.Log(1);
            //        if(localizationData.items.First(item=>item.key == importedItem.key) == null)
            //        {
            //            Debug.Log(2);

            //            localizationData.items.Add(importedItem);
            //            count++;
            //        }
            //    }
            //    feedbackText = count + " keys imported";
            //}
        }

    }
}
#endif