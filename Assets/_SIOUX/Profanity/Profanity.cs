﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

namespace Sioux
{
    public static class Profanity
    {
        // private
        const char codeChar = '*';
        static bool initialized = false;

        static string RemoveDiacritics(string text)
        {
            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
                if (System.Globalization.CharUnicodeInfo.GetUnicodeCategory(c) != System.Globalization.UnicodeCategory.NonSpacingMark)
                    stringBuilder.Append(c);

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }

        static string SoftRegex(string word)
        {
            return @"(\b|[^a-z])" + word + "($|[^a-z])";
        }

        static string Coded(int count)
        {
            var code = "";
            for (int i = 0; i < count; ++i)
                code += codeChar;
            return code;
        }

        static bool Check(string s, bool isSoft)
        {
            s = RemoveDiacritics(s.ToLower());
            foreach (string profanity in isSoft ? softProfanityWords : hardProfanityWords)
                if (Regex.IsMatch(s, isSoft ? SoftRegex(profanity) : profanity, RegexOptions.IgnoreCase))
                    return true;
            return false;
        }

        static string Replace(string s, bool isSoft)
        {
            var diacriticString = RemoveDiacritics(s.ToLower());

            foreach (string profanity in isSoft ? softProfanityWords : hardProfanityWords)
                diacriticString = Regex.Replace(diacriticString, isSoft ? SoftRegex(profanity) : profanity,
                    new MatchEvaluator(m => m.Value.Replace(profanity, Coded(profanity.Length))),
                     RegexOptions.IgnoreCase);

            for (int i = 0; i < diacriticString.Length; ++i)
                if (diacriticString[i] == codeChar)
                    s = s.Remove(i, 1).Insert(i, codeChar.ToString());

            return s;
        }

        static void RemoteSettingsUpdated()
        {
            Debug.Log("[Sioux.Profanity.RemoteSettingsUpdated]");
            softProfanityWords = RemoteSettings.GetString("soft_profanity_words").Split(',').ToList();
            softProfanityWords.RemoveAll(s => string.IsNullOrEmpty(s));
            hardProfanityWords = (RemoteSettings.GetString("hard_profanity_words1") + "," + RemoteSettings.GetString("hard_profanity_words2")).Split(',').ToList();
            hardProfanityWords.RemoveAll(s => string.IsNullOrEmpty(s));
        }

        // public
        public static void Init()
        {
            if (initialized)
                return;
            initialized = true;
            RemoteSettings.Updated += RemoteSettingsUpdated;
        }

        public static bool Check(string s)
        {
            return Check(s, true) || Check(s, false);
        }

        public static string Replace(string s)
        {
            return Replace(Replace(s, true), false);
        }

        // regex: checagem leve, ou seja, palavras isoladas
        static List<string> softProfanityWords = new List<string> {
            "anus",
            "bagos",
            "bicha",
            "bixa",
            "caga",
            "cu",
            "fag",
            "grelo",
            "naba",
            "paca",
            "pau",
            "peia",
            "pemba",
            "pica",
            "picao",
            "piru",
            "porra",
            "puta",
            "puto",
            "rabao",
            "rabo",
            "racha",
            "rola",
            "rosca",
            "viada",
            "viado",
            "xota",
            "xana",
            "ppk",
            "pepk",
        };

        // contains: checagem pesada, ou seja, palavras contidas dentro de outras palavras
        static List<string> hardProfanityWords = new List<string> {
            "babaca",
            "baitola",
            "biscate",
            "boceta",
            "bosseta",
            "boiola",
            "boquete",
            "bosta",
            "brioco",
            "bronha",
            "buceta",
            "bunda",
            "bunduda",
            "burra",
            "burro",
            "busseta",
            "cagada",
            "cagado",
            "cagao",
            "cagona",
            "canalha",
            "caralho",
            "casseta",
            "cassete",
            "checheca",
            "chereca",
            "chibumba",
            "chibumbo",
            "chifruda",
            "chifrudo",
            "chota",
            "chochota",
            "cocaina",
            "corna",
            "corno",
            "cornuda",
            "cornudo",
            "cretina",
            "cretino",
            "culhao",
            "culhoes",
            "curalho",
            "cusao",
            "cuzao",
            "cuzuda",
            "cuzudo",
            "debiloide",
            "escrota",
            "escroto",
            "esporrada",
            "esporrado",
            "estupida",
            "estupido",
            "fedorenta",
            "fedorento",
            "felacao",
            "foda",
            "fode",
            "fodida",
            "fodido",
            "fornica",
            "fornicacao",
            "fudendo",
            "fudecao",
            "fudida",
            "fudido",
            "furnica",
            "furnicar",
            "grelinho",
            "imbecil",
            "iscrota",
            "iscroto",
            "leprosa",
            "leproso",
            "machona",
            "machorra",
            "manguaca",
            "masturba",
            "masturbo",
            "merda",
            "mocrea",
            "mocreia",
            "olhota",
            "otaria",
            "otario",
            "paspalha",
            "paspalhao",
            "paspalho",
            "peido",
            "pepeca",
            "penis",
            "pentelha",
            "pentelho",
            "perereca",
            "pilantra",
            "piranha",
            "piroca",
            "piroco",
            "prostibulo",
            "prostituta",
            "prostituto",
            "punheta",
            "punhetao",
            "punhetinha",
            "pustula",
            "pqp",
            "puxa-saco",
            "puxasaco",
            "rabuda",
            "rabudao",
            "rabudo",
            "rabudona",
            "retardada",
            "retardado",
            "ridicula",
            "ridiculo",
            "sacanagem",
            "sacana",
            "safada",
            "safado",
            "sapata",
            "sapatao",
            "siririca",
            "tarada",
            "tarado",
            "tezao",
            "tezuda",
            "tezudo",
            "trocha",
            "trolha",
            "troucha",
            "trouxa",
            "troxa",
            "vadia",
            "vagaba",
            "vagabunda",
            "vagabundo",
            "vagina",
            "viadao",
            "viadinho",
            "veadinho",
            "xavasca",
            "xerereca",
            "xexeca",
            "xibiu",
            "xibumba",
            "xochota",
            "xoxota",
            "xaninha",
            "pepeka",
            "fdp",
            "disgraçado",
            "disgraçada",
            "desgraçado",
            "desgraçada",
        };
    }
}
