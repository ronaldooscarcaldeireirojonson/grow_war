﻿#if SIOUX_STORAGE
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System;

namespace Sioux
{
    public static class LocalData
    {
        // private
        const string FILE_NAME = "localdata.sav";
        static Dictionary<string, object> _storage;
        static Dictionary<string, object> storage
        {
            get
            {
                if (_storage == null)
                    _storage = Storage.Exists(FILE_NAME) ? Storage.Load<Dictionary<string, object>>(FILE_NAME) : new Dictionary<string, object>();
                return _storage;
            }
        }

        static void Save()
        {
            Storage.Save(FILE_NAME, storage);
        }

#if UNITY_EDITOR
        [MenuItem("Sioux Tools/Local Data/Log All")]
        private static void LogAll()
        {
            string s = "LOCAL DATA\n";
            s += storage == null ? "[null]" : storage.Count == 0 ? "[empty]" : "(clique aqui para ver todos os valores)\n";
            storage.Keys.ToList().ForEach(key => s += key + " -> " + storage[key] + "\n");
            Debug.Log(s);
        }
#endif

        // public
        public static T Get<T>(string key)
        {
            try
            {
                return (T)storage[key];
            }
            catch (Exception e)
            {
                Debug.LogError("[Sioux.LocalData.Get(" + key + ")] " + e);
                return default(T);
            }
        }

        public static void Set(string key, object value)
        {
            if (HasKey(key))
                storage[key] = value;
            else
                storage.Add(key, value);
            Save();
        }

        public static bool HasKey(string key)
        {
            return storage.ContainsKey(key);
        }

#if UNITY_EDITOR
        [MenuItem("Sioux Tools/Local Data/Delete All")]
#endif
        public static void DeleteAll()
        {
            Storage.Delete(FILE_NAME);
            _storage = null;
            Debug.LogError("[Sioux.LocalData.DeleteAll] Arquivo deletado com sucesso: " + Storage.GetPersistentPath(FILE_NAME));
        }

        public static void DeleteKeya(string key)
        {
            storage.Remove(key);
            Save();
        }
    }
}
#endif