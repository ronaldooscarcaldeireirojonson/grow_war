﻿#if SIOUX_STORAGE
#if UNITY_EDITOR
using UnityEditor;
#endif
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace Sioux
{
    public enum FormatType
    {
        Binary,
        Json,
        PlayerPrefs
    }

    public static class Storage
    {
        public static void Save(string fileName, object obj, byte[] bytes = null, FormatType type = FormatType.Binary)
        {
            switch (type)
            {
                case FormatType.Binary:
                    BinaryFormatter bf = new BinaryFormatter();
                    FileStream fileStream = File.Create(GetPersistentPath(fileName));
                    if (bytes == null)
                        bf.Serialize(fileStream, obj);
                    else
                        fileStream.Write(bytes, 0, bytes.Length);
                    fileStream.Close();
                    break;

                case FormatType.Json:
                    // http://stackoverflow.com/questions/16921652/how-to-write-a-json-file-in-c
                    File.WriteAllText(GetPersistentPath(fileName), JsonUtility.ToJson(obj));
                    break;

                case FormatType.PlayerPrefs:
                    if (obj.GetType() == typeof(int))
                    {
                        PlayerPrefs.SetInt(fileName, (int)obj);
                        PlayerPrefs.Save();
                    }
                    else if (obj.GetType() == typeof(float))
                    {
                        PlayerPrefs.SetFloat(fileName, (float)obj);
                        PlayerPrefs.Save();
                    }
                    else if (obj.GetType() == typeof(string))
                    {
                        PlayerPrefs.SetString(fileName, (string)obj);
                        PlayerPrefs.Save();
                    }
                    else
                    {
                        throw new Exception("!!! ERRO !!! ManagerStorage - Não existe esse tipo no PlayerPrefs");
                    }
                    break;

                default:
                    throw new Exception("!!! ERRO !!! ManagerStorage - Tipo errado");
            }
        }

        public static T Load<T>(string fileName, FormatType type = FormatType.Binary)
        {
            switch (type)
            {
                case FormatType.Binary:
                    FileStream file = null;
                    try
                    {
                        if (!Exists(fileName))
                        {
                            Debug.Log("[StorageManager.Load] não existe -> " + GetPersistentPath(fileName));
                            return default(T);
                        }
                        file = File.Open(GetPersistentPath(fileName), FileMode.Open);
                        var obj = (T)new BinaryFormatter().Deserialize(file);
                        file.Close();
                        return obj;
                    }
                    catch (Exception e)
                    {
                        Debug.LogError("[StorageManager.Load(" + fileName + ")] " + e);
                        if (file != null)
                            file.Close();
                        return default(T);
                    }

                case FormatType.Json:
                    return JsonUtility.FromJson<T>(File.ReadAllText(GetPersistentPath(fileName)));

                case FormatType.PlayerPrefs:
                    if (PlayerPrefs.GetInt(fileName, int.MaxValue) != int.MaxValue)
                    {
                        return (T)(object)PlayerPrefs.GetInt(fileName);
                    }
                    else if (PlayerPrefs.GetString(fileName, "XXXXXX") != "XXXXXX")
                    {
                        return (T)(object)PlayerPrefs.GetString(fileName);
                    }
                    else if (PlayerPrefs.GetFloat(fileName, float.MaxValue) != float.MaxValue)
                    {
                        return (T)(object)PlayerPrefs.GetFloat(fileName);
                    }
                    else
                    {
                        if (typeof(T) == typeof(int))
                        {
                            Save(fileName, 0, null, FormatType.PlayerPrefs);
                            return (T)(object)0;
                        }
                        else if (typeof(T) == typeof(string))
                        {
                            Save(fileName, "", null, FormatType.PlayerPrefs);
                            return (T)(object)"";
                        }
                        else if (typeof(T) == typeof(float))
                        {
                            Save(fileName, 0f, null, FormatType.PlayerPrefs);
                            return (T)(object)0f;
                        }
                        else
                        {
                            throw new Exception("!!! ERRO !!! ManagerStorage - Não existe esse tipo no PlayerPrefs");
                        }
                    }

                default:
                    throw new Exception("!!! ERRO !!! ManagerStorage - Tipo errado");
            }
        }

        public static void Delete(string fileName)
        {
            if (Exists(fileName))
                File.Delete(GetPersistentPath(fileName));
        }

        private static void DeleteDirectory(string path)
        {
            Directory.Delete(GetPersistentPath(path), true);
        }

        private static void CreateDirectory(string folderPath)
        {
            Directory.CreateDirectory(GetPersistentPath(folderPath));
        }

        public static bool Exists(string fileName)
        {
            return File.Exists(GetPersistentPath(fileName));
        }

        private static void Move(string sourceDirName, string destDirName)
        {
            Directory.Move(GetPersistentPath(sourceDirName), GetPersistentPath(destDirName));
        }

        public static byte[] ReadAllBytes(string file)
        {
            return File.ReadAllBytes(GetPersistentPath(file));
        }

        private static string Combine(string path1, string path2)
        {
            return Path.Combine(path1, path2);
        }

        public static string GetPersistentPath(string fileName = "")
        {
            return Path.Combine(Application.persistentDataPath, fileName);
        }

#if UNITY_EDITOR
        [MenuItem("Sioux Tools/Storage/Print Persistent Path")]
        private static void PrintPersistentPath()
        {
            Debug.Log(GetPersistentPath());
        }

        [MenuItem("Sioux Tools/Storage/Open Local Folder")]
        private static void OpenLocalFolder()
        {
#if UNITY_EDITOR_WIN
            System.Diagnostics.Process.Start("explorer.exe", "/select," + GetPersistentPath().Replace(@"/", @"\")); // explorer doesn't like front slashes
#else
            Debug.Log("[LocalData.OpenLocalFolder] Não está implementado para esta plataforma do editor.");
#endif
        }
#endif
    }

}
#endif