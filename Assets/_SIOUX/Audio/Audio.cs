﻿#if SIOUX_AUDIO && SIOUX_STORAGE
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace Sioux
{
    public static class Audio
    {
        public enum Channel
        {
            BGM,
            SFX
        }

        static GameObject gameObject;
        static AudioMixer audioMixer;
        static AudioSource bgmAudioSource;
        static List<AudioSource> sfxAudioSources = new List<AudioSource>();
        static Dictionary<string, AudioClip> cachedSFXs = new Dictionary<string, AudioClip>();

        static Audio()
        {
            Init();
        }

        public static void Init(int preCreatedAudioSources = 0)
        {
            if (gameObject)
                return;

            audioMixer = Resources.Load<AudioMixer>("AudioMixer");
            gameObject = new GameObject("AudioManager");
            Object.DontDestroyOnLoad(gameObject);

            bgmAudioSource = gameObject.AddComponent<AudioSource>();
            bgmAudioSource.outputAudioMixerGroup = audioMixer.FindMatchingGroups("BGM")[0];
            for (int i = 0; i < preCreatedAudioSources; ++i)
                CreateAudioSource();

            SetVolume(Channel.BGM, LocalData.HasKey("volume" + Channel.BGM) ? LocalData.Get<float>("volume" + Channel.BGM) : 1);
            SetVolume(Channel.SFX, LocalData.HasKey("volume" + Channel.SFX) ? LocalData.Get<float>("volume" + Channel.SFX) : 1);
            SetMute(Channel.BGM, LocalData.HasKey("mute" + Channel.BGM) ? LocalData.Get<bool>("mute" + Channel.BGM) : false);
            SetMute(Channel.SFX, LocalData.HasKey("mute" + Channel.SFX) ? LocalData.Get<bool>("mute" + Channel.SFX) : false);
        }

        public static void PlayBGM(AudioClip clip, float volume = 1, float pitch = 1)
        {
            bgmAudioSource.clip = clip;
            bgmAudioSource.loop = true;
            bgmAudioSource.volume = volume;
            bgmAudioSource.pitch = pitch;
            bgmAudioSource.Play();
        }

        public static void Play(string clipName, float volume = 1, float pitch = 1, bool loop = false, bool doCache = true)
        {
            AudioClip clip = null;
            if (cachedSFXs.ContainsKey(clipName))
                clip = cachedSFXs[clipName];
            else
            {
                clip = Resources.Load<AudioClip>(clipName);
                if (doCache)
                    cachedSFXs.Add(clipName, clip);
            }
            Play(clip, volume, pitch, loop);
        }

        public static void Play(AudioClip clip, float volume = 1, float pitch = 1, bool loop = false)
        {
            var audioSource = sfxAudioSources.Find(temp => !temp.isPlaying) ?? CreateAudioSource();
            if (!sfxAudioSources.Contains(audioSource))
                sfxAudioSources.Add(audioSource);

            audioSource.clip = clip;
            audioSource.volume = volume;
            audioSource.pitch = pitch;
            audioSource.loop = loop;
            audioSource.Play();
        }

        public static void SetVolume(Channel channel, float value)
        {
            if (value < 0 || value > 1)
                value = Mathf.Clamp01(value);

            float v = Mathf.Lerp(-80, 0, value);
            audioMixer.SetFloat("volume" + channel, v);
            LocalData.Set("volume" + channel, value);
        }

        public static void SetMute(Channel channel, bool value)
        {
            switch (channel)
            {
                case Channel.BGM:
                    LocalData.Set("mute" + channel, bgmAudioSource.mute = value);
                    break;

                case Channel.SFX:
                    sfxAudioSources.ForEach(audioSource => audioSource.mute = value);
                    LocalData.Set("mute" + channel, value);
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// Evitar usar este método pois está com um bug: o AudioSource gerado por ele nunca vai receber alteração de mute.
        /// </summary>
        public static AudioSource CreateAudioSource()
        {
            var newAudioSource = gameObject.AddComponent<AudioSource>();
            newAudioSource.outputAudioMixerGroup = audioMixer.FindMatchingGroups("SFX")[0];
            newAudioSource.mute = LocalData.Get<bool>("mute" + Channel.SFX);
            return newAudioSource;
        }
    }
}
#endif