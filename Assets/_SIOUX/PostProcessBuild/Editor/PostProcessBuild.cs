﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;
using System.IO;
using System.Collections.Generic;
using System;

namespace Sioux.PostProcessBuild
{
    [CreateAssetMenu(fileName = "SiouxPostProcessBuild", menuName = "Sioux/PostProcessBuild")]
    public class PostProcessBuild : ScriptableObject
    {
        // SCRIPTABLE OBJECT
        [Header("Geral")]
        public bool forceHideUnityLogo = true;

        [Header("iOS frameworks")]
        public bool UserNotifications;

        [Header("iOS pList")]
        public string NSPhotoLibraryUsageDescription;
        public string NSMicrophoneUsageDescription;
        public string NSAppleMusicUsageDescription;
        public string NSCameraUsageDescription;

        [Header("iOS PBX Capability Type")]
        public bool ApplePay;
        public bool WirelessAccessoryConfiguration;
        public bool Wallet;
        public bool Siri;
        public bool PushNotifications;
        public bool PersonalVPN;
        public bool Maps;
        public bool InterAppAudio;
        public bool InAppPurchase;
        public bool KeychainSharing;
        public bool HomeKit;
        public bool HealthKit;
        public bool GameCenter;
        public bool DataProtection;
        public bool BackgroundModes;
        public bool AssociatedDomains;
        public bool AppGroups;
        public bool iCloud;

        // STATIC
        static PostProcessBuild obj;

        [PostProcessBuild]
        public static void OnPostProcessBuild(BuildTarget buildTarget, string path)
        {
            Debug.Log("[Sioux.PostProcessBuild.OnPostProcessBuild] START -------------------------------------------------");
            obj = Resources.Load<PostProcessBuild>("SiouxPostProcessBuild");
            if (!obj)
            {
                Debug.Log("[Sioux.PostProcessBuild.OnPostProcessBuild] Não foi encontrado o arquivo Resources/SiouxPostProcessBuild");
                return;
            }

            if (buildTarget == BuildTarget.iOS)
            {
                BuildForiOS(buildTarget, path);
            }

            if (obj.forceHideUnityLogo)
                PlayerSettings.SplashScreen.showUnityLogo = false;
            Debug.Log("[Sioux.PostProcessBuild.OnPostProcessBuild] END --------------------------------------------------");
        }

        static void BuildForiOS(BuildTarget buildTarget, string path)
        {
            // PLIST
            string plistPath = path + "/Info.plist";
            PlistDocument pList = new PlistDocument();
            pList.ReadFromString(File.ReadAllText(plistPath));
            if (!string.IsNullOrEmpty(obj.NSPhotoLibraryUsageDescription))
                pList.root.SetString("NSPhotoLibraryUsageDescription", obj.NSPhotoLibraryUsageDescription);
            if (!string.IsNullOrEmpty(obj.NSMicrophoneUsageDescription))
                pList.root.SetString("NSMicrophoneUsageDescription", obj.NSMicrophoneUsageDescription);
            if (!string.IsNullOrEmpty(obj.NSAppleMusicUsageDescription))
                pList.root.SetString("NSAppleMusicUsageDescription", obj.NSAppleMusicUsageDescription);
            if (!string.IsNullOrEmpty(obj.NSCameraUsageDescription))
                pList.root.SetString("NSCameraUsageDescription", obj.NSCameraUsageDescription);
            File.WriteAllText(plistPath, pList.WriteToString());
            // END PLIST

            string projPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";
            Debug.Log("[Sioux.PostProcessBuild.BuildForiOS] Build iOS. path: " + projPath);

            // get project
            PBXProject proj = new PBXProject();
            proj.ReadFromString(File.ReadAllText(projPath));
            string target = proj.TargetGuidByName("Unity-iPhone");

            // FRAMEWORKS
            if (obj.UserNotifications)
                proj.AddFrameworkToProject(target, "UserNotifications.framework", true);

            // CAPABILITIES
            ProjectCapabilityManager capability = new ProjectCapabilityManager(projPath, path + "/Unity-iPhone/" + PlayerSettings.productName + ".entitlements", "Unity-iPhone");
            if (obj.BackgroundModes)
                capability.AddBackgroundModes(BackgroundModesOptions.RemoteNotifications);
            capability.WriteToFile();
            //proj.AddCapability(target, PBXCapabilityType.PushNotifications);
            //proj.AddCapability(target, PBXCapabilityType.InAppPurchase);
            //proj.AddCapability(target, PBXCapabilityType.BackgroundModes);
            //proj.AddFile("/_SIOUX/PostProcessBuild/project.entitlements", path + "/Unity-iPhone/project.entitlements");

            if (obj.ApplePay)
                Debug.LogError("[PostProcessBuild.BuildForiOS] ApplePay: " + proj.AddCapability(target, PBXCapabilityType.ApplePay));
            if (obj.WirelessAccessoryConfiguration)
                Debug.LogError("[PostProcessBuild.BuildForiOS] WirelessAccessoryConfiguration: " + proj.AddCapability(target, PBXCapabilityType.WirelessAccessoryConfiguration));
            if (obj.Wallet)
                Debug.LogError("[PostProcessBuild.BuildForiOS] Wallet: " + proj.AddCapability(target, PBXCapabilityType.Wallet));
            if (obj.Siri)
                Debug.LogError("[PostProcessBuild.BuildForiOS] Siri: " + proj.AddCapability(target, PBXCapabilityType.Siri));
            if (obj.PushNotifications)
                Debug.LogError("[PostProcessBuild.BuildForiOS] PushNotifications: " + proj.AddCapability(target, PBXCapabilityType.PushNotifications));
            if (obj.PersonalVPN)
                Debug.LogError("[PostProcessBuild.BuildForiOS] PersonalVPN: " + proj.AddCapability(target, PBXCapabilityType.PersonalVPN));
            if (obj.Maps)
                Debug.LogError("[PostProcessBuild.BuildForiOS] Maps: " + proj.AddCapability(target, PBXCapabilityType.Maps));
            if (obj.InterAppAudio)
                Debug.LogError("[PostProcessBuild.BuildForiOS] InterAppAudio: " + proj.AddCapability(target, PBXCapabilityType.InterAppAudio));
            if (obj.InAppPurchase)
                Debug.LogError("[PostProcessBuild.BuildForiOS] InAppPurchase: " + proj.AddCapability(target, PBXCapabilityType.InAppPurchase));
            if (obj.KeychainSharing)
                Debug.LogError("[PostProcessBuild.BuildForiOS] KeychainSharing: " + proj.AddCapability(target, PBXCapabilityType.KeychainSharing));
            if (obj.HomeKit)
                Debug.LogError("[PostProcessBuild.BuildForiOS] HomeKit: " + proj.AddCapability(target, PBXCapabilityType.HomeKit));
            if (obj.HealthKit)
                Debug.LogError("[PostProcessBuild.BuildForiOS] HealthKit: " + proj.AddCapability(target, PBXCapabilityType.HealthKit));
            if (obj.GameCenter)
                Debug.LogError("[PostProcessBuild.BuildForiOS] GameCenter: " + proj.AddCapability(target, PBXCapabilityType.GameCenter));
            if (obj.DataProtection)
                Debug.LogError("[PostProcessBuild.BuildForiOS] DataProtection: " + proj.AddCapability(target, PBXCapabilityType.DataProtection));
            if (obj.BackgroundModes)
                Debug.LogError("[PostProcessBuild.BuildForiOS] BackgroundModes: " + proj.AddCapability(target, PBXCapabilityType.BackgroundModes));
            if (obj.AssociatedDomains)
                Debug.LogError("[PostProcessBuild.BuildForiOS] AssociatedDomains: " + proj.AddCapability(target, PBXCapabilityType.AssociatedDomains));
            if (obj.AppGroups)
                Debug.LogError("[PostProcessBuild.BuildForiOS] AppGroups: " + proj.AddCapability(target, PBXCapabilityType.AppGroups));
            if (obj.iCloud)
                Debug.LogError("[PostProcessBuild.BuildForiOS] iCloud: " + proj.AddCapability(target, PBXCapabilityType.iCloud));

            // END CAPABILITIES

            MakeXcodeEntitlementsFile(buildTarget, path);

            // save
            File.WriteAllText(projPath, proj.WriteToString());
        }

        static void MakeXcodeEntitlementsFile(BuildTarget buildTarget, string path)
        {
            var dummy = ScriptableObject.CreateInstance<EntitlementsPostProcess>();
            var file = dummy.m_entitlementsFile;
            ScriptableObject.DestroyImmediate(dummy);
            if (file == null)
            {
                Debug.Log("[Sioux.PostProcessBuild.MakeEntitlementsFile] file is null");
                return;
            }

            var proj_path = PBXProject.GetPBXProjectPath(path);
            var proj = new PBXProject();
            proj.ReadFromFile(proj_path);

            // target_name = "Unity-iPhone"
            var target_name = PBXProject.GetUnityTargetName();
            var target_guid = proj.TargetGuidByName(target_name);
            var src = AssetDatabase.GetAssetPath(file);
            var file_name = Path.GetFileName(src);
            var dst = path + "/" + target_name + "/" + file_name;
            FileUtil.DeleteFileOrDirectory(dst);
            FileUtil.CopyFileOrDirectory(src, dst);
            proj.AddFile(target_name + "/" + file_name, file_name);
            proj.AddBuildProperty(target_guid, "CODE_SIGN_ENTITLEMENTS", target_name + "/" + file_name);

            proj.WriteToFile(proj_path);
        }
    }
}