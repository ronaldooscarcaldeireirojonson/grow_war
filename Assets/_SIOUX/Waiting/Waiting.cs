﻿using UnityEngine;

namespace Sioux
{
    public static class Waiting
    {
        static GameObject gameObject;

        static Waiting()
        {
            Init();
        }

        public static void Init()
        {
            if (gameObject)
                return;

            gameObject = Object.Instantiate(Resources.Load<GameObject>("SiouxWaitingCanvas"));
            Object.DontDestroyOnLoad(gameObject);
            gameObject.SetActive(false);
        }

        public static void Show()
        {
            gameObject.SetActive(true);
        }

        public static void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}