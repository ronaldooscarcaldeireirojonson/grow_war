﻿#if SIOUX_LOCALIZATION
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;

namespace Sioux.Localization
{
    public static class Localization
    {
        /// <summary>
        /// Para armazenar referências para os diferentes arquivos de localização
        /// O Primeiro valor será o default
        /// Key: PT, EN, JP
        /// Value: Localization_PT.json, Localization_EN.json, Localization_JP.json 
        /// </summary>
        static Dictionary<string, string> languages;
        static Dictionary<string, string> localizedText;
        static string currentLoadedFile;
        static bool isReady = false;

        /// <summary>
        /// Usada para inicializar o dicionario de linguagens.
        /// </summary>
        /// <param name="language">código da linguagem a carregar ex: "PT". Se vazio, a linguagem do sistema será escolhida. Caso não haja, o primeiro arquivo de tradução é selecionado.</param>
        public static void Init(string language = "")
        {
            languages = new Dictionary<string, string>();

            var files = new DirectoryInfo(Path.Combine(Application.streamingAssetsPath, "Localization")).GetFiles("*.json");
            foreach (var file in files)
            {
                if (file.Name.Contains("_"))
                {
                    languages.Add(file.Name.Split('_').Last().Split('.')[0], file.Name);
                }
                else
                {
                    Debug.LogError("Invalid file name: " + file.Name);
                }
            }

            if (languages.ContainsKey(language))
            {
                language = languages[language];
            }
            else if (languages.ContainsKey(Application.systemLanguage.ToCountryCode()))
            {
                language = languages[Application.systemLanguage.ToCountryCode()];
            }
            else
            {
                if (files.Length == 0)
                {
                    Debug.LogError("No Localization file detected. Use Window>Sioux>Localization Editor panel to create a file");
                    return;
                }
                language = languages.ToList()[0].Value;
                Debug.Log("Load the first available " + language);

            }
            currentLoadedFile = language;
            LoadLocalizedText(language);
        }

        /// <summary>
        /// Carrega arquivo json e guarda referências estáticas.
        /// </summary>
        /// <param name="fileName">Nome curto do arquivo</param>
        public static void LoadLocalizedText(string fileName)
        {
            localizedText = new Dictionary<string, string>();
            string filePath = Path.Combine(Path.Combine(Application.streamingAssetsPath, "Localization"), fileName);


            if (File.Exists(filePath))
            {
                string dataAsJson = File.ReadAllText(filePath);
                LocalizationData loadedData = JsonUtility.FromJson<LocalizationData>(dataAsJson);

                for (int i = 0; i < loadedData.items.Count; i++)
                {
                    localizedText.Add(loadedData.items[i].key, loadedData.items[i].value);
                }

            }
            else
            {
                Debug.Log("No Localization file loaded");

            }

            isReady = true;
        }


        /// <summary>
        /// Retorna o texto referente à chave
        /// </summary>
        /// <param name="key">Chave de tradução</param>
        /// <returns></returns>
        public static string GetValue(string key)
        {
            if (!isReady)
            {
                Init();
            }

            string result = "Localized text not found";
            if (localizedText.ContainsKey(key))
            {
                result = localizedText[key];
            }

            return result;

        }


        public static bool HasKey(string key)
        {
            if (!isReady)
            {
                Init();
            }

            return (string.IsNullOrEmpty(key) || localizedText == null) ? false : localizedText.ContainsKey(key);
        }

        public static string GetCurrentLanguageFile()
        {
            return currentLoadedFile;
        }

        private static readonly Dictionary<SystemLanguage, string> COUTRY_CODES = new Dictionary<SystemLanguage, string>()
        {
            { SystemLanguage.Afrikaans, "ZA" },
            { SystemLanguage.Arabic    , "SA" },
            { SystemLanguage.Basque    , "US" },
            { SystemLanguage.Belarusian    , "BY" },
            { SystemLanguage.Bulgarian    , "BJ" },
            { SystemLanguage.Catalan    , "ES" },
            { SystemLanguage.Chinese    , "CN" },
            { SystemLanguage.Czech    , "HK" },
            { SystemLanguage.Danish    , "DK" },
            { SystemLanguage.Dutch    , "BE" },
            { SystemLanguage.English    , "US" },
            { SystemLanguage.Estonian    , "EE" },
            { SystemLanguage.Faroese    , "FU" },
            { SystemLanguage.Finnish    , "FI" },
            { SystemLanguage.French    , "FR" },
            { SystemLanguage.German    , "DE" },
            { SystemLanguage.Greek    , "JR" },
            { SystemLanguage.Hebrew    , "IL" },
            { SystemLanguage.Icelandic    , "IS" },
            { SystemLanguage.Indonesian    , "ID" },
            { SystemLanguage.Italian    , "IT" },
            { SystemLanguage.Japanese    , "JP" },
            { SystemLanguage.Korean    , "KR" },
            { SystemLanguage.Latvian    , "LV" },
            { SystemLanguage.Lithuanian    , "LT" },
            { SystemLanguage.Norwegian    , "NO" },
            { SystemLanguage.Polish    , "PL" },
            { SystemLanguage.Portuguese    , "PT" },
            { SystemLanguage.Romanian    , "RO" },
            { SystemLanguage.Russian    , "RU" },
            { SystemLanguage.SerboCroatian    , "SP" },
            { SystemLanguage.Slovak    , "SK" },
            { SystemLanguage.Slovenian    , "SI" },
            { SystemLanguage.Spanish    , "ES" },
            { SystemLanguage.Swedish    , "SE" },
            { SystemLanguage.Thai    , "TH" },
            { SystemLanguage.Turkish    , "TR" },
            { SystemLanguage.Ukrainian    , "UA" },
            { SystemLanguage.Vietnamese    , "VN" },
            { SystemLanguage.ChineseSimplified    , "CN" },
            { SystemLanguage.ChineseTraditional    , "CN" },
            { SystemLanguage.Unknown    , "US" },
            { SystemLanguage.Hungarian    , "HU" },
        };

        /// <summary>
        /// Returns approximate country code of the language.
        /// </summary>
        /// <returns>Approximated country code.</returns>
        /// <param name="language">Language which should be converted to country code.</param>
        public static string ToCountryCode(this SystemLanguage language)
        {
            string result;
            if (COUTRY_CODES.TryGetValue(language, out result))
            {
                return result;
            }
            else
            {
                return COUTRY_CODES[SystemLanguage.Unknown];
            }
        }
    }
}
#endif