﻿#if SIOUX_EXTENSIONS
using UnityEngine;

namespace Sioux.Extensions
{
    static class Extensions
    {

        /// <summary>
        /// Função para girar matrix de pixels
        /// </summary>
        /// <param name="matrix">array de pixels</param>
        /// <param name="w">Largura antes da rotação</param>
        /// <param name="h">Altura antes da rotação</param>
        /// <returns></returns>
        static Color[] RotateMatrix(Color[] matrix, int w, int h)
        {
            Color[] ret = new Color[h * w];

            for (int i = 0; i < w; ++i)
            {
                for (int j = 0; j < h; ++j)
                {
                    ret[i * h + j] = matrix[(h - j - 1) * w + i];
                }
            }

            return ret;
        }
        /// <summary>
        /// Função para girar a textura
        /// </summary>
        /// <param name="tex">textura a ser girada</param>
        /// <param name="w">Largura antes da rotação</param>
        /// <param name="h">Altura antes da rotação</param>
        /// <returns></returns>
        static void RotateTexture(this Texture2D tex, int w, int h)
        {
            var matrix = tex.GetPixels();
            Color[] ret = new Color[h * w];

            for (int i = 0; i < w; ++i)
            {
                for (int j = 0; j < h; ++j)
                {
                    ret[i * h + j] = matrix[(h - j - 1) * w + i];
                }
            }

        }
    }
}
#endif