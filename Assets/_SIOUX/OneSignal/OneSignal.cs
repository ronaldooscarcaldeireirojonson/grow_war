﻿#if SIOUX_ONESIGNAL
using UnityEngine;

namespace Sioux
{
    public static class OneSignalManager
    {
        static bool Initialized = false;

        public static void Init(string appID, OneSignal.LOG_LEVEL logLevel = OneSignal.LOG_LEVEL.NONE)
        {
            if (Initialized)
                return;
            Initialized = true;

            // Enable line below to enable logging if you are having issues setting up OneSignal. (logLevel, visualLogLevel)
            OneSignal.SetLogLevel(logLevel, logLevel);

            OneSignal.StartInit(appID).HandleNotificationOpened(result =>
            {
                Debug.Log("[OneSignalManager.Init] START - result dump...");
                Debug.Log("action.actionID=" + result.action.actionID);
                Debug.Log("action.type=" + result.action.type);
                Debug.Log("notification.androidNotificationId=" + result.notification.androidNotificationId);
                Debug.Log("notification.displayType=" + result.notification.displayType);
                Debug.Log("notification.isAppInFocus=" + result.notification.isAppInFocus);
                Debug.Log("notification.payload.body=" + result.notification.payload.title);
                Debug.Log("notification.payload.body=" + result.notification.payload.body);
                Debug.Log("notification.shown=" + result.notification.shown);
                Debug.Log("notification.silentNotification=" + result.notification.silentNotification);
                Debug.Log("[OneSignalManager.Init] END");
            }).EndInit();

            OneSignal.inFocusDisplayType = OneSignal.OSInFocusDisplayOption.Notification;

            // Call syncHashedEmail anywhere in your app if you have the user's email.
            // This improves the effectiveness of OneSignal's "best-time" notification scheduling feature.
            //OneSignal.SyncHashedEmail(userEmail);
        }
    }
}
#endif