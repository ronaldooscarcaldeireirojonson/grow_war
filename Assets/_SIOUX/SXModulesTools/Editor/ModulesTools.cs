﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

namespace Sioux.Modules
{
    public class ModulesTools : EditorWindow
    {
        static List<string> definesString = new List<string>();

        /// <summary>
        /// Use this dictionary to register new modules.
        /// (Label,Symbol)
        /// </summary>
        static readonly Dictionary<string, string> Libs = new Dictionary<string, string>
        {
            {"Audio", "SIOUX_AUDIO"},
            {"Extensions", "SIOUX_EXTENSIONS"},
            {"Localization", "SIOUX_LOCALIZATION"},
            {"One Signal", "SIOUX_ONESIGNAL"},
            {"Storage", "SIOUX_STORAGE"},
            {"ADB", "SIOUX_ADB"},
            {"IAP", "SIOUX_IAP"}
        };


        static Dictionary<string, bool> LibsToggles = new Dictionary<string, bool>();

        [MenuItem("Sioux Tools/Open Sioux Modules Window")]
        static void Init()
        {
            EditorWindow.GetWindow(typeof(ModulesTools)).Show();
            EditorWindow.GetWindow(typeof(ModulesTools)).titleContent.text = "Sioux Modules";
            Load();
        }

        static void Load()
        {
            definesString = PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup).Split(';').ToList();
            LibsToggles.Clear();
            Libs.ToList().ForEach(libKV => LibsToggles.Add(libKV.Key, definesString.Contains(libKV.Value)));
        }

        private void OnGUI()
        {
            if (GUILayout.Button("Refresh"))
            {
                Load();
            }

            if (LibsToggles.Count > 0)
            {
                Libs.ToList().ForEach(kv =>
                {
                    LibsToggles[kv.Key] = EditorGUILayout.ToggleLeft(kv.Key, LibsToggles[kv.Key]);
                });

                if (GUILayout.Button("Apply"))
                {
                    AddDefineSymbols();
                }
            }
            else
            {
                EditorGUILayout.LabelField("Please Refresh");
            }
            var otherLibs = definesString.Except(Libs.ToList().Select(lib => lib.Value)).ToList();
            if (otherLibs.Count > 0)
            {
                EditorGUILayout.Space();
                EditorGUILayout.LabelField("Aditional Symbols");
                otherLibs.ForEach(def => EditorGUILayout.LabelField(def));
            }
        }

        static void AddDefineSymbols()
        {
            LibsToggles.ToList().ForEach(toggleKV =>
            {
                if (toggleKV.Value)
                {
                    if (!definesString.Contains(Libs[toggleKV.Key]))
                    {
                        definesString.Add(Libs[toggleKV.Key]);
                        Debug.Log("[Modules] Add " + Libs[toggleKV.Key]);
                    }
                }
                else
                {
                    if (definesString.Contains(Libs[toggleKV.Key]))
                    {
                        definesString.Remove(Libs[toggleKV.Key]);
                        Debug.Log("[Modules] Remove " + Libs[toggleKV.Key]);
                    }
                }
            });

            definesString.AddRange(definesString.Except(definesString));
            PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup, string.Join(";", definesString.ToArray()));
            EditorWindow.GetWindow(typeof(ModulesTools)).Close();
        }
    }
}