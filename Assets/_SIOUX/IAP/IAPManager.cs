﻿#if SIOUX_IAP
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Purchasing;

namespace Sioux
{
    public class IAPManager : IStoreListener
    {
        private static IStoreController m_StoreController;          // The Unity Purchasing system.
        private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.
        private static Dictionary<string, Action<Product>> purchaseCallbacks = new Dictionary<string, Action<Product>>();
        private static Dictionary<string, Action<Product>> restoreCallbacks = new Dictionary<string, Action<Product>>();
        private static Action<bool> initCallback;

        private IAPManager() { }

        public static void Init(Dictionary<string, ProductType> products, Dictionary<string, Action<Product>> restoreCallbacks, Action<bool> initCallback)
        {
            IAPManager.restoreCallbacks = restoreCallbacks ?? new Dictionary<string, Action<Product>>();
            products.Keys.ToList().ForEach(pId =>
            {
                if (products[pId] == ProductType.NonConsumable && !IAPManager.restoreCallbacks.ContainsKey(pId))
                    Debug.Log("[Sioux.IAPManager.Init] ATENÇÃO!!! O produto não-consumível " + pId + " não tem restore callback!!!");
            });

            if (IsInitialized())
            {
                var fetchedProducts = m_StoreController.products.all.ToList();
                var productsToFecth = products.Where(product => !fetchedProducts.Exists(p => p.definition.id == product.Key)).ToList();

                if (productsToFecth.Count > 0)
                    m_StoreController.FetchAdditionalProducts(new HashSet<ProductDefinition>(productsToFecth.Select(product => new ProductDefinition(product.Key, product.Value))), () =>
                    {
                        Debug.Log(m_StoreController.products.all.ToList().Aggregate("", (acc, p) => acc + p.definition.id + ", "));
                        initCallback(true);
                    }, reason =>
                    {
                        Debug.LogError("[IAPManager.Init] " + reason);
                        initCallback(false);
                    });
                else
                    initCallback(true);
            }
            else
            {
                IAPManager.initCallback = initCallback;
                var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance()); // Create a builder, first passing in a suite of Unity provided stores.
                products.Keys.ToList().ForEach(productId => builder.AddProduct(productId, products[productId]));
                UnityPurchasing.Initialize(new IAPManager(), builder); // Kick off the remainder of the set-up with an asynchrounous call, passing the configuration and this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.
            }
        }

        public static bool IsInitialized()
        {
            // Only say we are initialized if both the Purchasing references are set.
            return m_StoreController != null && m_StoreExtensionProvider != null;
        }

        public static void BuyProductID(string productId, Action<Product> callback)
        {
            if (!IsInitialized())
            {
                Debug.Log("BuyProductID FAIL. Not initialized.");
                callback(null);
                return;
            }

            restoreCallbacks.Clear();

            if (purchaseCallbacks.ContainsKey(productId))
            {
                Debug.Log("BuyProductID FAIL. Already purchasing.");
                callback(null);
                return;
            }

            Debug.Log(string.Format("Purchasing product asychronously: '{0}'", productId));
            purchaseCallbacks.Add(productId, callback);
            m_StoreController.InitiatePurchase(productId);
        }

        public static void RestorePurchases(Action<bool> callback)
        {
            if (!IsInitialized())
            {
                Debug.Log("RestorePurchases FAIL. Not initialized.");
                return;
            }

            // If we are running on an Apple device ... 
            if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXPlayer)
            {
                // ... begin restoring purchases
                Debug.Log("RestorePurchases started ...");

                // Fetch the Apple store-specific subsystem.
                var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
                // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
                // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
                apple.RestoreTransactions(callback);
            }
            else // Otherwise ...
            {
                // We are not running on an Apple device. No work is necessary to restore purchases.
                Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
            }
        }

        public static Product GetProduct(string id)
        {
            return m_StoreController == null || m_StoreController.products == null ? null : m_StoreController.products.WithID(id);
        }

        // --- IStoreListener
        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            // Purchasing has succeeded initializing. Collect our Purchasing references.
            Debug.Log("OnInitialized: PASS");

            // Overall Purchasing system, configured with products for this application.
            m_StoreController = controller;
            // Store specific subsystem, for accessing device-specific store features.
            m_StoreExtensionProvider = extensions;

            if (initCallback != null)
                initCallback(true);
            initCallback = null;
        }

        public void OnInitializeFailed(InitializationFailureReason error)
        {
            // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
            Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);

            if (initCallback != null)
                initCallback(false);
            initCallback = null;
        }

        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
        {
            var productid = args.purchasedProduct.definition.id;
            Debug.Log("[IAPManager.ProcessPurchase] " + productid);
            if (restoreCallbacks.ContainsKey(productid))
                restoreCallbacks[productid](args.purchasedProduct);
            else if (purchaseCallbacks.ContainsKey(productid))
            {
                purchaseCallbacks[productid](args.purchasedProduct);
                purchaseCallbacks.Remove(productid);
            }

            // Return a flag indicating whether this product has completely been received, or if the application needs 
            // to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
            // saving purchased products to the cloud, and when that save is delayed. 
            return PurchaseProcessingResult.Complete;
        }

        public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
        {
            var productid = product.definition.id;
            Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", productid, failureReason));

            if (!purchaseCallbacks.ContainsKey(productid))
            {
                Debug.LogError("[IAPManager.OnPurchaseFailed] Não foi encontrado o callback!");
                return;
            }

            purchaseCallbacks[productid](null);
            purchaseCallbacks.Remove(productid);
        }

        // android specific
        public static bool IsAppInstalled(string bundleID)
        {
#if UNITY_ANDROID && !UNITY_EDITOR
            AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject ca = up.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaObject packageManager = ca.Call<AndroidJavaObject>("getPackageManager");
            try
            {
                return null != packageManager.Call<AndroidJavaObject>("getLaunchIntentForPackage", bundleID); // se o app não estiver instalado, dá uma exception aqui
            }
            catch (Exception ex)
            {
                Debug.Log("[Sioux.IAPManager] " + ex.Message);
            }
#endif
            return false;
        }
    }
}
#endif