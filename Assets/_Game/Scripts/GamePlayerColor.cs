﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Grow.War
{

    public class GamePlayerColor : MonoBehaviour
    {


        public Image squareColor;
        public Image bgSelected;


        public void RefreshColor(Color color)
        {
            squareColor.color = color;
        }


        public void ShowSelected()
        {
            bgSelected.gameObject.SetActive(true);
        }

        public void HideSelected()
        {
            bgSelected.gameObject.SetActive(false);
        }
    }
}
