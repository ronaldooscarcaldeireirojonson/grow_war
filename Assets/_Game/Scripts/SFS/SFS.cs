﻿using Grow.War;

namespace Grow
{
    public static class SFS
    {
        public static string GameName = "War";
        public static GrowSFS Grow;
        public static FriendsSFS Friends;

        public static void CreateAndConnect()
        {
            if (Grow != null)
                return;

            ServerManager.instance.ShowWaitingAnim();
            //Grow = SFSManager.CreateAndConnect<WarSFS>("War", "sfsGameConfig.json");
            //Friends = SFSManager.CreateAndConnect<FriendsSFS>("Friends", "sfsFriendsConfig.json");
            Grow = SFSManager.CreateAndConnect<WarSFS>("War", VersionController.versionConfig.serverIp, VersionController.versionConfig.serverPort, VersionController.versionConfig.serverPortWebGl, VersionController.versionConfig.zoneName);
            Friends = SFSManager.CreateAndConnect<FriendsSFS>("Friends", VersionController.versionConfig.serverIp, VersionController.versionConfig.serverPort, VersionController.versionConfig.serverPortWebGl, VersionController.versionConfig.zoneNameFriends);
        }

        public static void DisconnectAndGoToLogin()
        {
            if (Grow)
                Grow.Disconnect();
            if (Friends)
                Friends.Disconnect();
            Grow = null;
            if (GameController.Instance)
                GameController.Instance.SERVER = null;
            Friends = null;
            SceneManager.instance.ChangeScene("Login", false);
        }
    }
}