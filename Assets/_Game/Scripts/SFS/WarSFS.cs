﻿using Sfs2X.Entities;
using Sfs2X.Entities.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Grow.War
{
    public class WarSFS : GrowSFS, ServerInterface
    {
        // Abstract
        public override Color GetColor(int id)
        {
            return WarColor.Get(id).color;
        }

        // server responses
        protected override void OnEventResponse(string type, IDictionary data)
        {
            switch (type)
            {
                case "roomJoin":
                    base.OnEventResponse(type, data);
                    break;

                case "userExitRoom":
                    var user = (User)data["user"];
                    if (user.IsItMe)
                        ServerManager.instance.HideWaitingAnim();
                    base.OnEventResponse(type, data);
                    break;

                case "connectionLost":
                    base.OnEventResponse(type, data);
                    if (GameController.Instance != null)
                        GameController.Instance.cameraHandler.canClickMap = false;
                    break;

                default:
                    base.OnEventResponse(type, data);
                    break;
            }
        }

        protected override void OnExtensionResponse(string cmd, SFSObject obj)
        {
            if (obj == null)
                obj = new SFSObject();

            if (cmd == "selfReconnect")
            {
                SelfReconnect(obj);
                return;
            }

            // se a partida ainda não começou...
            if (!GameController.Instance || GameController.gameState == null)
            {
                switch (cmd)
                {
                    case "gameStart":
                        SceneManager.instance.ChangeScene("Game");
                        try
                        {
                            var myId = GetVar(sfs.MySelf, "idGrow").GetIntValue();
                            StartCoroutine(SetGameState(new GameState(UserList, myId, obj.GetIntArray("po").ToList(), obj.GetSFSArray("t"), obj.GetInt("o"))));
                        }
                        catch (System.Exception e)
                        {
                            Debug.LogError("[WarSFS.OnExtensionResponse.gameStart] Não foi possível pegar a minha idGrow. O jogo não será iniciado!");
                            Debug.LogError(e);
                        }
                        break;

                    default:
                        base.OnExtensionResponse(cmd, obj);
                        break;
                }
            }
            else // aqui a partida com certeza já começou
            {
                switch (cmd)
                {
                    case "cdu":
                        if (GameController.Instance)
                            GameController.Instance.CountdownUpdate(obj.GetInt("cd"));
                        break;

                    case "ob":
                        Debug.Log("[WarSFS.OnExtensionResponse] ob?");
                        break;

                    case "snt": // start new turn
                        GameController.Instance.StartNewTurnResponse(obj.GetInt("cp"), obj.GetIntArray("ta").ToList());
                        break;

                    case "f":
                        GameController.Instance.FortifyResponse(obj.GetInt("t"), obj.GetInt("a"), obj.ContainsKey("w") ? obj.GetInt("w") : -1);
                        break;

                    case "aps":
                        GameController.Instance.AttackPhaseStartResponse();
                        break;

                    case "a":
                        GameController.Instance.AttackResponse(
                            obj.GetSFSObject("o"),
                            obj.GetSFSObject("t"),
                            obj.GetIntArray("da").ToList(),
                            obj.GetIntArray("dt").ToList(),
                            obj.GetBool("c"),
                            obj.ContainsKey("e") ? obj.GetInt("e") : -1,
                            obj.ContainsKey("w") ? obj.GetInt("w") : -1);
                        break;

                    case "ape":
                        GameController.Instance.AttackPhaseEndResponse();
                        break;

                    case "m":
                        GameController.Instance.MoveResponse(obj.GetSFSObject("o"), obj.GetSFSObject("t"), obj.ContainsKey("w") ? obj.GetInt("w") : -1);
                        break;

                    case "c":
                        GameController.Instance.CardsResponse(obj.GetInt("q"), obj.GetInt("p"), obj.ContainsKey("c") ? obj.GetIntArray("c").ToList() : null);
                        break;

                    case "tic":
                        var wildcards = obj.GetIntArray("w");
                        GameController.Instance.TradeInCardsResponse(obj.GetSFSArray("t"), obj.GetInt("b"), wildcards == null ? new List<int>() : wildcards.ToList());
                        break;

                    case "ud":
                        GameController.Instance.PlayerDisconnectedResponse(obj.GetInt("p"));
                        break;

                    case "canReconnect": // com a partida já iniciada, o aceite é automático
                        AcceptReconnect(obj.GetUtfString("roomName"));
                        break;

                    case "otherReconnect":
                        var playerId = obj.GetInt("p");
                        if (playerId != GameController.gameState.thisPlayer.idGrow)
                            GameController.Instance.OtherReconnectResponse(playerId);
                        break;

                    case "endGame":
                        EndGameController.PlayerPackage = new Dictionary<int, EndGamePlayerPackage>();
                        obj.GetSFSArray("scores").Cast<SFSObject>().ToList().ForEach(score => EndGameController.PlayerPackage.Add(score.GetInt("id"), new EndGamePlayerPackage(score)));
                        break;

                    // DEBUG
                    case "DEBUG_stsl":
                        GameController.Instance.DEBUG_SetTerritoryStateListResponse(obj.GetSFSArray("t"));
                        break;

                    default:
                        base.OnExtensionResponse(cmd, obj);
                        break;
                }
            }
        }

        // reconnect
        private void SelfReconnect(SFSObject obj)
        {
            var gameState = new GameState(obj.GetSFSArray("p").Cast<SFSObject>(), GetVar(sfs.MySelf, "idGrow").GetIntValue(), obj.GetSFSArray("t"), obj.GetInt("o"));
            gameState.currentPlayer = gameState.GetPlayer(obj.GetInt("cp"));

            // cards
            gameState.thisPlayer.cards = new List<Territory>();
            obj.GetIntArray("c").ToList().ForEach(cId =>
            {
                if (cId >= 0 && cId < 44)
                    gameState.thisPlayer.cards.Add(Territory.Get(cId));
            });

            //gameState.turnArmy.ForEach(ta => Debug.Log(ta));
            gameState.currentPlayer = gameState.GetPlayer(obj.GetInt("cp")); // currentPlayer
            gameState.phase = obj.GetByte("phase"); // phase
            //gameState.roundCount = obj.GetInt("rc"); // roundCount
            gameState.tradeInCount = obj.GetInt("tic"); // tradeInCount
            gameState.turnArmy = obj.GetIntArray("ta").ToList(); // turn army
            if (obj.ContainsKey("mak")) // move army
            {
                var moveArmyKeys = obj.GetIntArray("mak").ToList();
                var moveArmyValues = obj.GetIntArray("mav").ToList();
                gameState.moveArmy = new Dictionary<int, int>();
                for (int i = 0; i < moveArmyKeys.Count; ++i)
                    gameState.moveArmy.Add(moveArmyKeys[i], moveArmyValues[i]);
            }
            var conquerorTerritoryId = obj.ContainsKey("crt") ? obj.GetInt("crt") : -1;
            var conqueredTerritoryId = obj.ContainsKey("cdt") ? obj.GetInt("cdt") : -1;


            if (MainMenuController.Instance != null) // estamos na HOME
                SceneManager.instance.ChangeScene("Game");
            else
            {
                var loading = GameObject.FindWithTag("CanvasWaiting");
                Destroy(loading);
                GameController.Instance.cameraHandler.canClickMap = true;
            }

            StartCoroutine(ReconnectWaitRoutine(gameState, MainMenuController.Instance != null, conquerorTerritoryId, conqueredTerritoryId));
        }

        IEnumerator ReconnectWaitRoutine(GameState gameState, bool isHardReconnect, int conquerorTerritoryId, int conqueredTerritoryId)
        {
            yield return StartCoroutine(SetGameState(gameState));
            StartCoroutine(GameController.Instance.SelfReconnectResponse(isHardReconnect, conquerorTerritoryId, conqueredTerritoryId));
        }

        IEnumerator SetGameState(GameState gs)
        {
            yield return new WaitUntil(() => GameController.Instance);
            GameController.Instance.SERVER = this;
            GameController.Instance.gs = gs;
        }

        // gameplay (interface)
        public bool IsOnline()
        {
            return true;
        }

        public void FortifyRequest(int territoryId, int amount)
        {
            var obj = new SFSObject();
            obj.PutInt("t", territoryId);
            obj.PutInt("a", amount);
            Send("f", sfs.LastJoinedRoom, obj);
        }

        public void AttackRequest(int originId, int targetId)
        {
            var obj = new SFSObject();
            obj.PutInt("o", originId);
            obj.PutInt("t", targetId);
            Send("a", sfs.LastJoinedRoom, obj);
        }

        public void AttackPhaseEndRequest()
        {
            Send("ape", sfs.LastJoinedRoom, new SFSObject());
        }

        public void MoveRequest(int originId, int targetId, int amount)
        {
            var obj = new SFSObject();
            obj.PutInt("o", originId);
            obj.PutInt("t", targetId);
            obj.PutInt("a", amount);
            Send("m", sfs.LastJoinedRoom, obj);
        }

        public void EndTurnRequest()
        {
            Send("et", sfs.LastJoinedRoom, new SFSObject());
        }

        public void TradeInCardsRequest(List<int> cardIds)
        {
            var obj = new SFSObject();
            obj.PutIntArray("c", cardIds.ToArray());
            Send("tic", sfs.LastJoinedRoom, obj);
        }

        // debug 
        public void DEBUG_SetTerritoryStateListRequest(List<TerritoryState> tsList)
        {
            SFSArray tsArray = new SFSArray();
            tsList.ForEach(ts => tsArray.AddSFSObject(ts.ToSFSObject()));

            var obj = new SFSObject();
            obj.PutSFSArray("t", tsArray);

            Send("DEBUG_stsl", sfs.LastJoinedRoom, obj);
        }
    }
}