﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Grow.War
{
    public static class AI
    {
        static float STEP = 0.5f;

        static GameController GAME { get { return GameController.Instance; } }

        static void Debug(object s)
        {
            UnityEngine.Debug.Log("[!AI!] " + s);
        }
        public static void SetStep(bool fast)
        {
            if(fast)
            {
                STEP = 0.1f;
            }
            else
            {
                STEP = 0.5f;
            }
        }
        public static void Play(bool isTurnStart = false)
        {
            if (GAME.gs.currentPlayer.isHuman)
                return;

            GAME.StartCoroutine(PlayRoutine(isTurnStart));
        }

        static IEnumerator PlayRoutine(bool isTurnStart = false)
        {
            yield return new UnityEngine.WaitForSeconds(STEP);

            if (isTurnStart)
            {

                Debug(GAME.gs.currentPlayer.warColor.name + " : " + GAME.gs.currentPlayer.objective.text);

                alreadyChosenTerritoriesToAttack = new List<TerritoryState>();
                alreadyChosenTerritoriesToPutArmy = new List<TerritoryState>();
                territoriesToFortify = new Dictionary<int, Dictionary<TerritoryState, int>>();
                turnArmy = new List<int>(GAME.gs.turnArmy);

                for (int i = 0; i < 7; ++i)
                    territoriesToFortify.Add(i, new Dictionary<TerritoryState, int>());

                fortifyTerritoryStates = new List<TerritoryState>();
                GAME.gs.territoryStates.ForEach(ts => fortifyTerritoryStates.Add(new TerritoryState(ts.territory.id, ts.owner, ts.amountArmy)));

                // se trocar cartas, faz o PrepareFortify no retorno (AI.TradeInCards)
                if (CanTradeInCards(GAME.gs.currentPlayer.cards))
                {
                    GAME.TradeInCardsRequest(SelectCardsToTrade().ConvertAll(c => c.id));
                    yield break;
                }
                else
                    PrepareFortify();
            }

            switch (GAME.gs.phase)
            {
                case 0: PlayFortifyPhase(); break;
                case 1: PlayAttackPhase(); break;
                case 2: PlayMovePhase(); break;
                default: Debug("Fase desconhecida!"); break;
            }
        }

        #region Fortify
        /// Função para distribuir as tropas
        static List<int> turnArmy;
        static Dictionary<int, Dictionary<TerritoryState, int>> territoriesToFortify;
        static List<TerritoryState> fortifyTerritoryStates;
        static List<TerritoryState> alreadyChosenTerritoriesToAttack;
        static List<TerritoryState> alreadyChosenTerritoriesToPutArmy;

        static void PlayFortifyPhase()
        {
            if (territoriesToFortify.Values.ToList().TrueForAll(p => p.Values.ToList().TrueForAll(v => v <= 0)))
            {
                Debug("!ERRO! Mandaram eu jogar fase de fortificação mas o territoriesToFortify tá todo zerado =(");
                return;
            }

            for (int i = 0; i < territoriesToFortify.Count; ++i)
            {
                var keyList = territoriesToFortify[i].Keys.ToList();
                for (int j = 0; j < keyList.Count; ++j)
                    if (territoriesToFortify[i][keyList[j]] > 0)
                    {
                        GAME.FortyfyRequest(keyList[j].territory.id, territoriesToFortify[i][keyList[j]]);
                        territoriesToFortify[i][keyList[j]] = 0;
                        return;
                    }
            }

            Debug("!ERRO! Cheguei no fim da PlayFortifyPhase e não consegui fortificar nada O.o wtf");
        }
        
        static void PrepareFortify()
        {

            int i = 0;
            //Debug(gs.turnArmy.ConvertAll(a => a.ToString()).Aggregate((a, b) => a + " " + b));
            if (turnArmy == null || (i = turnArmy.FindIndex(a => a > 0)) < 0)
                return;

            List<TerritoryState> tToFortify = new List<TerritoryState>();
            switch (GAME.gs.currentPlayer.objective.info.objectiveType)
            {
                case ObjectiveType.PlayerElimination:
                    {
                        //GAME.gs.currentPlayer.objective.info.playerTarget.id
                        //verificar se tem fronteira com esta cor, caso n tiver fortificar igualmente de modo a expandir territorio
                    }
                    break;
                case ObjectiveType.ContinentConquest:
                    {
                        List<TerritoryState> toConquest = new List<TerritoryState>();
                        bool conquestedOtherTerritories = false;
                        GAME.gs.currentPlayer.objective.info.continentsToConquer.ForEach(cont => 
                        { 

                            if(cont != Continent.Type.ANY )
                            {
                                conquestedOtherTerritories = false;
                                Continent objCont = Continent.GetContinent(cont);
                                int contTerritories = objCont.territoriesCount;
                                int myContTerritories = 0;

                                GAME.gs.currentPlayer.GetTerritories(fortifyTerritoryStates).ForEach(t => 
                                {
                                    if (t.territory.continent.name == objCont.name)
                                        myContTerritories++;
                                    //else toConquest.Add()
                                });
                                if (myContTerritories >= contTerritories)
                                {
                                    conquestedOtherTerritories = true;
                                    Debug(objCont.name + " Conquested");
                                }else
                                {
                                    Debug(objCont.name + " Not Conquested, " + (contTerritories - myContTerritories) + " remaining");
                                }
                            }
                            else
                            {
                                //special logic
                            }
                        });
                        //verificar quais continentes devem ser conquistados e quais ja foram conquistados, 
                        //continente aleatorio deve ser conquistado apos os dois fixos ja serem conquistados
                        //caso n exista fronteira com o continente expandir universalmente (pathfinding nao vai funcionar pois o numero de tropas sempre muda)

                    }
                    break;
                case ObjectiveType.TerritoryConquest:
                    {
                        int nTroops = GAME.gs.currentPlayer.objective.info.numberOfTroopsRequired;
                        int validTerritories = 0;

                        GAME.gs.currentPlayer.GetTerritories(fortifyTerritoryStates).ForEach(ts =>
                        {
                            if (ts.amountArmy >= nTroops)
                            {
                                validTerritories++;
                            }else{
                                tToFortify.Add(ts);
                            }
                        });
                        Debug("Já tenho " + validTerritories +" territorios com " + nTroops + " tropas");
                    }
                    break;
                default:
                    break;
            }
            // efetua a fortificação
            if (i < 6)
            { // Se for específico de um continente
                while (turnArmy[i] > 0)
                {
                    int biggestEnemyArmy = -100;
                    TerritoryState dangerEnemyTerritory = null;
                    TerritoryState myThreatenedTerritory = null;
                    List<TerritoryState> myBorderTerritories = GetBorderTerritories(Continent.LIST[i]);

                    // Verifica se tem inimigos nas fronteiras
                    myBorderTerritories.ForEach(ts =>
                    { // Para cada território de fronteira no continente Continent.Array[i]
                        ts.territory.neighbours.ForEach(neighbour =>
                                { // Para cada vizinho do território
                                    var neighbourState = GetTS(neighbour.id, fortifyTerritoryStates);
                                    if (neighbourState.owner != ts.owner)
                                    { // Se esse vizinho não pertencer ao jogador
                                        if (biggestEnemyArmy < neighbourState.amountArmy - ts.amountArmy - 3)
                                        {
                                            myThreatenedTerritory = ts;
                                            dangerEnemyTerritory = neighbourState;
                                            biggestEnemyArmy = neighbourState.amountArmy - ts.amountArmy - 3;
                                        }
                                    }
                                });
                    });

                    // Não tenho inimigos nas fronteiras
                    // NOTE: Pelo que eu entendi até pode ter inimigos nas fronteiras, mas o exército deles não é tão relevante
                    if (biggestEnemyArmy <= 0)
                    {
                        if (turnArmy[i] > 0)
                        {     // Divide proporcionalmente por fronteira
                            var amount = (int)Math.Floor((float)turnArmy[i] / myBorderTerritories.Count);
                            if (amount == 0 && turnArmy[i] > 0)
                                amount = 1;
                            if (Fortify(myBorderTerritories.Aggregate((a, b) => a.amountArmy < b.amountArmy ? a : b), amount, i))
                                return;
                        }
                    }
                    else
                    {   // Se existe algum inimigo na fronteira com vários exércitos
                        // Se a maior quantidade de exércitos do inimigo for maior que a quantidade de exércitos que posso colocar
                        if (biggestEnemyArmy > turnArmy[i])
                            biggestEnemyArmy = turnArmy[i];
                        if (Fortify(myThreatenedTerritory, biggestEnemyArmy, i))
                            return;
                    }
                }
            }
            else
            {   // i == 6, portanto pode distribuir em qualquer lugar do mapa
                // Pega a lista de territórios de possíveis alvos para conquistar
                var targetTerritories = GetPossibleTerritoriesTarget(GetContinentToConquer().id);
                if (targetTerritories.Count > 0)
                {
                    TerritoryState tsChosen = null;
                    var biggestFactor = -1;

                    // Para cada território alvo
                    targetTerritories.ForEach(territoryState =>
                    {
                        var neighbourlength = GetAllNeighbourTerritories().Count;
                        var neighbourlengthPlus = GetAllNeighbourTerritoriesPlusNew(territoryState).Count;

                        // NOTE: Não entendi essa conta! Mas parece que ele prioriza os territórios que quando conquistados diminuem a quantidade de territórios vizinhos
                        var factor = ((neighbourlengthPlus - neighbourlength) * -5) + (5 + (territoryState.amountArmy * -1));

                        // Se o fator for maior e se não já estiver na lista dos territórios
                        if (factor > biggestFactor && alreadyChosenTerritoriesToAttack.IndexOf(territoryState) < 0)
                        {
                            biggestFactor = factor;
                            tsChosen = territoryState;
                        }
                    });

                    // Se não escolheu nenhum país
                    if (tsChosen == null)
                    {
                        // Se a lista de territórios já escolhidos não está vazia
                        if (alreadyChosenTerritoriesToAttack.Count > 0)
                        {
                            // Pega uma opção aleatória já da lista e reinicia a distribuição
                            tsChosen = alreadyChosenTerritoriesToAttack[Rand(alreadyChosenTerritoriesToAttack.Count)];
                            // NOTE: Eu acho que esse clear não precisa!
                            //alreadyChosenTerritoriesToAttack.Clear();
                            alreadyChosenTerritoriesToAttack.Add(tsChosen);
                        }
                        else
                        {   // Se a lista estiver vazia
                            // NOTE: No código do Rodrigo ele pega o primeiro. Eu fiz um random pra pegar qualquer território
                            var playerTerritories = GAME.gs.currentPlayer.GetTerritories(fortifyTerritoryStates);
                            if (playerTerritories.Count > 0)
                                tsChosen = playerTerritories[Rand(playerTerritories.Count)];

                            // NOTE: Isso não tava no código do Rodrigo, mas acho que precisa
                            alreadyChosenTerritoriesToAttack.Add(tsChosen);
                        }
                    }
                    else
                    {   // Se escolheu um território

                        // Apenas adiciona na lista
                        alreadyChosenTerritoriesToAttack.Add(tsChosen);
                    }

                    // NOTE: Pelo que eu entendi agora vamos escolher o território de origem (que vamos colocar os exércitos)

                    // O território de origem
                    Territory territoryOrigin = null;

                    // NOTE: Não entendi direito o +2, mas acho que é pra ter pelo menos 2 exércitos a mais que o território inimigo
                    var chosenTerritoryArmy = 2 + tsChosen.amountArmy;

                    // Pega os vizinhos e verifica qual vizinho vai atacar o pais escolhido
                    tsChosen.territory.neighbours.ForEach(neighbour =>
                    {
                        var neighbourState = GetTS(neighbour.id, fortifyTerritoryStates);
                        // Se o vizinho é o jogador e não está na lista de exércitos usados e está no mesmo continente
                        if (neighbourState.owner == GAME.gs.currentPlayer && neighbour.continent == tsChosen.territory.continent)
                            territoryOrigin = neighbour;
                    });

                    // Se não selecionou ninguém, refaz a busca expandida e reinicia a lista
                    if (territoryOrigin == null)
                    {
                        if (alreadyChosenTerritoriesToPutArmy.Count != 0)
                        { // Se a lista está vazia
                          // Pega uma opção aleatoria já da lista e reinicia a distribuição
                            territoryOrigin = alreadyChosenTerritoriesToPutArmy[Rand(alreadyChosenTerritoriesToPutArmy.Count)].territory;
                            //alreadyChosenTerritoriesToPutArmy.Clear();
                            alreadyChosenTerritoriesToPutArmy.Add(GetTS(territoryOrigin.id, fortifyTerritoryStates));
                        }
                        else
                        {   // Se a lista não está vazia
                            tsChosen.territory.neighbours.ForEach(neighbour =>
                            {
                                var neighbourState = GetTS(neighbour.id, fortifyTerritoryStates);
                                // Se o vizinho é o jogador e não está na lista de exércitos usados e está no continente correto
                                if (neighbourState.owner == GAME.gs.currentPlayer && alreadyChosenTerritoriesToPutArmy.IndexOf(neighbourState) < 0)
                                {
                                    territoryOrigin = neighbour;
                                    // NOTE: Isso não tava no código do Rodrigo, mas acho que precisa
                                    alreadyChosenTerritoriesToPutArmy.Add(GetTS(territoryOrigin.id, fortifyTerritoryStates));
                                }
                            });
                        }
                    }
                    else    // Esse 'else' não tinha no código do Rodrigo, mas acho que precisa
                        alreadyChosenTerritoriesToPutArmy.Add(GetTS(territoryOrigin.id, fortifyTerritoryStates));

                    // NOTE: Tinha um push na lista 'alreadyChosenTerritoriesToPutArmy' aqui no código do Rodrigo, mas acho que estava errado!

                    // Se o exército do inimigo for maior que a quantidade que eu posso colocar
                    if (chosenTerritoryArmy > turnArmy[i])
                        chosenTerritoryArmy = turnArmy[i];

                    // Se entrar neste if, quer dizer que algo deu errado (famoso bug do territoryOrigin==null)
                    // Para evitar a exception, fiz a AI pegar um território aleatório. Isso não tem estratégia, mas acontece tão raramente que não tem problema.
                    if (territoryOrigin == null)
                    {
                        var playerTerritories = GAME.gs.currentPlayer.GetTerritories(fortifyTerritoryStates);
                        territoryOrigin = playerTerritories[Rand(playerTerritories.Count)].territory;
                    }

                    // Adiciona exércitos
                    if (Fortify(GetTS(territoryOrigin.id, fortifyTerritoryStates), chosenTerritoryArmy, i))
                        return;
                }
            }

            // se chegar no final e não conseguir fortificar nada, fortifica onde tem mais (se tiver empate, é random)
            var tsTemp = GAME.gs.currentPlayer.GetTerritories(fortifyTerritoryStates).Where(ts => i == 6 || ts.territory.continent.id == i).Aggregate((a, b) => a.amountArmy > b.amountArmy ? a : a.amountArmy < b.amountArmy ? b : (Rand(2) == 0 ? a : b));
            Fortify(tsTemp, turnArmy[i], i);
        }

        static bool Fortify(TerritoryState ts, int amount, int i)
        {
            if (amount < 1)
            {
                Debug("Erro! Tentei fortificar " + amount + " em " + ts.territory.name);
                return false;
            }

            if (turnArmy[i] < 1)
            {
                Debug("Erro! Tentei fortificar em um continente que não tenho mais exércitos para colocar! (Tentei fortificar " + amount + " em " + ts.territory.name + ")");
                return false;
            }

            if (amount > turnArmy[i])
                amount = turnArmy[i];

            turnArmy[i] -= amount;

            if (territoriesToFortify[i].ContainsKey(ts))
                territoriesToFortify[i][ts] += amount;
            else
                territoriesToFortify[i].Add(ts, amount);

            PrepareFortify();
            return true;
        }

        public static void TradeInCards(int bonusArmy)
        {
            if (GameController.Instance.SERVER.IsOnline() || GAME.gs.currentPlayer.isHuman)
                return;

            if (turnArmy != null)
                turnArmy[6] += bonusArmy;
            else
                Debug("turnArmy é null! Vai dar merda!");
            PrepareFortify();
            Play();
        }
        #endregion

        #region Attack
        static ResultTerritoryAttack tta;

        /// Função chamada para a CPU atacar um território
        public static void PlayAttackPhase()
        {
            // só executa o Move se amountArmy do target for maior que zero; e só continua atacanod se o jogo n tiver acabado
            if (tta != null)
            {
                if (tta.tsTarget.amountArmy == 0)
                {
                    Move(tta.tsOrigin, tta.tsTarget, Math.Min(3, (int)Math.Ceiling((double)tta.tsOrigin.amountArmy / 2)));
                    return;
                }
            }

            // busca qual é o melhor ataque (origem e destino)
            tta = GetTerritoryToAttack() ?? GetTerritoryToAttackAgressive();

            if (tta == null) // se for null, o CPU não quer mais atacar e passa para a fase de movimentação
            {
                // encerra a fase de ataque e passa para a fase de movimentação
                GAME.AttackPhaseEndRequest();
                return;
            }

            // envia o sinal de ataque para o servidor
            GAME.AttackRequest(tta.tsOrigin.territory.id, tta.tsTarget.territory.id);
        }

        /// Função para escolher o território para atacar
        static ResultTerritoryAttack GetTerritoryToAttack()
        {
            var possibleTargets = new List<ResultTerritoryAttack>();
            switch (GAME.gs.currentPlayer.objective.info.objectiveType)
            {
                case ObjectiveType.PlayerElimination:
                    {
                        //GAME.gs.currentPlayer.objective.info.playerTarget.id
                        //atacar territorios inimigos de acordo com facilidade de vitoria, caso n haja territorio inimigo atacar outro territorio de mais facilidade

                    }
                    break;
                case ObjectiveType.ContinentConquest:
                    {
                        //atacar continentes a serem conquistados, caso continente aleatorio escolhe o de mais facilidade para se conquistar

                    }
                    break;
                case ObjectiveType.TerritoryConquest:
                    {
                        //verificar quantos territorios devem ser conquistados e quantas tropas por territorio(importante)
                        //fortificar fronteiras que sao mais faceis de conquistar
                    }
                    break;
                default:
                    break;
            }

            GAME.gs.currentPlayer.GetTerritories(GAME.gs.territoryStates).ForEach(territoryState =>
            { // Para cada território do jogador
                if (territoryState.amountArmy > 1)  // Se tiver pelo menos 1 exército
                    territoryState.territory.neighbours.ForEach(neighbour =>
                    { // Para cada vizinho
                        var neighbourState = GetTS(neighbour.id, GAME.gs.territoryStates);

                        if (GAME.gs.currentPlayer.idGrow != neighbourState.owner.idGrow)
                        { // Se o vizinho não pertence ao jogador
                            var armyAttack = territoryState.amountArmy - 1;
                            var armyDefense = neighbourState.amountArmy;
                            var factor = 1 - (Math.Pow(0.6, (armyAttack - armyDefense)));

                            // Verifica se esse território é o único que falta no continente
                            if (CountTerritoryLeftInContinent(neighbour.continent) == 1)
                                factor = 1;

                            // Se o index for menor que 0.6 e o território vizinho não for do mesmo continente
                            if (factor > 0.6 && territoryState.territory.continent.id != neighbourState.territory.continent.id)
                            {
                                // Se o jogador já conquistou o continente
                                if (CountTerritoryLeftInContinent(neighbour.continent) == 0)
                                    factor = 0.6 + (0.01 * armyAttack);
                                else
                                    factor = 0.4 + (0.01 * armyAttack);
                            }

                            // Se o fator for maior que 0.6 ou se o jogador ainda não conquistou nenhum território e o fator é maior que 0
                            if (factor >= 0.6 || factor >= 0 && !GAME.gs.hasConquerATerritory)
                            {
                                // Crio uma estruta com o território e o index calculado dele
                                var newTerritory = new ResultTerritoryAttack();
                                newTerritory.tsOrigin = territoryState;
                                newTerritory.tsTarget = neighbourState;
                                newTerritory.factor = factor;

                                // Verifico se já existe esse território na lista
                                if (!possibleTargets.Any(b => b.tsTarget.territory == neighbourState.territory))
                                    possibleTargets.Add(newTerritory);
                            }
                        }
                    });
            });

            if (possibleTargets.Count > 0)
            {
                // Colocar a lista de países em ordem decrescente
                //possibleTargets = possibleTargets.Sort((a, b) => b.factor - a.factor);

                return possibleTargets.Aggregate((a, b) => a.factor > b.factor ? a : b);
            }

            return null;
        }

        static ResultTerritoryAttack GetTerritoryToAttackAgressive()
        {
            var gs = GAME.gs;
            ResultTerritoryAttack rta = null;
            gs.currentPlayer.GetTerritories(GAME.gs.territoryStates).ForEach(myTS =>
            {
                if (rta == null && myTS.amountArmy > 1 && myTS.amountArmy >= gs.currentPlayer.agressiveMultiplier)
                    myTS.territory.neighbours.ForEach(neighbour =>
                    { // Para cada vizinho
                        var neighbourState = GetTS(neighbour.id, GAME.gs.territoryStates);
                        if (rta == null && gs.currentPlayer.idGrow != neighbourState.owner.idGrow && myTS.amountArmy >= neighbourState.amountArmy * gs.currentPlayer.agressiveMultiplier)
                        { // Se o vizinho não pertence ao jogador
                            rta = new ResultTerritoryAttack();
                            rta.tsOrigin = myTS;
                            rta.tsTarget = neighbourState;
                        }
                    });
            });
            return rta;
        }
        #endregion

        #region Move
        /// Função para mover os exércitos
        static List<TerritoryState> playerTerritoriesTemp = null; // lista temporária de territórios do jogador atual utilizada APENAS pela MoveArmy
        static int moveIndex = -1;
        static int lastTurnArmy = -1;

        static void PlayMovePhase()
        {
            if (moveIndex < 0) // primeira chamada da recursiva
            {
                playerTerritoriesTemp = GAME.gs.currentPlayer.GetTerritories(GAME.gs.territoryStates).OrderBy(t => Guid.NewGuid()).ToList();
                moveIndex = 0;
            }

            while (moveIndex >= playerTerritoriesTemp.Count || GAME.gs.moveArmy[playerTerritoriesTemp[moveIndex].territory.id] <= 0) // || GAME.gs.moveArmy[playerTerritoriesTemp[moveIndex].territory.id] == lastTurnArmy)
            {
                moveIndex++;

                if (moveIndex >= playerTerritoriesTemp.Count) // condição de parada
                {
                    moveIndex = -1;
                    lastTurnArmy = -1;
                    playerTerritoriesTemp = null;
                    GAME.EndTurnRequest();
                    return;
                }
            }

            lastTurnArmy = GAME.gs.moveArmy[playerTerritoriesTemp[moveIndex].territory.id];

            var ts = playerTerritoriesTemp[moveIndex]; // território desta iteração

            // Se tiver 1 exército (ou menos) este território não tem como fazer movimentação
            if (ts.amountArmy <= 1)
            {
                moveIndex++;
                PlayMovePhase();
                return;
            }

            // Se algum dos territórios vizinhos não pertence ao jogador atual, é melhor não mover exército deste território
            if (ts.territory.neighbours.Any(n => ts.owner.idGrow != GetTS(n.id, GAME.gs.territoryStates).owner.idGrow))
            {
                moveIndex++;
                PlayMovePhase();
                return;
            }

            var didMove = false;
            var myTerritoryArmy = ts.amountArmy - 1; // Contabiliza sempre deixando um exército no próprio território

            for (var i = 0; i < ts.territory.neighbours.Count; ++i) // Para cada vizinho do território
            {
                var neighbour = ts.territory.neighbours[i];
                var enemyArmySizeTotal = 0;

                neighbour.neighbours.ForEach(neighbourNeighbour => // Verifica se os vizinhos do vizinho são inimigos e pega a quantidade de exércitos
                {
                    var neighbourNeighbourState = GetTS(neighbour.id, GAME.gs.territoryStates);

                    if (GetTS(neighbour.id, GAME.gs.territoryStates).owner != neighbourNeighbourState.owner)
                    { // Se o vizinho do vizinho não é do jogador
                        if (neighbour.IsBorder())
                        { // Se o vizinho é um país de fronteira
                            if (enemyArmySizeTotal + neighbourNeighbourState.amountArmy - 1 < 2) // Se a soma dos exércitos dos vizinhos do vizinho for menor que 2
                                enemyArmySizeTotal += 2 + neighbourNeighbourState.amountArmy - 1; // Coloca 2 exércitos a mais como segurança
                            else
                                enemyArmySizeTotal += neighbourNeighbourState.amountArmy - 1; // Aumenta a quantidade total de exércitos
                        }
                        else
                            enemyArmySizeTotal += neighbourNeighbourState.amountArmy - 1; // Aumenta a quantidade total de exércitos
                    }
                });

                // Esses próximos 'if's são casos especiais onde o território que estamos analisando está 'bem longe' de ser atacado
                // Assim não precisamos deixar tantos exércitos nesses territórios
                // NOTE: me parece que está faltando uns 'if's
                if (enemyArmySizeTotal == 0 &&
                    (((ts.territory == Territory.AFRICA_DO_SUL || ts.territory == Territory.MADAGASCAR) && neighbour == Territory.SUDAO) ||
                        ((ts.territory == Territory.OTTAWA) && neighbour == Territory.LABRADOR)))
                {
                    Move(ts, GetTS(neighbour.id, GAME.gs.territoryStates), myTerritoryArmy);
                    didMove = true;
                    myTerritoryArmy = 0; // NOTE: essa linha não tinha no código do Rodrigo, mas acho que tem que ter pois caso contrário vai poder mais exércitos do que deveria
                    return;
                }

                if (myTerritoryArmy < enemyArmySizeTotal)
                { // Se a quantidade de exércitos no meu território for menor que o total da ameaça no vizinho
                    if (myTerritoryArmy > 0)  // Se a quantidade for maior que 0
                        if (neighbour.IsBorder() && ts.territory.IsBorder())  // Se os dois territórios forem de fronteira
                            if (myTerritoryArmy >= 2)
                            { // Se no território tiver pelo menos 2
                              // Garante que fica pelo menos 2 exércitos na fronteira
                                myTerritoryArmy -= 2;
                                Move(ts, GetTS(neighbour.id, GAME.gs.territoryStates), myTerritoryArmy);
                                didMove = true;
                                myTerritoryArmy = 0;
                                return;
                            }
                }
                else
                {   // A quantidade de exércitos no meu território é maior que o total da ameaça
                    if (enemyArmySizeTotal > 0)
                    { // Se o total de exércitos dos vizinhos do vizinho for maior que 0
                        if (neighbour.IsBorder() && ts.territory.IsBorder())
                        { // Se o meu território e o vizinho são de fronteira
                          // Tira pelo menos 2 exércitos do total (para deixar no mínimo 2 na fronteira)
                            enemyArmySizeTotal -= 2;

                            // Move os exércitos
                            Move(ts, GetTS(neighbour.id, GAME.gs.territoryStates), enemyArmySizeTotal);
                            didMove = true;
                            myTerritoryArmy -= enemyArmySizeTotal;
                            return;
                        }
                        else
                        {
                            // Move os exércitos
                            Move(ts, GetTS(neighbour.id, GAME.gs.territoryStates), enemyArmySizeTotal);
                            didMove = true;
                            myTerritoryArmy -= enemyArmySizeTotal;
                            return;
                        }
                    }
                }
            }

            // Depois que já vi todos os vizinhos do território
            // Aqui eu ainda tenho certeza que todos os vizinhos do território são do próprio jogador

            // Se o território não for fronteira com outro continente
            if (!ts.territory.IsBorder())
            {
                myTerritoryArmy = ts.amountArmy - 1;

                for (var i = 0; i < ts.territory.neighbours.Count; ++i)
                { // Para cada território vizinho
                    var neighbour = ts.territory.neighbours[i];
                    var neighbourState = GetTS(neighbour.id, GAME.gs.territoryStates);
                    for (var j = 0; j < neighbour.neighbours.Count; ++j) // Para cada vizinho do vizinho
                                                                         // Se o vizinho do vizinho não for do jogador e o continente for diferente
                        if (GetTS(neighbour.neighbours[j].id, GAME.gs.territoryStates).owner.idGrow != neighbourState.owner.idGrow ||
                            neighbour.neighbours[j].continent.id != neighbour.continent.id)
                            if (myTerritoryArmy > 0)
                            {
                                Move(ts, neighbourState, myTerritoryArmy);
                                didMove = true;
                                myTerritoryArmy = 0;
                                return;
                            }
                }
            }

            if (!didMove)
            {
                moveIndex++;
                PlayMovePhase();
            }
        }

        // efetua uma movimentação e retorna se o jogo acabou ou não
        static void Move(TerritoryState tsOrigin, TerritoryState tsDestiny, int amount)
        {
            if (amount < 1)
                amount = 1;
            if (GAME.gs.phase == 2 && amount > lastTurnArmy)
                amount = lastTurnArmy;
            GAME.MoveRequest(tsOrigin.territory.id, tsDestiny.territory.id, amount);
        }
        #endregion

        #region Fortify AUX
        static TerritoryState GetTS(int territoryId, List<TerritoryState> list)
        {
            return list.Find(ts => ts.territory.id == territoryId);
        }

        /// Função para escolher as cartas para trocar
        /// Precisa verificar se o jogador pode trocar antes, ou seja chamar a função 'CanTradeInCards'
        /// Esse método é bem mais fácil de entender!
        /// Eu pego todas as 10 diferentes combinações e vejo quais deles eu posso trocar. Aí depois eu vejo a combinação que mais tem cartas com territórios extras.
        static List<Territory> SelectCardsToTrade()
        {

            // Pega os territórios do jogador
            List<TerritoryState> playerTerritories = GAME.gs.currentPlayer.GetTerritories(fortifyTerritoryStates);

            // Pega as cartas do jogador
            List<Territory> pCards = GAME.gs.currentPlayer.cards;

            // lista com todas possibilidades de combinações com 3 cartas
            List<List<Territory>> possibilitiesTradeArray = new List<List<Territory>>();

            // lista com as possiblidades de combinações com 3 cartas que dá pra fazer a troca
            List<List<Territory>> canTradeArray = new List<List<Territory>>();

            // Primeiro vamos adicionar todas as possibilidades de troca com 3 cartas
            if (pCards.Count >= 3)
            {
                // 00111
                possibilitiesTradeArray.Add(new List<Territory>() { pCards[0], pCards[1], pCards[2] });

                if (pCards.Count >= 4)
                {   // Adicionar todas as possibilidades de troca com 4 cartas

                    // 01011
                    possibilitiesTradeArray.Add(new List<Territory>() { pCards[0], pCards[1], pCards[3] });
                    // 01101
                    possibilitiesTradeArray.Add(new List<Territory>() { pCards[0], pCards[2], pCards[3] });
                    // 01110
                    possibilitiesTradeArray.Add(new List<Territory>() { pCards[1], pCards[2], pCards[3] });

                    if (pCards.Count >= 5)
                    {   // Adicionar todas as possibilidades de troca com 5 cartas

                        // 10011
                        possibilitiesTradeArray.Add(new List<Territory>() { pCards[0], pCards[1], pCards[4] });
                        // 10101
                        possibilitiesTradeArray.Add(new List<Territory>() { pCards[0], pCards[2], pCards[4] });
                        // 10110
                        possibilitiesTradeArray.Add(new List<Territory>() { pCards[1], pCards[2], pCards[4] });
                        // 11001
                        possibilitiesTradeArray.Add(new List<Territory>() { pCards[0], pCards[3], pCards[4] });
                        // 11010
                        possibilitiesTradeArray.Add(new List<Territory>() { pCards[1], pCards[3], pCards[4] });
                        // 11100
                        possibilitiesTradeArray.Add(new List<Territory>() { pCards[2], pCards[3], pCards[4] });
                    }
                }
            }
            else
                Debug("!!! ERRO !!! AI - Já foi testado que o jogador podia trocar carta, mas a mão dele tem menos que 3 cartas!");

            // Para cada possibilidade de troca
            possibilitiesTradeArray.ForEach(p => // Verifica se dá pra fazer a troca usando essas cartas
            {
                if (CanTradeInCards(p)) // Dá pra fazer a troca
                    canTradeArray.Add(p); // Adiciona na lista de trocas realmente possíveis
            });

            // Se existir uma troca possível
            if (canTradeArray.Count > 0)
            {
                // Agora eu preciso ver dessas trocas quais eu tenho os territórios
                List<Territory> chosenCards = new List<Territory>();
                var maxAmount = -1;

                // Para cada possibilidade de troca
                canTradeArray.ForEach(trade =>
                {
                    var amountCardsWithTerritories = 0;

                    // Verifica quantas cartas possuem territórios
                    trade.ForEach(card =>
                    {
                        if (playerTerritories.Exists(ts => ts.territory.id == card.id)) // Se o jogador tiver esse território
                            amountCardsWithTerritories++;
                    });

                    // Se a quantidade de cartas com territórios for maior que a quantidade anterior
                    if (amountCardsWithTerritories > maxAmount)
                    {
                        chosenCards = trade;
                        maxAmount = amountCardsWithTerritories;
                    }
                });

                return chosenCards;
            }

            return null;
        }

        /// Função para pegar todos os territórios vizinhos dos territórios do jogador que não é do jogador
        static List<TerritoryState> GetAllNeighbourTerritories()
        {
            var neighbourArray = new List<TerritoryState>();
            GAME.gs.currentPlayer.GetTerritories(fortifyTerritoryStates).ForEach(ts =>
            {// Para cada território do jogador
                ts.territory.neighbours.ForEach(neighbour =>
                { // Para cada pais vizinho deste 
                    var neighbourState = GetTS(neighbour.id, fortifyTerritoryStates);
                    if (neighbourState.owner.idGrow != ts.owner.idGrow)  // Se este país não pertence ao jogador
                        if (neighbourArray.IndexOf(neighbourState) < 0)  // Se não contém na lista
                            neighbourArray.Add(neighbourState);
                });
            });
            return neighbourArray;
        }

        /// Função para pegar todos os territórios vizinhos dos territórios do jogador se o novo território for do jogador
        static List<TerritoryState> GetAllNeighbourTerritoriesPlusNew(TerritoryState newTerritory)
        {
            var neighbourArray = new List<TerritoryState>();
            var territoryArray = GAME.gs.currentPlayer.GetTerritories(fortifyTerritoryStates);

            var auxPlayer = newTerritory.owner; // Troca o dono do território
            newTerritory.owner = GAME.gs.currentPlayer;

            // Adiciona o território na lista dos territórios do jogador
            territoryArray.Add(newTerritory);

            // Para cada território do jogador
            territoryArray.ForEach(territoryState =>
            { // Para cada pais vizinho deste territorio
                territoryState.territory.neighbours.ForEach(neighbour =>
                {
                    var neighbourState = GetTS(neighbour.id, fortifyTerritoryStates);
                    if (neighbourState.owner.idGrow != territoryState.owner.idGrow) // Se este país não pertence ao jogador
                        if (neighbourArray.IndexOf(neighbourState) < 0) // Se não tiver esse vizinho já na lista
                            neighbourArray.Add(neighbourState);
                });
            });

            // Volta o território ao seu dono original
            newTerritory.owner = auxPlayer;

            return neighbourArray;
        }

        /// Função para pegar os territórios que podem ser atacados perto de um determinado continente
        /// Se o continente foi conquistado, pega os territórios vizinhos dele. 
        /// Se o continente não foi conquistado, pega os territórios dentro do continente
        static List<TerritoryState> GetPossibleTerritoriesTarget(int continentId)
        {
            var neighbourArray = new List<TerritoryState>();

            // Para cada território do jogador
            GAME.gs.currentPlayer.GetTerritories(fortifyTerritoryStates).ForEach(ts =>
            {
                if (CountTerritoryLeftInContinent(Continent.LIST[continentId]) == 0)
                { // Se o jogador conquistou o continente
                    ts.territory.neighbours.ForEach(neighbour => // Para cada território vizinho
                    {
                        var tsNeighbour = GetTS(neighbour.id, fortifyTerritoryStates);
                        if (tsNeighbour.owner != ts.owner) // Se o território vizinho não for do jogador
                            if (neighbourArray.IndexOf(tsNeighbour) < 0) // Se a lista não contém o território
                                neighbourArray.Add(tsNeighbour);
                    });
                }
                else
                {   // Se o jogador não conquistou o continente ainda

                    // Se o território faz parte do continente selecionado
                    if (ts.territory.continent.id == continentId)
                    {
                        // Para cada país vizinho deste território no continente
                        ts.territory.neighbours.ForEach(neighbour =>
                        {
                            var neighbourState = GetTS(neighbour.id, fortifyTerritoryStates);

                            // Se o território não for do jogador e o território é do continente que queremos
                            if (neighbourState.owner.idGrow != ts.owner.idGrow && neighbour.continent.id == continentId)
                            {
                                neighbourArray.Add(neighbourState);
                            }
                        });
                    }
                }
            });

            return neighbourArray;
        }

        /// Função para retornar um Array de territórios que fazem fronteira com determinado território
        static List<TerritoryState> GetBorderTerritories(Continent c)
        {
            var myBorderTerritories = new List<TerritoryState>();
            GAME.gs.currentPlayer.GetTerritories(fortifyTerritoryStates).ForEach(ts =>
            { // Para cada território do jogador
                ts.territory.neighbours.ForEach(neighbour =>
                { // Para cada vizinho
                    if (ts.territory.continent != neighbour.continent && ts.territory.continent == c) // Se o país é fronteira com outro continente e este continente é o que estamos procurando
                        if (myBorderTerritories.IndexOf(ts) < 0) // Se não tem o território no Array
                            myBorderTerritories.Add(ts);
                });
            });
            return myBorderTerritories;
        }

        /// Função que escolhe qual o continente com maiores chances de ser conquistado
        static Continent GetContinentToConquer()
        {
            var armyCountPerContinent = new List<int>() { 0, 0, 0, 0, 0, 0 };

            // Pega a quantidade de exércitos de cada continente
            GAME.gs.currentPlayer.GetTerritories(fortifyTerritoryStates).ForEach(ts => armyCountPerContinent[ts.territory.continent.id] += ts.amountArmy);

            // NOTE: no código do Rodrigo aqui vai um 'if' pra conquistar a Oceania.
            // Ver se realmente é necessário!

            // Fator de quanto maior, mais fácil de ser conquistado
            var biggestFactor = -1000;
            var continentId = -1;

            // Para cada continente
            for (var i = 0; i < 6; i++)
            {
                // Conta a quantidade de exércitos no continente inteiro
                var amountArmiesOnContinent = 0;
                fortifyTerritoryStates.ForEach(ts =>
                {
                    if (ts.territory.continent.id == i)
                        amountArmiesOnContinent += ts.amountArmy;
                });

                // Calcula o fator = quantidade de exércitos do jogador no continente dividido pela quantidade de exércitos total no continente
                var factor = armyCountPerContinent[i] / amountArmiesOnContinent;

                if (factor > biggestFactor)
                {
                    biggestFactor = factor;
                    continentId = i;
                }

                // Se tiver faltando apenas 1 território no continente
                if (CountTerritoryLeftInContinent(Continent.LIST[i]) == 1)
                {
                    biggestFactor = 1;
                    continentId = i;
                }
            }
            return Continent.LIST[continentId];
        }

        /// Função que conta quantos territórios o jogador tem em determinado continente e retorna a quantidade de territórios que faltam
        static int CountTerritoryLeftInContinent(Continent c)
        {
            return c.territoriesCount - fortifyTerritoryStates.Where(ts => ts.territory.continent.id == c.id && ts.owner == GAME.gs.currentPlayer).ToList().Count;
        }

        /// Função para a IA testar se existe nestas cartas uma combinação válida para troca
        static bool CanTradeInCards(List<Territory> cards)
        {
            var amountCircle = 0;
            var amountSquare = 0;
            var amountTriangle = 0;
            var amountJoker = 0;

            cards.ForEach(c =>
            {
                switch (c.symbol)
                {
                    case SymbolType.Circle:
                        amountCircle++;
                        break;
                    case SymbolType.Square:
                        amountSquare++;
                        break;
                    case SymbolType.Triangle:
                        amountTriangle++;
                        break;
                    case SymbolType.Wildcard:
                        amountJoker++;
                        break;
                    default:
                        break;
                }
            });

            // Verifica se tem 3 iguais
            if (amountCircle >= 3 || amountSquare >= 3 || amountTriangle >= 3)
                return true;

            // Verifica se tem 3 diferentes
            if (amountCircle >= 1 && amountSquare >= 1 && amountTriangle >= 1)
                return true;

            // Verifica se tem coringa
            if (amountJoker >= 1 && cards.Count >= 3)
                return true;

            return false;
        }
        #endregion

        /// AUX
        static int Rand(int x)
        {
            return UnityEngine.Random.Range(0, x);
        }
    }

    class ResultTerritoryAttack
    {
        // O território de origem
        public TerritoryState tsOrigin;

        // O território alvo
        public TerritoryState tsTarget;

        // O fator de ataque (código indo de um número negativo até 1)
        public double factor;
    }
}
