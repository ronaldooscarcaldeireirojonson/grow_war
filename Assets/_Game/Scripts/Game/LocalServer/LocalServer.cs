﻿using Sfs2X.Entities.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Grow.War
{
    [System.Serializable]
    public class LocalServer : ServerInterface
    {
        public static int dados = 0;

        public Player currentPlayer = null;
        public List<Player> players = new List<Player>();
        public List<TerritoryState> territoryStates = new List<TerritoryState>();
        public List<Territory> deck = new List<Territory>(); // Lista de cartas que vão ser compradas
        public byte turnPhase = 0; // 0=fort | 1=attack | 2=move

        // counters
        public int tradeInCount = 0;
        public int roundCount = 0;

        // aux
        public bool hasConquerATerritory = false; // Flag que indica que o jogador do turno atual conquistou um território neste turno
        public TerritoryState conqueredTerritory = null;
        public TerritoryState conquerorTerritory = null;
        public List<int> turnArmy = null; // exército que o jogador atual tem para distribuir neste turno
        public Dictionary<int, int> moveArmy; // exército que o jogador atual tem para mover para cada território
        public Player winner = null;

        [Header("Dices")]
        public Vector3 offsetLeftDice = new Vector3(-20, -23, -10);
        public Vector3 offsetRightDice = new Vector3(20, -23, 10);

        public LocalServer(){}
        public LocalServer(List<Player> playersList)
        {
            // players
            players = playersList.OrderBy(a => Guid.NewGuid()).ToList();

            // deck
            deck.AddRange(Territory.LIST);
            deck = deck.OrderBy(a => Guid.NewGuid()).ToList();

            // objectives
            List<Objective> objectiveList = new List<Objective>();
            objectiveList.AddRange(Objective.LIST);

            // retira do jogo os objetivos de eliminar cores que não estão na partida
            foreach (GrowColor wColor in GrowColor.LIST)
                if (players.TrueForAll(p => p.warColor != wColor))
                    objectiveList.RemoveAll(obj => obj.target == wColor);

            // Distribui os territórios entre os players
            for (int i = 0; i < deck.Count; i++)
                territoryStates.Add(new TerritoryState(deck[i].id, players[i % players.Count]));

            // Ordena os jogadores corretamente: o jogador depois do último que recebeu o território é o que começa o jogo
            int amountTimesToRollbackPlayers = deck.Count % players.Count;
            for (int i = 0; i < amountTimesToRollbackPlayers; i++)
            {
                Player temp = players[0]; // Guarda o primeiro jogador
                for (int j = 0; j < players.Count - 1; j++) // 'Reposiciona' todos os jogadores 1 posição
                    players[j] = players[j + 1];
                players[players.Count - 1] = temp; // Coloca o primeiro jogador em último lugar
            }

            // Distribui os objetivos entre os jogadores sem deixar um jogador pegar o objetivo de destruir a própria cor
            for (int i = 0; i < players.Count; i++)
            {
                objectiveList = objectiveList.OrderBy(a => Guid.NewGuid()).ToList();
                players[i].objective = (objectiveList[0].target == players[i].warColor ? objectiveList[1] : objectiveList[0]);
                objectiveList.Remove(players[i].objective);
            }

            // Adiciona os dois coringas no deck de cartas
            deck.Add(Territory.Wildcard1);
            deck.Add(Territory.Wildcard2);

            // Embaralha novamente o deck pois agora eles vão ser as cartas (senão vai ficar na ordem dos territórios e com os wildcards no final)
            deck = deck.OrderBy(a => Guid.NewGuid()).ToList();

            // fim da criação do "LocalServer.GameState"
            // agora são clonados os jogadores e os territoriesStates para irem para o "GameController.GameState"
            GameController.Instance.gs = new GameState(
                players.Select(p => new Player(p.idGrow, p.GetName, p.warColor, p.isHuman, p.objective)).ToList(),
                territoryStates.Select(ts => new TerritoryState(ts.territory.id, ts.owner)).ToList());

            // espera o draft terminar pra mandar o startgame
            GameController.Instance.StartCoroutine(StartGame());
        }
        public LocalServer(Save save)
        {
            this.tradeInCount = save.tradeInCount;
            bool[] eliminatedPlayers = new bool[save.playerNames.Length];
            // players
            for (int i = 0; i < save.playerNames.Length; i++)
            {

                Player player = new Player();
                player.idUsuario = player.idGrow = i;
                player.name = save.playerNames[i];
                player.isHuman = !save.isCPU[i];
                player.isEliminated = save.isEliminated[i];
                player.cardCount = save.playerCardCount[i];

                eliminatedPlayers[i] = save.isEliminated[i];

                for (int y = 0; y < GrowColor.LIST.Count; y++)
                {
                    if(GrowColor.LIST[y].id == save.playerColor[i])
                    {
                        player.warColor = GrowColor.LIST[y];
                    }
                }

                players.Add(player);


            }
            // save deck
            for (int i = 0; i < save.deck.Length; i++)
            {
                if(save.deck[i] == 42)
                {
                    deck.Add(Territory.Wildcard1);
                }
                else if(save.deck[i] == 43)
                {
                    deck.Add(Territory.Wildcard2);
                }else
                {
                    deck.Add(Territory.LIST[save.deck[i]]);
                }

            }

            // objectives
            List<Objective> objectiveList = new List<Objective>();
            objectiveList.AddRange(Objective.LIST);
            // Distribui os territórios entre os players
            for (int i = 0; i < save.territoryId.Length; i++)
            {
                var t = new TerritoryState(save.territoryId[i], players[save.territoryPossession[i]]);
                t.amountArmy = save.troopsInTerritory[i];
                territoryStates.Add(t);


            }

            // Distribui os objetivos entre os jogadores sem deixar um jogador pegar o objetivo de destruir a própria cor
            for (int i = 0; i < players.Count; i++)
            {
                players[i].objective = objectiveList[save.playerObjectives[i]];


                for (int y = 0; y < 5; y++)
                {
                    if (save.playerCards[i, y] - 1 >= 0)
                    {
                        players[i].cards.Add(Territory.Get(save.playerCards[i, y] - 1));
                    }
                }
            }
            currentPlayer = players[save.thisTurn];
            // fim da criação do "LocalServer.GameState"


            for (int i = players.Count - 1; i >= 0; i--)
            {

            }

            roundCount = save.round;
            // agora são clonados os jogadores e os territoriesStates para irem para o "GameController.GameState"
            GameController.Instance.gs = new GameState(
                players.Select(p => new Player(p.idGrow, p.GetName, p.warColor, p.isHuman, p.isEliminated, p.cardCount, p.objective )).ToList(),
                territoryStates.Select(ts => new TerritoryState(ts.territory.id, ts.owner, ts.amountArmy)).ToList(),
                save.thisTurn);
            // espera o draft terminar pra mandar o startgame
            GameController.Instance.skipStartAnim = true;
            GameController.Instance.StartCoroutine(StartLoadedGame(currentPlayer, save.tradeInCount));
        }

        IEnumerator StartGame()
        {
            yield return new WaitUntil(() => GameController.Instance.draftFinished);
            StartNewTurn();
        }
        IEnumerator StartLoadedGame(Player savedPlayer, int tradeCount)
        {

            yield return new WaitUntil(() => GameController.Instance.draftFinished);
            GameController.Instance.gs.currentPlayer = savedPlayer;
            GameController.Instance.gs.thisPlayer = savedPlayer;
            this.tradeInCount = tradeCount;
            //Debug.Log(tradeCount);
            LoadTurn(savedPlayer, tradeCount);
        }

        TerritoryState GetTS(int territoryId)
        {
            return territoryStates.Find(ts => ts.territory.id == territoryId);
        }

        bool IsGameOver()
        {
            return winner != null;
        }

        void StartNewTurn()
        {
            if (IsGameOver())
            {
                PlayerPrefs.DeleteKey("save");
                Debug.Log("Save deleted");
                PlayerPrefs.Save();
                //GameController.Instance.UpdateFastModeCanvasVisibility(false);
                return;
            }

            Player previousPlayer = currentPlayer;

            int round = roundCount;

            /// START GameState.StartNewTurn
            // confere se jogador anterior merece carta
            if (currentPlayer != null && hasConquerATerritory && currentPlayer.cards.Count < 5)
            {
                Territory card = deck[UnityEngine.Random.Range(0, deck.Count)];
                deck.Remove(card);
                currentPlayer.cards.Add(card);

                // se o deck ficou sem cartas, reconstrua-o
                if (deck.Count == 0)
                {
                    deck.AddRange(Territory.LIST);
                    deck.Add(Territory.Wildcard1);
                    deck.Add(Territory.Wildcard2);
                    players.ForEach(p => deck.RemoveAll(c => p.cards.Exists(c2 => c2.id == c.id)));
                    deck = deck.OrderBy(a => Guid.NewGuid()).ToList();
                }
            }

            // current player
            int lastIndex = players.IndexOf(currentPlayer);
            do
            {
                int nextIndex = currentPlayer == null ? 0 : (players.IndexOf(currentPlayer) + 1);
                if (nextIndex >= players.Count)
                {
                    nextIndex = 0;
                    ++roundCount;
                }
                currentPlayer = players[nextIndex];

                if (lastIndex == nextIndex)
                    break; // este break serve para evitar loop infinito
            } while (currentPlayer.isDisconnected || currentPlayer.isEliminated);

            // turn army
            turnArmy = new List<int>(7); // Existem 7 posições pois temos 6 continentes e 1 a mais só pra colocar os territórios que o jogador pode colocar onde quiser
            Continent.LIST.ForEach(c => turnArmy.Add(currentPlayer.HasContinent(c, territoryStates) ? c.bonusArmy : 0)); // Adiciona os exércitos por continente
            turnArmy.Add(Math.Max(3, territoryStates.Where(ts => ts.owner.idGrow == currentPlayer.idGrow).ToList().Count / 2)); // Coloca os exércitos que o jogador pode colocar onde quiser (ganha no mínimo 3)

            // etc
            hasConquerATerritory = false;
            turnPhase = 0;
            /// END GameState.StartNewTurn



            if (previousPlayer != null && round > 0)
                GameController.Instance.CardsResponse(previousPlayer.cards.Count, previousPlayer.idGrow, previousPlayer.cards.Select(c => c.id).ToList());


            if (round > 0 && round % 5 == 0) AdMobManager.Instance.OfflineInterstitial();
            GameController.Instance.StartNewTurnResponse(currentPlayer.idGrow, turnArmy.ToList());
        }
        void LoadTurn(Player savedPlayer, int tradeCount)
        {
            if (IsGameOver())
                return;

            int round = 5;
            currentPlayer = savedPlayer;
            Player previousPlayer = currentPlayer;
            /// START GameState.StartNewTurn
            // confere se jogador anterior merece carta
            if (currentPlayer != null && hasConquerATerritory && currentPlayer.cards.Count < 5)
            {
                Territory card = deck[UnityEngine.Random.Range(0, deck.Count)];
                deck.Remove(card);
                currentPlayer.cards.Add(card);

                // se o deck ficou sem cartas, reconstrua-o
                if (deck.Count == 0)
                {
                    deck.AddRange(Territory.LIST);
                    deck.Add(Territory.Wildcard1);
                    deck.Add(Territory.Wildcard2);
                    players.ForEach(p => deck.RemoveAll(c => p.cards.Exists(c2 => c2.id == c.id)));
                    deck = deck.OrderBy(a => Guid.NewGuid()).ToList();
                }
            }
            // current player
            int lastIndex = players.IndexOf(currentPlayer);
            do
            {
                int nextIndex = currentPlayer == null ? 0 : (players.IndexOf(currentPlayer) + 1);
                if (nextIndex >= players.Count)
                {
                    nextIndex = 0;
                    ++roundCount;
                }
                currentPlayer = players[nextIndex];

                if (lastIndex == nextIndex)
                    break; // este break serve para evitar loop infinito
            } while (currentPlayer.isDisconnected || currentPlayer.isEliminated);


            // turn army
            turnArmy = new List<int>(7); // Existem 7 posições pois temos 6 continentes e 1 a mais só pra colocar os territórios que o jogador pode colocar onde quiser
            Continent.LIST.ForEach(c => turnArmy.Add(currentPlayer.HasContinent(c, territoryStates) ? c.bonusArmy : 0)); // Adiciona os exércitos por continente
            turnArmy.Add(Math.Max(3, territoryStates.Where(ts => ts.owner.idGrow == currentPlayer.idGrow).ToList().Count / 2)); // Coloca os exércitos que o jogador pode colocar onde quiser (ganha no mínimo 3)

            // etc
            hasConquerATerritory = false;
            turnPhase = 0;
            /// END GameState.StartNewTurn
            //todo potencial bug
            for (int i = 0; i < players.Count; i++)
            {
                GameController.Instance.CardsResponse(players[i].cards.Count, players[i].idGrow, players[i].cards.Select(c => c.id).ToList());

            }

            if (previousPlayer != null && round > 0)
                GameController.Instance.CardsResponse(previousPlayer.cards.Count, previousPlayer.idGrow, previousPlayer.cards.Select(c => c.id).ToList());
            GameController.Instance.StartNewTurnResponse(currentPlayer.idGrow, turnArmy.ToList());

            this.tradeInCount = tradeCount;
            Debug.Log("Current trade count is : " + tradeCount);
            GameController.Instance.gs.tradeInCount = tradeCount;
            GameController.Instance.FooterUpdate();
        }

        void EndGame()
        {
            winner = currentPlayer;
            PlayerPrefs.DeleteKey("save");
            Debug.Log("Save deleted");
            PlayerPrefs.Save();
            GameController.Instance.UpdateFastModeCanvasVisibility(false);

        }

        // INTERFACE
        public bool IsOnline()
        {
            return false;
        }

        public void FortifyRequest(int territoryId, int amount)
        {
            var ts = GetTS(territoryId);

            if (turnArmy == null || turnPhase != 0 || amount <= 0 || ts.owner.idGrow != currentPlayer.idGrow)
            {
                Debug.LogError("[ANTI-CHEAT][LocalServer.FortifyRequest] " + currentPlayer.GetName);
                return;
            }

            int turnArmyIndex = turnArmy[ts.territory.continent.id] > 0 ? ts.territory.continent.id : 6;
            if (turnArmy[turnArmyIndex] < amount)
            {
                Debug.LogError("[ANTI-CHEAT][LocalServer.FortifyRequest] " + currentPlayer.GetName + " tentou fortificar mais exércitos do que o disponível.");
                return;
            }

            // fortificação está validada, então remove a quantidade dos exércitos disponíveis
            turnArmy[turnArmyIndex] -= amount;

            // faz a fortificação
            ts.amountArmy += amount;

            // se acabou os exércitos disponíveis, muda para a fase de ataque automaticamente

            if (turnArmy.TrueForAll(a => a <= 0))
            {
                turnArmy = null;
                turnPhase = 1;
            }

            int winnerObjectiveId = currentPlayer.CheckObjective(territoryStates) ? currentPlayer.objective.id : -1;

            GameController.Instance.FortifyResponse(territoryId, ts.amountArmy, winnerObjectiveId);

            if (winnerObjectiveId >= 0)
                EndGame();
            else if (turnPhase == 1)
            if (roundCount == 0){
                    GameController.Instance.SaveGame();
                    StartNewTurn();
            }
            else{
                    GameController.Instance.SaveGame();
                    GameController.Instance.AttackPhaseStartResponse();
            }
        }

        public void AttackRequest(int originId, int targetId)
        {
            TerritoryState origin = GetTS(originId);
            TerritoryState target = GetTS(targetId);
            Player defenderPlayer = target.owner;
            if(GameController.Instance.gs.currentPlayer.isHuman && Sioux.LocalData.Get<bool>("fastIA"))
            {
                do
                {
                    GameController.Instance.StartCoroutine(OnAttackRoutine(origin, target));
                } while (target.owner.idGrow != origin.owner.idGrow && origin.amountArmy > 1);
            }
            else
            {
                GameController.Instance.StartCoroutine(OnAttackRoutine(origin, target));

            }

        }

        public void AttackPhaseEndRequest()
        {
            if (turnPhase != 1)
            {
                Debug.Log("[ANTI-CHEAT][LocalServer.AttackPhaseEndRequest] " + currentPlayer.GetName + " tentou finalizar a fase de ataque mas não está na fase de ataque. turnPhase=" + turnPhase);
                return;
            }

            turnPhase = 2;
            moveArmy = new Dictionary<int, int>();
            currentPlayer.GetTerritories(territoryStates).ForEach(ts => moveArmy.Add(ts.territory.id, ts.amountArmy - 1));
            GameController.Instance.AttackPhaseEndResponse();
        }

        public void MoveRequest(int originId, int targetId, int amount)
        {
            var tsOrigin = GetTS(originId);
            var tsTarget = GetTS(targetId);

            /// START GameState.OnMove
            if (!(turnPhase == 2 || (turnPhase == 1 && conquerorTerritory != null && conqueredTerritory != null)))
            {
                Debug.Log("[ANTI-CHEAT][LocalServer.MoveRequest] " + currentPlayer.GetName + " tentou mover fora da fase de remanejamento (ou da fase de mover do ataque).");
                return;
            }

            if (turnPhase == 1)
            {
                if (conquerorTerritory != tsOrigin)
                {
                    Debug.Log("[ANTI-CHEAT][LocalServer.MoveRequest] " + currentPlayer.GetName + " tentou mover (na fase de mover do ataque) do terrítorio errado.");
                    return;
                }
                if (conqueredTerritory != tsTarget)
                {
                    Debug.Log("[ANTI-CHEAT][LocalServer.MoveRequest] " + currentPlayer.GetName + " tentou mover (na fase de mover do ataque) para o terrítorio errado.");
                    return;
                }
                if (amount > 3)
                {
                    Debug.Log("[ANTI-CHEAT][LocalServer.MoveRequest] " + currentPlayer.GetName + " tentou mover (na fase de mover do ataque) mais do que 3 exércitos.");
                    return;
                }
            }

            // confere se: os territórios de origem e destino são do jogador atual; a quantidade é maior que zero; e se o jogador possui essa quantidade pra mover
            if (tsOrigin.owner != currentPlayer || tsTarget.owner != currentPlayer || amount <= 0 || amount >= tsOrigin.amountArmy)
            {
                Debug.Log("[ANTI-CHEAT][LocalServer.MoveRequest] " + currentPlayer.GetName);
                return;
            }

            int id = tsOrigin.territory.id;

            if (turnPhase == 2 && (moveArmy == null || !moveArmy.ContainsKey(id) || amount > moveArmy[id]))
            {
                Debug.Log("[ANTI-CHEAT][LocalServer.MoveRequest] " + currentPlayer.GetName + " não pode mover pois o moveArmy é nulo, ou moveArmy não tem esse território, ou a quantidade é maior do que a permitida para este território.");
                return;
            }

            if (turnPhase == 2 && moveArmy != null)
                moveArmy[id] -= amount;

            tsOrigin.amountArmy -= amount;
            tsTarget.amountArmy += amount;
            conquerorTerritory = null;
            conqueredTerritory = null;
            /// END GameState.OnMove

            int winnerObjectiveId = currentPlayer.CheckObjective(territoryStates) ? currentPlayer.objective.id : -1;
            GameController.Instance.MoveResponse(tsOrigin.ToSFSObject(), tsTarget.ToSFSObject(), winnerObjectiveId);

            if (winnerObjectiveId >= 0)
                EndGame();
        }

        public void EndTurnRequest()
        {



            if (turnPhase != 2)
            {
                Debug.Log("[ANTI-CHEAT][LocalServer.EndTurnRequest] " + currentPlayer.GetName + " tentou finalizar o turno antes da fase de remanejamento.");
                return;
            }



            StartNewTurn();
        }

        public void TradeInCardsRequest(List<int> cardIds)
        {
            if (cardIds.Count != 3)
            {
                Debug.Log("[ANTI-CHEAT][GameState.OnTradeInCards] " + currentPlayer.GetName + " tentou trocar um número de cartas diferente de 3. cardIds.Count=" + cardIds.Count);
                return;
            }

            if (turnPhase != 0 || turnArmy == null || turnArmy[6] <= 0)
            {
                Debug.Log("[ANTI-CHEAT][GameState.OnTradeInCards] " + currentPlayer.GetName + " tentou trocar cartas fora da fase de fortificação.");
                return;
            }

            if (!cardIds.TrueForAll(cId => currentPlayer.cards.Any(c => c.id == cId)))
            {
                Debug.Log("[ANTI-CHEAT][GameState.OnTradeInCards] " + currentPlayer.GetName + " tentou trocar cartas que não possui. ENVIADAS: " + cardIds.Select(id => id.ToString()).Aggregate((a, b) => a + " " + b) + " POSSUIDAS: " + currentPlayer.cards.Select(c => c.id.ToString()).Aggregate((a, b) => a + " " + b));
                return;
            }

            List<Territory> territories = cardIds.Select(id => Territory.Get(id)).ToList();
            if (!CanTradeIn(territories))
            {
                Debug.Log("[ANTI-CHEAT][GameState.OnTradeInCards] " + currentPlayer.GetName + " tentou trocar cartas que não formam uma troca.");
                return;
            }

            SFSArray tsArray = new SFSArray();
            List<int> wildcards = new List<int>();

            // Verifica se o jogador tem algum território de alguma carta, pra adicionar +2 exércitos naquele território
            foreach (int id in cardIds)
            {
                if (id > 41)
                {
                    wildcards.Add(id);
                    continue;
                }
                TerritoryState ts = GetTS(id);
                if (ts.owner == currentPlayer)
                    ts.amountArmy += 2;
                tsArray.AddSFSObject(ts.ToSFSObject());
            }

            int bonusArmy = tradeInCount <= 4 ? 4 + 2 * tradeInCount : 15 + 5 * (tradeInCount - 5);

            turnArmy[6] += bonusArmy;
            currentPlayer.cards.RemoveAll(c => cardIds.Contains(c.id)); // remove as cartas da mão do jogador
            tradeInCount++;

            GameController.Instance.TradeInCardsResponse(tsArray, bonusArmy, wildcards);
        }

        // events
        IEnumerator OnAttackRoutine(TerritoryState tsOrigin, TerritoryState tsTarget)
        {
            if (Dice.allDice.Count > 0)
                yield break;
            if (tsOrigin.owner.idGrow != currentPlayer.idGrow)
            {
                Debug.Log("[ANTI-CHEAT][LocalServer.OnAttack] " + currentPlayer.GetName + " tentou fazer um ataque a partir de um território que não o pertence.");
                yield break;
            }
            if (tsTarget.owner.idGrow == currentPlayer.idGrow)
            {
                Debug.Log("[ANTI-CHEAT][LocalServer.OnAttack] " + currentPlayer.GetName + " tentou fazer um ataque em um território dele próprio.");

                yield break;
            }
            if (tsOrigin.amountArmy < 2)
            {
                Debug.Log("[ANTI-CHEAT][LocalServer.OnAttack] " + currentPlayer.GetName + " tentou fazer um ataque a partir de um território que possui apenas " + tsOrigin.amountArmy + " exército.");

                yield break;
            }
            if (turnPhase != 1)
            {
                Debug.Log("[ANTI-CHEAT][LocalServer.OnAttack] " + currentPlayer.GetName + " tentou fazer um ataque fora da fase de ataque.");

                yield break;
            }
            if (conquerorTerritory != null || conqueredTerritory != null)
            {
                Debug.Log("[ANTI-CHEAT][LocalServer.OnAttack] " + currentPlayer.GetName + " tentou fazer um ataque enquanto deveria fazer uma realocação pós-ataque.");

                yield break;
            }
            GameController.Instance.footer.phaseButton.interactable = false;
            Player defenderPlayer = tsTarget.owner;
            BattleResult result = new BattleResult();

            // Pega a quantidade de exércitos que está atacando e defendendo
            int amountArmyAttacking = Math.Min(3, tsOrigin.amountArmy - 1);
            int amountArmyAttacked = Math.Min(3, tsTarget.amountArmy);


            // Rola os dados
            if (Sioux.LocalData.Get<bool>("FastGame") || Sioux.LocalData.Get<bool>("fastIA"))
            {
                for (int i = 0; i < amountArmyAttacking; i++)
                    result.dicesAttack.Add(DiceRoll());
                for (int i = 0; i < amountArmyAttacked; i++)
                    result.dicesDefense.Add(DiceRoll());
            }
            else
            {
                GameController.Instance.floorDiceCollider.enabled = true;

                Dice.Roll(amountArmyAttacking + "d6", "d6-red-dots",
                    Camera.main.transform.position + new Vector3((offsetLeftDice.x * Mathf.Lerp(1, GameController.Instance.diceFactor, ((-30 + Camera.main.fieldOfView) / 50f))), offsetLeftDice.y, (offsetLeftDice.z * Mathf.Lerp(1, GameController.Instance.diceFactor, ((-30 + Camera.main.fieldOfView) / 50f)))),
                    (Vector3.right + Vector3.forward) * 29,
                    true);

                Dice.Roll(amountArmyAttacked + "d6", "d6-yellow-dots",
                    Camera.main.transform.position + new Vector3((offsetRightDice.x * Mathf.Lerp(1, GameController.Instance.diceFactor, ((-30 + Camera.main.fieldOfView) / 50f))), offsetRightDice.y, (offsetRightDice.z * Mathf.Lerp(1, GameController.Instance.diceFactor, ((-30 + Camera.main.fieldOfView) / 50f)))),
                    (Vector3.left + Vector3.back) * 29,
                    false);

                yield return new WaitUntil(() => Dice.rolling == false);

                for (int i = 0; i < Dice.allDice.Count; i++)
                {
                    ((RollingDie)Dice.allDice[i]).gameObject.layer = LayerMask.NameToLayer("DiceNoCollision");
                }
                yield return new WaitForSeconds(1.5f);

                Dice.Value(true).ForEach((die) => result.dicesAttack.Add(die));
                Dice.Value(false).ForEach((die) => result.dicesDefense.Add(die));
            }

            // Ordena do maior pro menor
            result.dicesAttack = result.dicesAttack.OrderByDescending(d => d).ToList();
            result.dicesDefense = result.dicesDefense.OrderByDescending(d => d).ToList();

            // Calcula o resultado
            // Quantidade de exércitos que os jogadores perderam
            int amountAttackArmyLoss = 0;
            int amountDefenseArmyLoss = 0;

            for (int i = 0; i < Math.Min(result.dicesAttack.Count, result.dicesDefense.Count); i++)
                if (result.dicesDefense[i] >= result.dicesAttack[i])
                    amountAttackArmyLoss++;
                else
                    amountDefenseArmyLoss++;

            // Diminui os exércitos
            tsOrigin.amountArmy = Math.Max(0, tsOrigin.amountArmy - amountAttackArmyLoss);
            tsTarget.amountArmy = Math.Max(0, tsTarget.amountArmy - amountDefenseArmyLoss);

            while (result.dicesAttack.Count < 3)
                result.dicesAttack.Add(0);

            while (result.dicesDefense.Count < 3)
                result.dicesDefense.Add(0);

            if (tsTarget.amountArmy == 0) // conquistou!
            {
                tsTarget.owner = tsOrigin.owner;
                result.isConquest = hasConquerATerritory = true;

                // confere se eliminou o defensor
                result.eliminated = defenderPlayer.GetTerritories(territoryStates).Count <= 0;
                if (result.eliminated)
                {
                    defenderPlayer.isEliminated = true;
                    defenderPlayer.cards.ForEach(c =>
                    {
                        if (currentPlayer.cards.Count < 5)
                            currentPlayer.cards.Add(c);
                    });
                    defenderPlayer.cards.Clear();
                }

                // já que eleminou, confere se o atacante ganhou a partida por eliminar o defensor
                if ((result.eliminated && tsOrigin.owner.objective.target == defenderPlayer.warColor) || currentPlayer.CheckObjective(territoryStates))
                {
                    result.winner = true;
                    tsOrigin.amountArmy--;
                    tsTarget.amountArmy++;
                    Debug.Log("VICTORY!");
                }
                else if (tsOrigin.amountArmy > 2)
                {
                    conquerorTerritory = tsOrigin; // isto é uma preparação para o move pós-conquista
                    conqueredTerritory = tsTarget;
                }
                else
                {
                    tsTarget.amountArmy = tsOrigin.amountArmy = 1;
                }
            }

            if (result == null)
                yield break;

            GameController.Instance.AttackResponse(
                tsOrigin.ToSFSObject(),
                tsTarget.ToSFSObject(),
                result.dicesAttack,
                result.dicesDefense,
                result.isConquest,
                result.eliminated ? defenderPlayer.idGrow : -1,
                result.winner ? currentPlayer.objective.id : -1);

            if (result.winner)
            {
                EndGame();
            }
            else if (result.eliminated)
            {
                GameController.Instance.CardsResponse(currentPlayer.cards.Count, currentPlayer.idGrow, currentPlayer.cards.Select(c => c.id).ToList());
            }

            //yield return new WaitForSeconds(0.5f);

            Dice.Clear();
            GameController.Instance.floorDiceCollider.enabled = false;
            GameController.Instance.footer.phaseButton.interactable = true;
        }

        // aux
        private int DiceRoll()
        {
            return UnityEngine.Random.Range(1, 7);
        }

        bool CanTradeIn(List<Territory> selectedCards)
        {
            if (selectedCards.Count != 3)
                return false; // para trocar, tem que ter 3 cards selecionados

            long jokersCount = selectedCards.Where(c => c.symbol == SymbolType.Wildcard).ToList().Count;
            selectedCards.RemoveAll(c => c.symbol == SymbolType.Wildcard);

            return jokersCount >= 1 || CompareCards(selectedCards, false) || CompareCards(selectedCards, true);
        }

        bool CompareCards(List<Territory> cards, bool equals)
        {
            for (int i = 0; i < cards.Count; ++i)
                for (int j = i + 1; j < cards.Count; ++j)
                    if ((cards[i].symbol != cards[j].symbol) == equals)
                        return false;
            return true;
        }
    }

    public class BattleResult
    {
        public bool isConquest = false; // Se o ataque conseguiu eliminar todos os exércitos da defesa e capturou o território
        public List<int> dicesAttack = new List<int>(); // Os dados de ataque
        public List<int> dicesDefense = new List<int>(); // Os dados de defesa
        public bool eliminated = false;
        public bool winner = false;
    }
}