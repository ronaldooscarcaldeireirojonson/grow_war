﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Grow.War
{
    public interface ServerInterface
    {
        bool IsOnline();

        void FortifyRequest(int territoryId, int amount);

        void AttackRequest(int originId, int targetId);

        void AttackPhaseEndRequest();

        void MoveRequest(int originId, int targetId, int amount);

        void EndTurnRequest();

        void TradeInCardsRequest(List<int> cardIds);
    }
}
