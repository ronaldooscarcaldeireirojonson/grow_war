﻿using Sfs2X.Entities;
using Sfs2X.Entities.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace Grow.War
{
    [System.Serializable]
    public class GameState
    {
        public Player thisPlayer;
        public Player currentPlayer;
        public List<Player> players = new List<Player>();
        public List<TerritoryState> territoryStates = new List<TerritoryState>();
        public int phase = 0;
        public bool hasConquerATerritory;
        public List<int> turnArmy;
        public Dictionary<int, int> moveArmy;


        // counters
        //public int roundCount = 0;
        public int tradeInCount = 0;

        public GameState() { }
        // cctor -> local mode
        public GameState(List<Player> players, List<TerritoryState> territoryStates)
        {
            this.players = players;
            thisPlayer = players[0];
            this.territoryStates = territoryStates;
        }
        public GameState(List<Player> players, List<TerritoryState> territoryStates, int thisPlayerIndex)
        {
            this.players = players;
            thisPlayer = players[thisPlayerIndex];
            this.territoryStates = territoryStates;
        }

        // cctor -> online mode
        public GameState(List<User> users, int selfGrowId, List<int> playerOrder, ISFSArray tsArray, int thisPlayerObjectiveId)
        {
            // players
            var tempPlayers = new List<Player>();
            users.ForEach(user => tempPlayers.Add(new Player(user)));
            playerOrder.ForEach(idGrow => players.Add(tempPlayers.Find(p => p.idGrow == idGrow)));

            // this player
            thisPlayer = players.Find(p => p.idGrow == selfGrowId);
            thisPlayer.objective = Objective.LIST[thisPlayerObjectiveId];

            // territory states
            for (int i = 0; i < tsArray.Count; ++i)
            {
                var obj = tsArray.GetSFSObject(i);
                territoryStates.Add(new TerritoryState(obj.GetInt("t")));
                UpdateTS(obj);
            }
        }

        // cctor -> reconnect
        public GameState(IEnumerable<SFSObject> playersObjects, int selfGrowId, ISFSArray tsArray, int thisPlayerObjectiveId)
        {
            // players
            foreach (var pObj in playersObjects)
                players.Add(new Player(pObj));

            // this player
            thisPlayer = players.Find(p => p.idGrow == selfGrowId);
            thisPlayer.objective = Objective.LIST[thisPlayerObjectiveId];

            // territory states
            for (int i = 0; i < tsArray.Count; ++i)
            {
                var obj = tsArray.GetSFSObject(i);
                territoryStates.Add(new TerritoryState(obj.GetInt("t")));
                if (GameController.Instance)
                    GameController.Instance.territoryObjects.Find(to => to.territoryId == territoryStates[i].territory.id).territoryState = territoryStates[i];
                UpdateTS(obj);
            }
        }

        public Player GetPlayer(int idGrow)
        {
            return players.Find(p => p.idGrow == idGrow);
        }

        public TerritoryState GetTS(int territoryId)
        {
            return territoryStates.Find(ts => ts.territory.id == territoryId);
        }

        public TerritoryObject GetTO(ISFSObject obj)
        {
            return GameController.Instance.territoryObjects.Find(tempTo => tempTo.territoryId == GetTS(obj.GetInt("t")).territory.id);
        }

        public void UpdateTS(ISFSObject obj)
        {
            var ts = GetTS(obj.GetInt("t"));
            ts.amountArmy = obj.GetInt("a");
            ts.owner = GetPlayer(obj.GetInt("o"));
            if (GameController.Instance == null)
                return;
            var to = GameController.Instance.territoryObjects.Find(tempTo => tempTo.territoryId == ts.territory.id);
            if (to != null)
                to.RefreshTerritory();
        }

        // checks
        public bool IsMyTurn()
        {
            return currentPlayer.isHuman && currentPlayer.idGrow == thisPlayer.idGrow;
        }

        public bool IsGameOver()
        {
            return false;// TODO: fazer
        }

        // events
        public void StartNewTurn(int currentPlayerIdGrow, List<int> turnArmy)
        {
            hasConquerATerritory = false;
            moveArmy = null;
            currentPlayer = GetPlayer(currentPlayerIdGrow);


            this.turnArmy = turnArmy;
            phase = 0;
        }

        public void OnFortify(int territoryId, int amountArmy)
        {
            var ts = GetTS(territoryId);
            var turnArmyIndex = turnArmy[ts.territory.continent.id] > 0 ? ts.territory.continent.id : 6;
            turnArmy[turnArmyIndex] -= amountArmy - ts.amountArmy;
            ts.amountArmy = amountArmy;
        }

        public void OnAttackPhaseStart()
        {
            phase = 1;
        }

        public void OnAttack(ISFSObject origin, ISFSObject target, int eliminatedPlayerId)
        {
            if (origin.GetInt("o") == target.GetInt("o"))
                hasConquerATerritory = true;
            UpdateTS(origin);
            UpdateTS(target);

            if (eliminatedPlayerId >= 0)
            {
                var eliminatedPlayer = GetPlayer(eliminatedPlayerId);
                eliminatedPlayer.isEliminated = true;
                eliminatedPlayer.cards.Clear();
                eliminatedPlayer.cardCount = 0;
            }
        }

        public void OnAttackPhaseEnd()
        {
            moveArmy = new Dictionary<int, int>();
            phase = 2;
            currentPlayer.GetTerritories(territoryStates).ForEach(ts => moveArmy.Add(ts.territory.id, ts.amountArmy - 1));
        }

        public int OnMove(ISFSObject origin, ISFSObject target)
        {
            var tsOrigin = GetTS(origin.GetInt("t"));
            var oldAmount = tsOrigin.amountArmy;
            UpdateTS(origin);
            UpdateTS(target);
            var amountMoved = oldAmount - tsOrigin.amountArmy;
            if (moveArmy != null && moveArmy.ContainsKey(tsOrigin.territory.id))
                moveArmy[tsOrigin.territory.id] -= amountMoved;
            return amountMoved;
        }

        public void OnTradeInCards(ISFSArray territories, int bonusArmy, List<int> wildcards)
        {
            foreach (ISFSObject obj in territories)
                UpdateTS(obj);

            tradeInCount++;

            currentPlayer.cardCount -= 3;

            if (turnArmy != null)
                turnArmy[6] += bonusArmy;

            var idsList = territories.Cast<SFSObject>().ToList().ConvertAll(obj => obj.GetInt("t")).Concat(wildcards).ToList();
            //Debug.Log("cartas que o currentPlayer possui: " + currentPlayer.cards.ConvertAll(a => a.id.ToString()).Aggregate((a, b) => a.ToString() + " " + b.ToString()));
            //Debug.Log("cartas a serem removidas: " + idsList.ConvertAll(a => a.ToString()).Aggregate((a, b) => a.ToString() + " " + b.ToString()));
            var x = currentPlayer.cards.RemoveAll(c => idsList.Contains(c.id));
            //Debug.Log("número de cartas removidas" + x);
        }

        public Player DisconectPlayer(int disconnectedPlayerId)
        {
            var p = GetPlayer(disconnectedPlayerId);
            p.isDisconnected = true;
            return p;
        }


    }
}