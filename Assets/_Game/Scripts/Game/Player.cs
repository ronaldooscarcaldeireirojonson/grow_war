﻿using Sfs2X.Entities;
using Sfs2X.Entities.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Grow.War
{
    //[Serializable]
    public class Player
    {
        // grow
        public int idUsuario;
        public int idGrow;
        public string name;
        public int level = 0;
        public string avatarString;

        //[NonSerialized]
        public Sprite avatar;

        // war
        public GrowColor warColor;
        public bool isDisconnected = false;
        public bool isEliminated = false;
        public Objective objective;
        public List<Territory> cards = new List<Territory>();
        public int cardCount = 0;
        public int patent = 1;

        // IA
        public bool isHuman = true;
        public int agressiveMultiplier = 0; // só é utilizado para a IA

        public string GetName { get { return (isDisconnected && !isEliminated ? "[BOT] " : "") + name; } }

        public Player(){}
        // cctor -> local mode
        public Player(int id, string name, GrowColor warColor, bool isHuman, Objective objective = null)
        {
            agressiveMultiplier = 2 + (int)Math.Floor(UnityEngine.Random.value * 4);
            idUsuario = idGrow = id;
            this.name = name;
            this.warColor = warColor;
            this.isHuman = isHuman;
            this.objective = objective;
        }
        public Player(int id, string name, GrowColor warColor, bool isHuman, bool eliminated, int cardC, Objective objective = null)
        {
            agressiveMultiplier = 2 + (int)Math.Floor(UnityEngine.Random.value * 4);
            idUsuario = idGrow = id;
            this.name = name;
            this.warColor = warColor;
            this.isHuman = isHuman;
            this.objective = objective;
            this.isEliminated = eliminated;
            this.cardCount = cardC;
        }

        // cctor -> online mode
        public Player(User user)
        {
            try
            {
                var uvAvatar = GrowSFS.GetVar(user, "avatar");
                avatarString = uvAvatar == null ? "" : uvAvatar.GetStringValue();

                ServerManager.instance.GetAvatarSprite(avatarString, sprite => avatar = sprite);

                name = user.Name;


                try { warColor = GrowColor.LIST[GrowSFS.GetVar(user, "color").GetIntValue()]; }
                catch (Exception e)
                {
                    warColor = GrowColor.LIST[0];
                    Debug.LogError("[Player.cctor] A UserVariable de COLOR voltou nula!!! Então a cor ZERO foi escolhida. Possivelmente haverá cor dupliada na partida.");
                    Debug.LogError(e);
                }

                try { idUsuario = GrowSFS.GetVar(user, "id").GetIntValue(); }
                catch (Exception e)
                {
                    idUsuario = -1;
                    Debug.LogError("[Player.cctor] A UserVariable de ID voltou nula!!! Então o ID foi definido como -1! Possivelmente haverá erro.");
                    Debug.LogError(e);
                }

                try { idGrow = GrowSFS.GetVar(user, "idGrow").GetIntValue(); }
                catch (Exception e)
                {
                    idGrow = -1;
                    Debug.LogError("[Player.cctor] A UserVariable de idGrow voltou nula!!! Então o idGrow foi definido como -1! Possivelmente haverá erro.");
                    Debug.LogError(e);
                }

                try { level = GrowSFS.GetVar(user, "level").GetIntValue(); }
                catch (Exception e)
                {
                    level = -1;
                    Debug.LogError("[Player.cctor] A UserVariable de LEVEL voltou nula!!! Então o LEVEL foi definido como -1! Possivelmente haverá erro.");
                    Debug.LogError(e);
                }

                try { patent = GrowSFS.GetVar(user, "patente").GetIntValue(); }
                catch (Exception e)
                {
                    patent = 1;
                    Debug.LogError("[Player.cctor] A UserVariable de patent voltou nula!!! Então o patent foi definido como 1! Possivelmente haverá erro.");
                    Debug.LogError(e);
                }
            }
            catch (Exception e)
            {
                Debug.LogError("[Player.cctor] blob -> " + e);
            }

        }

        // cctor -> reconnect
        public Player(SFSObject playerObject)
        {
            ServerManager.instance.GetAvatarSprite(playerObject.GetUtfString("avatar"), sprite => avatar = sprite);
            name = playerObject.GetUtfString("name");

            warColor = GrowColor.LIST[playerObject.GetInt("color")];

            idUsuario = playerObject.GetInt("idWar");
            idGrow = playerObject.GetInt("idGrow");
            level = playerObject.GetInt("level");
            patent = playerObject.GetInt("patente");


            isDisconnected = playerObject.GetBool("disc");
            isEliminated = playerObject.GetBool("elim");

            cardCount = playerObject.GetInt("cards");
        }

        public bool CheckObjective(List<TerritoryState> ts)
        {
            return objective.Check(this, ts);
        }

        public bool HasContinent(Continent c, List<TerritoryState> territoryStates)
        {
            return territoryStates.TrueForAll(ts => ts.territory.continent != c || ts.owner == this);
        }

        public List<TerritoryState> GetTerritories(List<TerritoryState> territoryStates)
        {
            return territoryStates.Where(ts => ts.owner.idGrow == idGrow).ToList();
        }
    }
}