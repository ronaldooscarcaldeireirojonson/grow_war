﻿using Sfs2X.Entities.Data;

namespace Grow.War
{
    [System.Serializable]
    public class TerritoryState
    {
        public Player owner; // O dono do território
        public Territory territory; // O território em si
        public int amountArmy = 1; // Quantidade de exércitos nesse território

        public TerritoryState()
        {
        }

        public TerritoryState(int territoryId)
        {
            territory = Territory.Get(territoryId);
        }

        public TerritoryState(int territoryId, Player owner, int amountArmy = 1) : this(territoryId)
        {
            this.owner = owner;
            this.amountArmy = amountArmy;
        }

        public SFSObject ToSFSObject()
        {
            SFSObject obj = new SFSObject();
            obj.PutInt("t", territory.id);
            obj.PutInt("o", owner.idGrow);
            obj.PutInt("a", amountArmy);
            return obj;
        }
    }
}
