﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Grow.War
{
    public enum ObjectiveType {PlayerElimination, TerritoryConquest, ContinentConquest};
    public class ObjectiveInfo
    {
        public ObjectiveType objectiveType;
        public GrowColor playerTarget;
        public int numberOfTroopsRequired = 1;
        public List<Continent.Type> continentsToConquer;
    }
    [System.Serializable]
    public class Objective
    {
        public static int idCount = 0; // O contador do Id
        public int id { get; set; } // Id do objetivo
        public string text { get; set; } // O texto do objetivo
        public Func<Player, List<TerritoryState>, bool> Check { get; set; } // Função que faz o check do objetivo
        public GrowColor target = null;

        public ObjectiveInfo info;

        public Objective(string text, Func<Player, List<TerritoryState>, bool> Check)
        {
            this.text = text;
            this.Check = Check;

            id = idCount++;
            LIST.Add(this);
        }
        public Objective(string text, ObjectiveInfo objectiveInfo, Func<Player, List<TerritoryState>, bool> Check)
        {
            this.text = text;
            this.Check = Check;
            this.info = objectiveInfo;
            id = idCount++;
            LIST.Add(this);
        }

        public Objective(GrowColor target)
        {
            this.target = target;
            this.text = "Seu objetivo é destruir totalmente o Exército " + target.name + ".\nSe é você quem possui o Exército " + target.name + " ou se o jogador que os possui for eliminado por outro jogador, o seu objetivo passa a ser automaticamente conquistar 24 Territórios.";
            this.Check = delegate (Player p, List<TerritoryState> territoryStates) { return p.warColor != target && territoryStates.Exists(ts => ts.owner.warColor == target) ? false : TERRITORY_24.Check(p, territoryStates); };
            id = idCount++;

            this.info = new ObjectiveInfo { objectiveType = ObjectiveType.PlayerElimination, playerTarget = target };


            LIST.Add(this); // Esta linha está comentada pois é um paliativo temporário para evitar problema de que não ganha por eliminação. Depois que a Apple aceitar a nova versão, essa linha será descomentada.
        }

        public Objective()
        {
        }

        // All Objectives
        public static List<Objective> LIST = new List<Objective>();

        // Objectives
        public static Objective TERRITORY_24 = new Objective(
            "Seu objetivo é conquistar 24 Territórios à sua escolha.",
            new ObjectiveInfo{objectiveType = ObjectiveType.TerritoryConquest, numberOfTroopsRequired = 1},
            delegate (Player p, List<TerritoryState> territoryStates) { return territoryStates.Where(ts => ts.owner == p).ToList().Count >= 24; }
        );
        public static Objective TERRITORY_18_2 = new Objective(
            "Seu objetivo é conquistar 18 Territórios e ocupar cada um deles com pelo menos 2 exércitos.",
            new ObjectiveInfo { objectiveType = ObjectiveType.TerritoryConquest, numberOfTroopsRequired = 2},
            delegate (Player p, List<TerritoryState> territoryStates)
            {
                int amountTerritories = 0;
                foreach (TerritoryState territory in territoryStates.Where(ts => ts.owner.idGrow == p.idGrow).ToList())
                    if (territory.amountArmy >= 2)
                        amountTerritories++;
                return amountTerritories >= 18;
            }
        );
        public static Objective DESTROY_YELLOW = new Objective(WarColor.YELLOW);
        public static Objective DESTROY_BLUE = new Objective(WarColor.BLUE);
        public static Objective DESTROY_WHITE = new Objective(WarColor.WHITE);
        public static Objective DESTROY_BLACK = new Objective(WarColor.BLACK);
        public static Objective DESTROY_GREEN = new Objective(WarColor.GREEN);
        public static Objective DESTROY_RED = new Objective(WarColor.RED);
        public static Objective NORTHAMERICA_OCEANIA = new Objective(
            "Seu objetivo é conquistar na totalidade a América do Norte e a Oceania.",
            new ObjectiveInfo { objectiveType = ObjectiveType.ContinentConquest, continentsToConquer = new List<Continent.Type>{Continent.Type.NORTH_AMERICA, Continent.Type.OCEANIA} },
            delegate (Player p, List<TerritoryState> gs) { return p.HasContinent(Continent.NORTH_AMERICA, gs) && p.HasContinent(Continent.OCEANIA, gs); }
        );
        public static Objective NORTHAMERICA_AFRICA = new Objective(
            "Seu objetivo é conquistar na totalidade a América do Norte e a África.",
            new ObjectiveInfo { objectiveType = ObjectiveType.ContinentConquest, continentsToConquer = new List<Continent.Type> { Continent.Type.NORTH_AMERICA, Continent.Type.AFRICA } },
            delegate (Player p, List<TerritoryState> gs) { return p.HasContinent(Continent.NORTH_AMERICA, gs) && p.HasContinent(Continent.AFRICA, gs); }
        );
        public static Objective ASIA_AFRICA = new Objective(
            "Seu objetivo é conquistar na totalidade a Ásia e a África.",
            new ObjectiveInfo { objectiveType = ObjectiveType.ContinentConquest, continentsToConquer = new List<Continent.Type> { Continent.Type.ASIA, Continent.Type.AFRICA } },
            delegate (Player p, List<TerritoryState> gs) { return p.HasContinent(Continent.ASIA, gs) && p.HasContinent(Continent.AFRICA, gs); }
        );
        public static Objective ASIA_SOUTHAMERICA = new Objective(
            "Seu objetivo é conquistar na totalidade a Ásia e a América do Sul.",
            new ObjectiveInfo { objectiveType = ObjectiveType.ContinentConquest, continentsToConquer = new List<Continent.Type> { Continent.Type.ASIA, Continent.Type.SOUTH_AMERICA } },
            delegate (Player p, List<TerritoryState> gs) { return p.HasContinent(Continent.ASIA, gs) && p.HasContinent(Continent.SOUTH_AMERICA, gs); }
        );
        public static Objective EUROPE_SOUTHAMERICA_OTHER = new Objective(
            "Seu objetivo é conquistar na totalidade a Europa, a América do Sul e mais um continente à sua escolha.",
            new ObjectiveInfo { objectiveType = ObjectiveType.ContinentConquest, continentsToConquer = new List<Continent.Type> { Continent.Type.EUROPE, Continent.Type.SOUTH_AMERICA, Continent.Type.ANY } },
            delegate (Player p, List<TerritoryState> gs) { return p.HasContinent(Continent.EUROPE, gs) && p.HasContinent(Continent.SOUTH_AMERICA, gs) && (p.HasContinent(Continent.AFRICA, gs) || p.HasContinent(Continent.ASIA, gs) || p.HasContinent(Continent.NORTH_AMERICA, gs) || p.HasContinent(Continent.OCEANIA, gs)); }
        );
        public static Objective EUROPE_OCEANIA_OTHER = new Objective(
            "Seu objetivo é conquistar na totalidade a Europa, a Oceania e mais um continente à sua escolha.",
            new ObjectiveInfo { objectiveType = ObjectiveType.ContinentConquest, continentsToConquer = new List<Continent.Type> { Continent.Type.EUROPE, Continent.Type.OCEANIA, Continent.Type.ANY } },
            delegate (Player p, List<TerritoryState> gs) { return p.HasContinent(Continent.EUROPE, gs) && p.HasContinent(Continent.OCEANIA, gs) && (p.HasContinent(Continent.AFRICA, gs) || p.HasContinent(Continent.ASIA, gs) || p.HasContinent(Continent.NORTH_AMERICA, gs) || p.HasContinent(Continent.SOUTH_AMERICA, gs)); }
        );
    }
}
