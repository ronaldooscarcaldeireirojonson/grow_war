﻿using System.Collections.Generic;
using System.Linq;

namespace Grow.War
{
    //[System.Serializable]
    // O símbolo da carta
    public enum SymbolType
    {
        Wildcard = 0,
        Circle = 1,
        Square = 2,
        Triangle = 3
    };
    //[System.Serializable]
    public class Territory
    {

        public static int idCount = 0; // O contador do Id
        public int id; // Id
        public string name; // Nome
        public Continent continent; // Continente do território
        public List<Territory> neighbours; // Lista dos territórios vizinhos
        public SymbolType symbol; // O símbolo do território


        public Territory(int joker = -1)
        {
            if (joker >= 0)
            {
                name = "Coringa";
                id = 42 + joker;
                symbol = SymbolType.Wildcard;
            }
            else
                id = idCount++;
        }

        // Função para verificar se o território faz parte da borda de seu continente (ou seja, se tem fronteira com um território que não é de seu continente)
        public bool IsBorder()
        {
            foreach (Territory neighbour in neighbours)
                if (continent.id != neighbour.continent.id)
                    return true;
            return false;
        }

        // Jokers
        public static Territory Wildcard1 = new Territory(0);
        public static Territory Wildcard2 = new Territory(1);

        // Lista de territórios
        public static List<Territory> LIST = new List<Territory>();

        public static Territory Get(int id)
        {
            return id == 42 ? Wildcard1 : id == 43 ? Wildcard2 : LIST.Find(t => t.id == id);
        }

        // North America
        public static Territory ALASKA = new Territory() { name = "Alaska", continent = Continent.NORTH_AMERICA, symbol = SymbolType.Triangle };
        public static Territory GROELANDIA = new Territory() { name = "Groelândia", continent = Continent.NORTH_AMERICA, symbol = SymbolType.Circle };
        public static Territory MACKENZIE = new Territory() { name = "Mackenzie", continent = Continent.NORTH_AMERICA, symbol = SymbolType.Circle };
        public static Territory VANCOUVER = new Territory() { name = "Vancouver", continent = Continent.NORTH_AMERICA, symbol = SymbolType.Triangle };
        public static Territory LABRADOR = new Territory() { name = "Labrador", continent = Continent.NORTH_AMERICA, symbol = SymbolType.Square };
        public static Territory OTTAWA = new Territory() { name = "Ottawa", continent = Continent.NORTH_AMERICA, symbol = SymbolType.Circle };
        public static Territory CALIFORNIA = new Territory() { name = "Califórnia", continent = Continent.NORTH_AMERICA, symbol = SymbolType.Square };
        public static Territory NOVA_YORK = new Territory() { name = "Nova York", continent = Continent.NORTH_AMERICA, symbol = SymbolType.Triangle };
        public static Territory MEXICO = new Territory() { name = "México", continent = Continent.NORTH_AMERICA, symbol = SymbolType.Square };

        // Asia
        public static Territory VLADVOSTOK = new Territory() { name = "Vladvostok", continent = Continent.ASIA, symbol = SymbolType.Circle };
        public static Territory SIBERIA = new Territory() { name = "Sibéria", continent = Continent.ASIA, symbol = SymbolType.Triangle };
        public static Territory TCHITA = new Territory() { name = "Tchita", continent = Continent.ASIA, symbol = SymbolType.Triangle };
        public static Territory DUDINKA = new Territory() { name = "Dudinka", continent = Continent.ASIA, symbol = SymbolType.Circle };
        public static Territory OMSK = new Territory() { name = "Omsk", continent = Continent.ASIA, symbol = SymbolType.Square };
        public static Territory MONGOLIA = new Territory() { name = "Mongolia", continent = Continent.ASIA, symbol = SymbolType.Circle };
        public static Territory ARAL = new Territory() { name = "Aral", continent = Continent.ASIA, symbol = SymbolType.Triangle };
        public static Territory CHINA = new Territory() { name = "China", continent = Continent.ASIA, symbol = SymbolType.Circle };
        public static Territory JAPAO = new Territory() { name = "Japão", continent = Continent.ASIA, symbol = SymbolType.Square };
        public static Territory ORIENTE_MEDIO = new Territory() { name = "Oriente Médio", continent = Continent.ASIA, symbol = SymbolType.Square };
        public static Territory INDIA = new Territory() { name = "Índia", continent = Continent.ASIA, symbol = SymbolType.Square };
        public static Territory VIETNA = new Territory() { name = "Vietnã", continent = Continent.ASIA, symbol = SymbolType.Triangle };

        // Europe
        public static Territory SUECIA = new Territory() { name = "Suécia", continent = Continent.EUROPE, symbol = SymbolType.Circle };
        public static Territory MOSCOU = new Territory() { name = "Moscou", continent = Continent.EUROPE, symbol = SymbolType.Triangle };
        public static Territory ALEMANHA = new Territory() { name = "Alemanha", continent = Continent.EUROPE, symbol = SymbolType.Circle };
        public static Territory POLONIA = new Territory() { name = "Polônia", continent = Continent.EUROPE, symbol = SymbolType.Square };
        public static Territory ISLANDIA = new Territory() { name = "Islândia", continent = Continent.EUROPE, symbol = SymbolType.Triangle };
        public static Territory INGLATERRA = new Territory() { name = "Inglaterra", continent = Continent.EUROPE, symbol = SymbolType.Circle };
        public static Territory FRANCA = new Territory() { name = "França", continent = Continent.EUROPE, symbol = SymbolType.Square };

        // South America
        public static Territory VENEZUELA = new Territory() { name = "Venezuela", continent = Continent.SOUTH_AMERICA, symbol = SymbolType.Triangle };
        public static Territory BRASIL = new Territory() { name = "Brasil", continent = Continent.SOUTH_AMERICA, symbol = SymbolType.Circle };
        public static Territory PERU = new Territory() { name = "Peru", continent = Continent.SOUTH_AMERICA, symbol = SymbolType.Triangle };
        public static Territory ARGENTINA = new Territory() { name = "Argentina", continent = Continent.SOUTH_AMERICA, symbol = SymbolType.Square };

        // Africa
        public static Territory EGITO = new Territory() { name = "Egito", continent = Continent.AFRICA, symbol = SymbolType.Triangle };
        public static Territory ARGELIA = new Territory() { name = "Argélia", continent = Continent.AFRICA, symbol = SymbolType.Circle };
        public static Territory SUDAO = new Territory() { name = "Sudão", continent = Continent.AFRICA, symbol = SymbolType.Square };
        public static Territory CONGO = new Territory() { name = "Congo", continent = Continent.AFRICA, symbol = SymbolType.Square };
        public static Territory AFRICA_DO_SUL = new Territory() { name = "África do Sul", continent = Continent.AFRICA, symbol = SymbolType.Triangle };
        public static Territory MADAGASCAR = new Territory() { name = "Madagascar", continent = Continent.AFRICA, symbol = SymbolType.Circle };

        // Oceania
        public static Territory AUSTRALIA = new Territory() { name = "Austrália", continent = Continent.OCEANIA, symbol = SymbolType.Triangle };
        public static Territory SUMATRA = new Territory() { name = "Sumatra", continent = Continent.OCEANIA, symbol = SymbolType.Square };
        public static Territory NOVA_GUINE = new Territory() { name = "Nova Guiné", continent = Continent.OCEANIA, symbol = SymbolType.Circle };
        public static Territory BORNEO = new Territory() { name = "Borneo", continent = Continent.OCEANIA, symbol = SymbolType.Square };

        static Territory()
        {
            LIST.AddRange(new List<Territory>() { AFRICA_DO_SUL, MADAGASCAR, SUDAO, EGITO, ARGELIA, CONGO });
            LIST.AddRange(new List<Territory>() { ALASKA, MEXICO, CALIFORNIA, GROELANDIA, MACKENZIE, NOVA_YORK, LABRADOR, OTTAWA, VANCOUVER });
            LIST.AddRange(new List<Territory>() { BRASIL, ARGENTINA, PERU, VENEZUELA });
            LIST.AddRange(new List<Territory>() { ARAL, CHINA, DUDINKA, INDIA, JAPAO, MONGOLIA, OMSK, ORIENTE_MEDIO, SIBERIA, TCHITA, VIETNA, VLADVOSTOK });
            LIST.AddRange(new List<Territory>() { ALEMANHA, FRANCA, INGLATERRA, ISLANDIA, MOSCOU, POLONIA, SUECIA });
            LIST.AddRange(new List<Territory>() { AUSTRALIA, BORNEO, NOVA_GUINE, SUMATRA });

            AFRICA_DO_SUL.neighbours = new List<Territory>() { MADAGASCAR, SUDAO, CONGO };
            MADAGASCAR.neighbours = new List<Territory>() { AFRICA_DO_SUL, SUDAO };
            SUDAO.neighbours = new List<Territory>() { EGITO, ARGELIA, CONGO, MADAGASCAR, AFRICA_DO_SUL };
            EGITO.neighbours = new List<Territory>() { ORIENTE_MEDIO, SUDAO, POLONIA, FRANCA, ARGELIA };
            ARGELIA.neighbours = new List<Territory>() { FRANCA, BRASIL, EGITO, SUDAO, CONGO };
            CONGO.neighbours = new List<Territory>() { ARGELIA, SUDAO, AFRICA_DO_SUL };

            ALASKA.neighbours = new List<Territory>() { MACKENZIE, VANCOUVER, VLADVOSTOK };
            MEXICO.neighbours = new List<Territory>() { CALIFORNIA, NOVA_YORK, VENEZUELA };
            CALIFORNIA.neighbours = new List<Territory>() { VANCOUVER, OTTAWA, NOVA_YORK, MEXICO };
            GROELANDIA.neighbours = new List<Territory>() { MACKENZIE, LABRADOR, ISLANDIA };
            MACKENZIE.neighbours = new List<Territory>() { OTTAWA, VANCOUVER, ALASKA, GROELANDIA };
            NOVA_YORK.neighbours = new List<Territory>() { OTTAWA, LABRADOR, MEXICO, CALIFORNIA };
            LABRADOR.neighbours = new List<Territory>() { GROELANDIA, OTTAWA, NOVA_YORK };
            OTTAWA.neighbours = new List<Territory>() { LABRADOR, VANCOUVER, CALIFORNIA, NOVA_YORK, MACKENZIE };
            VANCOUVER.neighbours = new List<Territory>() { ALASKA, MACKENZIE, OTTAWA, CALIFORNIA };

            BRASIL.neighbours = new List<Territory>() { VENEZUELA, ARGELIA, ARGENTINA, PERU };
            ARGENTINA.neighbours = new List<Territory>() { PERU, BRASIL };
            PERU.neighbours = new List<Territory>() { VENEZUELA, BRASIL, ARGENTINA };
            VENEZUELA.neighbours = new List<Territory>() { MEXICO, BRASIL, PERU };

            ARAL.neighbours = new List<Territory>() { MOSCOU, OMSK, CHINA, INDIA, ORIENTE_MEDIO };
            CHINA.neighbours = new List<Territory>() { MONGOLIA, TCHITA, VLADVOSTOK, JAPAO, VIETNA, INDIA, ARAL, OMSK };
            DUDINKA.neighbours = new List<Territory>() { SIBERIA, TCHITA, OMSK, MONGOLIA };
            INDIA.neighbours = new List<Territory>() { ORIENTE_MEDIO, ARAL, CHINA, VIETNA, SUMATRA };
            JAPAO.neighbours = new List<Territory>() { CHINA, VLADVOSTOK };
            MONGOLIA.neighbours = new List<Territory>() { TCHITA, CHINA, OMSK, DUDINKA };
            OMSK.neighbours = new List<Territory>() { DUDINKA, MONGOLIA, CHINA, ARAL, MOSCOU };
            ORIENTE_MEDIO.neighbours = new List<Territory>() { EGITO, ARAL, INDIA, POLONIA, MOSCOU };
            SIBERIA.neighbours = new List<Territory>() { VLADVOSTOK, TCHITA, DUDINKA };
            TCHITA.neighbours = new List<Territory>() { SIBERIA, VLADVOSTOK, CHINA, MONGOLIA, DUDINKA };
            VIETNA.neighbours = new List<Territory>() { INDIA, CHINA, BORNEO };
            VLADVOSTOK.neighbours = new List<Territory>() { ALASKA, JAPAO, CHINA, TCHITA, SIBERIA };

            ALEMANHA.neighbours = new List<Territory>() { INGLATERRA, POLONIA, FRANCA };
            FRANCA.neighbours = new List<Territory>() { ALEMANHA, INGLATERRA, ARGELIA, EGITO, POLONIA };
            INGLATERRA.neighbours = new List<Territory>() { ISLANDIA, SUECIA, ALEMANHA, FRANCA };
            ISLANDIA.neighbours = new List<Territory>() { GROELANDIA, INGLATERRA };
            MOSCOU.neighbours = new List<Territory>() { OMSK, ARAL, SUECIA, POLONIA, ORIENTE_MEDIO };
            POLONIA.neighbours = new List<Territory>() { MOSCOU, ALEMANHA, FRANCA, ORIENTE_MEDIO, EGITO };
            SUECIA.neighbours = new List<Territory>() { MOSCOU, INGLATERRA };

            AUSTRALIA.neighbours = new List<Territory>() { SUMATRA, BORNEO, NOVA_GUINE };
            BORNEO.neighbours = new List<Territory>() { VIETNA, AUSTRALIA, NOVA_GUINE };
            NOVA_GUINE.neighbours = new List<Territory>() { BORNEO, AUSTRALIA };
            SUMATRA.neighbours = new List<Territory>() { INDIA, AUSTRALIA };
        }
    }
}
