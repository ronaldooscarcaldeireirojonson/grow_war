﻿using System.Collections.Generic;

namespace Grow.War
{
    [System.Serializable]
    public class Continent
    {
        public static int idCount = 0; // O contador do Id
        public int id; // O Id do continente
        public string name; // Nome do continente
        public int territoriesCount; // Quantidade de territórios no continente
        public int bonusArmy; // A quantidade de exércitos que o jogador ganha ao dominar o território

        public static List<Continent> LIST = new List<Continent>();

        public static Continent NORTH_AMERICA = new Continent() { name = "América do Norte", territoriesCount = 9, bonusArmy = 5 };
        public static Continent SOUTH_AMERICA = new Continent() { name = "América do Sul", territoriesCount = 4, bonusArmy = 2 };
        public static Continent EUROPE = new Continent() { name = "Europa", territoriesCount = 7, bonusArmy = 5 };
        public static Continent AFRICA = new Continent() { name = "África", territoriesCount = 6, bonusArmy = 3 };
        public static Continent ASIA = new Continent() { name = "Ásia", territoriesCount = 12, bonusArmy = 7 };
        public static Continent OCEANIA = new Continent() { name = "Oceania", territoriesCount = 4, bonusArmy = 2 };

        public Continent()
        {
            id = idCount++;
            LIST.Add(this);
        }

        public enum Type
        {
            NORTH_AMERICA = 0,
            SOUTH_AMERICA,
            EUROPE,
            AFRICA,
            ASIA,
            OCEANIA,
            ANY
        }

        public static Continent GetContinent(Type continent)
        {
            Continent toReturn = null;
            switch (continent)
            {
                case Type.NORTH_AMERICA:
                    toReturn = NORTH_AMERICA;
                    break;
                case Type.SOUTH_AMERICA:
                    toReturn = SOUTH_AMERICA;
                    break;
                case Type.EUROPE:
                    toReturn = EUROPE;
                    break;
                case Type.AFRICA:
                    toReturn = AFRICA;
                    break;
                case Type.ASIA:
                    toReturn = ASIA;
                    break;
                case Type.OCEANIA:
                    toReturn = OCEANIA;
                    break;
                case Type.ANY:
                    toReturn = null;
                    break;
                default:
                    break;


            }
            return toReturn;
        }
    }
}