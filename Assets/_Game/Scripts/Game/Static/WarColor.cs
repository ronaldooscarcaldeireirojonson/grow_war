﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Grow.War
{
    public static class WarColor
    {

        public static void Init() { }


        public static GrowColor Get(int id)
        {
            return GrowColor.LIST.SingleOrDefault(b => b.id == id);
        }


        // Colors
        public static GrowColor YELLOW = new GrowColor("AMARELO", new Color(1, 0.855f, 0)); // #ffda00
        public static GrowColor BLUE = new GrowColor("AZUL", new Color(0.075f, 0.742f, 0.831f)); // #13bdd3
        public static GrowColor WHITE = new GrowColor("ROXO", new Color(0.342f, 0.173f, 0.533f)); // #572c89
        public static GrowColor BLACK = new GrowColor("CINZA", new Color(0.258f, 0.258f, 0.258f)); // #424242
        public static GrowColor GREEN = new GrowColor("VERDE", new Color(0.407f, 0.721f, 0.207f)); // #68b735
        public static GrowColor RED = new GrowColor("VERMELHO", new Color(0.8f, 0.090f, 0.207f)); // #cc1736
    }
}
