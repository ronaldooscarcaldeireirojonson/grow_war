﻿using Grow;
using Grow.War;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupNewProfileWar : PopupNewProfile
{
    [Header("Info War")]
    public Text textAmountMatches;
    public Text textConsecutiveWins;
    public Text textWonTroops;
    public Text textLostTroops;
    public Text textMostAchieveObjectives;
    public Text textMostAchieveContinents;

    [Header("Rating")]
    public GameObject ratingContainer;
    public List<RectTransform> ratingBars;
    public List<Text> ratingPercentage;


    [Header("Patent")]
    public Image iconPatent1;
    public Image iconPatent2;
    public Image iconPatent3;
    public Text textPatentLosts;
    public Text textPatentWins;
    public Text textPatent;
    public Text patentTextRight;
    public Text patentTextLeft;
    public GameObject arrows;
    public GameObject arrowsRight;
    public GameObject arrowsLeft;
    public RectTransform progressGreen;
    public RectTransform progressRed;
    // NOTE: A ordem é importante! Do menor para o maior
    public List<Sprite> patentSprites;
    public Image currentPatent;
    public Text currentPatentLabel;


    private int patent = 1;
    private float percentageLeft = 0;
    private float percentageRight = 0;

    public override void Start()
    {
        base.Start();

        if (GameController.Instance)
            GameController.Instance.isProfileOpen = true;
    }

    public override void OnDestroy()
    {
        base.OnDestroy();

        if (GameController.Instance)
            GameController.Instance.isProfileOpen = false;
    }

    //private void Update()
    //{
    //    if (Input.GetKeyDown(KeyCode.RightArrow))
    //    {
    //        if (patent < 5)
    //        {
    //            patent++;
    //            percentageLeft = UnityEngine.Random.Range(0f, 1f);
    //            percentageRight = UnityEngine.Random.Range(0f, 1f);
    //            UpdateMyProfileRightPart();
    //        }
    //    }
    //    if (Input.GetKeyDown(KeyCode.LeftArrow))
    //    {
    //        if (patent > 1)
    //        {
    //            patent--;
    //            percentageLeft = UnityEngine.Random.Range(0f, 1f);
    //            percentageRight = UnityEngine.Random.Range(0f, 1f);
    //            UpdateMyProfileRightPart();
    //        }
    //    }
    //}


    public override void UpdateMyProfileRightPart()
    {
        base.UpdateMyProfileRightPart();

        // Rating dos jogadores
        if (userDetails.UserGameData.Rating != null && userDetails.UserGameData.Rating.Count == 4)
        {
            for (int i = 0; i < userDetails.UserGameData.Rating.Count; i++)
            {
                //ratingNames[i].text = userDetails.UserGameData.Rating[i].Rating;
                switch (userDetails.UserGameData.Rating[i].Rating.ToUpper())
                {
                    case "ESTRATEGISTA":
                        ratingBars[0].SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, (float)userDetails.UserGameData.Rating[i].Percentual);
                        ratingPercentage[0].text = userDetails.UserGameData.Rating[i].Percentual.ToString("0") + "%";
                        break;

                    case "HONRADO":
                        ratingBars[1].SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, (float)userDetails.UserGameData.Rating[i].Percentual);
                        ratingPercentage[1].text = userDetails.UserGameData.Rating[i].Percentual.ToString("0") + "%";
                        break;

                    case "SORTUDO":
                        ratingBars[2].SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, (float)userDetails.UserGameData.Rating[i].Percentual);
                        ratingPercentage[2].text = userDetails.UserGameData.Rating[i].Percentual.ToString("0") + "%";
                        break;

                    case "VINGATIVO":
                        ratingBars[3].SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, (float)userDetails.UserGameData.Rating[i].Percentual);
                        ratingPercentage[3].text = userDetails.UserGameData.Rating[i].Percentual.ToString("0") + "%";
                        break;

                    default:
                        break;
                }
            }
        }
        else
        {
            ratingContainer.SetActive(false);
        }

        // Patents
        patent = userDetails.Patent.Id;
        percentageRight = (float)userDetails.Patent.Vitorias / (float)userDetails.Patent.VitoriasSobe;
        percentageLeft = (float)userDetails.Patent.Derrotas / (float)userDetails.Patent.DerrotasDesce;

        textPatent.text = Globals.gamePatentLabels[patent - 1].Label;

        currentPatent.sprite = patentSprites[patent - 1];
        currentPatentLabel.text = Globals.gamePatentLabels[patent - 1].Label;

        // Verifica se é o último nível
        if (patent == 5)
        {   // Nível 5

            // - Troca a quantidade de vitórias/derrotas
            textPatentLosts.text = userDetails.Patent.DerrotasDesce + " DERROTAS";
            textPatentWins.text = userDetails.Patent.VitoriasSobe + " VITÓRIAS";
            textPatentLosts.gameObject.SetActive(true);
            textPatentWins.gameObject.SetActive(true);
            patentTextLeft.text = "CORONEL";


            progressRed.anchoredPosition = new Vector2(-305, 0);
            progressGreen.anchoredPosition = new Vector2(305, 0);
            progressRed.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, percentageLeft * 225);
            progressGreen.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, percentageRight * 225);
            progressRed.gameObject.SetActive(true);
            progressGreen.gameObject.SetActive(true);

            arrows.SetActive(true);
            arrowsRight.SetActive(false);

            iconPatent1.sprite = patentSprites[3];
            iconPatent2.sprite = patentSprites[4];

            iconPatent1.gameObject.SetActive(true);
            iconPatent2.gameObject.SetActive(true);
            iconPatent3.gameObject.SetActive(false);
        }
        else if (patent > 1 && patent < 5)
        {   // Nível 2 a 4

            // - Troca a quantidade de vitórias/derrotas
            textPatentLosts.text = userDetails.Patent.DerrotasDesce + " DERROTAS";
            textPatentWins.text = userDetails.Patent.VitoriasSobe + " VITÓRIAS";
            textPatentLosts.gameObject.SetActive(true);
            textPatentWins.gameObject.SetActive(true);
            patentTextLeft.text = Globals.gamePatentLabels[patent - 2].Label;
            patentTextRight.text = Globals.gamePatentLabels[patent].Label;


            progressRed.anchoredPosition = new Vector2(-305, 0);
            progressGreen.anchoredPosition = new Vector2(305, 0);
            progressRed.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, percentageLeft * 225);
            progressGreen.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, percentageRight * 225);
            progressRed.gameObject.SetActive(true);
            progressGreen.gameObject.SetActive(true);

            arrows.SetActive(true);
            arrowsRight.SetActive(false);

            iconPatent1.sprite = patentSprites[patent - 2];
            iconPatent2.sprite = patentSprites[patent - 1];
            iconPatent3.sprite = patentSprites[patent];

            iconPatent1.gameObject.SetActive(true);
            iconPatent2.gameObject.SetActive(true);
            iconPatent3.gameObject.SetActive(true);
        }
        else
        {   // Nível 1

            // - Troca a quantidade de vitórias/derrotas
            textPatentWins.text = userDetails.Patent.VitoriasSobe + " VITÓRIAS";
            textPatentLosts.gameObject.SetActive(false);
            textPatentWins.gameObject.SetActive(true);
            textPatent.gameObject.SetActive(false);
            patentTextLeft.text = "SOLDADO";
            patentTextRight.text = "SARGENTO";


            progressGreen.anchoredPosition = new Vector2(33, 0);
            progressGreen.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, percentageRight * 500);
            progressRed.gameObject.SetActive(false);
            progressGreen.gameObject.SetActive(true);

            arrows.SetActive(false);
            arrowsRight.SetActive(true);

            iconPatent1.sprite = patentSprites[0];
            iconPatent3.sprite = patentSprites[1];

            iconPatent1.gameObject.SetActive(true);
            iconPatent2.gameObject.SetActive(false);
            iconPatent3.gameObject.SetActive(true);
        }
    }


    public void UpdateTabInfo(UsuarioEstatisticas stats)
    {
        // TEST
        //stats.Itens = new List<UsuarioEstatisticas.EstatisticaItem>();

        //UsuarioEstatisticas.EstatisticasResult data = new UsuarioEstatisticas.EstatisticasResult();
        //data.id_estatistica = 1;
        //data.nu_valor = 1;
        //data.tx_titulo = "TempoDeJogo";
        //UsuarioEstatisticas.EstatisticaItem item = new UsuarioEstatisticas.EstatisticaItem(data);
        //stats.Itens.Add(item);

        //data.id_estatistica = 2;
        //data.nu_valor = 2;
        //data.tx_titulo = "NumeroDePartidas";
        //item = new UsuarioEstatisticas.EstatisticaItem(data);
        //stats.Itens.Add(item);

        //data.id_estatistica = 3;
        //data.nu_valor = 3;
        //data.tx_titulo = "QuantidadeAcertosCategoria";
        //item = new UsuarioEstatisticas.EstatisticaItem(data);
        //stats.Itens.Add(item);

        //data.id_estatistica = 4;
        //data.nu_valor = 4;
        //data.tx_titulo = "MediaDicasAcerto";
        //item = new UsuarioEstatisticas.EstatisticaItem(data);
        //stats.Itens.Add(item);

        //data.id_estatistica = 5;
        //data.nu_valor = 5;
        //data.tx_titulo = "MediaRatingOutrosJogadores";
        //item = new UsuarioEstatisticas.EstatisticaItem(data);
        //stats.Itens.Add(item);

        //data.id_estatistica = 6;
        //data.nu_valor = 6;
        //data.tx_titulo = "VitoriasConsecutivas";
        //item = new UsuarioEstatisticas.EstatisticaItem(data);
        //stats.Itens.Add(item);

        //data.id_estatistica = 7;
        //data.nu_valor = 7;
        //data.tx_titulo = "TotalVitorias";
        //item = new UsuarioEstatisticas.EstatisticaItem(data);
        //stats.Itens.Add(item);

        //data.id_estatistica = 8;
        //data.nu_valor = 8;
        //data.tx_titulo = "TotalDerrotas";
        //item = new UsuarioEstatisticas.EstatisticaItem(data);
        //stats.Itens.Add(item);

        //data.id_estatistica = 9;
        //data.nu_valor = 9;
        //data.tx_titulo = "TropasGanhas";
        //item = new UsuarioEstatisticas.EstatisticaItem(data);
        //stats.Itens.Add(item);

        //data.id_estatistica = 10;
        //data.nu_valor = 10;
        //data.tx_titulo = "TropasPerdidas";
        //item = new UsuarioEstatisticas.EstatisticaItem(data);
        //stats.Itens.Add(item);

        //data.id_estatistica = 11;
        //data.nu_valor = 11;
        //data.tx_titulo = "ObjetivosMaisAtingidos";
        //item = new UsuarioEstatisticas.EstatisticaItem(data);
        //stats.Itens.Add(item);

        //data.id_estatistica = 12;
        //data.nu_valor = 0;
        //data.tx_titulo = "ContinentesMaisConquistados";
        //item = new UsuarioEstatisticas.EstatisticaItem(data);
        //stats.Itens.Add(item);

        //data.id_estatistica = 13;
        //data.nu_valor = 13;
        //data.tx_titulo = "TotalAtributosOutrosJogadores";
        //item = new UsuarioEstatisticas.EstatisticaItem(data);
        //stats.Itens.Add(item);

        //stats.TempoJogo = 973;
        //// FIM TEST

        textVictories30.text = stats.Totalizadores30Dias.Vitorias.ToString();
        textLosts30.text = stats.Totalizadores30Dias.Derrotas.ToString();
        textAbandon30.text = stats.Totalizadores30Dias.Abandonos.ToString();
        textPoints30.text = stats.Totalizadores30Dias.Pontos.ToString();

        long hours = stats.TempoJogo / 60;
        long minutes = stats.TempoJogo % 60;
        textTimePlaying.text = hours + "h " + minutes + "min";

        textVictories.text = stats.TotalizadoresGeral.Vitorias.ToString();
        textLosts.text = stats.TotalizadoresGeral.Derrotas.ToString();
        textAbandon.text = stats.TotalizadoresGeral.Abandonos.ToString();
        textPoints.text = stats.TotalizadoresGeral.Pontos.ToString();


        if (stats.Itens != null && stats.Itens.Count > 0)
        {
            for (int i = 0; i < stats.Itens.Count; i++)
            {
                //Debug.Log("[" + stats.Itens[i].Tipo + "]: " + stats.Itens[i].Valor);

                switch (stats.Itens[i].Tipo)
                {
                    case UsuarioEstatisticas.EnTipoEstatisticaItem.NumeroDePartidas:
                        textAmountMatches.text = stats.Itens[i].Valor.ToString();
                        break;

                    case UsuarioEstatisticas.EnTipoEstatisticaItem.VitoriasConsecutivas:
                        textConsecutiveWins.text = stats.Itens[i].Valor.ToString();
                        break;

                    case UsuarioEstatisticas.EnTipoEstatisticaItem.TropasGanhas:
                        textWonTroops.text = stats.Itens[i].Valor.ToString();
                        break;

                    case UsuarioEstatisticas.EnTipoEstatisticaItem.TropasPerdidas:
                        textLostTroops.text = stats.Itens[i].Valor.ToString();
                        break;

                    case UsuarioEstatisticas.EnTipoEstatisticaItem.ObjetivosMaisAtingidos:
                        textMostAchieveObjectives.text = stats.Itens[i].Titulo.Split(':')[1];
                        break;

                    case UsuarioEstatisticas.EnTipoEstatisticaItem.ContinentesMaisConquistados:
                        textMostAchieveContinents.text = stats.Itens[i].Titulo.Split(':')[1];
                        break;

                    default:
                        break;
                }
            }
        }
    }



    public void UpdateTabHistoric(List<Partida> matches)
    {
        //// TEST
        //Partida.Jogador player1 = new Partida.Jogador(0, "Photo10.jpg", "");
        //player1.Id = 9;
        //player1.IdGrow = 79422;
        //player1.Apelido = "FOKUBO01";
        //player1.Vencedor = true;
        //player1.Cor = 0;

        //Partida.Jogador player2 = new Partida.Jogador(0, "Photo11.jpg", "");
        //player2.Id = 10;
        //player2.IdGrow = 79423;
        //player2.Apelido = "FOKUBO02";
        //player2.Vencedor = false;
        //player2.Cor = 1;

        //Partida.Jogador player3 = new Partida.Jogador(0, "Photo12.jpg", "");
        //player3.Id = 20;
        //player3.IdGrow = 79424;
        //player3.Apelido = "FOKUBO03";
        //player3.Vencedor = false;
        //player3.Cor = 2;

        //Partida.Jogador player4 = new Partida.Jogador(0, "Photo13.jpg", "");
        //player4.Id = 21;
        //player4.IdGrow = 79425;
        //player4.Apelido = "FOKUBO04";
        //player4.Vencedor = false;
        //player4.Cor = 3;

        //Partida match1 = new Partida();
        //match1.IdJogo = 1;
        //match1.Vencedor = true;
        //match1.Data = new DateTime(2017, 1, 1, 1, 1, 1);
        //match1.Jogadores = new List<Partida.Jogador>() { player1, player2, player3, player4 };

        //player1.Vencedor = false;
        //player3.Vencedor = true;

        //Partida match2 = new Partida();
        //match2.IdJogo = 2;
        //match2.Vencedor = false;
        //match2.Data = new DateTime(2017, 2, 2, 2, 2, 2);
        //match2.Jogadores = new List<Partida.Jogador>() { player1, player2, player3 };

        //player3.Vencedor = false;
        //player4.Vencedor = true;

        //Partida match3 = new Partida();
        //match3.IdJogo = 3;
        //match3.Vencedor = false;
        //match3.Data = new DateTime(2017, 3, 3, 3, 3, 3);
        //match3.Jogadores = new List<Partida.Jogador>() { player1, player2, player3, player4 };

        //player4.Vencedor = false;
        //player1.Vencedor = true;

        //Partida match4 = new Partida();
        //match4.IdJogo = 4;
        //match4.Vencedor = true;
        //match4.Data = new DateTime(2017, 4, 4, 4, 4, 4);
        //match4.Jogadores = new List<Partida.Jogador>() { player1, player2, player3, player4 };

        //player1.Vencedor = false;
        //player2.Vencedor = true;

        //Partida match5 = new Partida();
        //match5.IdJogo = 5;
        //match5.Vencedor = true;
        //match5.Data = new DateTime(2017, 5, 5, 5, 5, 5);
        //match5.Jogadores = new List<Partida.Jogador>() { player1, player2, player3 };

        //matches = new List<Partida>();
        //matches.Add(match1);
        //matches.Add(match2);
        //matches.Add(match3);
        //matches.Add(match4);
        //matches.Add(match5);
        //// FIM TEST

        if (matches != null && matches.Count > 0)
        {
            textStatusHistoric.gameObject.SetActive(false);

            StartCoroutine(FillHistoric(matches));
        }
        else
        {
            textStatusHistoric.text = "Você não tem nenhuma partida no seu histórico";
            textStatusHistoric.gameObject.SetActive(true);
        }
    }

    private IEnumerator FillHistoric(List<Partida> matches)
    {
        for(int i = 0; i < matches.Count; i++)
        {
            GameObject match = Instantiate(prefabItemHistoric, contentHistoric);
            match.GetComponent<ItemHistoric>().Configure(matches[i]);
            yield return new WaitForSeconds(0.1f);
        }
    }



    public void UpdateTabAchievements(Usuario.UserData achieves)
    {
        //// TEST
        //NewProfileModel.AchievementModel achieve1 = new NewProfileModel.AchievementModel();
        //achieve1.id = 1;
        //achieve1.name = "Acertar a primeira pergunta";
        //achieve1.progressCurrent = 5;
        //achieve1.progressTotal = 30;
        //achieve1.progressText = "acertos";
        //achieve1.hasCopperMedal = true;
        //achieve1.hasSilverMedal = false;
        //achieve1.hasGoldMedal = false;

        //NewProfileModel.AchievementModel achieve2 = new NewProfileModel.AchievementModel();
        //achieve2.id = 2;
        //achieve2.name = "Ganhar partidas";
        //achieve2.progressCurrent = 2;
        //achieve2.progressTotal = 100;
        //achieve2.progressText = "partidas";
        //achieve2.hasCopperMedal = false;
        //achieve2.hasSilverMedal = false;
        //achieve2.hasGoldMedal = false;

        //NewProfileModel.AchievementModel achieve3 = new NewProfileModel.AchievementModel();
        //achieve3.id = 3;
        //achieve3.name = "Ficar em segundo lugar";
        //achieve3.progressCurrent = 4;
        //achieve3.progressTotal = 5;
        //achieve3.progressText = "ganhos";
        //achieve3.hasCopperMedal = true;
        //achieve3.hasSilverMedal = true;
        //achieve3.hasGoldMedal = true;

        //NewProfileModel.AchievementModel achieve4 = new NewProfileModel.AchievementModel();
        //achieve4.id = 4;
        //achieve4.name = "Usar carta Palpite a Qualquer Hora";
        //achieve4.progressCurrent = 26;
        //achieve4.progressTotal = 100;
        //achieve4.progressText = "usos";
        //achieve4.hasCopperMedal = true;
        //achieve4.hasSilverMedal = false;
        //achieve4.hasGoldMedal = false;

        //NewProfileModel.AchievementModel achieve5 = new NewProfileModel.AchievementModel();
        //achieve5.id = 5;
        //achieve5.name = "Usar carta Escolher Categoria";
        //achieve5.progressCurrent = 8;
        //achieve5.progressTotal = 100;
        //achieve5.progressText = "usos";
        //achieve5.hasCopperMedal = false;
        //achieve5.hasSilverMedal = false;
        //achieve5.hasGoldMedal = false;

        //NewProfileModel.AchievementModel achieve6 = new NewProfileModel.AchievementModel();
        //achieve6.id = 6;
        //achieve6.name = "Usar carta Anula Questão";
        //achieve6.progressCurrent = 87;
        //achieve6.progressTotal = 100;
        //achieve6.progressText = "usos";
        //achieve6.hasCopperMedal = true;
        //achieve6.hasSilverMedal = false;
        //achieve6.hasGoldMedal = false;

        //achieves = new List<NewProfileModel.AchievementModel>();
        //achieves.Add(achieve1);
        //achieves.Add(achieve2);
        //achieves.Add(achieve3);
        //achieves.Add(achieve4);
        //achieves.Add(achieve5);
        //achieves.Add(achieve6);
        //// FIM TEST

        //for (int i = 0; i < Globals.playerAchievements.Count; i++)
        //{
        //    AchievementSnippet achiev = Instantiate(prefabItemAchievements, contentAchievements);
        //    achiev.Configure(Globals.playerAchievements[i]);
        //}


        if (achieves != null && achieves.Conquistas != null && achieves.Conquistas.Count > 0)
        {
            textStatusAchievements.gameObject.SetActive(false);

            for (int i = 0; i < achieves.Conquistas.Count; i++)
            {
                AchievementSnippet achieve = Instantiate(prefabItemAchievements, contentAchievements);
                achieve.Configure(achieves.Conquistas[i]);
            }
        }
        else
        {
            textStatusAchievements.text = "EM BREVE";
            textStatusAchievements.gameObject.SetActive(true);
        }
    }




    public void OnClickTabInfoWar()
    {
        if (currentTab != tabInfo)
        {
            Sioux.Audio.Play("Click");
            currentTab.color = Grow.Grow.Properties.COLOR_TAB_OFF;
            currentTextTab.color = Grow.Grow.Properties.COLOR_TEXT_OFF;

            DisableAllPanels();

            tabInfo.color = Grow.Grow.Properties.COLOR_TAB_ON;
            textTabInfo.color = Grow.Grow.Properties.COLOR_TEXT_ON;
            panelMyProfileAndInfo.SetActive(true);
            panelProfileMain.SetActive(true);
            panelInfo.SetActive(true);

            currentTab = tabInfo;
            currentTextTab = textTabInfo;

            if (responseInfo == null)
            {
                ServerManager.instance.GetUserProfileInfo(userDetails.User.IdGrowGames,
                    (response) =>
                    {
                        //Debug.Log("response: " + response);

                        if (!string.IsNullOrEmpty(response))
                        {
                            UsuarioEstatisticas stats = JsonConvert.DeserializeObject<UsuarioEstatisticas>(response);

                            responseInfo = stats;

                            UpdateTabInfo(stats);
                        }
                    });
            }
            //else
            //{
            //    UpdateTabInfo(responseInfo);
            //}
        }
    }




    public void OnClickTabHistoricWar()
    {
        if (currentTab != tabHistoric)
        {
            Sioux.Audio.Play("Click");
            currentTab.color = Grow.Grow.Properties.COLOR_TAB_OFF;
            currentTextTab.color = Grow.Grow.Properties.COLOR_TEXT_OFF;

            DisableAllPanels();

            tabHistoric.color = Grow.Grow.Properties.COLOR_TAB_ON;
            textTabHistoric.color = Grow.Grow.Properties.COLOR_TEXT_ON;
            panelHistoric.SetActive(true);

            currentTab = tabHistoric;
            currentTextTab = textTabHistoric;

            if (responseMatches == null)
            {
                ServerManager.instance.GetUserProfileMatches(userDetails.User.IdGrowGames,
                    (response) =>
                    {
                        //Debug.Log("response: " + response);

                        if (!string.IsNullOrEmpty(response))
                        {
                            List<Partida> matches = JsonConvert.DeserializeObject<List<Partida>>(response);

                            responseMatches = matches;

                            UpdateTabHistoric(matches);
                        }
                        else
                        {
                            textStatusHistoric.text = "Você não tem nenhuma partida no seu histórico";
                            textStatusHistoric.gameObject.SetActive(true);
                        }
                    });
            }
            //else
            //{
            //    UpdateTabHistoric(responseMatches);
            //}
        }
    }


    public void OnClickTabAchievementsWar()
    {
        if (currentTab != tabAchievements)
        {
            Sioux.Audio.Play("Click");
            currentTab.color = Grow.Grow.Properties.COLOR_TAB_OFF;
            currentTextTab.color = Grow.Grow.Properties.COLOR_TEXT_OFF;

            DisableAllPanels();

            tabAchievements.color = Grow.Grow.Properties.COLOR_TAB_ON;
            textTabAchievements.color = Grow.Grow.Properties.COLOR_TEXT_ON;
            panelAchievements.SetActive(true);

            currentTab = tabAchievements;
            currentTextTab = textTabAchievements;

            if (responseAchieves == null)
            {
                ServerManager.instance.GetUserProfileAchievements(userDetails.User.Id,
                    (response) =>
                    {
                        //Debug.Log("response: " + response);

                        if (!string.IsNullOrEmpty(response))
                        {
                            Usuario.UserData achieves = JsonConvert.DeserializeObject<Usuario.UserData>(response);

                            responseAchieves = achieves;

                            UpdateTabAchievements(achieves);
                        }
                        else
                        {
                            textStatusAchievements.text = "EM BREVE";
                            textStatusAchievements.gameObject.SetActive(true);
                        }
                    });
            }
            else
            {
                UpdateTabAchievements(responseAchieves);
            }
        }
    }



    public void OnClickHelpPatent()
    {
        GameObject popup = PopupManager.instance.OpenPopup("PopupMessage");
        popup.GetComponent<PopupMessage>().Configure("AJUDA - PATENTE", "A patente de um jogador indica seu desempenho recente. Cada patente possui uma quantidade de derrotas para rebaixamento e uma quantidade de vitórias para promoção. Não há rebaixamento na patente minima. Na patente máxima, a pontuação de promoção anula as derrotas acumuladas.",
            () =>
            {
                PopupManager.instance.ClosePopup();
            });
    }



    public void OnClickHelpRating()
    {
        GameObject popup = PopupManager.instance.OpenPopup("PopupMessage");
        popup.GetComponent<PopupMessage>().Configure("AJUDA - CLASSIFICAÇÃO", "A nova classificação de jogador permite que os jogadores avaliem o comportamento dos outros competidores da partida",
            () =>
            {
                PopupManager.instance.ClosePopup();
            });
    }
}
