﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Grow.War;
using UnityEngine.UI;
using UnityEngine.Events;

public class PlayerSnippetScrollSnap : ScrollSnap
{
    public Image alphaDisabled;

    public class OnEndLerp : UnityEvent<int> { };
    public OnEndLerp onEndLerp;

    protected override void Awake()
    {
        base.Awake();
        this.onEndLerp = new OnEndLerp();

    }

    void LateUpdate()
    {
        if (isLerping)
        {
            LerpToElement();
            if (ShouldStopLerping())
            {

                int modulo = (actualIndex % 3);
                modulo = modulo == -1 ? 2 : modulo == -2 ? 1 : modulo;
                //Debug.Log(modulo);
                if (modulo == 2)
                    alphaDisabled.gameObject.SetActive(true);
                else
                    alphaDisabled.gameObject.SetActive(false);

                isLerping = false;
                canvasGroup.blocksRaycasts = true;
                onLerpComplete.Invoke();
                onEndLerp.Invoke(modulo);
                onLerpComplete.RemoveListener(WrapElementAround);
            }
        }
    }
}
