﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using System.Linq;
using TMPro;
using Newtonsoft.Json;

namespace Grow.War
{
    public class EndGameSnippet : MonoBehaviour
    {
        public Transform endGameCard;

        public Image borderColor;
        public Image soldier;
        public Image avatar;
        public Image avatarBorder;
        public Image bgName;
        public GameObject victoryLabel;
        public Image patente;
        public List<Sprite> patents;

        public TextMeshProUGUI nameText;
        public TextMeshProUGUI troopsText;
        public TextMeshProUGUI territoriesText;
        public TextMeshProUGUI pontosText;

        //bottom panel
        public bool isBottomPanel;
        public Image borderRightColor;
        public TextMeshProUGUI pontosTotal;
        public TextMeshProUGUI rankingPosition;

        public Image objectiveImage;

        public Sprite objectiveYellow;
        public Sprite objectiveBlue;
        public Sprite objectiveWhite;
        public Sprite objectiveBlack;
        public Sprite objectiveGreen;
        public Sprite objectiveRed;

        public EndGameController endGameController;
        public Button btBackToWinner;

        private Player player;

        public Image ratingImage;

        public GameObject bgPontos;
        public Image hexagonoBorder;
        public Image hexagono;

        public Image hexagonoGlow;
        public Image cardGlow;
        public GameObject panelOnlyOnlineMode;

        public VictoryAnimation victoryAnim;

        public TextMeshProUGUI textBottomPanelSubtitle;




        public void Configure(Player _player, int matchPoints = -1, int totalPoints = -1, int _rankingPosition = -1, int objectiveId = -1, int patentLevel = -1, string lastUpdateTime = "")
        {
            player = _player;
            if (objectiveId >= 0)
                player.objective = Objective.LIST[objectiveId];
            borderColor.color = player.warColor.color;
            soldier.color = player.warColor.color;

            if (GameController.Instance.SERVER.IsOnline())
            {
                // Patente
                patente.gameObject.SetActive(true);
                patente.sprite = patents[player.patent - 1];

                if (player.idGrow == Globals.playerIdGrowGames)
                {
                    // Atualiza a patente do jogador com a patente vinda da proc EncerrarJogo
                    Globals.playerPatent = patentLevel;
                }

                avatar.sprite = player.avatar;
            }
            else
            {
                patente.gameObject.SetActive(false);
                bgPontos.SetActive(false);
                avatar.gameObject.SetActive(false);
                avatarBorder.GetComponent<Button>().interactable = false;
            }

            avatarBorder.color = player.warColor.color;
            bgName.color = new Color(player.warColor.color.r / 4, player.warColor.color.g / 4, player.warColor.color.b / 4, 1);

            nameText.text = player.GetName;
            var playerTerritories = player.GetTerritories(GameController.gameState.territoryStates);

            troopsText.text = playerTerritories.Sum(ts => ts.amountArmy).ToString();
            territoriesText.text = playerTerritories.Count.ToString() + "/42";
            pontosText.text = "+" + matchPoints + " PONTOS";

            if (player.warColor == WarColor.YELLOW)
            {
                objectiveImage.sprite = objectiveYellow;
                victoryAnim.objetivo.sprite = objectiveYellow;
            }
            else if (player.warColor == WarColor.BLUE)
            {
                objectiveImage.sprite = objectiveBlue;
                victoryAnim.objetivo.sprite = objectiveBlue;
            }
            else if (player.warColor == WarColor.WHITE)
            {
                objectiveImage.sprite = objectiveWhite;
                victoryAnim.objetivo.sprite = objectiveWhite;
            }
            else if (player.warColor == WarColor.BLACK)
            {
                objectiveImage.sprite = objectiveBlack;
                victoryAnim.objetivo.sprite = objectiveBlack;
            }
            else if (player.warColor == WarColor.GREEN)
            {
                objectiveImage.sprite = objectiveGreen;
                victoryAnim.objetivo.sprite = objectiveGreen;
            }
            else if (player.warColor == WarColor.RED)
            {
                objectiveImage.sprite = objectiveRed;
                victoryAnim.objetivo.sprite = objectiveRed;
            }

            if (GameController.gameState.currentPlayer == player)
            {
                //victoryLabel.color = player.warColor.color;
                objectiveImage.enabled = false;
                victoryAnim.faixa.color = player.warColor.color;
                victoryAnim.pontaEsquerda.color = player.warColor.color;
                victoryAnim.pontaDireita.color = player.warColor.color;
                victoryLabel.gameObject.SetActive(true);
            }
            //Gradient gradient = new Gradient();
            //gradient.SetKeys(new GradientColorKey[] { new GradientColorKey(Color.blue, 0.0f), new GradientColorKey(Color.red, 1.0f) }, new GradientAlphaKey[] { new GradientAlphaKey(1.0f, 0.0f), new GradientAlphaKey(0.0f, 1.0f) });
            //gradient.alphaKeys = new GradientAlphaKey[] { new GradientAlphaKey(0, 0), new GradientAlphaKey(1, 0.5f), new GradientAlphaKey(0, 1) };
            //gradient.colorKeys = new GradientColorKey[] { new GradientColorKey(player.warColor.color, 0), new GradientColorKey(player.warColor.color, 0.5f), new GradientColorKey(player.warColor.color, 1) };

            //var colorOverLifeTime = particle.colorOverLifetime;
            //colorOverLifeTime.enabled = true;
            //colorOverLifeTime.color = gradient;

            if (isBottomPanel)
            {
                if (!GameController.Instance.SERVER.IsOnline())
                    panelOnlyOnlineMode.gameObject.SetActive(true);

                if (totalPoints < 1)
                {
                    pontosTotal.text = "0 PONTOS";
                }
                else if (totalPoints == 1)
                {
                    pontosTotal.text = "1 PONTO";
                }
                else
                {
                    pontosTotal.text = totalPoints + " PONTOS";
                }

                if (_rankingPosition == 0)
                {
                    rankingPosition.text = "-º";
                }
                else
                {
                    rankingPosition.text = _rankingPosition + "º";
                }

                borderRightColor.color = player.warColor.color;

                // Atualiza a data que foi calculado o ranking
                if (textBottomPanelSubtitle != null)
                {
                    textBottomPanelSubtitle.text = lastUpdateTime;
                    textBottomPanelSubtitle.gameObject.SetActive(true);
                }
            }
            else
            {
                if (!GameController.Instance.SERVER.IsOnline())
                    hexagono.gameObject.SetActive(false);
                else
                    hexagonoBorder.color = player.warColor.color;

                // Atualiza a data que foi calculado o ranking
                if (textBottomPanelSubtitle != null)
                {
                    textBottomPanelSubtitle.gameObject.SetActive(false);
                }
            }
        }

        public void UpdateRating(Sprite sprite)
        {
            //if (ratingImage.gameObject.activeInHierarchy)
            //{

            //}


            ratingImage.sprite = sprite;
            ratingImage.gameObject.SetActive(true);
        }

        public void AnimaCard()
        {
            endGameCard.DOLocalMoveY(0, 0.3f).SetEase(Ease.OutQuad);
        }


        /// <summary>
        /// Função para pegar o Id do rating de acordo com o nome do sprite
        /// </summary>
        private int GetRatingIdBySpriteName(string spriteName)
        {
            switch (spriteName)
            {
                case "sortudo":
                    return 1;

                case "vingativo":
                    return 2;

                case "estrategista":
                    return 3;

                case "honrado":
                    return 4;

                default:
                    return 1;
            }
        }



        public void OnClickObjetivo()
        {
            endGameController = FindObjectOfType<EndGameController>();
            endGameController.GetComponent<EndGameController>().ShowObjective(player);
        }

        public void OnClickVerMapa()
        {
            Sioux.Audio.Play("Click");
            endGameController.blockMap = false;
            btBackToWinner.gameObject.SetActive(true);
            endGameController.GetComponent<CanvasGroup>().DOFade(0, 0.2f).OnComplete(() => endGameController.gameObject.SetActive(false));
        }

        public void OnClickBackToEndGame()
        {
            Sioux.Audio.Play("Click");
            endGameController.GetComponent<EndGameController>().blockMap = true;
            btBackToWinner.gameObject.SetActive(false);
            endGameController.gameObject.SetActive(true);
            endGameController.GetComponent<CanvasGroup>().DOFade(1, 0.2f);
        }

        public void OnClickSair()
        {
            Sioux.Audio.Play("Click");
            if (GameController.Instance.SERVER.IsOnline())
            {
                // Envia o rating dos usuários se existirem
                // NOTE: Podíamos fazer no smartfox mas eu não sei o que acontece se saímos da sala, sinto um pouco mais de segurança na API
                EndGameSnippet[] endGameSnippets = GameObject.FindObjectsOfType<EndGameSnippet>();

                List<RatingModel> ratingModels = new List<RatingModel>();

                // - Percorre todos os snippets pra ver se tem algum deles preenchido
                for (int i = 0; i < endGameSnippets.Length; i++)
                {
                    EndGameSnippet snippet = endGameSnippets[i];

                    if (snippet.ratingImage.sprite != null)
                    {
                        if (!string.IsNullOrEmpty(snippet.ratingImage.sprite.name))
                        {
                            RatingModel ratingModel = new RatingModel();
                            ratingModel.idRoom = SFS.Grow.sfs.LastJoinedRoom.Name;
                            ratingModel.idUsuarioAutor = Globals.playerId;
                            ratingModel.idUsuario = snippet.player.idUsuario;
                            ratingModel.idRating = GetRatingIdBySpriteName(snippet.ratingImage.sprite.name);
                            ratingModels.Add(ratingModel);
                        }
                    }
                }

                // - Envia para o server
                ServerManager.instance.SendRating(ratingModels, (response) => { });


                SFS.Grow.ExitRoomRequest();
                SceneManager.instance.ChangeScene("MainMenu");
            }
            else
            {
                SceneManager.instance.ChangeScene("ChooseMode");
            }
        }

        public void OnClickAvatar()
        {
            Sioux.Audio.Play("Click");
            ServerManager.instance.OpenNewProfilePopup(player.idGrow);
        }


    }
}


