﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using System.Linq;

namespace Grow.War
{
    public class PanelNewArmies : MonoBehaviour
    {

        private CanvasGroup cg;
        public Text troopsText;
        public Text troopsSumText;
        public Footer footer;

        public void Start()
        {
            cg = GetComponent<CanvasGroup>();
            transform.localScale = Vector3.zero;
            cg.alpha = 0;
            gameObject.SetActive(false);
        }

        public void SetText()
        {
            troopsText.text = "";

            if (GameController.gameState.turnArmy[0] > 0)
            {
                troopsText.text += GameController.gameState.turnArmy[0] + " exércitos por dominar a América do Norte \n";
            }
            if (GameController.gameState.turnArmy[1] > 0)
            {
                troopsText.text += GameController.gameState.turnArmy[1] + " exércitos por dominar a América do Sul \n";
            }
            if (GameController.gameState.turnArmy[2] > 0)
            {
                troopsText.text += GameController.gameState.turnArmy[2] + " exércitos por dominar a Europa \n";
            }
            if (GameController.gameState.turnArmy[3] > 0)
            {
                troopsText.text += GameController.gameState.turnArmy[3] + " exércitos por dominar a África \n";
            }
            if (GameController.gameState.turnArmy[4] > 0)
            {
                troopsText.text += GameController.gameState.turnArmy[4] + " exércitos por dominar a Ásia \n";
            }
            if (GameController.gameState.turnArmy[5] > 0)
            {
                troopsText.text += GameController.gameState.turnArmy[5] + " exércitos por dominar a Oceania \n";
            }
            if (GameController.gameState.turnArmy[6] > 0)
            {
                troopsText.text += GameController.gameState.turnArmy[6] + " exércitos por ter " + GameController.gameState.thisPlayer.GetTerritories(GameController.gameState.territoryStates).Count + " territórios \n";
            }


            troopsSumText.text = GameController.gameState.turnArmy.Sum().ToString() + " exércitos a serem posicionados";
        }

        public void TradeText(int bonusArmy)
        {
            troopsText.text += bonusArmy + " exércitos por ter feito uma troca";
            troopsSumText.text = GameController.gameState.turnArmy.Sum().ToString() + " exércitos a serem posicionados";
        }

        public void Show()
        {
            transform.DOScale(1f, 0.5f);
            gameObject.SetActive(true);

            cg.DOFade(1f, 0.3f);
        }

        public void Hide()
        {
            Sioux.Audio.Play("Click");
            transform.DOScale(0f, 0.5f).OnComplete(
                () =>
                {
                    gameObject.SetActive(false);

                });
            cg.DOFade(0f, 0.3f);

        }
    }
}
