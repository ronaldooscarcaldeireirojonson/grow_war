﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Grow.War
{
    public class GameHeader : MonoBehaviour
    {
        private RectTransform rect;
        public Transform container;
        public GamePlayerColor playerColorPrefab;
        public Image avatar;
        public List<GamePlayerColor> playersColor;
        private bool isVisible;

        [Header("Phase Sprites")]
        public Sprite fortifyOff;
        public Sprite fortifyOn;
        public Sprite attackOff;
        public Sprite attackOn;
        public Sprite moveOff;
        public Sprite moveOn;

        public Image fortifyImage;
        public Image attackImage;
        public Image moveImage;
        public CameraHandler cameraHandler;


        // Use this for initialization
        IEnumerator Start()
        {
            yield return new WaitUntil(() => GameController.gameState != null);
            rect = GetComponent<RectTransform>();
            for (int i = 0; i < GameController.gameState.players.Count; i++)
            {
                GamePlayerColor playerSnippet = Instantiate(playerColorPrefab, container);
                playerSnippet.RefreshColor(GameController.gameState.players[i].warColor.color);
                playersColor.Add(playerSnippet);
            }
        }

        public void Update()
        {

        }






        public void RefreshHeader()
        {
            //LIGA O SPRITE DE QUAL FASE DO TURNO ELE ESTÁ
            fortifyImage.sprite = GameController.gameState.phase == 0 ? fortifyOn : fortifyOff;
            attackImage.sprite = GameController.gameState.phase == 1 ? attackOn : attackOff;
            moveImage.sprite = GameController.gameState.phase == 2 ? moveOn : moveOff;

            for (int i = 0; i < playersColor.Count; i++)
            {

                if (GameController.gameState.players[i] == GameController.gameState.currentPlayer)
                {
                    playersColor[i].ShowSelected();
                }
                else
                {
                    playersColor[i].HideSelected();
                }
            }
            Show();
        }

        public void Show()
        {
            if (!isVisible)
            {
                rect.DOAnchorPosY(0, 0.3f);
                isVisible = true;
            }
        }
    }
}
