﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Grow.War;
using Grow;

public class EliminatedPlayer : MonoBehaviour
{
    public CanvasGroup cg;
    public Text descText;
    public Button btOk;
    public Button btVoltarInicio;
    public Button btAssistir;
    public Button btSair;
    public Image bandeiraImg;

    // Use this for initialization
    void Start()
    {
        cg.alpha = 0;
        gameObject.SetActive(false);
    }

    public void SetText(Player player, Player eliminated)
    {
        bandeiraImg.color = eliminated.warColor.color;
        descText.text = "<color=" + ColorTypeConverter.ToRGBHex(eliminated.warColor.color) + ">" + eliminated.GetName + " </color>" + "foi eliminado. \n" + "Suas cartas foram transferidas para " + "<color=" + ColorTypeConverter.ToRGBHex(player.warColor.color) + ">" + player.GetName + " </color>";

        if (eliminated == GameController.gameState.thisPlayer)
        {
            btVoltarInicio.gameObject.SetActive(true);
            btAssistir.gameObject.SetActive(true);
            btOk.gameObject.SetActive(false);
        }
        else
        {
            btVoltarInicio.gameObject.SetActive(false);
            btAssistir.gameObject.SetActive(false);
            btOk.gameObject.SetActive(true);
        }
    }

    public void Show()
    {
        gameObject.SetActive(true);
        cg.DOFade(1, 0.5f);
    }

    public void Hide()
    {
        Sioux.Audio.Play("Click");
        cg.DOFade(0, 0.5f).OnComplete(() => gameObject.SetActive(false));
    }

    public void AssistirButton()
    {
        Sioux.Audio.Play("Click");
        cg.DOFade(0, 0.5f).OnComplete(
            () =>
            {
                btSair.gameObject.SetActive(true);
                gameObject.SetActive(false);
            });

        GameController.Instance.footer.gameObject.SetActive(false);
        GameController.Instance.footer.rectFooterWait.gameObject.SetActive(false);

    }

    public void BackHomeButton()
    {
        Sioux.Audio.Play("Click");
        if (GameController.Instance.SERVER.IsOnline())
        {
            SFS.Grow.ExitRoomRequest();
            SceneManager.instance.ChangeScene("MainMenu");
        }
        else
        {
            SceneManager.instance.ChangeScene("ChooseMode");
        }
    }

}
