﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class VictoryAnimation : MonoBehaviour
{

    public Image brilho;
    public Image objetivo;
    public Image louroEsquerda;
    public Image louroDireita;
    public Image pontaEsquerda;
    public Image pontaDireita;
    public Image faixa;


    // Use this for initialization
    void Start()
    {
        faixa.transform.DOScale(1, 0.7f);

        pontaEsquerda.transform.DOLocalMoveX(-76.6f, 0.7f);
        pontaEsquerda.transform.DOLocalMoveY(5.8f, 0.7f);

        pontaDireita.transform.DOLocalMoveX(76.6f, 0.6f);
        pontaDireita.transform.DOLocalMoveY(5.8f, 0.6f).OnComplete(() =>
        {
            louroEsquerda.transform.DOLocalMoveY(7, 0.6f);
            louroDireita.transform.DOLocalMoveY(7, 0.6f).OnComplete(() =>
            {
                objetivo.transform.DOLocalMoveY(5, 0.6f).OnComplete(() =>
                {
                    brilho.DOFade(1, 0.3f);
                    brilho.transform.DOLocalRotate(Vector3.forward * 180, 5f).SetLoops(-1, LoopType.Incremental).SetEase(Ease.Linear);
                });

            });
        });

    }
}
