﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
namespace Grow.War
{
    [CreateAssetMenu]
    public class DebugCommands : ScriptableObject
    {

        public List<string> players;
        public List<TerritoryStateSerializable> territoryStateSerializables;
        public enum playerNumber
        {
            Player_1,
            Player_2,
            Player_3,
            Player_4,
            Player_5,
            Player_6
        }

        public void LoadStates()
        {
            territoryStateSerializables = null;
            players = null;
            if (GameController.gameState != null)
            {
                territoryStateSerializables = new List<TerritoryStateSerializable>();
                players = new List<string>();

                GameController.gameState.players.ForEach(p=> {
                    players.Add(p.GetName);
                });

                GameController.gameState.territoryStates.ForEach(ts =>
                {
                    territoryStateSerializables.Add(new TerritoryStateSerializable() {
                        owner = name = ts.owner.GetName,
                        territory = new TerritorySerializable() { name = ts.territory.name, id = ts.territory.id },
                        amountArmy = ts.amountArmy,
                        territoryName = ts.territory.name
                    });
                });
            }
            else
            {
                Debug.LogError("No GameState Available");
            }
        }

        public void SubmitChanges()
        {
            var error = false;
            territoryStateSerializables.ForEach(tss=>
            {
                if (!players.Contains(tss.owner))
                {
                    Debug.Log("Wrong Name! " + tss.owner);
                    error = true;
                }
                //Check terrotory
            });


            if (!error)
            {
                for (int i = 0; i < GameController.gameState.territoryStates.Count; i++)
                {
                    GameController.gameState.territoryStates[i].owner = GameController.gameState.players.Find(p => p.GetName == territoryStateSerializables[i].owner);
                    GameController.gameState.territoryStates[i].amountArmy = territoryStateSerializables[i].amountArmy;
                }

                GameController.Instance.DEBUG_SetTerritoryStateListRequest(GameController.gameState.territoryStates);
            }
        }

#if UNITY_EDITOR
        [MenuItem("Sioux Tools/Debug Commands")]
        static void Select()
        {
            Selection.activeObject = AssetDatabase.LoadAssetAtPath<DebugCommands>("Assets/Scripts/Tools/DebugCommands.asset");
        }
        [MenuItem("Sioux Tools/Play Login Scene %&po")]
        static void PlayGame()
        {
            Debug.Log("PLAY");
        }
#endif
        [System.Serializable]
        public class TerritoryStateSerializable
        {
            public string territoryName;
            public string owner; // O dono do território
            [HideInInspector]
            public TerritorySerializable territory; // O território em si
            public int amountArmy = 1; // Quantidade de exércitos nesse território
        }

        [System.Serializable]
        public class PlayerSerializable
        {
            public string name;
        }

        [System.Serializable]
        public class TerritorySerializable
        {
            public int id; // Id
            public string name; // Nome
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(DebugCommands))]
    public class BookContentRegisterEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DebugCommands myScript = (DebugCommands)target;
            GUILayout.Label("Commands");

            if (GUILayout.Button("Load States"))
                myScript.LoadStates();
            if (GUILayout.Button("Submit Changes"))
                myScript.SubmitChanges();

            GUILayout.Space(20);

            DrawDefaultInspector();
        }
    }

#endif

}