﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Grow.War
{
    public class PanelAlertObjective : MonoBehaviour
    {

        private CanvasGroup cg;

        public void Start()
        {
            cg = GetComponent<CanvasGroup>();
            transform.localScale = Vector3.zero;
            cg.alpha = 0;
            gameObject.SetActive(false);
        }

        public void Show()
        {
            transform.DOScale(1f, 0.5f);
            cg.DOFade(1f, 0.3f);
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            transform.DOScale(0f, 0.5f).OnComplete(() => gameObject.SetActive(false));
            cg.DOFade(0f, 0.3f);

        }
    }
}
