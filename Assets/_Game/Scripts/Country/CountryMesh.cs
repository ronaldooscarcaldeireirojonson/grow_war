﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CountryMesh : MonoBehaviour {

    public Transform centerPosition;

    public List<CountryMesh> adjacentCountries;

    [SerializeField]
    private List<MeshRenderer> faceMeshes;
    [SerializeField]
    private List<MeshRenderer> borderMeshes;


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawCube(centerPosition.position, Vector3.one*0.5f);
        
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.white;
        for (int i = 0; i < adjacentCountries.Count; i++)
        {
            Gizmos.DrawLine(centerPosition.position, adjacentCountries[i].centerPosition.position);
        }
    }
}
