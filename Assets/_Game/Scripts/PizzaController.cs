﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Grow.War
{
    public class PizzaController : MonoBehaviour
    {
        public List<PizzaSlot> slots;
        public PizzaSlot slotPrefab;
        public Transform container;
        public RectTransform cursor;


        public void UpdatePizza()
        {
            int count = 0;

            for (int i = 0; i < GameController.gameState.players.Count; i++)
            {
                //TODO: OTIMIZAR
                for (int j = 0; j < GameController.gameState.players[i].GetTerritories(GameController.gameState.territoryStates).Count; j++)
                {
                    slots[count++].slotImage.color = GameController.gameState.players[i].warColor.color;
                    slots[count - 1].player = GameController.gameState.players[i];
                }
            }
        }

        public void UpdateCursor()
        {
            var posInicial = -1;
            for (int i = 0; i < slots.Count; i++)
            {
                if (slots[i].player.idGrow == GameController.gameState.currentPlayer.idGrow)
                {
                    posInicial = i;
                    break;
                }
            }

            var posFinal = GameController.gameState.currentPlayer.GetTerritories(GameController.gameState.territoryStates).Count + posInicial - 1;
            //Debug.Log("posFinal: " + posFinal);
            //Debug.Log("posInicial: " + posInicial);
            //Debug.Log("posFinal Z: " + slots[posFinal].GetComponent<RectTransform>().localRotation.eulerAngles.z);
            //Debug.Log("posInicial Z: " + slots[posInicial].GetComponent<RectTransform>().localRotation.eulerAngles.z);
            cursor.DOLocalRotate((slots[posFinal].GetComponent<RectTransform>().localRotation.eulerAngles + slots[posInicial].GetComponent<RectTransform>().localRotation.eulerAngles) / 2, 0.5f, RotateMode.FastBeyond360);
            //cursor.DOLocalRotateQuaternion(Quaternion.Euler(0, 0, (slots[posFinal].GetComponent<RectTransform>().localRotation.eulerAngles.z + slots[posInicial].GetComponent<RectTransform>().localRotation.eulerAngles.z) / 2), 0.5f);

        }

























        [ContextMenu("SetUp")]
        public void SetUp()
        {
            slots = new List<PizzaSlot>();

            for (int j = 0; j < 42; j++)
            {
                PizzaSlot slot = Instantiate(slotPrefab, container);
                slot.transform.localScale = Vector3.one;
                slots.Add(slot);
            }


            for (int i = 0; i < slots.Count; i++)
            {
                slots[i].transform.localRotation = Quaternion.Euler(0, 0, ((-360f * i) - (360f / (float)slots.Count)) / (float)slots.Count);
            }
        }
    }
}