﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Grow.War
{
    public class NewTurnLog : MonoBehaviour
    {
        public TextMeshProUGUI lineLeft;
        public TextMeshProUGUI playerName;
        public TextMeshProUGUI lineRight;


        public void Configure(Player player)
        {
            //lineLeft.color = player.warColor.color;
            //lineRight.color = player.warColor.color;
            playerName.text = player.GetName;
            //playerName.color = player.warColor.color;
        }

    }
}