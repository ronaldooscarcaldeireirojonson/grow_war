﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Grow.War
{
    public class MoveLog : MonoBehaviour
    {

        public TextMeshProUGUI territory;
        public TextMeshProUGUI territoryTarget;
        public Text amountArmy;


        public void Configure(Player player, string territoryName, string territoryTargetName, int movedArmy)
        {
            //territory.color = player.warColor.color;
            territory.text = territoryName;

            // territoryTarget.color = player.warColor.color;
            territoryTarget.text = territoryTargetName;

            amountArmy.text = movedArmy.ToString();
        }

    }
}