﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Grow.War
{
    public class ReconnectLog : MonoBehaviour
    {

        public TextMeshProUGUI playerName;


        public void Configure(Player player)
        {
            //playerName.color = player.warColor.color;
            playerName.text = player.GetName;
        }

    }
}