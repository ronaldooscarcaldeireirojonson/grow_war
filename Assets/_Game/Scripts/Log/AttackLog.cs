﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Grow.War
{
    public class AttackLog : MonoBehaviour
    {

        public List<Image> attackDicesImage;
        public List<Image> defDicesImage;
        public TextMeshProUGUI attackOrigin;
        public TextMeshProUGUI attackTarget;
        public Image starAttack;
        public Image starDef;

        public Sprite dice1;
        public Sprite dice2;
        public Sprite dice3;
        public Sprite dice4;
        public Sprite dice5;
        public Sprite dice6;

        private int attackPoints;
        private int defPoints;


        public void Configure(TerritoryObject toOrigin, TerritoryObject toTarget, List<int> attackDices, List<int> defenceDices)
        {
            for (int i = 0; i < attackDices.Count; i++)
            {
                switch (attackDices[i])
                {
                    case 0:
                        attackDicesImage[i].color = Color.clear;
                        break;

                    case 1:
                        attackDicesImage[i].sprite = dice1;
                        break;

                    case 2:
                        attackDicesImage[i].sprite = dice2;
                        break;

                    case 3:
                        attackDicesImage[i].sprite = dice3;
                        break;

                    case 4:
                        attackDicesImage[i].sprite = dice4;
                        break;

                    case 5:
                        attackDicesImage[i].sprite = dice5;
                        break;

                    case 6:
                        attackDicesImage[i].sprite = dice6;
                        break;

                    default:
                        Debug.Log("ERROR!");
                        break;
                }

                if (attackDices[i] != 0 && defenceDices[i] != 0)
                {
                    if (attackDices[i] > defenceDices[i])
                    {
                        attackDicesImage[i].color = new Color32(255, 204, 204, 255);
                        if (attackDices[i] != 0 && defenceDices[i] != 0)
                            attackPoints++;
                    }
                    else
                    {
                        defDicesImage[i].color = new Color32(255, 255, 204, 255);
                        defPoints++;
                    }
                }
            }

            for (int i = 0; i < defenceDices.Count; i++)
            {
                switch (defenceDices[i])
                {
                    case 0:
                        defDicesImage[i].color = Color.clear;
                        //attackDicesImage[i].gameObject.SetActive(false);
                        break;

                    case 1:
                        defDicesImage[i].sprite = dice1;
                        break;

                    case 2:
                        defDicesImage[i].sprite = dice2;
                        break;

                    case 3:
                        defDicesImage[i].sprite = dice3;
                        break;

                    case 4:
                        defDicesImage[i].sprite = dice4;
                        break;

                    case 5:
                        defDicesImage[i].sprite = dice5;
                        break;

                    case 6:
                        defDicesImage[i].sprite = dice6;
                        break;

                    default:
                        Debug.Log("ERROR!");
                        break;
                }
            }

            if (defPoints >= attackPoints)
            {
                starDef.gameObject.SetActive(true);
                starAttack.gameObject.SetActive(false);
            }
            else
            {
                starDef.gameObject.SetActive(false);
                starAttack.gameObject.SetActive(true);
            }

            attackOrigin.text = toOrigin.territoryState.territory.name;
            //attackOrigin.color = toOrigin.territoryState.owner.warColor.color;

            attackTarget.text = toTarget.territoryState.territory.name;
            //attackTarget.color = toTarget.territoryState.owner.warColor.color;
        }

    }
}