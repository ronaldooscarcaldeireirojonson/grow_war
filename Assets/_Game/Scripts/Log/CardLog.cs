﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Grow.War
{
    public class CardLog : MonoBehaviour
    {

        public TextMeshProUGUI playerName;
        public TextMeshProUGUI amountCard;


        public void Configure(Player player)
        {
            playerName.text = player.GetName;
            //playerName.color = player.warColor.color;
            amountCard.text = "+ 1 carta";
        }

    }
}