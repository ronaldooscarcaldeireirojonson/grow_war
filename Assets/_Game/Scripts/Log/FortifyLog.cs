﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Grow.War
{
    public class FortifyLog : MonoBehaviour
    {

        public TextMeshProUGUI territory;
        public TextMeshProUGUI army;


        public void Configure(Player player, string territoryName, int amountArmy)
        {
            //territory.color = player.warColor.color;
            territory.text = territoryName;
            if (amountArmy == 1)
                army.text = "+ " + amountArmy + " exército";
            else
                army.text = "+ " + amountArmy + " exércitos";

        }

    }
}