﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Grow.War
{
    public class LookAt : MonoBehaviour
    {


        public Transform target;
        public Canvas canvas;
        public Camera cam;
        public TextMeshProUGUI territoryName;
        public bool showingName = true;
        public bool canShowName = true;


        // Use this for initialization
        void Start()
        {
            territoryName = GetComponentInChildren<TextMeshProUGUI>(true);
        }

        // Update is called once per frame
        void LateUpdate()
        {
            //transform.LookAt(target, Vector3.forward);
            canvas.transform.localScale = (Vector3.one * cam.fieldOfView) / 20000f;

            if (!canShowName)
                return;

            if (cam.fieldOfView <= 60)
            {
                if (!showingName && cam.gameObject.GetComponent<CameraHandler>().canClickMap)
                {
                    territoryName.DOFade(1, 0.5f);
                    showingName = true;
                }
            }
            else
            {
                if (showingName)
                {
                    territoryName.DOFade(0, 0.5f);
                    showingName = false;
                }

            }
        }

    }
}
