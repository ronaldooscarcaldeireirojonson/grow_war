﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Grow.War
{
    public class DragChat : MonoBehaviour, IDragHandler, IBeginDragHandler, IPointerDownHandler, IPointerUpHandler, IEndDragHandler
    {

        public RectTransform panel;
        public float minX = -616;
        public float maxX = -5;
        public CameraHandler camHandler;
        public bool startDrag;
        public WarChatPanel warChatPanel;



        public void LateUpdate()
        {
            ClampToBounds();
        }

        void ClampToBounds()
        {
            Vector3 pos = panel.transform.position;
            pos.x = Mathf.Clamp(panel.transform.position.x, minX * warChatPanel.scaleFactor, maxX * warChatPanel.scaleFactor);

            panel.transform.position = pos;
        }

        public void OnDrag(PointerEventData data)
        {
            //Debug.Log("mouse: " + Input.mousePosition);
            panel.transform.position = new Vector3(Input.mousePosition.x - ((60 + panel.sizeDelta.x) * warChatPanel.scaleFactor), panel.transform.position.y, panel.transform.position.z);
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            if (camHandler != null)
                camHandler.canPan = false;
            startDrag = true;
            ChatButtons.instance.gameObject.SetActive(false);
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (camHandler != null)
                camHandler.canPan = false;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (camHandler != null)
                camHandler.canPan = true;
            //Debug.Log(-panel.anchoredPosition.x);
            //Debug.Log(panel.sizeDelta.x / 1.4f);

            if (!startDrag)
                return;

            if (-panel.anchoredPosition.x > panel.sizeDelta.x * (warChatPanel.isChatOpen ? 0.1f : 0.9f))
                warChatPanel.isChatOpen = true;
            else
                warChatPanel.isChatOpen = false;


            warChatPanel.OnClickOpenPanel();
        }

        public void OnEndDrag(PointerEventData eventData)
        {

            startDrag = false;
        }
    }
}
