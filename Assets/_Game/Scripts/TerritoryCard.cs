﻿using Grow.War;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using DG.Tweening;

public class TerritoryCard : MonoBehaviour
{

    public Text cardName;
    public Image cardSymbol;

    public Sprite circle;
    public Sprite square;
    public Sprite triangle;
    public Sprite joker;
    public bool isSelected;
    private Territory territory;
    public static List<Territory> selectedCards = new List<Territory>();
    private Button BtTrocar;
    public RectTransform bgTransform;

    // Use this for initialization
    void Start()
    {
        BtTrocar = GameObject.Find("BtTrade").GetComponent<Button>();
    }

    public void SetCard(Territory _territory)
    {
        territory = _territory;
        cardName.text = _territory.name;
        if (_territory.symbol == SymbolType.Wildcard)
        {
            cardSymbol.sprite = joker;
            cardSymbol.SetNativeSize();
            cardSymbol.transform.localPosition = new Vector3(0, 13.6f, 0);
            cardName.gameObject.SetActive(false);
        }
        else if (_territory.symbol == SymbolType.Circle)
            cardSymbol.sprite = circle;
        else if (_territory.symbol == SymbolType.Square)
            cardSymbol.sprite = square;
        else if (_territory.symbol == SymbolType.Triangle)
            cardSymbol.sprite = triangle;

    }

    public void OnClick()
    {
        if (GameController.gameState.phase != 0 || !GameController.gameState.IsMyTurn())
            return;

        if (isSelected)
        {
            DeselectCard();
        }
        else
        {
            if (selectedCards.Count == 3)
                return;

            SelectCard();
        }
        isSelected = !isSelected;
        BtTrocar.interactable = CanTradeIn(selectedCards);
        Sioux.Audio.Play(GameController.Instance.cardsClip);

    }

    public static bool CanTradeIn(List<Territory> selectedCards)
    {
        if (selectedCards.Count != 3)
            return false; // para trocar, tem que ter 3 cards selecionados

        return selectedCards.Sum(c => c.symbol == SymbolType.Wildcard ? 1 : 0) >= 1
            || CompareCards(selectedCards, false)
            || CompareCards(selectedCards, true);
    }

    static bool CompareCards(List<Territory> cards, bool equals)
    {
        for (var i = 0; i < cards.Count; ++i)
            for (var j = i + 1; j < cards.Count; ++j)
                if ((cards[i].symbol != cards[j].symbol) == equals)
                    return false;
        return true;
    }

    public void DeselectCard()
    {
        bgTransform.DOAnchorPosY(0, 0.5f);
        selectedCards.Remove(territory);
    }

    public void SelectCard()
    {
        bgTransform.DOAnchorPosY(30, 0.5f);
        selectedCards.Add(territory);
    }


}
