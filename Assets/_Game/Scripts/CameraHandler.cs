﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.EventSystems;
namespace Grow.War
{



    public class CameraHandler : MonoBehaviour
    {
        public static CameraHandler Instance;
        private static readonly float PanSpeed = 30f;
        private static readonly float ZoomSpeedTouch = 0.1f;
        private static readonly float ZoomSpeedMouse = 30f;

        public Vector2 BoundsX = new Vector2(-22f, 18f);
        public Vector2 BoundsZ = new Vector2(-8f, 11f);
        public Vector2 ZoomBounds = new Vector2(30f, 90f);

        private Camera cam;

        public bool panActive;
        private Vector3 lastPanPosition;
        private int panFingerId; // Touch mode only

        public bool zoomActive;
        private Vector2[] lastZoomPositions; // Touch mode only


        private Vector3 _velocity;
        public bool _underInertia;
        private float _time = 0.0f;
        public float smoothTime;

        private Vector3 _screenPoint;
        private Vector3 _offset;
        private Vector3 _curScreenPoint;
        private Vector3 _curPosition;
        private float tempV;


        public float maxVelocity;

        public float clickThreshold;
        public bool isValidClick;
        private Vector3 firstPanPosition;
        private Canvas canvasMain;
        public bool canClickMap = false;
        public bool canPan;

        public MapController mapController;
        public Footer footer;
        public bool isGameOver;
        //public RectTransform scrollChat;
        public Vector3 camPosInicial;

        public EndGameController endGameController;

        void Awake()
        {
            Instance = this;
            cam = GetComponent<Camera>();
            canvasMain = GameObject.FindWithTag("CanvasMain").GetComponent<Canvas>();
            ClampToBounds();
            //#if UNITY_ANDROID || UNITY_IOS
            //            cam.fieldOfView = 80;
            //#endif
            float aspect = (float)Screen.width / (float)Screen.height;
            if (aspect >= 1.6f)
            {
                ZoomBounds.y = 80;
                cam.fieldOfView = 80;
                camPosInicial = transform.position = new Vector3(2.848194f, 28.78f, -1.977779f);
            }
            else
            {
                ZoomBounds.y = 90;
                cam.fieldOfView = 90;
                camPosInicial = transform.position = new Vector3(2.136423f, 28.78f, -2.997939f);

            }
            camPosInicial += new Vector3(4.2f, 0, 0);
        }

        public void LateUpdate()
        {
            if (Input.touchSupported && Application.platform != RuntimePlatform.WebGLPlayer)
            {
                HandleTouch();
            }
            else
            {
                HandleMouse();
            }

            if (_underInertia && _time <= smoothTime)
            {
                if (_velocity.magnitude > tempV)
                {
                    tempV = _velocity.magnitude;
                }
                transform.position += new Vector3(_velocity.x, 0, _velocity.z);
                _velocity = Vector3.Lerp(_velocity, Vector3.zero, _time / smoothTime);
                _time += Time.deltaTime;
                ClampToBounds();
            }
            else
            {
                _underInertia = false;
                _time = 0.0f;
            }
        }

        public void ResetCamPos()
        {
            transform.DOMove(camPosInicial, 0.5f);
            cam.DOFieldOfView(ZoomBounds.y, 0.5f);
        }
        void HandleTouch()
        {
            switch (Input.touchCount)
            {
                case 1: // Panning
                    zoomActive = false;

                    // If the touch began, capture its position and its finger ID.
                    // Otherwise, if the finger ID of the touch doesn't match, skip it.
                    Touch touch = Input.GetTouch(0);
                    if (touch.phase == TouchPhase.Began)
                    {
                        lastPanPosition = touch.position;
                        firstPanPosition = touch.position;
                        panFingerId = touch.fingerId;
                        panActive = true;
                        isValidClick = true;
                        _screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
                        _offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(touch.position.x, touch.position.y, _screenPoint.z));
                        _underInertia = false;
                        _velocity = Vector3.zero;
                        StartCoroutine("CheckValidClick");
                        RaycastClickSea();
                        //Debug.Log(clickThreshold * canvasMain.scaleFactor);
                    }
                    else if (touch.fingerId == panFingerId && touch.phase == TouchPhase.Moved && (firstPanPosition - (Vector3)touch.position).magnitude > clickThreshold * canvasMain.scaleFactor)
                    {
                        PanCamera(touch.position);
                    }
                    else if (touch.phase == TouchPhase.Ended)
                    {
                        _underInertia = true;
                        lastPanPosition = Vector3.zero;
                        firstPanPosition = Vector3.zero;
                        panFingerId = -50;
                        panActive = false;
                        RaycastClick();
                        StopCoroutine("CheckValidClick");
                        UIBlocker.uiBlocked = false;
                        footer.OnPlusClickUp();
                        footer.OnminusClickUp();
                    }
                    break;

                case 2: // Zooming
                    panActive = false;

                    Vector2[] newPositions = new Vector2[] { Input.GetTouch(0).position, Input.GetTouch(1).position };
                    if (!zoomActive)
                    {
                        lastZoomPositions = newPositions;
                        zoomActive = true;
                    }
                    else
                    {
                        // Zoom based on the distance between the new positions compared to the 
                        // distance between the previous positions.
                        float newDistance = Vector2.Distance(newPositions[0], newPositions[1]);
                        float oldDistance = Vector2.Distance(lastZoomPositions[0], lastZoomPositions[1]);
                        float offset = newDistance - oldDistance;

                        ZoomCamera(offset, ZoomSpeedTouch);
                        ClampToBounds();
                        lastZoomPositions = newPositions;
                    }
                    break;

                default:
                    panActive = false;
                    zoomActive = false;
                    isValidClick = false;
                    UIBlocker.uiBlocked = false;
                    break;
            }
        }

        void HandleMouse()
        {
            // On mouse down, capture it's position.
            // On mouse up, disable panning.
            // If there is no mouse being pressed, do nothing.
            if (Input.GetMouseButtonDown(0))
            {
                panActive = true;
                lastPanPosition = Input.mousePosition;
                firstPanPosition = Input.mousePosition;
                _screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
                _offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, _screenPoint.z));
                _underInertia = false;
                _velocity = Vector3.zero;
                isValidClick = true;
                StartCoroutine("CheckValidClick");
                RaycastClickSea();
            }
            else if (Input.GetMouseButtonUp(0))
            {
                lastPanPosition = Vector3.zero;
                firstPanPosition = Vector3.zero;
                panActive = false;
                _underInertia = true;
                RaycastClick();
                isValidClick = false;
                UIBlocker.uiBlocked = false;
                StopCoroutine("CheckValidClick");
                footer.OnPlusClickUp();
                footer.OnminusClickUp();
            }
            else if (Input.GetMouseButton(0) && (firstPanPosition - Input.mousePosition).magnitude > clickThreshold * canvasMain.scaleFactor)
            {
                PanCamera(Input.mousePosition);
            }

            // Check for scrolling to zoom the camera
            float scroll = Input.GetAxis("Mouse ScrollWheel");
            zoomActive = true;
            ZoomCamera(scroll, ZoomSpeedMouse);
            zoomActive = false;
            ClampToBounds();
        }

        IEnumerator CheckValidClick()
        {
            var pos = Input.mousePosition;
            yield return new WaitForSeconds(0.2f);
            var newPos = Input.mousePosition;
            isValidClick = ((pos - newPos).magnitude < clickThreshold * canvasMain.scaleFactor);
            if (isValidClick)
                RaycastClick();
        }

        void PanCamera(Vector3 newPanPosition)
        {
            if (!panActive || !canPan || UIBlocker.uiBlocked || GameController.Instance.chatPanel.isChatOpen || endGameController.blockMap)
                return;

            //Debug.Log("entrou no panCamera");
            // Translate the camera position based on the new input position
            Vector3 offset = cam.ScreenToViewportPoint(lastPanPosition - newPanPosition);
            isValidClick = ((firstPanPosition - newPanPosition).magnitude < clickThreshold * canvasMain.scaleFactor);

            Vector3 move = new Vector3(offset.x * PanSpeed, 0, offset.y * PanSpeed);
            transform.Translate(move, Space.World);


            lastPanPosition = newPanPosition;

            Vector3 _prevPosition = _curPosition;
            _curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, _screenPoint.z);
            _curPosition = Camera.main.ScreenToWorldPoint(_curScreenPoint) + _offset;
            _velocity = _curPosition - _prevPosition;
            _velocity = Vector3.ClampMagnitude(_velocity, maxVelocity);
            tempV = 0;
            transform.position = _curPosition;

            ClampToBounds();
        }

        public void ZoomCamera(float offset, float speed)
        {
            if (UIBlocker.uiBlocked || !zoomActive || offset == 0 || GameController.Instance.chatPanel.isChatOpen || footer.cardsOpened || endGameController.blockMap || GameController.Instance.isProfileOpen)
                return;

            cam.fieldOfView = Mathf.Clamp(cam.fieldOfView - (offset * speed), ZoomBounds.x, ZoomBounds.y);
        }

        void ClampToBounds()
        {
            Vector3 pos = transform.position;
            float fovFactor = Mathf.Lerp(1, 0.3f, (cam.fieldOfView - ZoomBounds.x) / (ZoomBounds.y - ZoomBounds.x));
            pos.x = Mathf.Clamp(transform.position.x, BoundsX.x * fovFactor, BoundsX.y * fovFactor);
            pos.z = Mathf.Clamp(transform.position.z, BoundsZ.x * fovFactor, BoundsZ.y * fovFactor);

            transform.position = pos;
        }

        public void RaycastClick()
        {
            if (!canClickMap || UIBlocker.uiBlocked || isGameOver || GameController.Instance.chatPanel.isChatOpen)
                return;


            Ray raycast;
            if (Input.touchSupported && Application.platform != RuntimePlatform.WebGLPlayer)
            {
                raycast = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            }
            else
            {
                raycast = Camera.main.ScreenPointToRay(Input.mousePosition);
            }

            RaycastHit raycastHit;
            if (Physics.Raycast(raycast, out raycastHit))
            {
                var to = raycastHit.collider.GetComponentInParent<TerritoryObject>();
                if (to != null)
                {
                    //Debug.Log("touch territory");
                    if (isValidClick)
                        to.OnClick();
                }
                else if (raycastHit.collider.CompareTag("mapAlpha"))
                {
                    if (isValidClick)
                    {
                        if ((GameController.gameState.phase == 1 && !GameController.Instance.isMovingAttack) || GameController.gameState.phase == 2)
                        {
                            GameController.Instance.territoryObjects.ForEach((toTemp) =>
                            {
                                toTemp.Lower();
                                //toTemp.SetBorderColor(Color.black);
                                toTemp.RefreshTerritory();
                            });
                            if (footer.sliderActive)
                            {
                                footer.panelSlider.DOAnchorPosY(-612, 0.5f);
                                footer.rectFooter.DOAnchorPosY(-25, 0.5f);

                            }

                            mapController.mapAlpha.GetComponent<BoxCollider>().enabled = false;
                            mapController.mapAlpha.DOFade(0f, 0.6f);
                            GameController.Instance.toOrigin = null;
                            GameController.Instance.toTarget = null;

                            if (GameController.gameState.phase == 1)
                                footer.tipText.text = "Selecione o seu território que vai partir o ataque";
                            else if (GameController.gameState.phase == 2)
                                footer.tipText.text = "Selecione o território para transferir os exércitos";

                        }
                    }
                }
                //else if (raycastHit.collider.CompareTag("Sea"))
                //{
                //    if (GameController.gameState.phase == 0)
                //    {
                //        GameController.Instance.CleanMap();
                //        if (footer.sliderActive)
                //        {
                //            footer.panelSlider.DOAnchorPosY(-612, 0.5f);
                //            footer.rectFooter.DOAnchorPosY(-25, 0.5f);

                //        }
                //    }
                //}
            }
        }

        public void RaycastClickSea()
        {
            if (!canClickMap || UIBlocker.uiBlocked || isGameOver)
                return;

            Ray raycast;
            if (Input.touchSupported && Application.platform != RuntimePlatform.WebGLPlayer)
            {
                raycast = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            }
            else
            {
                raycast = Camera.main.ScreenPointToRay(Input.mousePosition);
            }

            RaycastHit raycastHit;
            if (Physics.Raycast(raycast, out raycastHit))
            {
                if (raycastHit.collider.CompareTag("Sea"))
                {
                    if (GameController.gameState.phase == 0)
                    {
                        GameController.Instance.CleanMap();
                        if (footer.sliderActive)
                        {
                            footer.HideSlider();
                            //footer.panelSlider.DOAnchorPosY(-612, 0.5f);
                            //footer.rectFooter.DOAnchorPosY(-25, 0.5f);
                        }
                    }
                }
            }
        }

        public void Focus(TerritoryObject to1, TerritoryObject to2)
        {
            canClickMap = false;
            var centerPosition = to1.transform.position + ((to2.transform.position - to1.transform.position) * (Vector3.Distance(to1.centerPosition.position, to2.centerPosition.position) / 2));

        }

        public TerritoryObject to1;
        public TerritoryObject to2;
        [ContextMenu("DebugFocus")]
        public void DebugFocus()
        {
            canClickMap = false;
            //var to1 = GameObject.Find("Brasil").GetComponent<TerritoryObject>();
            //var to2 = GameObject.Find("Argentina").GetComponent<TerritoryObject>();
            var vec1 = to1.centerPosition.transform.position;
            var vec2 = to2.centerPosition.transform.position;
            var centerPosition = to1.centerPosition.transform.position + (to2.centerPosition.transform.position - to1.centerPosition.transform.position) / 2;

            var camPlanePos1 = vec1 + (Vector3.Cross(vec1 - vec2, Vector3.up) * Vector3.Distance(vec1, vec2) / 3f);
            var camPlanePos2 = vec2 + (Vector3.Cross(vec2 - vec1, Vector3.up) * Vector3.Distance(vec1, vec2) / 3f);
            Vector3 cross;
            cross = camPlanePos1.z > camPlanePos2.z ? camPlanePos2 : camPlanePos1;
            transform.DOLookAt(centerPosition, 0.8f).SetEase(Ease.InOutQuad).OnComplete(() =>
            {
                transform.DOMove(new Vector3(cross.x, 10, cross.z), 0.8f).OnComplete(() => transform.DOLookAt(centerPosition, 0.4f)).SetEase(Ease.InOutQuad);
                cam.DOFieldOfView(30, 0.8f).SetEase(Ease.InOutQuad);
                StartCoroutine(FocusRoutine(centerPosition));
            });


        }

        //public void Focus(TerritoryObject to1, TerritoryObject to2)
        //{
        //    canClickMap = false;
        //    //var to1 = GameObject.Find("Brasil").GetComponent<TerritoryObject>();
        //    //var to2 = GameObject.Find("Argentina").GetComponent<TerritoryObject>();
        //    var vec1 = to1.centerPosition.transform.position;
        //    var vec2 = to2.centerPosition.transform.position;
        //    var centerPosition = to1.centerPosition.transform.position + (to2.centerPosition.transform.position - to1.centerPosition.transform.position) / 2;

        //    var camPlanePos1 = vec1 + (Vector3.Cross(vec1 - vec2, Vector3.up) * Vector3.Distance(vec1, vec2) / 1.4f);
        //    var camPlanePos2 = vec2 + (Vector3.Cross(vec2 - vec1, Vector3.up) * Vector3.Distance(vec1, vec2) / 1.4f);

        //    Vector3 cross;
        //    cross = camPlanePos1.z > camPlanePos2.z ? camPlanePos2 : camPlanePos1;
        //    transform.DOLookAt(centerPosition, 0.8f).SetEase(Ease.InOutQuad).OnComplete(() =>
        //    {
        //        transform.DOMove(new Vector3(cross.x, 10, cross.z), 0.8f).OnComplete(() => transform.DOLookAt(centerPosition, 0.4f)).SetEase(Ease.InOutQuad);
        //        cam.DOFieldOfView(30, 0.8f).SetEase(Ease.InOutQuad);
        //        StartCoroutine(FocusRoutine(centerPosition));
        //    });


        //}


        IEnumerator FocusRoutine(Vector3 target)
        {
            var startTime = Time.time;
            while (Time.time - startTime < 0.8f)
            {
                transform.LookAt(target);
                yield return new WaitForEndOfFrame();
            }
        }

        //[ContextMenu("ResetCamera")]
        //public void ResetCamera()
        //{
        //    transform.DOMove(new Vector3(0, 28.78f, 0), 0.8f).SetEase(Ease.InOutQuad);
        //    transform.DORotate(new Vector3(70, 0, 0), 0.8f).SetEase(Ease.InOutQuad);
        //    cam.DOFieldOfView(80, 0.8f).OnComplete(() => canClickMap = true).SetEase(Ease.InOutQuad);
        //}

        [ExecuteInEditMode]
        public void OnDrawGizmos()
        {
            var to1 = GameObject.Find("Brasil").GetComponent<TerritoryObject>();
            var to2 = GameObject.Find("Argentina").GetComponent<TerritoryObject>();
            var vec1 = to1.centerPosition.transform.position;
            var vec2 = to2.centerPosition.transform.position;
            if (to1.centerPosition.position.z > to2.centerPosition.position.z)
            {
                var aux = vec1;
                vec1 = vec2;
                vec2 = aux;
            }
            var centerPosition = to1.centerPosition.transform.position + (to2.centerPosition.transform.position - to1.centerPosition.transform.position) / 2;
            var cp = centerPosition;

            var camPlanePos = vec1 + (Vector3.Cross(vec1 - vec2, Vector3.up) * Vector3.Distance(vec1, vec2) / 2);

            Gizmos.color = Color.yellow;
            Gizmos.DrawCube(cp, Vector3.one * 0.5f);

            Gizmos.color = Color.blue;
            Gizmos.DrawCube(camPlanePos, Vector3.one * 0.5f);
        }


    }
}