﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using DG.Tweening;

namespace Grow.War
{
    public class PlayerSnippet : MonoBehaviour
    {
        public Text nome;
        public Image avatar;
        public GameObject avatarMask;
        public Image soldier;
        //public Image playerColor;
        public Text territories;
        public Text troops;
        public Text cards;
        public int growId;
        public RectTransform rect;
        public Image modeIcon;
        public Text labelYou;
        public Image bgLabelYou;

        [Header("BG Sprites")]
        public Sprite bgYellow;
        public Sprite bgBlue;
        public Sprite bgWhite;
        public Sprite bgBlack;
        public Sprite bgGreen;
        public Sprite bgRed;

        [Header("Mode Sprites")]
        public Sprite onlineMode;
        public Sprite cpuMode;
        public Sprite friendsMode;

        public Image bgSnippet;
        public Image bgAvatar;
        public GrowColor playerColor;

        public List<Sprite> patents;
        public Image patentImage;
        public Image disconnectedImage;

        public Image deathFade;

        public void SetPlayer(int _playerIdGrow)
        {
            growId = _playerIdGrow;

            bgLabelYou.gameObject.SetActive(false);
            playerColor = GameController.gameState.GetPlayer(growId).warColor;
            bgAvatar.color = GameController.gameState.GetPlayer(growId).warColor.color;
            cards.color = GameController.gameState.GetPlayer(growId).warColor.color;
            if (playerColor == WarColor.YELLOW)
            {
                bgSnippet.sprite = bgYellow;
                bgLabelYou.color = new Color32(58, 54, 5, 255);
            }
            else if (playerColor == WarColor.BLUE)
            {
                bgSnippet.sprite = bgBlue;
                bgLabelYou.color = new Color32(5, 52, 58, 255);
            }
            else if (playerColor == WarColor.WHITE)
            {
                bgSnippet.sprite = bgWhite;
                bgLabelYou.color = new Color32(26, 5, 58, 255);
            }
            else if (playerColor == WarColor.BLACK)
            {
                bgSnippet.sprite = bgBlack;
                bgLabelYou.color = new Color32(12, 13, 12, 255);
            }
            else if (playerColor == WarColor.GREEN)
            {
                bgSnippet.sprite = bgGreen;
                bgLabelYou.color = new Color32(0, 37, 0, 255);
            }
            else
            {
                bgSnippet.sprite = bgRed;
                bgLabelYou.color = new Color32(58, 5, 11, 255);
            }
            if (GameController.Instance.SERVER.IsOnline())
            {
                // Patente
                patentImage.sprite = patents[GameController.gameState.GetPlayer(growId).patent - 1];
            }
            else
            {
                patentImage.gameObject.SetActive(false);
            }
            UpdatePlayer();
            UpdatePlayerDead();

            if (!GameController.Instance.SERVER.IsOnline())
                Destroy(bgAvatar.GetComponent<CursorListenerHelper>());
        }

        public void UpdatePlayer()
        {

            if (GameController.gameState.GetPlayer(growId) == GameController.gameState.currentPlayer)
            {
                rect.DOAnchorPosY(-358, 0.5f);
            }
            else
            {
                rect.DOAnchorPosY(-458, 0.5f);
            }
            var playerTerritories = GameController.gameState.GetPlayer(growId).GetTerritories(GameController.gameState.territoryStates);
            nome.text = GameController.gameState.GetPlayer(growId).GetName;
            if (GameController.Instance.SERVER.IsOnline())
            {
                disconnectedImage.gameObject.SetActive(GameController.gameState.GetPlayer(growId).isDisconnected);
                avatar.sprite = GameController.gameState.GetPlayer(growId).avatar;
            }
            else
            {
                bgAvatar.GetComponent<Button>().interactable = false;
                avatarMask.SetActive(false);
                soldier.color = playerColor.color;
            }
            //playerColor.color = player.warColor.color;
            territories.text = "Territórios: " + playerTerritories.Count.ToString();
            troops.text = "Tropas: " + playerTerritories.Sum(ts => ts.amountArmy);
            cards.text = GameController.gameState.GetPlayer(growId).cardCount.ToString();


            if (GameController.Instance.SERVER.IsOnline())
            {
                //Debug.Log("SFS.Friends" + SFS.Friends);

                SFS.Friends.UpdateList();


                if (SFS.Friends.BuddyList.Exists(f =>
                {
                    try
                    {
                        //Debug.Log(f.Name);
                        return (int)FriendsSFS.GetVar(f, "$idGrow") == growId;
                    }
                    catch (System.Exception e)
                    {
                        Debug.Log(e);
                        return false;
                    }
                }))
                    modeIcon.sprite = friendsMode;
                else
                    modeIcon.sprite = onlineMode;
            }
            else
            {
                if (GameController.gameState.GetPlayer(growId).isHuman)
                    modeIcon.sprite = friendsMode;
                else
                    modeIcon.sprite = cpuMode;
            }
            modeIcon.SetNativeSize();

            if (!GameController.gameState.thisPlayer.isHuman)
                return;
            if (growId == GameController.gameState.thisPlayer.idGrow)
            {
                bgLabelYou.gameObject.SetActive(true);
                modeIcon.gameObject.SetActive(false);
            }
            else
            {
                bgLabelYou.gameObject.SetActive(false);
                modeIcon.gameObject.SetActive(true);
            }

        }

        public void UpdatePlayerDead()
        {
            if (GameController.gameState.GetPlayer(growId).isEliminated)
            {
                deathFade.gameObject.SetActive(true);
            }
        }

        public void OnClickAvatar()
        {
            ServerManager.instance.OpenNewProfilePopup(growId);

        }
    }
}
