﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class ToolTipHelper : MonoBehaviour, IPointerClickHandler
{

    [TextArea(4, 10)]
    public string message;
    public float duration = 3f;
    public TipActivation tooltipType;
    private RectTransform rect;

    public enum TipActivation
    {
        WhenButtonIsInactive,
        WhenButtonIsActive
    }

    private void Start()
    {
        rect = GetComponent<RectTransform>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        OpenTooltip();
    }

    public void OpenTooltip()
    {
        // TODO: Tratar cliques repetidos
        var b = GetComponent<Button>();

        switch (tooltipType)
        {
            case TipActivation.WhenButtonIsInactive:
                if (b != null && !b.interactable)
                    Tooltip.CreateTooltip(message, rect, duration);
                break;
            case TipActivation.WhenButtonIsActive:
                if (b != null && b.interactable)
                    Tooltip.CreateTooltip(message, rect, duration);
                break;
            default:
                break;
        }
    }
}
