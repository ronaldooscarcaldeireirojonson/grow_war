﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.EventSystems;


public class Tooltip : MonoBehaviour
{


    public const string RESOURCE_PATH = "TooltipPrefab";

    public Text tooltipText;
    public bool isAvailable = true;
    public static List<Tooltip> pool = new List<Tooltip>();
    public static int poolSize = 2;

    [SerializeField]
    private RectTransform rect;
    [SerializeField]
    private CanvasGroup canvasGroup;

    private void Awake()
    {
        canvasGroup.alpha = 0;
    }

    private void ShowTooltip(float duration)
    {
        isAvailable = false;
        DOTween.Kill(canvasGroup);
        canvasGroup.alpha = 0;
        canvasGroup.DOFade(1, 0.5f).SetDelay(0.2f).OnComplete(() =>
        {
            canvasGroup.DOFade(0, 0.5f).SetDelay(duration).OnComplete(() => isAvailable = true);
        });
    }

    public void SetupTooltip(string _message, RectTransform anchorRect, Vector2 offset, float duration = 3f)
    {
        tooltipText.text = _message;
        var root = GameObject.Find("_TooltipRoot").transform;
        rect.SetParent(root, false);
        rect.SetAsLastSibling();
        rect.localScale = Vector2.one;
        rect.position = anchorRect.position;
        ShowTooltip(duration);
    }
    public void SetupTooltip(string _message, RectTransform anchorRect, float duration = 3f)
    {
        SetupTooltip(_message, anchorRect, Vector2.zero);
    }

    public static Tooltip CreateTooltip(string tooltipMessage, RectTransform anchorRect, Vector2 offset, float duration = 3f)
    {
        for (int i = 0; i < pool.Count; i++)
        {
            if (pool[i].isAvailable)
            {
                pool[i].SetupTooltip(tooltipMessage, anchorRect, offset);
                return pool[i];
            }
        }

        if (pool.Count < Tooltip.poolSize)
        {
            Tooltip t = Instantiate(Resources.Load<Tooltip>(RESOURCE_PATH));
            t.SetupTooltip(tooltipMessage, anchorRect, offset);
            pool.Add(t);
            return t;
        }
        else
        {
            Tooltip temp = pool[0].rect.parent.GetChild(0).GetComponent<Tooltip>();
            temp.SetupTooltip(tooltipMessage, anchorRect, offset);
            return temp;
        }

    }
    public static Tooltip CreateTooltip(string tooltipMessage, RectTransform anchorRect, float duration = 3f)
    {
        return CreateTooltip(tooltipMessage, anchorRect, Vector2.zero);
    }

}