﻿using Grow.War;
using UnityEngine;
using UnityEngine.UI;

public class SelectPlayerSnippet : MonoBehaviour
{
    public ColorType playerColor;
    [HideInInspector]
    public InputField playerName;
    public int playerType;
    public Grow.GrowColor warColor;

    private PlayerSnippetScrollSnap scrollSnap;
    public SelectPlayersController selectPlayersController;

    public enum ColorType
    {
        YELLOW,
        BLUE,
        WHITE,
        BLACK,
        GREEN,
        RED
    }


    // Use this for initialization
    void Start()
    {
        playerName = gameObject.GetComponentInChildren<InputField>();
        scrollSnap = gameObject.GetComponentInChildren<PlayerSnippetScrollSnap>();
        scrollSnap.onEndLerp.AddListener(OnEndLerp);
        OnEndLerp(playerType);
    }

    private void OnDestroy()
    {
        scrollSnap.onEndLerp.RemoveListener(OnEndLerp);
    }

    public void OnEndLerp(int _playerType)
    {
        string playerString = "";
        string playerColorString = "";
        playerType = _playerType;
        switch (_playerType)
        {
            case 0:
                playerString = "JOGADOR";
                break;
            case 1:
                playerString = "CPU";
                break;
            case 2:
                playerString = "DESATIVADO";
                break;

            default:
                break;
        }

        switch (playerColor)
        {
            case ColorType.YELLOW:
                playerColorString = " AMARELO";
                warColor = WarColor.YELLOW;
                break;

            case ColorType.BLUE:
                playerColorString = " AZUL";
                warColor = WarColor.BLUE;
                break;

            case ColorType.WHITE:
                playerColorString = " ROXO";
                warColor = WarColor.WHITE;
                break;

            case ColorType.BLACK:
                playerColorString = " CINZA";
                warColor = WarColor.BLACK;
                break;

            case ColorType.GREEN:
                playerColorString = " VERDE";
                warColor = WarColor.GREEN;
                break;

            case ColorType.RED:
                playerColorString = " VERMELHO";
                warColor = WarColor.RED;
                break;

            default:
                break;
        }

        playerName.text = playerString + (playerType != 2 ? playerColorString : "");
        selectPlayersController.UpdateButtonStart();
    }
}
