﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class CircleAlphaAlert : MonoBehaviour
{

    public Color32 oddColor;
    public Color32 evenColor;
    public float spinSpeed = 0.5f;
    public float showSpeed = 0.8f;
    public Vector3 size;
    public List<Image> fuses;

    private CanvasGroup canvasGroup;


    private void Start()
    {
        transform.localScale = Vector3.zero;
        canvasGroup = gameObject.AddComponent<CanvasGroup>();
    }

    public void OnEnable()
    {
        DOTween.Kill(transform);
        transform.rotation = Quaternion.identity;
        transform.DORotate(new Vector3(0, 0, 36), spinSpeed).SetLoops(-1).SetEase(Ease.Linear);
    }

    [ContextMenu("Show")]
    public void Show()
    {
        transform.DOScale(size, showSpeed);
        canvasGroup.DOFade(1, showSpeed);
    }

    [ContextMenu("Hide")]
    public void Hide()
    {
        transform.DOScale(Vector3.zero, showSpeed);
        canvasGroup.DOFade(0, showSpeed);
    }

    [ContextMenu("SetColors")]
    public void SetColors()
    {
        for (int i = 0; i < fuses.Count; i++)
        {
            fuses[i].color = (i % 2 == 0) ? evenColor : oddColor;
        }
    }
}
