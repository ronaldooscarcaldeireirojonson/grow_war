﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Grow.War;
using Grow;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class SaveManager : MonoBehaviour
{
    public bool hasSave;

    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);

        hasSave = false;

        if (PlayerPrefs.HasKey("save"))
        {
            hasSave = true;
            Debug.Log("Save file detected");
        }
    }


    public void DecodeSave()
    {
        Debug.Log("Start Loading");
        Save savedState = new Save();

        byte[] bytes = System.Convert.FromBase64String(PlayerPrefs.GetString("save"));

        BinaryFormatter formatter = new BinaryFormatter();

        MemoryStream ms = new MemoryStream(bytes);

        object obj = formatter.Deserialize(ms);
        savedState = ((Save)obj);


        SceneManager.instance.StartCoroutine(LoadGame(savedState));
        SceneManager.instance.ChangeScene("Game");
    }
    IEnumerator LoadGame(Save save)
    {
        yield return new WaitUntil(() => GameController.Instance);
        GameController.Instance.SERVER = new LocalServer(save);
    }

}
