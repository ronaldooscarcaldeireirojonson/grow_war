﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using DG.Tweening;

namespace Grow.War
{
    public class MapController : MonoBehaviour
    {

        public SpriteRenderer mapAlpha;

        // Use this for initialization
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {

        }

        public List<TerritoryObject> GetContinentTerritories(TerritoryObject to)
        {
            return GameController.Instance.territoryObjects.Where((t) => t.territoryState.territory.continent.id == to.territoryState.territory.continent.id).ToList();
        }

        public List<TerritoryObject> GetContinentTerritories(int continentId)
        {
            return GameController.Instance.territoryObjects.Where((t) => t.territoryState.territory.continent.id == continentId).ToList();
        }

        public IEnumerator RaiseContinent(int continentId)
        {
            mapAlpha.GetComponent<BoxCollider>().enabled = true;
            var tos = GetContinentTerritories(continentId);
            foreach (TerritoryObject to in tos)
            {
                to.Raise(false);
                //Debug.Log("to nam" + to.name);
                yield return new WaitForSeconds(0.05f);
            }


            mapAlpha.DOFade(0.5f, 0.3f);
        }

        public IEnumerator LowerContinent(int continentId)
        {
            //Debug.Log("lowerContinent");
            mapAlpha.GetComponent<BoxCollider>().enabled = false;
            var tos = GetContinentTerritories(continentId);
            mapAlpha.DOFade(0f, 0.3f);
            foreach (TerritoryObject to in tos)
            {
                to.Lower();
                yield return new WaitForSeconds(0.05f);
            }



        }
    }
}
