﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Save
{
    public int round;
    public int tradeInCount;
    public string[] playerNames;
    public bool[] isCPU;
    public int[] playerColor;
    public bool[] isEliminated;
    public int[] playerCardCount;

    public int[] troopsInTerritory;
    public int[] territoryPossession;
    public int[] territoryId;

    public int thisTurn;

    public int[] playerObjectives;
    public int[,] playerCards;

    public int[] deck;
}
