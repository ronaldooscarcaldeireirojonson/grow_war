﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShadeOnAwake : MonoBehaviour 
{
    public Animation shade;
    public AnimationClip shadeAnim;

    private void OnEnable()
    {
        shade.AddClip(shadeAnim, shadeAnim.name);
        shade.Play(shadeAnim.name);
    }

    
	
}
