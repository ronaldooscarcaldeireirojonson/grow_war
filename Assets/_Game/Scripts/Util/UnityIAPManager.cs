﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

public class UnityIAPManager : MonoBehaviour, IStoreListener
{
    private static IStoreController storeController;          // The Unity Purchasing system.
    private static IExtensionProvider storeExtensionProvider; // The store-specific Purchasing subsystems.

    public static string noAds = "br.com.grow.war.noads";

    public static string kProductIDSubscription1year = "grow_subscription_1year";
    private static string kProductNameAppleSubscription1year = "grow_subscription_1year";
    private static string kProductNameGooglePlaySubscription1year = "grow_subscription_1year";
    public static string kProductIDSubscription3month = "grow_subscription_3month";
    private static string kProductNameAppleSubscription3month = "grow_subscription_3month";
    private static string kProductNameGooglePlaySubscription3month = "grow_subscription_3month";
    public static string kProductIDSubscriptionMonth = "grow_subscription_1month";
    private static string kProductNameAppleSubscriptionMonth = "grow_subscription_1month";
    private static string kProductNameGooglePlaySubscriptionMonth = "grow_subscription_1month";

    public static UnityIAPManager Instance { get; private set; }

    void Start()
    {
        if (Instance == null && storeController == null)
        {
            Instance = this;
            InitializePurchasing();
            DontDestroyOnLoad(this.gameObject);
        }
        //else
        //{
        //    Destroy(this.gameObject);
        //}
    }

    public void InitializePurchasing()
    {
        if (IsInitialized())
        {
            return;
        }

        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        builder.AddProduct(noAds, ProductType.NonConsumable);
        builder.AddProduct(kProductIDSubscription1year, ProductType.Subscription, new IDs(){
                { kProductNameAppleSubscription1year, AppleAppStore.Name },
                { kProductNameGooglePlaySubscription1year, GooglePlay.Name },
            });
        builder.AddProduct(kProductIDSubscription3month, ProductType.Subscription, new IDs(){
                { kProductNameAppleSubscription3month, AppleAppStore.Name },
                { kProductNameGooglePlaySubscription3month, GooglePlay.Name },
            });
        builder.AddProduct(kProductIDSubscriptionMonth, ProductType.Subscription, new IDs(){
                { kProductNameAppleSubscriptionMonth, AppleAppStore.Name },
                { kProductNameGooglePlaySubscriptionMonth, GooglePlay.Name },
            });

        UnityPurchasing.Initialize(this, builder);
    }


    private bool IsInitialized()
    {
        return storeController != null && storeExtensionProvider != null;
    }


    public void BuyNoAdsConsumable()
    {
        BuyProductID(noAds);
    }

    public void BuySubscription()
    {
        BuyProductID(kProductIDSubscription3month);
    }
    public void BuySubscription(int typeOfSubscription)
    {
        switch (typeOfSubscription)
        {
            case 0:
                BuyProductID(kProductIDSubscriptionMonth);
                break;
            case 1:
                BuyProductID(kProductIDSubscription3month);
                break;
            case 2:
                BuyProductID(kProductIDSubscription1year);
                break;
            default:
                BuyProductID(kProductIDSubscription1year);
                break;
        }

    }


    void BuyProductID(string productId)
    {
        if (IsInitialized())
        {
            Product product = storeController.products.WithID(productId);

            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));

                storeController.InitiatePurchase(product);
            }
            else
            {
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        else
        {
            Debug.Log("BuyProductID FAIL. Not initialized.");
        }
    }


    public void RestorePurchases()
    {
        if (!IsInitialized())
        {
            Debug.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }

        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer)
        {
            Debug.Log("RestorePurchases started ...");

            var apple = storeExtensionProvider.GetExtension<IAppleExtensions>();

            apple.RestoreTransactions((result) =>
            {
                Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
            });
        }
        else
        {
            Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }
    }


    //  
    // --- IStoreListener
    //

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        Debug.Log("OnInitialized: PASS");

        storeController = controller;
        storeExtensionProvider = extensions;
    }


    public void OnInitializeFailed(InitializationFailureReason error)
    {
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }


    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {

        if (String.Equals(args.purchasedProduct.definition.id, noAds, StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            // TODO: The non-consumable item has been successfully purchased, grant this item to the player.
            AdMobManager.Instance.SetNoAdsForever();
        }
        else if (String.Equals(args.purchasedProduct.definition.id, kProductIDSubscriptionMonth, StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            if (args.purchasedProduct.hasReceipt)
            {
                Debug.Log(args.purchasedProduct.receipt);

            }
            Debug.Log(args.purchasedProduct.transactionID);
            // TODO: The subscription item has been successfully purchased, grant this to the player.
        }
        else if (String.Equals(args.purchasedProduct.definition.id, kProductIDSubscription3month, StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            if (args.purchasedProduct.hasReceipt)
            {
                Debug.Log(args.purchasedProduct.receipt);
            }
            Debug.Log(args.purchasedProduct.transactionID);

            // TODO: The subscription item has been successfully purchased, grant this to the player.
        }
        else if (String.Equals(args.purchasedProduct.definition.id, kProductIDSubscription1year, StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            if (args.purchasedProduct.hasReceipt)
            {
                Debug.Log(args.purchasedProduct.receipt);
            }
            Debug.Log(args.purchasedProduct.transactionID);
            // TODO: The subscription item has been successfully purchased, grant this to the player.
        }

        else
        {
            Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
        }

        return PurchaseProcessingResult.Complete;
    }


    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }
}