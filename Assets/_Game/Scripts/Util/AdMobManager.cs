﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
using System;
using System.Globalization;
using UnityEngine.SceneManagement;

public class AdMobManager : MonoBehaviour
{
    public bool isPremium = false;
    public bool noAds = false;
    BannerView bannerView;
    InterstitialAd offlineAd;
    RewardBasedVideoAd rewardBasedVideo;
    public static AdMobManager Instance { get; private set; }
    public void Awake()
    {
        if(Instance == null){
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }else{
            Destroy(this.gameObject);
        }
        if (PlayerPrefs.HasKey("premium")){
            DateTime lastLogin = DateTime.FromBinary(BitConverter.ToInt64(Convert.FromBase64String(PlayerPrefs.GetString("premium")), 0));
            DateTime expireDate = lastLogin.AddDays(30);
            if(DateTime.UtcNow > expireDate)
            {
                isPremium = false;
                PlayerPrefs.DeleteKey("premium");
            }
            else
            {
                isPremium = true;
            }
        }

        if (PlayerPrefs.HasKey("purchasedNoAds"))
            noAds = true;


#if UNITY_ANDROID
        string appId = "ca-app-pub-7824191759896978~3325051708";
#elif UNITY_IPHONE
        string appId = "ca-app-pub-7824191759896978~3256359822";
#else
            string appId = "unexpected_platform";
#endif


        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(appId);
        RequestInterstitialOffline();
        rewardBasedVideo = RewardBasedVideoAd.Instance;
        Banner();

        if (!isPremium)
            HideBanner();

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

    }
    private void RequestInterstitialOffline()
    {
#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-2349488947919676/5709721632";
#elif UNITY_IPHONE
        string adUnitId = "ca-app-pub-2349488947919676/1191343384";
#else
        string adUnitId = "unexpected_platform";
#endif

        // Initialize an InterstitialAd.
        offlineAd = new InterstitialAd(adUnitId);
        // Called when an ad request has successfully loaded.
        offlineAd.OnAdLoaded += HandleOnAdLoaded;
        // Called when an ad request failed to load.
        offlineAd.OnAdFailedToLoad += HandleOnAdFailedToLoad;
        // Called when an ad is shown.
        offlineAd.OnAdOpening += HandleOnAdOpened;
        // Called when the ad is closed.
        offlineAd.OnAdClosed += HandleOnAdClosed;
        // Called when the ad click caused the user to leave the application.
        offlineAd.OnAdLeavingApplication += HandleOnAdLeavingApplication;
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        offlineAd.LoadAd(request);
    }

    private void RequestBanner()
    {
#if UNITY_ANDROID
            string adUnitId = "ca-app-pub-2349488947919676/5322160082";
#elif UNITY_IPHONE
        string adUnitId = "ca-app-pub-2349488947919676/2021758068";
#else
            string adUnitId = "unexpected_platform";
#endif
        if (bannerView != null)
        {
            return;
        }
        // Create a 320x50 banner at the top of the screen.
        bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);

        // Called when an ad request has successfully loaded.
        bannerView.OnAdLoaded += HandleOnAdLoaded;
        // Called when an ad request failed to load.
        bannerView.OnAdFailedToLoad += HandleOnAdFailedToLoad;
        // Called when an ad is clicked.
        bannerView.OnAdOpening += HandleOnAdOpened;
        // Called when the user returned from the app after an ad click.
        bannerView.OnAdClosed += HandleOnAdClosed;
        // Called when the ad click caused the user to leave the application.
        bannerView.OnAdLeavingApplication += HandleOnAdLeavingApplication;
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        
        // Load the banner with the request.
        bannerView.LoadAd(request);

        Debug.Log("Banner Request");


    }
    private void RequestRewardBasedVideo()
    {
#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-3940256099942544/5224354917";
#elif UNITY_IPHONE
            string adUnitId = "ca-app-pub-3940256099942544/1712485313";
#else
            string adUnitId = "unexpected_platform";
#endif
        // Called when an ad request has successfully loaded.
        rewardBasedVideo.OnAdLoaded += HandleRewardBasedVideoLoaded;
        // Called when an ad request failed to load.
        rewardBasedVideo.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
        // Called when an ad is shown.
        rewardBasedVideo.OnAdOpening += HandleRewardBasedVideoOpened;
        // Called when the ad starts to play.
        rewardBasedVideo.OnAdStarted += HandleRewardBasedVideoStarted;
        // Called when the user should be rewarded for watching a video.
        rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
        // Called when the ad is closed.
        rewardBasedVideo.OnAdClosed += HandleRewardBasedVideoClosed;
        // Called when the ad click caused the user to leave the application.
        rewardBasedVideo.OnAdLeavingApplication += HandleRewardBasedVideoLeftApplication;
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the rewarded video ad with the request.
        this.rewardBasedVideo.LoadAd(request, adUnitId);
    }





    public void HandleOnAdLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdLoaded event received");
    }

    public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print("HandleFailedToReceiveAd event received with message: "
                            + args.Message);
    }

    public void HandleOnAdOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdOpened event received");
    }

    public void HandleOnAdClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdClosed event received");
    }

    public void HandleOnAdLeavingApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdLeavingApplication event received");
    }
    public void HandleRewardBasedVideoLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoLoaded event received");
    }

    public void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print(
            "HandleRewardBasedVideoFailedToLoad event received with message: "
                             + args.Message);
    }

    public void HandleRewardBasedVideoOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoOpened event received");
    }

    public void HandleRewardBasedVideoStarted(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoStarted event received");
    }

    public void HandleRewardBasedVideoClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoClosed event received");
    }

    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        string type = args.Type;
        double amount = args.Amount;
        MonoBehaviour.print(
            "HandleRewardBasedVideoRewarded event received for "
                        + amount.ToString() + " " + type);
    }

    public void HandleRewardBasedVideoLeftApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoLeftApplication event received");
    }


    public void Banner(){
        if (!isPremium)
            RequestBanner();
    }
    public void HideBanner()
    {
        if (!isPremium){
            Debug.Log("Banner Hide");
            bannerView.Hide();

        }
    }
    public void RevealBanner()
    {
        if(!isPremium)
        bannerView.Show();
    }
    public void OfflineInterstitial()
    {
        if (offlineAd.IsLoaded() && !isPremium && !noAds)
        {
            offlineAd.Show();
        }
    }

    public void RewardVideo()
    {
        if(!isPremium)
        RequestRewardBasedVideo();
    }

    public void SetPremium(bool premium)
    {
        if(premium)
        {
            DateTime utcDate = DateTime.UtcNow;
            string toSave = Convert.ToBase64String(BitConverter.GetBytes(utcDate.ToBinary()));
            PlayerPrefs.SetString("premium", toSave);
            isPremium = true;

            if (bannerView != null) { bannerView.Hide(); }
        }
        else
        {
            PlayerPrefs.DeleteKey("premium");
            isPremium = false;
            if (bannerView == null)
            {
                Banner();
                HideBanner();
            }
        }
    }
    public void SetNoAdsForever()
    {
        DateTime utcDate = DateTime.UtcNow;
        string toSave = Convert.ToBase64String(BitConverter.GetBytes(utcDate.ToBinary()));
        PlayerPrefs.SetString("premium", toSave);
        PlayerPrefs.SetInt("purchasedNoAds", 1);
        isPremium = true;

        if (bannerView != null) { bannerView.Hide(); }
    }

}