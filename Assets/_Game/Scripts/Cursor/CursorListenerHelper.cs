﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CursorListenerHelper : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    public void OnPointerEnter(PointerEventData eventData)
    {
#if UNITY_WEBGL || UNITY_EDITOR
        CursorManager.SetHover(true);
#endif
    }

    public void OnPointerExit(PointerEventData eventData)
    {
#if UNITY_WEBGL || UNITY_EDITOR
        CursorManager.SetHover(false);
#endif
    }

}
