﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CursorManager : MonoBehaviour
{
    static CursorManager instance;

    public Vector2 offset;
    public Sprite cursor;
    public Sprite handCursor;
    Canvas canvas;
    public Image cursorImage;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public static void SetHover(bool isHover)
    {
        if (instance.cursorImage)
        {
            instance.cursorImage.sprite = isHover ? instance.handCursor : instance.cursor;
            instance.offset = isHover ? new Vector2(4, -12) : new Vector2(8, -10);
        }
    }

    private void Start()
    {
#if UNITY_WEBGL || UNITY_EDITOR
        cursorImage = GetComponentInChildren<Image>();
        canvas = GetComponent<Canvas>();
        cursorImage.sprite = cursor;
        Cursor.visible = false;
        SetHover(false);
#else
        Destroy(gameObject);
#endif
    }

    private void Update()
    {

#if UNITY_WEBGL || UNITY_EDITOR
        Vector2 viewport = Camera.main.ScreenToViewportPoint(Input.mousePosition);
        cursorImage.rectTransform.anchoredPosition = new Vector2((viewport.x * canvas.pixelRect.width) - (canvas.pixelRect.width / 2) + offset.x, (viewport.y * canvas.pixelRect.height) - (canvas.pixelRect.height / 2) + offset.y);
#endif
    }
}
