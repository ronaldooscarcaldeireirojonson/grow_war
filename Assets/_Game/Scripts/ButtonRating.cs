﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Grow.War
{
    public class ButtonRating : MonoBehaviour
    {

        public Image ratingImage;
        public DragRatingIcon dragRatingIcon;


        public void OnClickButtonRating()
        {
            dragRatingIcon.OnClickIcon(ratingImage, ratingImage.sprite);
        }

        public void OnUpButtonRating()
        {
            dragRatingIcon.OnClickUp();
        }
    }
}
