﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Grow.War
{
    public class PanelInfoCard : MonoBehaviour
    {
        private CanvasGroup cg;
        public bool isVisible;

        public void Start()
        {
            cg = GetComponent<CanvasGroup>();
            transform.localScale = Vector3.zero;
            cg.alpha = 0;
            gameObject.SetActive(false);
        }

        public void Show()
        {
            transform.DOScale(1f, 0.5f);
            cg.DOFade(1f, 0.3f);
            gameObject.SetActive(true);
            isVisible = true;
        }

        public void Hide()
        {
            Sioux.Audio.Play("Click");
            transform.DOScale(0f, 0.5f);
            cg.DOFade(0f, 0.3f).OnComplete(
                () =>
                {
                    isVisible = false;
                    gameObject.SetActive(false);
                });
        }
    }
}
