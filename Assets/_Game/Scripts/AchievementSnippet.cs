﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementSnippet : MonoBehaviour
{

    public Image bg;
    public Image icon;
    public Text title;
    public Text description;

    // Sprites dos achievements
    public Sprite achievementIconEliminationPB;
    public Sprite achievementIconGeneralPB;
    public Sprite achievementIconPatentPB;
    public Sprite achievementIconTerritoryPB;
    public Sprite achievementIconRankingPB;

    public Sprite achievementIconElimination;
    public Sprite achievementIconGeneral;
    public Sprite achievementIconPatent;
    public Sprite achievementIconTerritory;
    public Sprite achievementIconRanking;


    public void Configure(Achievement achievement)
    {
        title.text = achievement.Title;
        description.text = achievement.Description;

        if (achievement.Conquistou)
        {
            switch (achievement.IdConquistaTipo)
            {
                case 1:
                    icon.sprite = achievementIconGeneral;
                    break;

                case 2:
                    icon.sprite = achievementIconElimination;
                    break;

                case 3:
                    icon.sprite = achievementIconPatent;
                    break;

                case 4:
                    icon.sprite = achievementIconTerritory;
                    break;

                case 5:
                    icon.sprite = achievementIconRanking;
                    break;

                default:
                    icon.sprite = achievementIconGeneral;
                    break;
            }
        }
        else
        {
            switch (achievement.IdConquistaTipo)
            {
                case 1:
                    icon.sprite = achievementIconGeneralPB;
                    break;

                case 2:
                    icon.sprite = achievementIconEliminationPB;
                    break;

                case 3:
                    icon.sprite = achievementIconPatentPB;
                    break;

                case 4:
                    icon.sprite = achievementIconTerritoryPB;
                    break;

                case 5:
                    icon.sprite = achievementIconRankingPB;
                    break;

                default:
                    icon.sprite = achievementIconGeneralPB;
                    break;
            }
        }
    }
}
