﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
using UnityEngine.UI;
using Newtonsoft.Json;
using System;

namespace Grow.War
{
    public class EndGameController : MonoBehaviour
    {
        public static Dictionary<int, EndGamePlayerPackage> PlayerPackage = null;

        public CanvasGroup cg;
        public EndGameSnippet endGameSnippetPrefab;
        public Transform horizontalLayout;
        public Transform panelBottom;

        public List<EndGameSnippet> endGameSnippets = new List<EndGameSnippet>();

        public List<Transform> ratingButtons;
        public bool blockMap = false;

        public GameObject alphaObjective;
        public RectTransform panelObjective;
        public TextMeshProUGUI objectiveText;
        public Image objctiveBorderColor;

        public GameObject ratingButtonsGroup;
        public TextMeshProUGUI titleText;
        public GameObject logo;


        // Use this for initialization
        IEnumerator Start()
        {
            blockMap = true;

            if (!GameController.Instance.SERVER.IsOnline())
            {
                ratingButtonsGroup.SetActive(false);
                titleText.gameObject.SetActive(false);
            }
            else if (PlayerPackage == null)
            {
                ServerManager.instance.ShowWaitingAnim();
                yield return new WaitUntil(() => PlayerPackage != null);
                ServerManager.instance.HideWaitingAnim();
            }

            if (GameController.Instance.SERVER.IsOnline())
            {
                // Pega os achievements que não estão vistos
                ServerManager.instance.GetUnseenAchievements(
                    (response) =>
                    {
                        List<Achievement> achieves = JsonConvert.DeserializeObject<List<Achievement>>(response);

                        for (int i = 0; i < achieves.Count; i++)
                        {
                            AchievementManager.Show(achieves[i].Id);
                        }
                    });
            }

            // Analytics
            AnalyticsManager.Instance.SendDesignEvent("Tempo:Partida", ((Time.time - GameController.startGameTime) / 60));


            EndGamePlayerPackage pack;

            cg.DOFade(1, 0.2f);
            for (int i = 0; i < GameController.gameState.players.Count; i++)
            {
                var p = GameController.gameState.players[i];

                //end game online
                if (GameController.Instance.SERVER.IsOnline())
                {
                    pack = PlayerPackage[p.idGrow];

                    // se não sou eu instancia o card
                    if (GameController.gameState.thisPlayer != p)
                    {
                        var endGameSnippet = Instantiate(endGameSnippetPrefab, horizontalLayout);
                        endGameSnippet.Configure(p, pack.points, pack.rankingPoints, pack.rank, pack.objectiveId, pack.patent);
                        endGameSnippets.Add(endGameSnippet);

                    }
                    //se sou eu configura os dados no painel inferior
                    else
                    {
                        panelBottom.GetComponentInChildren<EndGameSnippet>().Configure(p, pack.points, pack.rankingPoints, pack.rank, pack.objectiveId, pack.patent, pack.lastUpdateTime);
                    }

                }
                //end game offline
                else
                {
                    //se não for o vencedor instancia o card
                    if (p != GameController.gameState.currentPlayer)
                    {
                        var endGameSnippet = Instantiate(endGameSnippetPrefab, horizontalLayout);
                        endGameSnippet.Configure(p);
                        endGameSnippets.Add(endGameSnippet);
                    }
                    //vencedor configura os dados no painel inferior
                    else
                    {
                        panelBottom.GetComponentInChildren<EndGameSnippet>().Configure(p);
                    }
                }
            }

            for (int i = 0; i < endGameSnippets.Count; i++)
            {
                endGameSnippets[i].AnimaCard();
                yield return new WaitForSeconds(0.1f);
            }

            panelBottom.DOLocalMoveY(-512, 0.2f).SetEase(Ease.OutQuad);
            yield return new WaitForSeconds(0.2f);

            if (!GameController.Instance.SERVER.IsOnline())
            {
                logo.SetActive(true);
                logo.transform.DOScale(1.3f, 0.1f).OnComplete(() => logo.transform.DOScale(1, 0.1f).SetEase(Ease.OutQuad));
            }

            for (int i = 0; i < ratingButtons.Count; i++)
            {
                ratingButtons[i].transform.DOScale(1.3f, 0.1f).OnComplete(() => ratingButtons[i].transform.DOScale(1, 0.1f).SetEase(Ease.OutQuad));
                yield return new WaitForSeconds(0.1f);
            }

            PlayerPackage = null;
        }

        public void ShowObjective(Player player)
        {
            Sioux.Audio.Play("Click");
            Sioux.Audio.Play(GameController.Instance.objectiveClip);
            objectiveText.text = "O objetivo de " + player.name + player.objective.text.Substring(12, Objective.LIST[player.objective.id].text.Length - 12);
            objctiveBorderColor.color = player.warColor.color;
            alphaObjective.SetActive(true);
            panelObjective.DOScale(1f, 0.5f);
            panelObjective.GetComponent<CanvasGroup>().DOFade(1f, 0.3f);
        }

        public void OnClickCloseObjective()
        {
            Sioux.Audio.Play("Click");
            panelObjective.DOScale(0f, 0.5f);
            panelObjective.GetComponent<CanvasGroup>().DOFade(0f, 0.3f).OnComplete(() =>
            {
                alphaObjective.SetActive(false);
            });
        }
    }

    public class EndGamePlayerPackage
    {
        public int id;
        public int idUsuario;
        public int points;
        public int totalPoints;
        public int rank;
        public int objectiveId;
        public int patent;
        public int rankingPoints;
        public string lastUpdateTime;

        public EndGamePlayerPackage(Sfs2X.Entities.Data.SFSObject data)
        {
            id = data.GetInt("id");
            idUsuario = data.GetInt("idUsuario");
            points = data.GetInt("pts");
            totalPoints = data.GetInt("totalpts");
            rank = data.GetInt("pos");
            objectiveId = data.GetInt("objectiveId");
            patent = data.GetInt("patente");
            rankingPoints = data.GetInt("rankingpts");
            lastUpdateTime = data.GetUtfString("lastUpdateTime");
        }
    }
}
