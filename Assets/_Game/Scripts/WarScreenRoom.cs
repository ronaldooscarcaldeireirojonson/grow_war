﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Grow.War
{

    public class WarScreenRoom : ScreenRoom
    {
        private float timer;
        private List<string> tips = new List<string>();
        public Text tipText;

        public override void Awake()
        {
            base.Awake();

            tips.Add("O painel de log na lateral esquerda mostra os resultados dos dados de ataque e defesa");
            tips.Add("Os quadrados coloridos no circulo do canto inferior esquerdo representam a quantidade de territórios de cada jogador");
            tips.Add("É possível conversar com os outros jogadores pelo chat no painel lateral esquerdo");
            tips.Add("A duração da rodada é representada pela barra inferior");
            tips.Add("A sua cor é mostrada no botão de objetivo. A cor do jogador atual é mostrada no painel central");
            tips.Add("Segundo o manual oficial do War, as trocas de cartas por exércitos não se referem às trocas do jogador mas sim às trocas do jogo");
            tips.Add("Segundo o manual oficial do War, na fase de deslocamento, cada território deve permanecer sempre pelo menos um exército(de ocupação) que nunca pode ser deslocado");
            tips.Add("No final da partida você consegue olhar os objetivos de seus adversários");
            if (Globals.GetBuildPlatform() != Plataforma.WEB)
            {
                tips.Add("Para ver o nome dos países utilize o zoom movendo dois dedos na tela (gesto de 'pinça')");
                tips.Add("Você pode ver as informações dos outros jogadores quando tocar no botão de binóculo no canto inferior esquerdo");
                tips.Add("Você pode mover o mapa tocando na tela e arrastando");
                tips.Add("Você pode fortificar rapidamente mantendo o toque pressionado sobre o território ou sobre o botão + no rodapé");
                tips.Add("Para fazer o zoom durante o jogo você pode tocar e mover dois dedos na tela (gesto de 'pinça')");
                tips.Add("Para ver seu objetivo, toque no alvo no canto inferior direito da tela");
            }
            else
            {
                tips.Add("Para ver o nome dos países utilize o zoom com o scroll do mouse ou os botões + e - do teclado");
                tips.Add("Você pode ver as informações dos outros jogadores quando clicar no botão de binóculo no canto inferior esquerdo");
                tips.Add("Você pode mover o mapa clicando na tela e arrastando, utilizando as teclas W,A,S,D ou as direcionais do teclado");
                tips.Add("Você pode fortificar rapidamente mantendo o mouse pressionado sobre o território ou sobre o botão + no rodapé");
                tips.Add("Para fazer o zoom durante o jogo você pode usar o scroll do mouse ou os botões + e - do teclado");
                tips.Add("Para ver seu objetivo, clique no alvo no canto inferior direito da tela");
            }

            int random = Random.Range(0, tips.Count);
            tipText.text = tips[random];
        }

        public override void Update()
        {
            base.Update();

            timer += Time.deltaTime;
            if (timer > 10)
            {
                int random = Random.Range(0, tips.Count);
                tipText.text = tips[random];
                timer = 0;
            }
        }
    }
}

