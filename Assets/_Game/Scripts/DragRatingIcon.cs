﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;

namespace Grow.War
{
    public class DragRatingIcon : MonoBehaviour
    {
        public bool startDrag;
        public RectTransform spritePrefab;
        private RectTransform spriteSelected;
        private Sprite spriteType;
        public Canvas canvas;
        private Image btRatingIcon;
        public EndGameController endGameController;

        private void Update()
        {
            if (startDrag)
            {
                spriteSelected.transform.localPosition = new Vector3(Input.mousePosition.x - (Screen.width / 2), Input.mousePosition.y - (Screen.height / 2), 0) / canvas.scaleFactor;
            }
        }

        public void OnClickIcon(Image btRating, Sprite sprite)
        {
            startDrag = true;
            spriteType = sprite;
            btRatingIcon = btRating;

            spriteSelected = Instantiate(spritePrefab, this.transform);
            spriteSelected.GetComponent<Image>().sprite = spriteType;
            //btRatingIcon.enabled = false;

            endGameController.endGameSnippets.ForEach((snippet) =>
            {
                //if (!snippet.ratingImage.gameObject.activeInHierarchy)
                //{
                snippet.hexagonoGlow.gameObject.SetActive(true);
                snippet.hexagonoGlow.DOFade(0.8f, 0.8f).SetLoops(-1, LoopType.Yoyo);
                snippet.cardGlow.gameObject.SetActive(true);
                snippet.cardGlow.DOFade(0.8f, 0.8f).SetLoops(-1, LoopType.Yoyo);
                //snippet.hexagono.DOColor(new Color(snippet.hexagono.color.r * 1.6f, snippet.hexagono.color.g * 1.6f, snippet.hexagono.color.b * 1.6f, 1), 0.4f).SetLoops(-1, LoopType.Yoyo);
                //}
            });
        }

        public void OnClickUp()
        {
            if (!startDrag)
                return;

            startDrag = false;

            endGameController.endGameSnippets.ForEach((snippet) =>
            {
                snippet.hexagonoGlow.DOKill();
                snippet.hexagonoGlow.color = new Color32(255, 255, 255, 77);

                snippet.cardGlow.DOKill();
                snippet.cardGlow.color = new Color32(255, 255, 255, 77);

                snippet.hexagonoGlow.gameObject.SetActive(false);
                snippet.cardGlow.gameObject.SetActive(false);
                //snippet.hexagono.DOKill();
                //snippet.hexagono.color = new Color32(49, 51, 51, 255);
            });

            RaycastClick();
        }

        public void RaycastClick()
        {
            Ray raycast;
            if (Input.touchSupported && Application.platform != RuntimePlatform.WebGLPlayer)
            {
                raycast = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            }
            else
            {
                raycast = Camera.main.ScreenPointToRay(Input.mousePosition);
            }

            RaycastHit raycastHit;
            if (Physics.Raycast(raycast, out raycastHit))
            {
                if (raycastHit.collider.CompareTag("endGameCard"))
                {
                    //if (!raycastHit.collider.GetComponent<EndGameSnippet>().ratingImage.gameObject.activeInHierarchy)
                    raycastHit.collider.GetComponent<EndGameSnippet>().UpdateRating(spriteType);
                    //else
                    //btRatingIcon.enabled = true;
                }
                else
                {
                    btRatingIcon.enabled = true;
                }

                Destroy(spriteSelected.gameObject);

            }
        }
    }
}
