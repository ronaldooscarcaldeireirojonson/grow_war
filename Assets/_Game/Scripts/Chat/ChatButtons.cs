﻿using Sfs2X.Entities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Grow.War
{
    public class ChatButtons : MonoBehaviour
    {
        public Text nameText;
        private int userId;
        public static ChatButtons instance;
        private WarChatPanel warChatPanel;
        public Button btSilenciar;
        public Image btSilenciarImage;
        public Sprite muted;
        public Sprite unmuted;


        public void Awake()
        {
            if (instance == null)
            {
                instance = this;
                gameObject.SetActive(false);
            }

            warChatPanel = FindObjectOfType<WarChatPanel>();
        }
        public void OnClickPerfil()
        {
            Sioux.Audio.Play("Click");
            ServerManager.instance.OpenNewProfilePopup(userId);
            instance.gameObject.SetActive(false);
            warChatPanel.OnClickOpenPanel();
        }

        public void OnClickSilenciar()
        {
            Sioux.Audio.Play("Click");
            warChatPanel.ToggleMuteUser(userId);
            instance.gameObject.SetActive(false);
        }

        public static void Show(ChatLog chatLog)
        {
            instance.userId = chatLog.userId;
            instance.nameText.text = chatLog.userName;

            instance.gameObject.SetActive(true);
            instance.transform.position = chatLog.transform.position + (Vector3.right * instance.GetComponent<RectTransform>().sizeDelta.x / 2);
            instance.btSilenciar.gameObject.SetActive(!chatLog.isItMe);
            if (instance.warChatPanel.IsUserMuted(instance.userId))
                instance.btSilenciarImage.sprite = instance.unmuted;
            else
                instance.btSilenciarImage.sprite = instance.muted;
        }
    }
}
