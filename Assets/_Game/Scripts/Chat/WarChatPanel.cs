﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System;
using Sfs2X.Entities;
using System.Text.RegularExpressions;

namespace Grow.War
{
    public class WarChatPanel : ChatPanel
    {

        //public CameraHandler camHandler;
        public RectTransform seta;


        public Transform container;
        public FortifyLog fortifyLogPrefab;
        public MoveLog moveLogPrefab;
        public NewTurnLog newTurnLogPrefab;
        public CardLog cardLogPrefab;
        public AttackLog attackLogPrefab;
        public DisconnectLog disconnectLogPrefab;
        public ReconnectLog reconnectLogPrefab;

        public ChatLog chatPrefab;
        public Transform containerChat;

        private List<int> mutedUsersId = new List<int>();

        //public void CheckMode()
        //{
        //    if (!GameController.Instance.SERVER.IsOnline())
        //    {
        //        OnClickBtLog();
        //        //btChat.interactable = false;
        //    }
        //}


        public override void UpdateIsYourTurnAlert(bool isYourTurn)
        {
            base.UpdateIsYourTurnAlert(isYourTurn);
            if (!GameController.Instance.SERVER.IsOnline())
                isYourTurnAlertPanel.DOScale(Vector3.zero, 0.3f);
        }

        public void ToggleMuteUser(int userId)
        {
            if (!mutedUsersId.Contains(userId))
                mutedUsersId.Add(userId);
            else
                mutedUsersId.Remove(userId);
        }

        public bool IsUserMuted(int userId)
        {
            return mutedUsersId.Contains(userId);
        }


        public override void UpdateChatRoll(User user, Color color, string message)
        {
            if (user != null)
            {
                if (mutedUsersId.Contains(SFSManager.GetVar(user, "idGrow").GetIntValue()))
                    return;

                ChatLog chat = Instantiate(chatPrefab, containerChat);
                chat.Configure(SFSManager.GetVar(user, "idGrow").GetIntValue(), user.Name, user.IsItMe, color, message);

                UpdateNotificationAlert(false);
            }
            else
            {
                ChatLog chat = Instantiate(chatPrefab, containerChat);
                chat.Configure(-1, "", false, color, message);
            }


            if (GameController.Instance != null)
            {
                if (GameController.gameState != null && (user == null || user.Name != GameController.gameState.thisPlayer.name))
                    UpdateNotificationAlert(false);
            }

            if (containerChat.childCount > 50)
                Destroy(containerChat.GetChild(0).gameObject);
        }





        public void UpdateLogRollNewTurn(Player player)
        {
            NewTurnLog newTurnPrefab = Instantiate(newTurnLogPrefab, container);
            newTurnPrefab.Configure(player);
            TruncateLogRoll();
        }

        public void UpdateLogRollFortify(Player player, string territoryName, int amountArmy)
        {
            FortifyLog fortifyPrefab = Instantiate(fortifyLogPrefab, container);
            fortifyPrefab.Configure(player, territoryName, amountArmy);
            TruncateLogRoll();
        }

        public void UpdateLogRollAttack(TerritoryObject toOrigin, TerritoryObject toTarget, List<int> attackDices, List<int> defenceDices)
        {

            AttackLog attackPrefab = Instantiate(attackLogPrefab, container);
            attackPrefab.Configure(toOrigin, toTarget, attackDices, defenceDices);
            TruncateLogRoll();
        }

        public void UpdateLogRollMove(Player player, string territoryName, string territoryTargetName, int movedArmy)
        {
            MoveLog movePrefab = Instantiate(moveLogPrefab, container);
            movePrefab.Configure(player, territoryName, territoryTargetName, movedArmy);
            TruncateLogRoll();
        }

        public void UpdateLogRollCard(Player player)
        {
            CardLog cardPrefab = Instantiate(cardLogPrefab, container);
            cardPrefab.Configure(player);
            TruncateLogRoll();
        }

        public void UpdateLogRollDisconnect(Player player)
        {
            DisconnectLog disconnectPrefab = Instantiate(disconnectLogPrefab, container);
            disconnectPrefab.Configure(player);
            TruncateLogRoll();
        }

        public void UpdateLogRollReconnect(Player player)
        {
            ReconnectLog reconnectPrefab = Instantiate(reconnectLogPrefab, container);
            reconnectPrefab.Configure(player);
            TruncateLogRoll();
        }

        public void TruncateLogRoll()
        {
            if (container.childCount > 50)
                Destroy(container.GetChild(0).gameObject);

            Canvas.ForceUpdateCanvases();
        }


        public override void OnClickOpenPanel()
        {
            if (DOTween.IsTweening(chatPanel))
                return;

            if (!isChatOpen)
            {
                backdropButton.gameObject.SetActive(true);
                UpdateNotificationAlert(true);
                //if (camHandler != null)
                //    camHandler.canClickMap = false;
                chatPanel.DOLocalMoveX(611, 0.4f).SetRelative().OnComplete(() =>
                {
                    isChatOpen = true;
                    UIBlocker.uiBlocked = true;
                    seta.localScale = new Vector3(1, -1, 1);
#if UNITY_WEBGL || UNITY_EDITOR
                    inputField.ActivateInputField();
#endif
                }).SetEase(Ease.Linear);
            }
            else
            {
                UpdateNotificationAlert(true);
                ChatButtons.instance.gameObject.SetActive(false);
                chatPanel.DOLocalMoveX(-611, 0.4f).SetRelative().OnComplete(() =>
               {
                   backdropButton.gameObject.SetActive(false);
                   isChatOpen = false;
                   UIBlocker.uiBlocked = false;
                   //if (camHandler != null)
                   //    camHandler.canClickMap = true;
                   seta.localScale = new Vector3(1, 1, 1);

               }).SetEase(Ease.Linear);
            }
        }

        public override void OnClickBackdrop()
        {
            OnClickOpenPanel();
        }


    }
}