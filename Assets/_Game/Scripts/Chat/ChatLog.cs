﻿using Sfs2X.Entities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Grow.War
{
    public class ChatLog : MonoBehaviour
    {


        public Text chatText;
        public ChatButtons chatButtons;
        public int userId;
        public bool isItMe;
        public string userName;

        public void Configure(int _userId, string _name, bool _isItMe, Color cor, string message)
        {
            userId = _userId;
            isItMe = _isItMe;
            userName = _name;
            if (_userId != -1)
            {
                chatText.text += "\n > <b>" + _name + "</b>" + ": " + message + "\n";
                //chatText.text += "\n <color=" + ColorTypeConverter.ToRGBHex(cor) + ">" + "<b>" + "> " + "</b>" + "</color>" + "<b>" + _name + "</b>" + ": " + message + "\n";
            }
            else
                chatText.text += "\n[ " + message + " ]\n";
        }

        public void OnClickMessage()
        {
            if (userId != -1)
                ChatButtons.Show(this);
        }
    }
}
