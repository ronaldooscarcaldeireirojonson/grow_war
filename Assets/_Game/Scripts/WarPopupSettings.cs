﻿using Grow;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Grow.War;
using DG.Tweening;

public class WarPopupSettings : PopupSettings
{

    public RectTransform fastGameRect;
    public bool isFastGame;
    public GameObject fullscreen;
    public GameObject fastGame;
    public GameObject abandonPopUp;
    public GameObject btRestorePurchases;

    public void Start()
    {
        if (Sioux.LocalData.Get<bool>("muteBGM"))
        {
            buttonMusicOn.SetActive(false);
            buttonMusicOff.SetActive(true);
        }
        else
        {
            buttonMusicOn.SetActive(true);
            buttonMusicOff.SetActive(false);
        }

        if (Sioux.LocalData.Get<bool>("muteSFX"))
        {
            buttonSoundEffectsOn.SetActive(false);
            buttonSoundEffectsOff.SetActive(true);
        }
        else
        {
            buttonSoundEffectsOn.SetActive(true);
            buttonSoundEffectsOff.SetActive(false);
        }

        bool fastIA = false;
        if (Sioux.LocalData.HasKey("fastIA"))
        fastIA = Sioux.LocalData.Get<bool>("fastIA");
        if (Sioux.LocalData.Get<bool>("FastGame") || fastIA)
        {
            fastGameRect.DOAnchorPosX(96, 0f);
        }
        else
        {
            fastGameRect.DOAnchorPosX(-96, 0f);
        }

        if (Globals.GetBuildPlatform() != Plataforma.WEB)
        {
            fullscreen.SetActive(false);
        }

        if (GameController.Instance)
        {
            if (GameController.Instance.SERVER.IsOnline())
                fastGame.SetActive(false);

        }
        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer)
        {
            if (btRestorePurchases != null)
                btRestorePurchases.SetActive(true);
        }
        else{
            if(btRestorePurchases != null)
            btRestorePurchases.SetActive(false);
        }


        if (GameController.Instance != null)
        {
            GameController.Instance.canvasUnderPopUp.SetActive(false);
        }
    }

    public override void OnClickClose()
    {
        Sioux.Audio.Play("Click");
        if (GameController.Instance != null)
        {
            GameController.Instance.canvasUnderPopUp.SetActive(true);
        }
            base.OnClickClose();
    }

    //public void OnClickSaveExit()
    //{
    //    Sioux.Audio.Play("Click");
    //    AnalyticsManager.Instance.ClickAbandonarPartida();
    //    SceneManager.instance.ChangeScene("ChooseMode");
    //}
    //public void OnClickDontSaveExit()
    //{
    //    Sioux.Audio.Play("Click");
    //    AnalyticsManager.Instance.ClickAbandonarPartida();
    //    PlayerPrefs.DeleteKey("save");
    //    PlayerPrefs.Save();
    //    SceneManager.instance.ChangeScene("ChooseMode");
    //}
    public void OnClickLeave()
    {
        Sioux.Audio.Play("Click");

        if (GameController.Instance.SERVER.IsOnline())
        {
            AnalyticsManager.Instance.ClickAbandonarPartida();
            SFS.Grow.AbandonMatchRequest();
            SceneManager.instance.ChangeScene("MainMenu");
        }
        else
        {
            abandonPopUp.SetActive(true);
            OnClickClose();
        }
    }

    public void OnClickFastGame()
    {
        Sioux.Audio.Play("Click");

        if (!Sioux.LocalData.Get<bool>("FastGame"))
        {
            fastGameRect.DOAnchorPosX(96, 0.5f);
            Sioux.LocalData.Set("FastGame", true);
            Sioux.LocalData.Set("fastIA", false);
            if(SceneManager.instance.scenesHistoric.Contains("Game")){
                GameController.Instance.UpdateFastModeBt(false);
                AI.SetStep(false);
            }

            //isFastGame = true;
        }
        else
        {
            fastGameRect.DOAnchorPosX(-96, 0.5f);
            Sioux.LocalData.Set("FastGame", false);
            Sioux.LocalData.Set("fastIA", false);
            if (SceneManager.instance.scenesHistoric.Contains("Game"))
            {
                GameController.Instance.UpdateFastModeBt(false);
                AI.SetStep(false);
            }
            //isFastGame = false;
        }
    }

    public void OnClickFullscreen()
    {
        Sioux.Audio.Play("Click");
        Screen.fullScreen = !Screen.fullScreen;
    }

    public void OnClickSupport()
    {
        Sioux.Audio.Play("Click");
#if UNITY_WEBGL && !UNITY_EDITOR
        //Application.ExternalEval("window.open(\"" + Globals.MORE_GAMES_URL + "\")");
        PopupOpenerCaptureClick("https://ajuda.growgames.com.br/hc/pt-br");
#else
        Application.OpenURL("https://ajuda.growgames.com.br/hc/pt-br");
#endif
    }

    public void OnClickRestorePurchases()
    {
        UnityIAPManager.Instance.RestorePurchases();
    }

   

}
