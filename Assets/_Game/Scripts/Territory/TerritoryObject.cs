﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Linq;

namespace Grow.War
{
    public class TerritoryObject : MonoBehaviour
    {

        public TerritoryState territoryState;
        public int territoryId;
        public Transform centerPosition;

        //public Text debugText;
        public Text troopsCountText;
        public Image toopsCountImage;

        [SerializeField]
        private List<MeshRenderer> faceMeshes;
        [SerializeField]
        private List<MeshRenderer> borderMeshes;
        [SerializeField]
        private List<MeshRenderer> sideMeshes;

        private int shaderColorId;
        private MaterialPropertyBlock faceMatPropBlock;
        private MaterialPropertyBlock sideMatPropBlock;
        private MaterialPropertyBlock borderMatPropBlock;

        public float raisePosY;
        public float idlePosY;

        IEnumerator Start()
        {
            faceMatPropBlock = new MaterialPropertyBlock();
            sideMatPropBlock = new MaterialPropertyBlock();

            borderMatPropBlock = new MaterialPropertyBlock();
            shaderColorId = Shader.PropertyToID("_Color");
            yield return new WaitUntil(() => GameController.Instance && GameController.gameState != null);
            territoryState = GameController.gameState.GetTS(territoryId);
            SetFaceColor(Color.gray);
            SetBorderColor(new Color32(30, 30, 30, 255));
        }

#if UNITY_EDITOR
        [ContextMenu("RefreshEditorReference")]
        public void RefreshEditorReference()
        {
            //var faceName = gameObject.name.Split('+')[1].Split('_')[0];
            var faceName = "+" + gameObject.name + "_F";
            //Debug.Log(faceName);
            transform.parent.Find("+-- " + faceName + "_L").SetParent(transform);
            transform.parent.Find("+-- " + faceName + "_B").SetParent(transform);

            var f = transform.Find(faceName);
            //faceMeshes.Add(transform.parent.Find(faceName).GetComponent<MeshRenderer>());
            borderMeshes.Add(transform.parent.Find("+-- " + gameObject.name + "_B").GetComponent<MeshRenderer>());
            sideMeshes.Add(transform.parent.Find("+-- " + gameObject.name + "_L").GetComponent<MeshRenderer>());

            //MeshFilter filter = f.GetComponent(typeof(MeshFilter)) as MeshFilter;
            //if (filter != null)
            //{
            //    Mesh mesh = filter.sharedMesh;

            //    Vector3[] normals = mesh.normals;
            //    for (int i = 0; i < normals.Length; i++)
            //    {
            //        //Debug.Log(normals[i]);
            //        normals[i] = new Vector3(0, 0, 1);
            //    }
            //    mesh.normals = normals;

            //    for (int m = 0; m < mesh.subMeshCount; m++)
            //    {
            //        int[] triangles = mesh.GetTriangles(m);
            //        for (int i = 0; i < triangles.Length; i += 3)
            //        {
            //            int temp = triangles[i + 0];
            //            triangles[i + 0] = triangles[i + 1];
            //            triangles[i + 1] = temp;
            //        }
            //        mesh.SetTriangles(triangles, m);
            //    }
            //}

            //troopsCountText = transform.Find("TroopsCountPrefab/Canvas/troopsCountImage/troopsCountText").GetComponent<Text>();
            //toopsCountImage = transform.Find("TroopsCountPrefab/Canvas/troopsCountImage").GetComponent<Image>();
        }
#endif

        //private void OnDrawGizmos()
        //{
        //    Gizmos.color = Color.yellow;
        //    Gizmos.DrawCube(centerPosition.position, Vector3.one * 0.5f);
        //}

        public void OnClick()
        {
            if (GameController.gameState.IsMyTurn())
            {
                switch (GameController.gameState.phase)
                {

                    case 0:

                        GameController.Instance.OnFortifyClick(this);
                        break;

                    case 1:
                        GameController.Instance.OnAttackClick(this);
                        break;

                    case 2:
                        GameController.Instance.OnMoveClick(this);
                        break;
                }
            }
        }

        public void SetFaceColor(Color newColor)
        {
            faceMatPropBlock.SetColor(shaderColorId, newColor);
            faceMeshes.ForEach((m) => m.SetPropertyBlock(faceMatPropBlock));

            sideMatPropBlock.SetColor(shaderColorId, newColor);
            //sideMeshes.ForEach((m) => m.SetPropertyBlock(sideMatPropBlock));

            //block.SetColor("_Color", new Color(Random.value, Random.value, Random.value));
        }

        public void SetBorderColor(Color newColor)
        {
            borderMatPropBlock.SetColor(shaderColorId, newColor);
            //borderMeshes.ForEach((m) => m.SetPropertyBlock(borderMatPropBlock));
        }

        public void SetTroopCounterColor(Color newColor)
        {
            toopsCountImage.DOColor(new Color(newColor.r, newColor.g, newColor.b, 1), 0.3f);
        }

        private void OnDrawGizmosSelected()
        {
            //Gizmos.color = Color.white;
            //for (int i = 0; i < adjacentCountries.Count; i++)
            //{
            //    Gizmos.DrawLine(centerPosition.position, adjacentCountries[i].centerPosition.position);
            //}
        }

        public void RefreshTerritory()
        {
            troopsCountText.text = territoryState.amountArmy.ToString();
            if (!GameController.Instance.footer.cardsOpened || GameController.gameState.thisPlayer.cards.Any((card) => card.id == territoryId))
            {
                SetTroopCounterColor(territoryState.owner.warColor.color);
                SetFaceColor(territoryState.owner.warColor.color);
            }
            else
            {
                SetTroopCounterColor(territoryState.owner.warColor.color / 2);
                SetFaceColor(territoryState.owner.warColor.color / 4);
            }
            SetBorderColor(territoryState.owner.warColor.color / 2f);
        }

        public void Raise(bool paintBorder = true)
        {
            if (paintBorder)
                SetBorderColor(Color.white);
            transform.DOLocalMoveY(raisePosY, 0.1f).SetEase(Ease.InOutQuad).OnComplete(
                () =>
                {
                    if (paintBorder)
                        SetBorderColor(Color.white);

                });
            //SetBorderColor(Color.gray);
        }

        public void Lower()
        {
            if (transform.localPosition.y == idlePosY)
                return;
            transform.DOLocalMoveY(idlePosY, 0.1f).SetEase(Ease.InOutQuad).OnComplete(() => SetBorderColor(territoryState.owner.warColor.color / 2f));
        }

        [ContextMenu("Pulse")]
        public void Pulse()
        {
            if (DOTween.IsTweening(toopsCountImage.rectTransform))
                return;

            toopsCountImage.rectTransform.DOPunchScale(Vector3.one * 1.5f, 0.4f, 1);
        }

    }
}
