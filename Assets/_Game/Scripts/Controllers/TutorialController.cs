﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Grow.War
{
    public class TutorialController : MonoBehaviour
    {
        public ScrollSnap scrollSnap;

        public List<Image> balls;
        public Sprite emptyBall;
        public Sprite fullBall;

        public Image leftArrow;
        public Image rightArrow;

        public CanvasGroup cg;
        public Image alphaTut;

        public List<Sprite> spritesMobile;
        public List<Image> imagesTutorial;

        private int count;

        // Use this for initialization
        void Start()
        {
            scrollSnap.onLerpComplete.AddListener(() => Refresh(scrollSnap.actualIndex));
            count = scrollSnap.LayoutElementCount();

            if (Globals.GetBuildPlatform() != Plataforma.WEB)
            {
                for (int i = 0; i < imagesTutorial.Count; i++)
                {
                    imagesTutorial[i].sprite = spritesMobile[i];
                }
            }
        }


        public void Refresh(int actualIndex)
        {

            leftArrow.gameObject.SetActive(actualIndex != 0);
            rightArrow.gameObject.SetActive(actualIndex < count - 1);


            for (int i = 0; i < balls.Count; i++)
            {
                if (i == actualIndex)
                    balls[i].sprite = fullBall;
                else
                    balls[i].sprite = emptyBall;
            }

            //if (actualIndex == count - 1)
            //{
            //    btFinishTut.gameObject.SetActive(true);
            //    btSkipTut.gameObject.SetActive(false);
            //}
            //else
            //{
            //    btFinishTut.gameObject.SetActive(false);
            //    btSkipTut.gameObject.SetActive(true);
            //}
        }

        public void Show()
        {
            alphaTut.gameObject.SetActive(true);
            gameObject.SetActive(true);
            transform.DOScale(1f, 0.5f);
            cg.DOFade(1f, 0.3f);
        }

        public void Hide()
        {
            transform.DOScale(0f, 0.5f).OnComplete(
                () =>
                {
                    gameObject.SetActive(false);
                    alphaTut.gameObject.SetActive(false);


                });
            cg.DOFade(0f, 0.3f);

        }



    }
}
