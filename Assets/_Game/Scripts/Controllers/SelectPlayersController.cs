﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Grow.War
{


    public class SelectPlayersController : MonoBehaviour
    {


        public List<SelectPlayerSnippet> selectSnippets;
        public Button btStart;
        public Button btFullscreen;
        public GameObject btVip;
        public GameObject btRemoveAds;


        public void Start()
        {
            // Atualiza o botão de fullscreen
            if (Globals.GetBuildPlatform() != Plataforma.WEB)
            {
                btFullscreen.gameObject.SetActive(false);
            }

            UpdateButtonStart();
            AdMobManager.Instance.HideBanner();
            if(Globals.GetBuildPlatform() == Plataforma.WEB)
            {
                btRemoveAds.SetActive(false);
            }
            else
                if (AdMobManager.Instance.noAds)
            {
                btRemoveAds.SetActive(false);

            }
            if(AdMobManager.Instance.isPremium){
                btVip.SetActive(false);
                btRemoveAds.SetActive(false);
            }

        }

        // checagens: se tem pelo menos um humano, se tem pelo menos 3 jogadores
        public void UpdateButtonStart()
        {
            int jogadorCount = 0;
            int cpuCount = 0;
            int desativadoCount = 0;

            // 0 jogador / 1 CPU / 2 desativado
            for (int i = 0; i < selectSnippets.Count; i++)
            {
                if (selectSnippets[i].playerType == 0)
                    jogadorCount++;
                else if (selectSnippets[i].playerType == 1)
                    cpuCount++;
                else if (selectSnippets[i].playerType == 2)
                    desativadoCount++;
            }

            if (desativadoCount <= 3 && jogadorCount >= 1)
                btStart.interactable = true;
            else
                btStart.interactable = false;
        }

        public void OnClickSettings()
        {
            Sioux.Audio.Play("Click");
            PopupManager.instance.OpenPopup("PopupSettingsOffline");
        }
        public void OnClickRemoveAds()
        {
            Sioux.Audio.Play("Click");

            PopupManager.instance.OpenPopup("PopupRemoveAds");

        }

        public void OnClickBack()
        {
            Sioux.Audio.Play("Click");
            SceneManager.instance.ChangeScene("ChooseMode");
        }

        public void OnClickStart()
        {
            AdMobManager.Instance.OfflineInterstitial();
            Sioux.Audio.Play("Click");
            List<Player> players = new List<Player>();
            for (int i = 0; i < selectSnippets.Count; i++)
            {
                if (selectSnippets[i].playerType == 2)
                    continue;
                Player player = new Player(i, selectSnippets[i].playerName.text, selectSnippets[i].warColor, selectSnippets[i].playerType == 0);
                players.Add(player);
            }

            SceneManager.instance.StartCoroutine(CreateLocalServer(players));
            SceneManager.instance.ChangeScene("Game");
        }

        IEnumerator CreateLocalServer(List<Player> players)
        {
            yield return new WaitUntil(() => GameController.Instance);
            GameController.Instance.SERVER = new LocalServer(players);
        }

        public void OnClickFullscreen()
        {
            Sioux.Audio.Play("Click");
            Screen.fullScreen = !Screen.fullScreen;
        }
        public void OnClickVIP()
        {
            Sioux.Audio.Play("Click");
            AnalyticsManager.Instance.SendDesignEvent("VipBanner:Click");
#if UNITY_WEBGL && !UNITY_EDITOR
            //Application.ExternalEval("window.open(\"" + Globals.MORE_GAMES_URL + "\", \"_blank\")");
            Application.OpenURL("https://assinatura.growgames.com.br/Assinatura");
#else
            Application.OpenURL("https://assinatura.growgames.com.br/Assinatura");
#endif
        }
    }
}
