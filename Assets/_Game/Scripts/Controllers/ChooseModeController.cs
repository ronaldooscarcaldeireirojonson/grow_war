﻿using Grow;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Grow.War
{

    public class ChooseModeController : MonoBehaviour
    {
        public AudioClip bgmClip;
        public Button btFullscreen;
        public GameObject cheatText;
        public Sprite spriteTest;
        public SaveManager save;
        public GameObject popUp;
        public GameObject btVip;
        public GameObject blackScreen;


        public void Start()
        {
            cheatText.SetActive(VersionController.IS_DEV);
#if !UNITY_WEBGL
            Application.targetFrameRate = 60;
#endif
            // Atualiza o botão de fullscreen
            if (Globals.GetBuildPlatform() != Plataforma.WEB)
            {
                btFullscreen.gameObject.SetActive(false);
            }

            Sioux.Profanity.Init();
            Sioux.OneSignalManager.Init("e4e04aca-127c-4690-928e-de2497a1c3f2");
            Sioux.Audio.PlayBGM(bgmClip, 0.6f);
            WarColor.Init();
            ServerManager.spriteTest = spriteTest;

            if (!Sioux.LocalData.HasKey("FastGame"))
                Sioux.LocalData.Set("FastGame", false);

            // Verifica se estamos voltando do Login e deu algum erro na hora de pegar a versão do jogo
            if (Globals.triedToGetVersionController)
            {
                Globals.triedToGetVersionController = false;

                // Mostra a mensagem de erro
                GameObject popup = PopupManager.instance.OpenPopup("PopupMessage");
                popup.GetComponent<PopupMessage>().Configure("ERRO", "Houve um erro na conexão com o servidor. Por favor, verifique sua conexão com a internet e tente novamente mais tarde",
                    () =>
                    {
                        PopupManager.instance.ClosePopup();
                    });
            }
            if (AdMobManager.Instance.isPremium){
                btVip.SetActive(false);

            }
        }

        public void OnClickLocal()
        {

            Sioux.Audio.Play("Click");

            if(save.hasSave)
            {
                //AdMobManager.Instance.OfflineInterstitial();
                popUp.SetActive(true);

            }else
            {
                AdMobManager.Instance.HideBanner();

                AnalyticsManager.Instance.SendDesignEvent("Modo:Local");
                SceneManager.instance.ChangeScene("SelectPlayers");

            }
        }
        public void NewLocalGame()
        {
            AdMobManager.Instance.HideBanner();
            AnalyticsManager.Instance.SendDesignEvent("Modo:Local");
            PlayerPrefs.DeleteKey("save");
            PlayerPrefs.Save();
            SceneManager.instance.ChangeScene("SelectPlayers");
        }
        public void LoadSavedGame()
        {
            blackScreen.SetActive(true);
            AdMobManager.Instance.OfflineInterstitial();
            AnalyticsManager.Instance.SendDesignEvent("Modo:Local");
            AdMobManager.Instance.HideBanner();
            save.DecodeSave();
        }

        public void OnClickOnline()
        {
            AdMobManager.Instance.HideBanner();
            //AdMobManager.Instance.OnlineInterstitial();
            Sioux.Audio.Play("Click");
            AnalyticsManager.Instance.SendDesignEvent("Modo:Multiplayer");
            SceneManager.instance.ChangeScene("Login");
        }

        public void OnClickSettings()
        {
            Sioux.Audio.Play("Click");
            PopupManager.instance.OpenPopup("PopupSettingsOffline");


        }

        public void OnClickFullscreen()
        {
            Sioux.Audio.Play("Click");
            Screen.fullScreen = !Screen.fullScreen;
        }
        public void OnClickVIP()
        {
            Sioux.Audio.Play("Click");
            AnalyticsManager.Instance.SendDesignEvent("VipBanner:Click");
#if UNITY_WEBGL && !UNITY_EDITOR
            //Application.ExternalEval("window.open(\"" + Globals.MORE_GAMES_URL + "\", \"_blank\")");
            Application.OpenURL("https://assinatura.growgames.com.br/Assinatura");
#else
            Application.OpenURL("https://assinatura.growgames.com.br/Assinatura");
#endif
        }

        static int cheatClicks = 20;
        public void OnClickCheat()
        {
            if (cheatClicks <= 0)
                cheatText.SetActive(VersionController.IS_DEV = !VersionController.IS_DEV);
            else
                --cheatClicks;
        }

    }
}