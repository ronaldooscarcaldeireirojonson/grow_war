﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu]
public class ResizeToFullHD : ScriptableObject
{
    public List<RectTransform> rects;

    public float resizeFactor;



    public void Resize()
    {
        if (rects.Count == 0)
        {
            rects = new List<RectTransform>(GameObject.FindObjectsOfType<RectTransform>());
        }

        foreach (var rect in rects)
        {
            // Posição
            rect.localPosition = new Vector3(Mathf.RoundToInt(rect.localPosition.x * resizeFactor), Mathf.RoundToInt(rect.localPosition.y * resizeFactor), Mathf.RoundToInt(rect.localPosition.z * resizeFactor));

            // Image
            Image image = rect.GetComponent<Image>();
            if (image != null)
            {
                rect.sizeDelta = new Vector2(Mathf.RoundToInt(rect.sizeDelta.x * resizeFactor), Mathf.RoundToInt(rect.sizeDelta.y * resizeFactor));
            }

            // Text
            Text text = rect.GetComponent<Text>();
            if (text != null)
            {
                text.rectTransform.sizeDelta = new Vector2(Mathf.RoundToInt(text.rectTransform.sizeDelta.x * resizeFactor), Mathf.RoundToInt(text.rectTransform.sizeDelta.y * resizeFactor));
                text.fontSize = Mathf.RoundToInt(text.fontSize * resizeFactor);
            }
        }
    }
}


#if UNITY_EDITOR
[CustomEditor(typeof(ResizeToFullHD))]
public class ResizeToFullHDEditor : Editor
{
    public override void OnInspectorGUI()
    {
        ResizeToFullHD myScript = (ResizeToFullHD)target;
        GUILayout.Label("Commands");

        DrawDefaultInspector();

        GUILayout.Space(10);

        if (GUILayout.Button("Resize"))
            myScript.Resize();
    }
}
#endif
