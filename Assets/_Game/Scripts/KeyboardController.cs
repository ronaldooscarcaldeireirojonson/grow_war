﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Grow.War
{
    public class KeyboardController : MonoBehaviour
    {

        public CameraHandler camHandler;
        public float panVelocity;
        public Footer footer;
        public EndGameController endGameController;


#if UNITY_WEBGL || UNITY_EDITOR
        // Update is called once per frame
        void Update()
        {
            if (!camHandler.canClickMap || UIBlocker.uiBlocked || GameController.Instance.chatPanel.isChatOpen || footer.cardsOpened || endGameController.blockMap || GameController.Instance.isProfileOpen)
                return;


            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
            {
                transform.Translate(0, 0, Time.deltaTime * panVelocity, Space.World);
            }
            if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
            {
                transform.Translate(0, 0, -Time.deltaTime * panVelocity, Space.World);
            }


            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
            {
                transform.Translate(-Time.deltaTime * panVelocity, 0, 0, Space.World);
            }
            if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
            {
                transform.Translate(Time.deltaTime * panVelocity, 0, 0, Space.World);
            }

            if (Input.GetKey(KeyCode.KeypadPlus) || Input.GetKey(KeyCode.Equals))
            {
                camHandler.zoomActive = true;
                camHandler.ZoomCamera(0.1f, 20);
                camHandler.zoomActive = false;
            }

            if (Input.GetKey(KeyCode.KeypadMinus) || Input.GetKey(KeyCode.Minus))
            {
                camHandler.zoomActive = true;
                camHandler.ZoomCamera(-0.1f, 20);
                camHandler.zoomActive = false;
            }

            if (!footer.sliderActive)
                return;

            if (Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Return))
            {
                footer.OnConfirmClick();
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                footer.OnCancelClick();
            }
        }
#endif
    }
}
