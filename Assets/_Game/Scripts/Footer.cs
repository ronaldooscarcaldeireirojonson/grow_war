﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Linq;
using TMPro;

namespace Grow.War
{

    public class Footer : MonoBehaviour
    {
        public Image soldier;
        public Image soldierWait;
        [Header("Phase Sprites")]
        public Sprite fortifyOff;
        public Sprite fortifyOn;
        public Sprite attackOff;
        public Sprite attackOn;
        public Sprite moveOff;
        public Sprite moveOn;

        [Header("BG Sprites")]
        public Sprite bgYellow;
        public Sprite bgBlue;
        public Sprite bgWhite;
        public Sprite bgBlack;
        public Sprite bgGreen;
        public Sprite bgRed;

        [Header("BG Cards Sprites")]
        public Sprite bgCardsYellow;
        public Sprite bgCardsBlue;
        public Sprite bgCardsWhite;
        public Sprite bgCardsBlack;
        public Sprite bgCardsGreen;
        public Sprite bgCardsRed;

        [Header("BG Wait Sprites")]
        public Sprite bgWaitYellow;
        public Sprite bgWaitBlue;
        public Sprite bgWaitWhite;
        public Sprite bgWaitBlack;
        public Sprite bgWaitGreen;
        public Sprite bgWaitRed;

        [Header("BG Slider Sprites")]
        public Sprite bgSliderYellow;
        public Sprite bgSliderBlue;
        public Sprite bgSliderWhite;
        public Sprite bgSliderBlack;
        public Sprite bgSliderGreen;
        public Sprite bgSliderRed;

        [Header("BG Bt Players Sprites")]
        public Sprite bgPlayerYellow;
        public Sprite bgPlayerBlue;
        public Sprite bgPlayerWhite;
        public Sprite bgPlayerBlack;
        public Sprite bgPlayerGreen;
        public Sprite bgPlayerRed;

        [Header("Cards Sprites")]
        public Sprite cardYellow;
        public Sprite cardBlue;
        public Sprite cardWhite;
        public Sprite cardBlack;
        public Sprite cardGreen;
        public Sprite cardRed;

        [Header("Objective Sprites")]
        public Sprite objectiveYellow;
        public Sprite objectiveBlue;
        public Sprite objectiveWhite;
        public Sprite objectiveBlack;
        public Sprite objectiveGreen;
        public Sprite objectiveRed;

        [Header("PhaseButton Sprites")]
        public Sprite phaseButtonYellow;
        public Sprite phaseButtonBlue;
        public Sprite phaseButtonWhite;
        public Sprite phaseButtonBlack;
        public Sprite phaseButtonGreen;
        public Sprite phaseButtonRed;

        [Header("CurrentPlayerCards Sprites")]
        public Sprite currentPlayerCardsYellow;
        public Sprite currentPlayerCardsBlue;
        public Sprite currentPlayerCardsWhite;
        public Sprite currentPlayerCardsBlack;
        public Sprite currentPlayerCardsGreen;
        public Sprite currentPlayerCardsRed;


        public Image fortifyImage;
        public Image attackImage;
        public Image moveImage;

        public Image fortifyImageWait;
        public Image attackImageWait;
        public Image moveImageWait;

        [Header("Panels")]
        public RectTransform panelCards;
        public RectTransform panelInfoTrade;
        public RectTransform panelObjective;
        public RectTransform panelSlider;
        public RectTransform panelAvisoTroca;
        public RectTransform panelPlayers;
        public RectTransform rectFooter;
        public RectTransform rectFooterWait;
        public RectTransform panelTimer;
        public RectTransform panelObjectiveWinner;
        public CanvasGroup panelEndGame;
        public PanelAlertObjective panelAlertObjective;
        public PanelAlertObjective panelAlertCards;
        public TutorialController panelTutorial;

        [Header("Others")]
        public CameraHandler camHandler;
        public GameObject alphaObjective;
        public GameObject alphaCards;

        public Button BtCards;
        public Slider slider;
        public Text minSliderText;
        public Text maxSliderText;
        public Text handleText;
        public Text remainingArmiesText;
        public Text phaseText;
        public MapController mapController;
        public Button buttonCloseCards;
        public Button btTrade;
        public Text infoTradeText;

        public Image timerFill;
        public Image avatar;
        public Image avatarWait;
        public Image bg;
        public Image bgWait;
        public Image avatarBorder;
        public Image avatarBorderWait;
        public GameObject avatarMask;
        public GameObject avatarMaskWait;
        public Image cardsImage;
        public Image objectiveImage;
        public Image phaseButtonImage;
        public Button phaseButton;
        public Image currentPlayerCards;
        public Image currentPlayerCardsWait;
        public Text nameText;
        public Text nameTextWait;
        public Text currentPlayerCardsText;
        public Text currentPlayerCardsTextWait;


        private bool needAnim = true;
        public bool sliderActive;

        //estados do botão que encerra ataque e turno
        public GameObject fortifyPhase;
        public GameObject attackPhase;
        public GameObject movePhase;
        public GameObject waitPhase;

        public Text titlePhaseSlider;
        public GameObject scrollNumberPrefab;
        public Transform content;
        public ScrollSnap scrollSnap;

        public bool isPressed;

        public float initialClickDelay = 0.5f;
        public float repetitionClickDelay;

        public Image bgBtPlayers;
        public Image btPlayersTint;
        public Image btPlayersBackTint;
        public Image bgSlider;
        public Button btPlus;
        public Button btMinus;

        public RectTransform rectBtPlayers;
        public RectTransform rectBtCards;
        public RectTransform rectBtObjective;

        private bool showingPlayers;
        public GameObject btPlayers;
        public GameObject btPlayersBack;
        public RectTransform bgPanelPlayers;
        public RectTransform scrollPanelPlayers;
        public Image timerPoint;

        public Image bgCards;
        public Button btCancelSlider;
        public Button btBackToWinner;
        public Image bgWinner;
        public Text winnerText;
        public Text winnerName;
        public GameObject winnerAvatarGroup;
        public Image winnerAvatar;
        public Image winnerSoldier;
        public Image winnerAvatarBorder;
        public Image lineUp;
        public Image lineDown;
        public Image starLeft;
        public Image starRight;

        public List<Sprite> patents;
        public Image patentImage;
        public Image patentImageWait;

        public GameObject quadPanelCards;

        public Text endGameText;
        public Image endGameSoldier;

        public TextMeshProUGUI tipPlayerNameText;
        public Text tipText;

        public Button btFullscreen;
        public bool cardsOpened;

        public GameObject abandonPopUp;



        // Use this for initialization
        IEnumerator Start()
        {
            // Atualiza o botão de fullscreen
            if (Globals.GetBuildPlatform() != Plataforma.WEB)
            {
                btFullscreen.gameObject.SetActive(false);
            }

            alphaObjective.SetActive(false);
            alphaCards.SetActive(false);
            panelObjective.localScale = Vector3.zero;
            panelObjective.GetComponent<CanvasGroup>().alpha = 0;
            panelPlayers.gameObject.SetActive(false);
            fortifyPhase.SetActive(true);
            attackPhase.SetActive(false);
            movePhase.SetActive(false);
            waitPhase.SetActive(false);
            timerPoint.DOFade(0, 0.5f).SetLoops(-1, LoopType.Yoyo);

            yield return new WaitUntil(() => GameController.gameState != null);

            if (GameController.Instance.SERVER.IsOnline())
            {
                var playerColor = GameController.gameState.thisPlayer.warColor;
                if (playerColor == WarColor.YELLOW)
                {
                    cardsImage.sprite = cardYellow;
                    objectiveImage.sprite = objectiveYellow;
                    bgBtPlayers.sprite = bgPlayerYellow;
                    bgSlider.sprite = bgSliderYellow;
                    bg.sprite = bgYellow;
                    phaseButtonImage.sprite = phaseButtonYellow;
                    currentPlayerCards.sprite = currentPlayerCardsYellow;
                    bgCards.sprite = bgCardsYellow;
                }
                else if (playerColor == WarColor.BLUE)
                {
                    cardsImage.sprite = cardBlue;
                    objectiveImage.sprite = objectiveBlue;
                    bgBtPlayers.sprite = bgPlayerBlue;
                    bgSlider.sprite = bgSliderBlue;
                    bg.sprite = bgBlue;
                    phaseButtonImage.sprite = phaseButtonBlue;
                    currentPlayerCards.sprite = currentPlayerCardsBlue;
                    bgCards.sprite = bgCardsBlue;
                }
                else if (playerColor == WarColor.WHITE)
                {
                    cardsImage.sprite = cardWhite;
                    objectiveImage.sprite = objectiveWhite;
                    bgBtPlayers.sprite = bgPlayerWhite;
                    bgSlider.sprite = bgSliderWhite;
                    bg.sprite = bgWhite;
                    phaseButtonImage.sprite = phaseButtonWhite;
                    currentPlayerCards.sprite = currentPlayerCardsWhite;
                    bgCards.sprite = bgCardsWhite;
                }
                else if (playerColor == WarColor.BLACK)
                {
                    cardsImage.sprite = cardBlack;
                    objectiveImage.sprite = objectiveBlack;
                    bgBtPlayers.sprite = bgPlayerBlack;
                    bgSlider.sprite = bgSliderBlack;
                    bg.sprite = bgBlack;
                    phaseButtonImage.sprite = phaseButtonBlack;
                    currentPlayerCards.sprite = currentPlayerCardsBlack;
                    bgCards.sprite = bgCardsBlack;
                }
                else if (playerColor == WarColor.GREEN)
                {
                    cardsImage.sprite = cardGreen;
                    objectiveImage.sprite = objectiveGreen;
                    bgBtPlayers.sprite = bgPlayerGreen;
                    bgSlider.sprite = bgSliderGreen;
                    bg.sprite = bgGreen;
                    phaseButtonImage.sprite = phaseButtonGreen;
                    currentPlayerCards.sprite = currentPlayerCardsGreen;
                    bgCards.sprite = bgCardsGreen;
                }
                else
                {
                    cardsImage.sprite = cardRed;
                    objectiveImage.sprite = objectiveRed;
                    bgBtPlayers.sprite = bgPlayerRed;
                    bgSlider.sprite = bgSliderRed;
                    bg.sprite = bgRed;
                    phaseButtonImage.sprite = phaseButtonRed;
                    currentPlayerCards.sprite = currentPlayerCardsRed;
                    bgCards.sprite = bgCardsRed;
                }

                nameText.text = GameController.gameState.thisPlayer.GetName;
                avatar.sprite = GameController.gameState.thisPlayer.avatar;
                avatarBorder.color = GameController.gameState.thisPlayer.warColor.color;
                currentPlayerCardsText.color = GameController.gameState.thisPlayer.warColor.color;
                currentPlayerCardsText.text = GameController.gameState.thisPlayer.cardCount.ToString();
                btPlayersTint.color = GameController.gameState.thisPlayer.warColor.color;
                btPlayersBackTint.color = GameController.gameState.thisPlayer.warColor.color;

                // Patente
                patentImage.sprite = patents[GameController.gameState.thisPlayer.patent - 1];
            }
            else
            {
                Destroy(avatarBorder.GetComponent<CursorListenerHelper>());
                patentImageWait.gameObject.SetActive(false);
                patentImage.gameObject.SetActive(false);
                timerPoint.gameObject.SetActive(false);
            }
        }


        public void Refresh()
        {
            RefreshPhase();
        }

        public void RefreshPhase()
        {
            if (needAnim)
            {
                rectBtPlayers.DOAnchorPosY(39f, 0.5f);
                rectBtCards.DOAnchorPosY(45.8f, 0.5f);
                rectBtObjective.DOAnchorPosY(52, 0.5f);
                panelTimer.DOAnchorPosY(0, 0.5f);
                needAnim = false;
            }

            //LIGA O SPRITE DE QUAL FASE DO TURNO ELE ESTÁ
            fortifyImage.sprite = GameController.gameState.phase == 0 ? fortifyOn : fortifyOff;
            attackImage.sprite = GameController.gameState.phase == 1 ? attackOn : attackOff;
            moveImage.sprite = GameController.gameState.phase == 2 ? moveOn : moveOff;


            fortifyImageWait.sprite = GameController.gameState.phase == 0 ? fortifyOn : fortifyOff;
            attackImageWait.sprite = GameController.gameState.phase == 1 ? attackOn : attackOff;
            moveImageWait.sprite = GameController.gameState.phase == 2 ? moveOn : moveOff;


            if ((GameController.gameState.IsMyTurn() && GameController.gameState.currentPlayer.isHuman) || (!GameController.Instance.SERVER.IsOnline() && GameController.gameState.currentPlayer.isHuman))
            {
                //if (GameController.Instance.SERVER.IsOnline())
                currentPlayerCardsText.text = GameController.gameState.thisPlayer.cardCount.ToString();


                rectFooter.DOAnchorPosY(-27, 0.5f);
                rectFooterWait.DOAnchorPosY(-606, 0.5f);
                rectBtCards.DOAnchorPosY(45.8f, 0.5f);
                rectBtObjective.DOAnchorPosY(52, 0.5f);

                tipPlayerNameText.text = GameController.gameState.currentPlayer.GetName;
                //tipPlayerNameText.color = GameController.gameState.currentPlayer.warColor.color;

                switch (GameController.gameState.phase)
                {
                    case 0:
                        phaseText.text = "FORTIFICAR TERRITÓRIOS";

                        fortifyPhase.SetActive(true);
                        attackPhase.SetActive(false);
                        movePhase.SetActive(false);
                        waitPhase.SetActive(false);

                        remainingArmiesText.text = GameController.gameState.turnArmy.Sum().ToString();


                        if (GameController.gameState.turnArmy[0] > 0)
                        {
                            GameController.Instance.isBonusArmy = true;
                            tipText.text = "Distribua " + GameController.gameState.turnArmy[0] + " exércitos na América do Norte";
                            if (GameController.gameState.turnArmy[0] == 1)
                                tipText.text = "Distribua " + GameController.gameState.turnArmy[0] + " exército na América do Norte";
                        }
                        else if (GameController.gameState.turnArmy[1] > 0)
                        {
                            GameController.Instance.isBonusArmy = true;
                            tipText.text = "Distribua " + GameController.gameState.turnArmy[1] + " exércitos na América do Sul";
                            if (GameController.gameState.turnArmy[1] == 1)
                                tipText.text = "Distribua " + GameController.gameState.turnArmy[1] + " exército na América do Sul";
                        }
                        else if (GameController.gameState.turnArmy[2] > 0)
                        {
                            GameController.Instance.isBonusArmy = true;
                            tipText.text = "Distribua " + GameController.gameState.turnArmy[2] + " exércitos na Europa";
                            if (GameController.gameState.turnArmy[2] == 1)
                                tipText.text = "Distribua " + GameController.gameState.turnArmy[2] + " exército na Europa";
                        }
                        else if (GameController.gameState.turnArmy[3] > 0)
                        {
                            GameController.Instance.isBonusArmy = true;
                            tipText.text = "Distribua " + GameController.gameState.turnArmy[3] + " exércitos na África";
                            if (GameController.gameState.turnArmy[3] == 1)
                                tipText.text = "Distribua " + GameController.gameState.turnArmy[3] + " exército na África";
                        }
                        else if (GameController.gameState.turnArmy[4] > 0)
                        {
                            GameController.Instance.isBonusArmy = true;
                            tipText.text = "Distribua " + GameController.gameState.turnArmy[4] + " exércitos na Ásia";
                            if (GameController.gameState.turnArmy[4] == 1)
                                tipText.text = "Distribua " + GameController.gameState.turnArmy[4] + " exército na Ásia";
                        }
                        else if (GameController.gameState.turnArmy[5] > 0)
                        {
                            GameController.Instance.isBonusArmy = true;
                            tipText.text = "Distribua " + GameController.gameState.turnArmy[5] + " exércitos na Oceania";
                            if (GameController.gameState.turnArmy[5] == 1)
                                tipText.text = "Distribua " + GameController.gameState.turnArmy[5] + " exército na Oceania";
                        }
                        else if (GameController.gameState.turnArmy[6] > 0)
                        {
                            GameController.Instance.isBonusArmy = false;
                            tipText.text = "Distribua seus exércitos em quaisquer de seus territórios";

                        }

                        if (HasCardsToTradeIn(GameController.gameState.thisPlayer.cards))
                        {


                            if (GameController.gameState.thisPlayer.cards.Count >= 5)
                            {
                                BtShowCards();
                                buttonCloseCards.interactable = false;
                            }
                            else
                                panelAvisoTroca.DOAnchorPosY(263, 0.5f);
                        }
                        else
                        {
                            panelAvisoTroca.DOAnchorPosY(100, 0.5f);
                            buttonCloseCards.interactable = true;
                        }
                        break;
                    case 1:
                        phaseText.text = "ATACAR";
                        tipText.text = "Selecione o seu território que vai partir o ataque";

                        fortifyPhase.SetActive(false);
                        attackPhase.SetActive(true);
                        movePhase.SetActive(false);
                        waitPhase.SetActive(false);

                        panelAvisoTroca.DOAnchorPosY(100, 0.5f);
                        break;
                    case 2:
                        phaseText.text = "DESLOCAR EXÉRCITOS";
                        tipText.text = "Selecione o território para transferir os exércitos";

                        fortifyPhase.SetActive(false);
                        attackPhase.SetActive(false);
                        movePhase.SetActive(true);
                        waitPhase.SetActive(false);

                        break;
                    default:
                        Debug.Log("ERROR!");
                        break;
                }
            }
            else
            {
                if (GameController.Instance.SERVER.IsOnline())
                {
                    rectFooter.DOAnchorPosY(-606, 0.5f);
                    rectFooterWait.DOAnchorPosY(-27, 0.5f);
                    phaseText.text = "AGUARDE SUA VEZ";
                }
                else
                {
                    if (!GameController.gameState.currentPlayer.isHuman)
                    {
                        UpdateCurrentPlayer();
                        rectFooter.DOAnchorPosY(-606, 0.5f);
                        rectFooterWait.DOAnchorPosY(-27, 0.5f);
                        rectBtCards.DOAnchorPosY(-224f, 0.5f);
                        rectBtObjective.DOAnchorPosY(-201, 0.5f);
                    }
                }

                panelAvisoTroca.DOAnchorPosY(389, 0.5f);

                if (sliderActive)
                    HideSlider();
            }


        }

        public void UpdateCurrentPlayer()
        {
            timerFill.color = GameController.gameState.currentPlayer.warColor.color;
            nameTextWait.text = GameController.gameState.currentPlayer.GetName;
            if (GameController.Instance.SERVER.IsOnline())
            {
                avatarWait.sprite = GameController.gameState.currentPlayer.avatar;

                // Patente
                patentImageWait.sprite = patents[GameController.gameState.currentPlayer.patent - 1];
            }
            else
            {
                avatarMaskWait.SetActive(false);
                avatarBorderWait.GetComponent<Button>().interactable = false;
                soldierWait.color = GameController.gameState.currentPlayer.warColor.color;
            }

            avatarBorderWait.color = GameController.gameState.currentPlayer.warColor.color;
            currentPlayerCardsTextWait.color = GameController.gameState.currentPlayer.warColor.color;
            currentPlayerCardsTextWait.text = GameController.gameState.currentPlayer.cardCount.ToString();

            var wColor = GameController.gameState.currentPlayer.warColor;
            if (wColor == WarColor.YELLOW)
            {
                bgWait.sprite = bgWaitYellow;
                currentPlayerCardsWait.sprite = currentPlayerCardsYellow;

            }
            else if (wColor == WarColor.BLUE)
            {
                bgWait.sprite = bgWaitBlue;
                currentPlayerCardsWait.sprite = currentPlayerCardsBlue;

            }
            else if (wColor == WarColor.WHITE)
            {
                bgWait.sprite = bgWaitWhite;
                currentPlayerCardsWait.sprite = currentPlayerCardsWhite;

            }
            else if (wColor == WarColor.BLACK)
            {
                bgWait.sprite = bgWaitBlack;
                currentPlayerCardsWait.sprite = currentPlayerCardsBlack;
                timerFill.color = new Color32(79, 79, 79, 255);

            }
            else if (wColor == WarColor.GREEN)
            {
                bgWait.sprite = bgWaitGreen;
                currentPlayerCardsWait.sprite = currentPlayerCardsGreen;

            }
            else
            {
                bgWait.sprite = bgWaitRed;
                currentPlayerCardsWait.sprite = currentPlayerCardsRed;
            }
        }

        public void UpdateCurrentPlayerLocal()
        {
            timerFill.color = GameController.gameState.currentPlayer.warColor.color;

            var playerColor = GameController.gameState.currentPlayer.warColor;
            if (playerColor == WarColor.YELLOW)
            {
                cardsImage.sprite = cardYellow;
                objectiveImage.sprite = objectiveYellow;
                bgBtPlayers.sprite = bgPlayerYellow;
                bgSlider.sprite = bgSliderYellow;
                bg.sprite = bgYellow;
                phaseButtonImage.sprite = phaseButtonYellow;
                currentPlayerCards.sprite = currentPlayerCardsYellow;
                bgCards.sprite = bgCardsYellow;
            }
            else if (playerColor == WarColor.BLUE)
            {
                cardsImage.sprite = cardBlue;
                objectiveImage.sprite = objectiveBlue;
                bgBtPlayers.sprite = bgPlayerBlue;
                bgSlider.sprite = bgSliderBlue;
                bg.sprite = bgBlue;
                phaseButtonImage.sprite = phaseButtonBlue;
                currentPlayerCards.sprite = currentPlayerCardsBlue;
                bgCards.sprite = bgCardsBlue;
            }
            else if (playerColor == WarColor.WHITE)
            {
                cardsImage.sprite = cardWhite;
                objectiveImage.sprite = objectiveWhite;
                bgBtPlayers.sprite = bgPlayerWhite;
                bgSlider.sprite = bgSliderWhite;
                bg.sprite = bgWhite;
                phaseButtonImage.sprite = phaseButtonWhite;
                currentPlayerCards.sprite = currentPlayerCardsWhite;
                bgCards.sprite = bgCardsWhite;
            }
            else if (playerColor == WarColor.BLACK)
            {
                cardsImage.sprite = cardBlack;
                objectiveImage.sprite = objectiveBlack;
                bgBtPlayers.sprite = bgPlayerBlack;
                bgSlider.sprite = bgSliderBlack;
                bg.sprite = bgBlack;
                phaseButtonImage.sprite = phaseButtonBlack;
                currentPlayerCards.sprite = currentPlayerCardsBlack;
                bgCards.sprite = bgCardsBlack;
            }
            else if (playerColor == WarColor.GREEN)
            {
                cardsImage.sprite = cardGreen;
                objectiveImage.sprite = objectiveGreen;
                bgBtPlayers.sprite = bgPlayerGreen;
                bgSlider.sprite = bgSliderGreen;
                bg.sprite = bgGreen;
                phaseButtonImage.sprite = phaseButtonGreen;
                currentPlayerCards.sprite = currentPlayerCardsGreen;
                bgCards.sprite = bgCardsGreen;
            }
            else
            {
                cardsImage.sprite = cardRed;
                objectiveImage.sprite = objectiveRed;
                bgBtPlayers.sprite = bgPlayerRed;
                bgSlider.sprite = bgSliderRed;
                bg.sprite = bgRed;
                phaseButtonImage.sprite = phaseButtonRed;
                currentPlayerCards.sprite = currentPlayerCardsRed;
                bgCards.sprite = bgCardsRed;
            }

            nameText.text = GameController.gameState.currentPlayer.GetName;
            soldier.color = GameController.gameState.currentPlayer.warColor.color;
            btPlayersTint.color = GameController.gameState.thisPlayer.warColor.color;
            btPlayersBackTint.color = GameController.gameState.thisPlayer.warColor.color;
            avatarBorder.color = GameController.gameState.currentPlayer.warColor.color;
            avatarBorder.GetComponent<Button>().interactable = false;
            avatarMask.SetActive(false);
            currentPlayerCardsText.color = GameController.gameState.currentPlayer.warColor.color;
            currentPlayerCardsText.text = GameController.gameState.currentPlayer.cardCount.ToString();
            GameController.Instance.objectiveText.text = Objective.LIST[GameController.gameState.currentPlayer.objective.id].text;
            GameController.Instance.objectiveBorderColor.color = GameController.gameState.currentPlayer.warColor.color;
        }

        public void OnClickPhase()
        {
            if (GameController.Instance.SERVER.IsOnline())
            {
                if (!GameController.gameState.IsMyTurn())
                    return;
            }

            Sioux.Audio.Play("Click");
            switch (GameController.gameState.phase)
            {
                case 0:
                    GameController.Instance.panelNewArmies.Show();
                    break;
                case 1:
                    GameController.Instance.AttackPhaseEndRequest();
                    break;
                case 2:
                    GameController.Instance.EndTurnRequest();
                    break;
            }
        }

        public void BtShowCards()
        {
            GameController.Instance.UpdateFastModeCanvasVisibility(false);
            if (!GameController.Instance.SERVER.IsOnline() && GameController.gameState.players.Count((p) => p.isHuman) > 1)
            {
                panelAlertCards.Show();
                Sioux.Audio.Play("Click");
                return;
            }
            BtShowCardsLocal();
        }

        public void BtShowCardsLocal()
        {
            Sioux.Audio.Play("Click");
            camHandler.canClickMap = false;
            panelAvisoTroca.DOAnchorPosY(100, 0.5f);
            alphaCards.SetActive(true);
            quadPanelCards.SetActive(true);
            panelCards.DOAnchorPosY(28f, 0.5f);
            panelInfoTrade.DOAnchorPosX(9, 0.5f);
            if (!panelPlayers.gameObject.activeInHierarchy)
                rectBtPlayers.DOAnchorPosY(-242f, 0.5f);
            rectBtCards.DOAnchorPosY(-250f, 0.5f);
            rectBtObjective.DOAnchorPosY(-163f, 0.5f);
            cardsOpened = true;

            CameraHandler.Instance.ResetCamPos();
            GameController.Instance.territoryObjects.ForEach((to) =>
            {
                if (GameController.gameState.thisPlayer.cards.Contains(to.territoryState.territory))
                {
                    to.GetComponentInChildren<LookAt>().canShowName = false;
                    to.GetComponentInChildren<LookAt>().territoryName.DOFade(1, 0.5f);
                }
                else
                {
                    to.SetFaceColor(to.territoryState.owner.warColor.color / 4);
                    to.SetTroopCounterColor(to.territoryState.owner.warColor.color / 2);
                }
            });
        }

        public void BtCloseCards()
        {
            GameController.Instance.UpdateFastModeCanvasVisibility(true);
            Sioux.Audio.Play("Click");
            if (!camHandler.isGameOver)
            {
                rectBtPlayers.DOAnchorPosY(39f, 0.5f);
                rectBtCards.DOAnchorPosY(45.8f, 0.5f);
                rectBtObjective.DOAnchorPosY(52, 0.5f);
            }

            panelCards.DOAnchorPosY(-247f, 0.5f).OnComplete(() =>
            {
                alphaCards.SetActive(false);
                quadPanelCards.SetActive(false);
                foreach (Transform tc in GameController.Instance.containerCards)
                {
                    tc.GetComponent<TerritoryCard>().DeselectCard();
                    tc.GetComponent<TerritoryCard>().isSelected = false;
                }
                btTrade.interactable = false;
            });
            panelInfoTrade.DOAnchorPosX(382, 0.5f);
            cardsOpened = false;
            camHandler.canClickMap = true;


            GameController.Instance.territoryObjects.ForEach((to) =>
            {
                if (GameController.gameState.thisPlayer.cards.Contains(to.territoryState.territory))
                {
                    to.GetComponentInChildren<LookAt>().canShowName = true;
                    to.GetComponentInChildren<LookAt>().territoryName.DOFade(0, 0.5f);
                }
                else
                {
                    to.SetFaceColor(to.territoryState.owner.warColor.color);
                    to.SetTroopCounterColor(to.territoryState.owner.warColor.color);
                }
            });

            //GameController.Instance.CleanMap();
        }

        public void BtObjective()
        {
            if (!GameController.Instance.SERVER.IsOnline() && GameController.gameState.players.Count((p) => p.isHuman) > 1)
            {
                panelAlertObjective.Show();
                Sioux.Audio.Play("Click");
                return;
            }
            BtObjectiveLocal();
        }

        public void BtObjectiveLocal()
        {
            Sioux.Audio.Play("Click");
            Sioux.Audio.Play(GameController.Instance.objectiveClip);
            alphaObjective.SetActive(true);
            panelObjective.DOScale(1f, 0.5f);
            panelObjective.GetComponent<CanvasGroup>().DOFade(1f, 0.3f);
        }

        public void BtCloseObjective()
        {
            Sioux.Audio.Play("Click");
            panelObjective.DOScale(0f, 0.5f);
            panelObjective.GetComponent<CanvasGroup>().DOFade(0f, 0.3f).OnComplete(() =>
            {
                alphaObjective.SetActive(false);
            });
        }

        public void BtShowPlayers()
        {
            AnalyticsManager.Instance.ClickPlayers();
            Sioux.Audio.Play("Click");
            Sioux.LocalData.Set("ShowPlayers", true);
            DOTween.Kill(GameController.Instance.glowBtPlayers);
            GameController.Instance.glowBtPlayers.color = Color.clear;
            if (DOTween.IsTweening(bgPanelPlayers))
                return;
            showingPlayers = !showingPlayers;
            if (showingPlayers)
            {
                btPlayersBack.SetActive(true);
                btPlayers.SetActive(false);
                panelPlayers.gameObject.SetActive(true);
                bgPanelPlayers.DOAnchorPosY(0, 0.5f);
                scrollPanelPlayers.DOAnchorPosY(0, 0.5f);
                GameController.Instance.UpdatePanelPlayer();
            }
            else
            {
                bgPanelPlayers.DOAnchorPosY(-389f, 0.5f);
                scrollPanelPlayers.DOAnchorPosY(-774f, 0.5f).OnComplete(() => panelPlayers.gameObject.SetActive(false));
                btPlayersBack.SetActive(false);
                btPlayers.SetActive(true);
            }

        }


        public void ShowSlider(TerritoryObject to, int turnArmy)
        {
            rectBtPlayers.DOAnchorPosY(-242f, 0.5f);
            rectBtCards.DOAnchorPosY(-250f, 0.5f);
            rectBtObjective.DOAnchorPosY(-163f, 0.5f);

            foreach (Transform item in content)
            {
                Destroy(item.gameObject);
            }
            scrollSnap.cellIndex = 0;
            scrollSnap.actualIndex = 0;
            sliderActive = true;
            panelSlider.DOAnchorPosY(-10, 0.5f);
            rectFooter.DOAnchorPosY(-605, 0.5f);
            if (GameController.gameState.phase == 0)
            {
                btCancelSlider.interactable = true;
                titlePhaseSlider.text = "FORTIFICAR";
                for (int i = to.territoryState.amountArmy; i <= to.territoryState.amountArmy + turnArmy; i++)
                {
                    GameObject prefab = Instantiate(scrollNumberPrefab, content);
                    prefab.GetComponentInChildren<Text>().text = i.ToString();
                }

                minSliderText.text = to.territoryState.amountArmy.ToString();
                maxSliderText.text = (to.territoryState.amountArmy + turnArmy).ToString();
            }
            else if (GameController.gameState.phase == 1)
            {
                btCancelSlider.interactable = false;
                titlePhaseSlider.text = "INVADIR";
                for (int i = 1; i <= turnArmy; i++)
                {
                    GameObject prefab = Instantiate(scrollNumberPrefab, content);
                    prefab.GetComponentInChildren<Text>().text = i.ToString();
                }

                minSliderText.text = "1";
                maxSliderText.text = turnArmy.ToString();
            }
            else if (GameController.gameState.phase == 2)
            {
                btCancelSlider.interactable = true;
                titlePhaseSlider.text = "DESLOCAR";
                for (int i = GameController.Instance.toTarget.territoryState.amountArmy; i <= GameController.Instance.toTarget.territoryState.amountArmy + turnArmy; i++)
                {
                    GameObject prefab = Instantiate(scrollNumberPrefab, content);
                    prefab.GetComponentInChildren<Text>().text = i.ToString();
                }

                minSliderText.text = GameController.Instance.toTarget.territoryState.amountArmy.ToString();
                maxSliderText.text = (GameController.Instance.toTarget.territoryState.amountArmy + turnArmy).ToString();
            }

        }

        public void OnPlusClick()
        {
            Sioux.Audio.Play("Click");
            StartCoroutine("Plus");
        }

        public void OnPlusClickUp()
        {
            StopCoroutine("Plus");
        }

        public IEnumerator Plus()
        {
            int i = 0;
            scrollSnap.SnapToNext();
            yield return new WaitForSeconds(initialClickDelay);
            int multiplier = 1;
            while (i < 400)
            {
                for (int j = 0; j < multiplier; j++)
                {
                    scrollSnap.SnapToNext();
                }
                multiplier = multiplier * 2;
                yield return new WaitForSeconds(repetitionClickDelay * 4);
                i++;
            }
        }
        public void OnMaxClick()
        {
            Sioux.Audio.Play("Click");
            StartCoroutine("Max");
        }
        public IEnumerator Max()
        {
            scrollSnap.SnapToIndex(scrollSnap.LayoutElementCount() - 1);
            yield return new WaitForSeconds(initialClickDelay);
            OnConfirmClick();
        }
        //private void Update()
        //{
        //    if (Input.GetKeyDown(KeyCode.Q))
        //    {
        //        OnMaxClick();
        //    } 
        //}

        public void OnMinusClick()
        {
            Sioux.Audio.Play("Click");
            StartCoroutine("Minus");
        }

        public void OnminusClickUp()
        {
            StopCoroutine("Minus");
        }


        public IEnumerator Minus()
        {
            int i = 0;
            scrollSnap.SnapToPrev();
            yield return new WaitForSeconds(initialClickDelay);
            int multiplier = 1;
            while (i < 400)
            {
                for (int j = 0; j < multiplier; j++)
                {
                    scrollSnap.SnapToPrev();
                }
                multiplier = multiplier * 2;
                yield return new WaitForSeconds(repetitionClickDelay * 4);
                i++;
            }
        }

        public void RefreshHandleText()
        {
            for (int i = 0; i < content.childCount; i++)
            {
                if (i == scrollSnap.cellIndex)
                    content.GetChild(i).GetChild(0).DOScale(0.6f, 0.2f);
                else
                    content.GetChild(i).GetChild(0).DOScale(0.4f, 0.2f);
            }

            if (scrollSnap.cellIndex == 0)
                btMinus.interactable = false;
            else
                btMinus.interactable = true;

            if (scrollSnap.cellIndex == content.childCount - 1)
                btPlus.interactable = false;
            else
                btPlus.interactable = true;

            if (GameController.Instance.toOrigin == null)
                return;

            if (GameController.gameState.phase == 0)
                GameController.Instance.toOrigin.troopsCountText.text = (GameController.Instance.toOrigin.territoryState.amountArmy + scrollSnap.cellIndex).ToString();
            else if (GameController.gameState.phase == 2 && GameController.Instance.toTarget != null)
            {
                GameController.Instance.toTarget.troopsCountText.text = (GameController.Instance.toTarget.territoryState.amountArmy + scrollSnap.cellIndex).ToString();
                GameController.Instance.toOrigin.troopsCountText.text = (GameController.Instance.toOrigin.territoryState.amountArmy - scrollSnap.cellIndex).ToString();
            }
        }

        public void HideSlider()
        {
            if ((GameController.gameState.currentPlayer.isHuman) || (!GameController.Instance.SERVER.IsOnline() && GameController.gameState.currentPlayer.isHuman))
            {
                rectBtPlayers.DOAnchorPosY(39f, 0.5f);
                rectBtCards.DOAnchorPosY(45.8f, 0.5f);
                rectBtObjective.DOAnchorPosY(52, 0.5f);
                panelSlider.DOAnchorPosY(-612, 0.5f);
            }

            if (GameController.Instance.SERVER.IsOnline())
            {
                if (GameController.gameState.IsMyTurn())
                    rectFooter.DOAnchorPosY(-27, 0.5f);
                else
                    rectFooterWait.DOAnchorPosY(-27, 0.5f);

                panelSlider.DOAnchorPosY(-612, 0.5f);
            }
            else
            {
                if (GameController.gameState.currentPlayer.isHuman)
                    rectFooter.DOAnchorPosY(-27, 0.5f);
                else
                    rectFooterWait.DOAnchorPosY(-27, 0.5f);
            }

            sliderActive = false;
        }

        public void OnCancelClick()
        {
            Sioux.Audio.Play("Click");
            HideSlider();

            if (GameController.gameState.phase == 0)
            {

                GameController.Instance.toOrigin.troopsCountText.text = GameController.Instance.toOrigin.territoryState.amountArmy.ToString();
                if (!GameController.Instance.isBonusArmy)
                    GameController.Instance.toOrigin.Lower();
                else
                    GameController.Instance.toOrigin.SetBorderColor(GameController.Instance.toOrigin.territoryState.owner.warColor.color / 2f);

                GameController.Instance.toOrigin = null;
            }
            else if (GameController.gameState.phase == 2)
            {
                tipText.text = "Selecione o território para transferir os exércitos";
                GameController.Instance.toOrigin.troopsCountText.text = GameController.Instance.toOrigin.territoryState.amountArmy.ToString();
                GameController.Instance.toTarget.troopsCountText.text = GameController.Instance.toTarget.territoryState.amountArmy.ToString();
                GameController.Instance.toOrigin.Lower();
                GameController.Instance.toTarget.Lower();
                GameController.Instance.toOrigin = null;
                GameController.Instance.toTarget = null;
                mapController.mapAlpha.GetComponent<BoxCollider>().enabled = false;
                mapController.mapAlpha.DOFade(0f, 0.3f);
            }

        }

        public void OnConfirmClick()
        {
            if (GameController.gameState.IsMyTurn())
            {
                Sioux.Audio.Play("Click");
                HideSlider();

                if (GameController.gameState.phase == 0)
                {
                    int totalFortified = scrollSnap.cellIndex;
                    if (GameController.Instance.isBonusArmy)
                        GameController.Instance.toOrigin.SetBorderColor(GameController.Instance.toOrigin.territoryState.owner.warColor.color / 2f);
                    GameController.Instance.FortyfyRequest(GameController.Instance.toOrigin.territoryId, totalFortified);
                    GameController.Instance.toOrigin = null;
                }
                else if (GameController.gameState.phase == 1)
                {
                    int totalMoved = scrollSnap.cellIndex + 1;
                    GameController.Instance.MoveRequest(GameController.Instance.toOrigin.territoryId, GameController.Instance.toTarget.territoryId, totalMoved);
                }
                else if (GameController.gameState.phase == 2)
                {
                    int totalMoved = scrollSnap.cellIndex;
                    GameController.Instance.MoveRequest(GameController.Instance.toOrigin.territoryId, GameController.Instance.toTarget.territoryId, totalMoved);
                }
            }

        }

        public void OnTradeClick()
        {
            GameController.Instance.UpdateFastModeCanvasVisibility(true);
            Sioux.Audio.Play("Click");
            GameController.Instance.TradeInCardsRequest(TerritoryCard.selectedCards.ConvertAll(c => c.id));
        }

        public bool HasCardsToTradeIn(List<Territory> cards)
        {
            if (cards.Count < 3)
                return false;

            var amountCircle = 0;
            var amountSquare = 0;
            var amountTriangle = 0;
            var amountJoker = 0;

            cards.ForEach(c =>
            {
                switch (c.symbol)
                {
                    case SymbolType.Circle:
                        amountCircle++;
                        break;
                    case SymbolType.Square:
                        amountSquare++;
                        break;
                    case SymbolType.Triangle:
                        amountTriangle++;
                        break;
                    case SymbolType.Wildcard:
                        amountJoker++;
                        break;
                    default:
                        break;
                }
            });

            // Verifica se tem coringa
            if (amountJoker > 0)
                return true;

            // Verifica se tem 3 iguais
            if (amountCircle >= 3 || amountSquare >= 3 || amountTriangle >= 3)
                return true;

            // Verifica se tem 3 diferentes
            if (amountCircle >= 1 && amountSquare >= 1 && amountTriangle >= 1)
                return true;

            return false;
        }

        public void SetInfoTradeText()
        {
            infoTradeText.text = "";
            for (int i = GameController.gameState.tradeInCount; i < GameController.gameState.tradeInCount + 5; ++i)
            {
                if (i == GameController.gameState.tradeInCount)
                    infoTradeText.text += (i + 1) + "ª troca: " + "+" + GameController.Instance.TradeInArmy(i) + " exércitos" + "\n";
                else
                    infoTradeText.text += (i + 1) + "ª troca: " + "+" + GameController.Instance.TradeInArmy(i) + " exércitos" + "\n";
            }
        }

        public void RefreshTimer(int t)
        {
            if (t >= GameController.Instance.turnDuration)
                timerFill.fillAmount = 1;
            DOTween.To(
                v =>
                {
                    timerFill.fillAmount = v;
                    timerPoint.rectTransform.anchoredPosition = new Vector2(-(1 - v) * timerFill.rectTransform.rect.width, timerPoint.rectTransform.anchoredPosition.y);
                }, // setter
                timerFill.fillAmount, // startValue
                (float)(t - 1) / GameController.Instance.turnDuration, // endValue
                1) // duration
                .SetEase(Ease.Linear); // linear easing
        }

        public void OnClickSettings()
        {
            Sioux.Audio.Play("Click");
            GameObject p = PopupManager.instance.OpenPopup("PopupSettingsGame");
            WarPopupSettings w = p.GetComponent<WarPopupSettings>();
            w.abandonPopUp = abandonPopUp;
            //camHandler.canClickMap = false;
        }

        public void OnClickTutorial()
        {
            Sioux.Audio.Play("Click");
            panelTutorial.Show();
        }

        public void OnClickCloseTutorial()
        {
            Sioux.Audio.Play("Click");
            panelTutorial.Hide();
        }

        #region EndGame
        public void ShowWinner(int winnerObjectiveId)
        {
            panelObjectiveWinner.localScale = Vector3.one;
            panelObjectiveWinner.GetComponent<CanvasGroup>().alpha = 1;
            bgWinner.transform.DOScaleX(1, 0.1f).SetEase(Ease.OutQuad).OnComplete(() =>
            {
                winnerText.transform.DOScale(Vector3.one, 0.2f).SetEase(Ease.OutQuad);
                winnerName.transform.DOScale(Vector3.one, 0.2f).SetEase(Ease.OutQuad);

                winnerAvatarGroup.transform.DOScale(Vector3.one * 1.3f, 0.2f).OnComplete(() =>
                {
                    winnerAvatarGroup.transform.DOScale(Vector3.one, 0.2f).SetEase(Ease.OutQuad).OnComplete(() =>
                    {
                        starLeft.transform.DOScale(Vector3.one * 1.3f, 0.1f).SetDelay(0.5f).OnComplete(() =>
                        {
                            starLeft.transform.DOScale(Vector3.one, 0.1f).SetEase(Ease.OutQuad).OnComplete(() =>
                            {
                                starRight.transform.DOScale(Vector3.one * 1.3f, 0.1f).SetDelay(0.3f).OnComplete(() =>
                                {
                                    starRight.transform.DOScale(Vector3.one, 0.1f).SetEase(Ease.OutQuad).OnComplete(() =>
                                    {
                                        panelObjectiveWinner.GetComponent<CanvasGroup>().DOFade(0, 0.2f).SetDelay(4f).OnComplete(() =>
                                        {
                                            panelObjectiveWinner.gameObject.SetActive(false);
                                            panelEndGame.gameObject.SetActive(true);
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });

            camHandler.isGameOver = true;
            rectBtPlayers.DOAnchorPosY(-242f, 0.5f);
            rectBtCards.DOAnchorPosY(-250f, 0.5f);
            rectBtObjective.DOAnchorPosY(-163f, 0.5f);
            rectFooterWait.DOAnchorPosY(-606, 0.5f);
            rectFooter.DOAnchorPosY(-606, 0.5f);
            panelSlider.DOAnchorPosY(-612, 0.5f);
            BtCloseCards();
            BtCloseObjective();
            winnerName.text = GameController.gameState.currentPlayer.name;
            //winnerName.color = GameController.gameState.currentPlayer.warColor.color;
            lineUp.color = GameController.gameState.currentPlayer.warColor.color;
            lineDown.color = GameController.gameState.currentPlayer.warColor.color;
            winnerAvatar.sprite = GameController.gameState.currentPlayer.avatar;
            winnerAvatarBorder.color = GameController.gameState.currentPlayer.warColor.color;
            panelTimer.DOAnchorPosY(-259, 0.5f);
            if (GameController.Instance.SERVER.IsOnline())
            {
                if (GameController.gameState.thisPlayer == GameController.gameState.currentPlayer)
                {
                    endGameText.text = "PARABÉNS VOCÊ VENCEU!";
                }
                else
                {
                    endGameText.text = "<color=" + ColorTypeConverter.ToRGBHex(GameController.gameState.currentPlayer.warColor.color) + ">" + "<b>" + GameController.gameState.currentPlayer.GetName + "</b>" + "</color>" + " atingiu o objetivo e ganhou a guerra!";
                }
                endGameSoldier.color = GameController.gameState.currentPlayer.warColor.color;
            }
            else
            {
                AdMobManager.Instance.OfflineInterstitial();
                endGameText.text = "<color=" + ColorTypeConverter.ToRGBHex(GameController.gameState.currentPlayer.warColor.color) + ">" + "<b>" + GameController.gameState.currentPlayer.GetName + "</b>" + "</color>" + " atingiu o objetivo e ganhou a guerra!";
                winnerSoldier.color = GameController.gameState.currentPlayer.warColor.color;
                winnerAvatar.gameObject.SetActive(false);
            }

            endGameSoldier.color = GameController.gameState.currentPlayer.warColor.color;

            GameController.Instance.CleanMap();
        }


        #endregion

        public void OnClickAvatar()
        {
            AnalyticsManager.Instance.ClickPerfilUsuario();
            Sioux.Audio.Play("Click");
            ServerManager.instance.OpenNewProfilePopup(Globals.playerIdGrowGames);
        }

        public void OnClickOtherAvatar()
        {
            AnalyticsManager.Instance.ClickPerfilOutroUsuario();
            Sioux.Audio.Play("Click");
            ServerManager.instance.OpenNewProfilePopup(GameController.gameState.currentPlayer.idGrow);
        }

        public void OnClickFullscreen()
        {
            Sioux.Audio.Play("Click");
            Screen.fullScreen = !Screen.fullScreen;
        }
    }
}
