﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Grow;
using UnityEngine.UI;

public class RemoveAdsPopup : MonoBehaviour
{
    public GameObject separator;
    public GameObject optionIAP;
    void Start()
    {
        AnalyticsManager.Instance.SendDesignEvent("NoAdsPopUp:Open");
        if (Globals.GetBuildPlatform() == Plataforma.WEB)
        {
            separator.SetActive(false);
            optionIAP.SetActive(false);
        }
    }

    public void OnClickClose()
    {
        Sioux.Audio.Play("Click");
        AnalyticsManager.Instance.SendDesignEvent("NoAdsPopUp:Close");
        PopupManager.instance.ClosePopup();
    }

    public void OnClickVIP()
    {
        Sioux.Audio.Play("Click");
        AnalyticsManager.Instance.SendDesignEvent("NoAdsPopUp:VIP");
#if UNITY_WEBGL && !UNITY_EDITOR
            //Application.ExternalEval("window.open(\"" + Globals.MORE_GAMES_URL + "\", \"_blank\")");
            Application.OpenURL("https://assinatura.growgames.com.br/Assinatura");
#else
        Application.OpenURL("https://assinatura.growgames.com.br/Assinatura");
#endif
    }

    public void OnClickNoAds()
    {
        Sioux.Audio.Play("Click");
        AnalyticsManager.Instance.SendDesignEvent("NoAdsPopUp:IAP");
        UnityIAPManager.Instance.BuyNoAdsConsumable();
    }

    public void OnClickLogin()
    {
        Sioux.Audio.Play("Click");
        AnalyticsManager.Instance.SendDesignEvent("NoAdsPopUp:Login");
        SceneManager.instance.ChangeScene("Login");
    }
    public void OnClickSubscription()
    {
        Sioux.Audio.Play("Click");
        UnityIAPManager.Instance.BuySubscription();
    }


}
