﻿using Grow;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Grow.War;
using DG.Tweening;

public class LeavePopUpManager : MonoBehaviour 
{
    public void OnClickSaveExit()
    {
        //AdMobManager.Instance.RevealBanner();
        Sioux.Audio.Play("Click");
        AnalyticsManager.Instance.ClickAbandonarPartida();
        SceneManager.instance.ChangeScene("ChooseMode");
    }
    public void OnClickDontSaveExit()
    {
        //AdMobManager.Instance.RevealBanner();
        Sioux.Audio.Play("Click");
        AnalyticsManager.Instance.ClickAbandonarPartida();
        PlayerPrefs.DeleteKey("save");
        PlayerPrefs.Save();
        SceneManager.instance.ChangeScene("ChooseMode");
    }
}
