﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class DottedLine : MonoBehaviour
{

    public Sprite dotSprite;
    public Vector3 startPoint;
    public Vector3 endPoint;
    public Color32 startColor = Color.white;
    public Color32 endColor = Color.white;
    public AnimationCurve curve;
    public int dotsCount;
    public float spacing = 1;
    public float height = 1;
    public float dotSize = 1;

    public List<SpriteRenderer> dots;

    //[ContextMenu("SetLine")]
    //public void SetLine()
    //{
    //    foreach (var child in dots)
    //    {
    //        DestroyImmediate(child.gameObject);
    //    }
    //    dots.Clear();

    //    dots = new List<SpriteRenderer>();
    //    for (var i = 0; i < dotsCount; i++)
    //    {
    //        var d = new GameObject("dot", typeof(SpriteRenderer)).GetComponent<SpriteRenderer>();
    //        d.transform.SetParent(transform);
    //        var pos = Vector3.Lerp(startPoint, endPoint, (float)i / dotsCount);
    //        d.transform.position = pos + Vector3.up * curve.Evaluate((float)i / dotsCount) * height;
    //        d.transform.localScale = Vector3.one * dotSize;
    //        d.sprite = dotSprite;
    //        d.transform.LookAt(Camera.main.transform, Vector3.up);
    //        dots.Add(d);
    //    }
    //}

    public void SetLine()
    {
        foreach (var child in dots)
        {
            DestroyImmediate(child.gameObject);
        }
        dots.Clear();

        dots = new List<SpriteRenderer>();
        var steps = (Vector3.Distance(startPoint, endPoint) * 10) / spacing;
        for (var i = 0; i < steps; i++)
        {
            var d = new GameObject("dot", typeof(SpriteRenderer)).GetComponent<SpriteRenderer>();
            d.transform.SetParent(transform);
            var pos = Vector3.Lerp(startPoint, endPoint, (float)i / steps);
            var color = Color32.Lerp(startColor, endColor, (float)i / steps);
            d.color = color;
            d.transform.position = pos + Vector3.up * curve.Evaluate((float)i / steps) * height;
            d.transform.localScale = Vector3.one * dotSize;
            d.sprite = dotSprite;
            d.transform.LookAt(Camera.main.transform, Vector3.up);
            dots.Add(d);
        }
    }



    public void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(startPoint, 0.3f);
        Gizmos.DrawSphere(endPoint, 0.3f);
    }

#if UNITY_EDITOR
    [CanEditMultipleObjects]
    [CustomEditor(typeof(DottedLine))]
    public class DottedLineEditor : Editor
    {

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            DottedLine myScript = (DottedLine)target;
            if (GUILayout.Button("Set Line"))
            {
                myScript.SetLine();
            }
        }
    }
#endif
}
