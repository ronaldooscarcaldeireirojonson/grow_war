﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(LineRenderer))]
public class ContinentLines : MonoBehaviour
{

    LineRenderer lineRenderer;
    public Transform[] points;
    private Vector3[] vP;
    int seg = 20;

    [ContextMenu("DrawLines")]
    void DrawLines()
    {
        points = new Transform[transform.childCount];
        int i = 0;
        foreach (Transform child in transform)
        {
            points[i++] = child;

        }
        lineRenderer = GetComponent<LineRenderer>();
        Line();
    }
    void Line()
    {
        seg = points.Length;
        vP = new Vector3[points.Length];
        for (int i = 0; i < points.Length; i++)
        {
            vP[i] = points[i].localPosition;
        }
        for (int i = 0; i < seg; i++)
        {
            float t = i / (float)seg;
            lineRenderer.positionCount = seg;
            lineRenderer.SetPositions(vP);
        }

    }
}