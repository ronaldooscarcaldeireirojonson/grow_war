﻿using UnityEngine;
using UnityEngine.UI;

namespace ProfanityTest
{
    public class RegexTest : MonoBehaviour
    {
        public InputField input;
        public Text textResult;

        public void OnValueChanged()
        {
            input.textComponent.color = Sioux.Profanity.Check(input.text) ? Color.red : Color.black;
            textResult.text = Sioux.Profanity.Replace(input.text);
        }

        public void OnEndEdit()
        {
            //Debug.Log(Profanity.Check(input.text) ? "NÃO PODE!" : "PODE SIM SIM SIM");
        }
    }
}
