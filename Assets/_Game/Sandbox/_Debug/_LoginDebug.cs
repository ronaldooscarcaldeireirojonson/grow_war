﻿using Grow;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class _LoginDebug : MonoBehaviour
{

    private int clicks = 0;

    [SerializeField]
    private InputField name;
    [SerializeField]
    private InputField pass;
    [SerializeField]
    private Text versionText;

    public string version = "vA 0.0.1_";
    private string rouleteVersionText;
    private string forcedHintVersionText;

    private IEnumerator Start()
    {
        yield return new WaitUntil(() => AnalyticsManager.Instance != null);
        UpdateVersionText();
    }

    public void OnClick()
    {
        switch (clicks)
        {
            case 0:
            case 1:
            case 2:
                break;
            case 3:
                name.text = "fokubo01";
                pass.text = "sioux123";
                break;
            case 4:
                name.text = "fokubo02";
                break;
            case 5:
                name.text = "fokubo03";
                break;
            case 6:
                name.text = "fokubo04";
                break;
            case 7:
                name.text = "donatelloG";
                pass.text = "growgames#VIP";
                break;
            case 8:
                name.text = "rafaelG";
                break;
            case 9:
                name.text = "leonardoG";
                break;
            case 10:
                name.text = "michelangeloG";
                break;
            case 11:
                clicks = 2;
                break;

        }
        clicks++;
    }

    public void OnClickRounletteSetup()
    {
        //AnalyticsManager.Instance.roletaMinigame = !AnalyticsManager.Instance.roletaMinigame;
        UpdateVersionText();
    }

    public void OnClickForcedHintSetup()
    {

        //AnalyticsManager.Instance.forceHintView = !AnalyticsManager.Instance.forceHintView;
        UpdateVersionText();
    }

    private void UpdateVersionText()
    {
        //var rmg = AnalyticsManager.Instance.roletaMinigame ? "R" : "0";
        //var fh = AnalyticsManager.Instance.forceHintView ? "F" : "0";
        //versionText.text = version + rmg + fh;
    }
}
